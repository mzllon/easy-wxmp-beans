/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 编辑摇一摇出来的页面信息
 * <p>包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundPageUpdateRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = 5627497882731898281L;

    /**
     * 新增页面的页面id
     */
    @ApiField("page_id")
    private String pageId;

    /**
     * 在摇一摇页面展示的主标题，不超过6个汉字或12个英文字母
     */
    private String title;
    /**
     * 在摇一摇页面展示的副标题，不超过7个汉字或14个英文字母
     */
    private String description;

    /**
     * 在摇一摇页面展示的图片。
     * 图片需先上传至微信侧服务器，用“素材管理-上传图片素材”接口上传图片，返回的图片URL再配置在此处
     */
    @ApiField("icon_url")
    private String iconUrl;

    /**
     * 页面的备注信息，不超过15个汉字或30个英文字母
     */
    private String comment;

    /**
     * 跳转链接
     */
    @ApiField("page_url")
    private String pageUrl;

    public ShakeAroundPageUpdateRequest() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/page/update";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
