/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 发送卡券消息
 * <p>
 * 特别注意客服消息接口投放卡券仅支持非自定义Code码和导入code模式的卡券的卡券
 * </p>
 *
 * @author mzlion on 2017/4/16.
 */
public class MessageWxCardSendRequest extends AbsMessageCustomSendRequest {

    /**
     * 卡券消息
     */
    @ApiField("text")
    private WxCardMessage wxCardMessage;

    public MessageWxCardSendRequest() {
        super.msgType = MsgTypeEnum.WX_CARD;
    }

    public WxCardMessage getWxCardMessage() {
        return wxCardMessage;
    }

    public void setWxCardMessage(WxCardMessage wxCardMessage) {
        this.wxCardMessage = wxCardMessage;
    }

    /**
     * 发送卡券消息
     *
     * @author mzlion
     */
    public static class WxCardMessage implements Serializable {

        /**
         * 卡券ID
         */
        @ApiField("card_id")
        private String cardId;

        public WxCardMessage(String cardId) {
            this.cardId = cardId;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }
    }


}
