/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.CardSubMerchantStatusEnum;
import com.mzlion.weixin.easywxmp.response.card.CardSubMerchantBatchGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 子商户信息批量查询接口请求对象封装
 * <p>
 * 母商户可以通过该接口批量拉取子商户的相关信息，一次调用最多拉取100个子商户的信息，可以通过多次拉去满足不同的查询需求
 * </p>
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantBatchGetRequest extends WxRequest<CardSubMerchantBatchGetResponse> {

    private static final long serialVersionUID = -9003113808634932678L;

    /**
     * 起始的子商户id，一个母商户公众号下唯一
     */
    @ApiField("begin_id")
    private String beginId;

    /**
     * 拉取的子商户的个数，最大值为100
     */
    private int limit;

    /**
     * 子商户审核状态，填入后，只会拉出当前状态的子商户
     */
    @ApiField("status")
    private CardSubMerchantStatusEnum cardSubMerchantStatus;

    public CardSubMerchantBatchGetRequest() {
    }

    public String getBeginId() {
        return beginId;
    }

    public void setBeginId(String beginId) {
        this.beginId = beginId;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        if (limit > 100) {
            this.limit = 100;
            return;
        }
        if (limit <= 0) {
            this.limit = 1;
            return;
        }
        this.limit = limit;
    }

    public CardSubMerchantStatusEnum getCardSubMerchantStatus() {
        return cardSubMerchantStatus;
    }

    public void setCardSubMerchantStatus(CardSubMerchantStatusEnum cardSubMerchantStatus) {
        this.cardSubMerchantStatus = cardSubMerchantStatus;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/submerchant/batchget";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardSubMerchantBatchGetResponse> responseClass() {
        return CardSubMerchantBatchGetResponse.class;
    }
}
