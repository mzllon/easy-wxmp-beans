/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.media;

import com.mzlion.weixin.easywxmp.UploadWxRequest;
import com.mzlion.weixin.easywxmp.response.media.MediaUploadImgResponse;

import java.io.File;
import java.io.InputStream;

/**
 * 上传图片接口
 * <p>
 * 开发者需调用该接口上传商户图标至微信服务器，获取相应logo_url/icon_list/image_url，用于卡券创建。
 * 开发者注意事项
 * 1.上传的图片限制文件大小限制1MB，仅支持JPG、PNG格式。
 * 2.调用接口获取图片url仅支持在微信相关业务下使用。
 * </p>
 * <p>
 * 本接口所上传的图片不占用公众号的素材库中图片数量的5000个的限制。图片仅支持jpg/png格式，大小必须在1MB以下。
 * </p>
 *
 * @author mzlion on 2016/12/22.
 */
public class MediaUploadImgRequest extends UploadWxRequest<MediaUploadImgResponse> {

    private static final long serialVersionUID = -9152097865860592040L;

    public MediaUploadImgRequest(final File imgFile) {
        this.uploadFile = imgFile;
    }

    public MediaUploadImgRequest(final InputStream inputStream, final long contentLength, final String fileName, final String contentType) {
        this.uploadStream = inputStream;
        this.contentLength = contentLength;
        this.filename = fileName;
        this.contentType = contentType;
    }


    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/media/uploadimg";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MediaUploadImgResponse> responseClass() {
        return MediaUploadImgResponse.class;
    }

    /**
     * POST素材是的参数名
     *
     * @return POST素材是的参数名
     */
    @Override
    public String formName() {
        return "buffer";
    }
}
