/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.merchant.MerchantCategoryGetSkuResponse;

/**
 * 获取指定分类的所有SKU
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantCategoryGetSkuRequest extends WxRequest<MerchantCategoryGetSkuResponse> {

    /**
     * 商品子分类ID
     */
    private String cateId;

    public MerchantCategoryGetSkuRequest() {
    }

    public MerchantCategoryGetSkuRequest(String cateId) {
        this.cateId = cateId;
    }

    public String getCateId() {
        return cateId;
    }

    public void setCateId(String cateId) {
        this.cateId = cateId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/category/getsku";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MerchantCategoryGetSkuResponse> responseClass() {
        return MerchantCategoryGetSkuResponse.class;
    }
}
