/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.scan;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.ProductBranInfoModel;
import com.mzlion.weixin.easywxmp.response.scan.ScanProductCreateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 创建商品
 * <p>使用该接口，商户可以创建商品信息，设置商品主页，支持条码和二维码两种类型。目前，一个账号最多支持创建10万条商品信息</p>
 * <p>
 * 注意：
 * 1、推广服务区action_info部分，入口数量规则如下：
 * 1)至少设置一个推广类型；
 * 2)文字介绍、公众号和微信卡券三种推广类型，每种类型最多只能分别设置1个；
 * 3)普通链接、公众号、微信卡券三种类型合计最多设置3个；
 * 4)图片跳转链接和视频播放只能二选一展示；
 * 2、商品主页中必须存在至少一个价格信息，价格的展示渠道有四种：
 * 1)在base_info中定义store_mgr_type和store_vendorid_list字段。如果商品在设置的电商渠道有销售，则会有售卖入口在【购买区】展示。例如：“洋河-天之蓝 480ml”在“京东商城”有销售，假设京东商城又在该商品设置的电商渠道列表中，则会有“京东商城”的售卖入口出现。
 * 2)在微信小店（Product）类型中设置productid，购买区将出现对应商品的微信小店售卖入口。
 * 3)在电商链接（Store）类型中设置目标地址链接（link），购买区将出现所配置的外部商城售卖入口。
 * 4)在建议零售价（Price）类型中设置retail_price字段。如果商品没有设置前文所述的任何一种购买渠道，也会有该商品建议零售价展示。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductCreateRequest extends WxRequest<ScanProductCreateResponse> {

    private static final long serialVersionUID = -8522665319714326944L;

    /**
     * 商品编码标准，支持ean13、ean8和qrcode标准
     */
    @ApiField("keystandard")
    String keyStandard;

    /**
     * 商品编码内容。直接填写商品条码，
     * 标准是ean13，则直接填写商品条码，如“6901939621608”。
     * 标准是qrcode，二维码的内容可由商户自定义，建议使用商品条码，≤20个字符，由大小字母、数字、下划线和连字符组成。
     * 注意：编码标准是ean13时，编码内容必须在商户的号段之下，否则会报错
     */
    @ApiField("keystr")
    String keyStr;

    /**
     * 商品信息
     */
    @ApiField("brand_info")
    ProductBranInfoModel productBranInfo;

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public ProductBranInfoModel getProductBranInfo() {
        return productBranInfo;
    }

    public void setProductBranInfo(ProductBranInfoModel productBranInfo) {
        this.productBranInfo = productBranInfo;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "scan/product/create";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ScanProductCreateResponse> responseClass() {
        return ScanProductCreateResponse.class;
    }
}
