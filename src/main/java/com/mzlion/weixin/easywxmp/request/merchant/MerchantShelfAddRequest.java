/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.merchant.MerchantShelfAddResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 增加货架
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantShelfAddRequest extends WxRequest<MerchantShelfAddResponse> {

    /**
     * 货架名称
     */
    @ApiField("shelf_name")
    private String shelfName;

    /**
     * 货架招牌图片Url(图片需调用图片上传接口获得图片Url填写至此，否则添加货架失败
     * 建议尺寸为640*120，仅控件1-4有banner，控件5没有banner)
     */
    @ApiField("shelf_banner")
    private String shelfBanner;

    /**
     * 货架信息(数据说明详见《货架控件说明》)
     */
    @ApiField("shelf_data")
    private ShelfData shelfData;

    public MerchantShelfAddRequest() {
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    public String getShelfBanner() {
        return shelfBanner;
    }

    public void setShelfBanner(String shelfBanner) {
        this.shelfBanner = shelfBanner;
    }

    public ShelfData getShelfData() {
        return shelfData;
    }

    public void setShelfData(ShelfData shelfData) {
        this.shelfData = shelfData;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/shelf/add";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MerchantShelfAddResponse> responseClass() {
        return MerchantShelfAddResponse.class;
    }

    /**
     * 货架新型
     */
    public static class ShelfData implements Serializable {

        @ApiField("module_infos")
        private List<ModuleInfo> moduleInfoList;

        public ShelfData() {
        }

        public ShelfData(List<ModuleInfo> moduleInfoList) {
            this.moduleInfoList = moduleInfoList;
        }

        public List<ModuleInfo> getModuleInfoList() {
            return moduleInfoList;
        }

        public void setModuleInfoList(List<ModuleInfo> moduleInfoList) {
            this.moduleInfoList = moduleInfoList;
        }
    }

    public static class ModuleInfo implements Serializable {
        /**
         * 控件ID
         */
        private String eid;

        /**
         * 控件数据信息
         */
        @ApiField("group_info")
        private GroupInfo groupInfo;

        public ModuleInfo() {
        }

        public String getEid() {
            return eid;
        }

        public void setEid(String eid) {
            this.eid = eid;
        }

        public GroupInfo getGroupInfo() {
            return groupInfo;
        }

        public void setGroupInfo(GroupInfo groupInfo) {
            this.groupInfo = groupInfo;
        }

    }

    public static class GroupInfo implements Serializable {

        /**
         * 分组ID
         */
        @ApiField("group_id")
        private String groupId;

        /**
         * 控件数
         */
        private Filter filter;


        private List<Group> groups;

        private String img;

        @ApiField("img_background")
        private String backgroundImg;

        public GroupInfo() {
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public Filter getFilter() {
            return filter;
        }

        public void setFilter(Filter filter) {
            this.filter = filter;
        }

        public List<Group> getGroups() {
            return groups;
        }

        public void setGroups(List<Group> groups) {
            this.groups = groups;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getBackgroundImg() {
            return backgroundImg;
        }

        public void setBackgroundImg(String backgroundImg) {
            this.backgroundImg = backgroundImg;
        }

        /**
         * 获取控件数
         *
         * @return 控件数
         */
        public int getCount() {
            return this.filter == null ? 0 : this.filter.count;
        }

        /**
         * 设置控件数
         *
         * @param count 控件数
         */
        public void setCount(int count) {
            if (this.filter == null) {
                this.filter = new Filter();
            }
            this.filter.count = count;
        }

        public static class Filter implements Serializable {

            /**
             * 该控件展示商品个数
             */
            private int count;

            public Filter() {
            }

            public Filter(int count) {
                this.count = count;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }
        }
    }


    public static class Group implements Serializable {

        private String groupId;

        private String img;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }
    }

}
