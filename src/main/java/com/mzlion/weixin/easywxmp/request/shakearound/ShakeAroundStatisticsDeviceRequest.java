/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundStatisticsDeviceResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 以设备为维度的数据统计接口
 * <p>
 * 查询单个设备进行摇周边操作的人数、次数，点击摇周边消息的人数、次数；
 * 查询的最长时间跨度为30天。只能查询最近90天的数据。
 * 此接口无法获取当天的数据，最早只能获取前一天的数据。
 * 由于系统在凌晨处理前一天的数据，太早调用此接口可能获取不到数据，建议在早上8：00之后调用此接口。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundStatisticsDeviceRequest extends WxRequest<ShakeAroundStatisticsDeviceResponse> {

    private static final long serialVersionUID = -3611239707575787563L;

    /**
     * 指定页面的设备ID
     */
    @ApiField("device_identifier")
    private ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier;

    /**
     * 起始日期时间戳，最长时间跨度为30天，单位为秒
     */
    @ApiField("begin_date")
    private Long beginDate;

    /**
     * 结束日期时间戳，最长时间跨度为30天，单位为秒
     */
    @ApiField("end_date")
    private Long endDate;


    public ShakeAroundStatisticsDeviceRequest() {
    }

    public ShakeAroundDeviceIdentifierModel getShakeAroundDeviceIdentifier() {
        return shakeAroundDeviceIdentifier;
    }

    public void setShakeAroundDeviceIdentifier(ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier) {
        this.shakeAroundDeviceIdentifier = shakeAroundDeviceIdentifier;
    }

    public Long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Long beginDate) {
        this.beginDate = beginDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/statistics/device";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundStatisticsDeviceResponse> responseClass() {
        return ShakeAroundStatisticsDeviceResponse.class;
    }
}
