/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 配置设备与页面的关联关系
 * <p>
 * 配置时传入该设备需要关联的页面的id列表（该设备原有的关联关系将被直接清除）；
 * 页面的id列表允许为空，当页面的id列表为空时则会清除该设备的所有关联关系。
 * 配置完成后，在此设备的信号范围内，即可摇出关联的页面信息。
 * 在申请设备ID后，可直接使用接口直接配置页面。
 * 若设备配置多个页面，则随机出现页面信息。一个设备最多可配置30个关联页面。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceBindPageRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = 944006699021699794L;

    /**
     * 申请的批次ID
     */
    @ApiField("device_identifier")
    private ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier;

    /**
     * 待关联的页面列表
     */
    @ApiField("page_ids")
    private List<String> pageIdList;


    public ShakeAroundDeviceBindPageRequest() {
    }

    public ShakeAroundDeviceIdentifierModel getShakeAroundDeviceIdentifier() {
        return shakeAroundDeviceIdentifier;
    }

    public void setShakeAroundDeviceIdentifier(ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier) {
        this.shakeAroundDeviceIdentifier = shakeAroundDeviceIdentifier;
    }

    public List<String> getPageIdList() {
        return pageIdList;
    }

    public void setPageIdList(List<String> pageIdList) {
        this.pageIdList = pageIdList;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/bindpage";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
