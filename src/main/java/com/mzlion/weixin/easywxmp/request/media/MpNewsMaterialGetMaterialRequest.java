/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.media;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.media.MpNewsMaterialGetMaterialResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取图文素材
 *
 * @author mzlion on 2017/4/17.
 */
public class MpNewsMaterialGetMaterialRequest extends WxRequest<MpNewsMaterialGetMaterialResponse> {

    private static final long serialVersionUID = -7449497987308879439L;

    /**
     * 要获取的素材的media_id
     */
    @ApiField("media_id")
    private final String mediaId;

    public MpNewsMaterialGetMaterialRequest(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/material/get_material";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MpNewsMaterialGetMaterialResponse> responseClass() {
        return MpNewsMaterialGetMaterialResponse.class;
    }
}
