/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiHomepageGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询商家主页
 * <p>通过门店ID查询商户主页的信息。</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiHomepageGetRequest extends WxRequest<BizWifiHomepageGetResponse> {

    private static final long serialVersionUID = -3549877526123020499L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    public BizWifiHomepageGetRequest() {
    }

    public BizWifiHomepageGetRequest(String shopId) {
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/homepage/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiHomepageGetResponse> responseClass() {
        return BizWifiHomepageGetResponse.class;
    }

}
