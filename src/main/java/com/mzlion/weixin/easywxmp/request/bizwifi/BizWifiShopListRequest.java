/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiShopListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取Wi-Fi门店列表
 * <p>
 * 通过此接口获取WiFi的门店列表，该列表包括公众平台的门店信息、以及添加设备后的WiFi相关信息。
 * 创建门店方法请参考“微信门店接口”。
 * 注：微信连Wi-Fi下的所有接口中的shop_id，必需先通过此接口获取。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiShopListRequest extends WxRequest<BizWifiShopListResponse> {

    private static final long serialVersionUID = -3334944901181035052L;

    /**
     * 分页下标，默认从1开始
     */
    @ApiField("pageindex")
    private int pageIndex = 1;

    /**
     * 每页的个数，默认10个，最大20个
     */
    @ApiField("pagesize")
    private int pageSize = 10;

    public BizWifiShopListRequest() {
    }

    public BizWifiShopListRequest(int pageIndex, int pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/shop/list";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiShopListResponse> responseClass() {
        return BizWifiShopListResponse.class;
    }
}
