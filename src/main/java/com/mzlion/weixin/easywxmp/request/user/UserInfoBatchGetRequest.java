/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.user;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.user.UserInfoBatchGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 批量获取用户基本信息
 * <p>
 * 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class UserInfoBatchGetRequest extends WxRequest<UserInfoBatchGetResponse> {

    /**
     * 用户列表
     */
    private final List<UserInfoWrapper> userInfoList;

    public UserInfoBatchGetRequest(List<UserInfoWrapper> userInfoList) {
        this.userInfoList = userInfoList;
    }

    public List<UserInfoWrapper> getUserInfoList() {
        return userInfoList;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/user/info/batchget";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<UserInfoBatchGetResponse> responseClass() {
        return UserInfoBatchGetResponse.class;
    }

    public static class UserInfoWrapper implements Serializable {

        /**
         * 用户的标识，对当前公众号唯一
         */
        @ApiField("openid")
        private String openId;

        /**
         * 国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语，默认为zh-CN
         */
        private String lang;


    }
}
