/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardPayRechargeResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 充值券点接口
 * <p>
 * 开发者可以通过此接口为券点账户充值券点，1元等于1点。
 * 开发者调用接口后可以获得一个微信支付的支付二维码链接， 开发者可以将链接转化为二维码扫码支付。
 * </p>
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayRechargeRequest extends WxRequest<CardPayRechargeResponse> {

    private static final long serialVersionUID = -8294333230320391085L;

    /**
     * 需要充值的券点数目，1点=1元
     */
    @ApiField("coin_count")
    private int coinCount;

    public CardPayRechargeRequest() {
    }

    public CardPayRechargeRequest(int coinCount) {
        this.coinCount = coinCount;
    }

    public int getCoinCount() {
        return coinCount;
    }

    public void setCoinCount(int coinCount) {
        this.coinCount = coinCount;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/pay/recharge";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayRechargeResponse> responseClass() {
        return CardPayRechargeResponse.class;
    }
}
