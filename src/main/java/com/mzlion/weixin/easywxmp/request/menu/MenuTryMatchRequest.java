/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.menu;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.menu.MenuTryMatchResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 测试个性化菜单匹配结果请求参数
 *
 * @author mzlion on 2017/1/5.
 */
public class MenuTryMatchRequest extends WxRequest<MenuTryMatchResponse> {

    private static final long serialVersionUID = -4543560749072247366L;

    /**
     * 可以是粉丝的OpenID，也可以是粉丝的微信号
     */
    @ApiField("user_id")
    private final String userId;

    public MenuTryMatchRequest(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/menu/trymatch";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MenuTryMatchResponse> responseClass() {
        return MenuTryMatchResponse.class;
    }
}
