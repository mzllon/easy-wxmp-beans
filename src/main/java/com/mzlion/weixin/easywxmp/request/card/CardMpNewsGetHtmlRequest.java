/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardMpNewsGetHtmlResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 图文消息群发卡券
 * <p>
 * 支持开发者调用该接口获取卡券嵌入图文消息的标准格式代码，将返回代码填入上传图文素材接口中content字段，即可获取嵌入卡券的图文消息素材。
 * 特别注意：目前该接口仅支持填入非自定义code的卡券,自定义code的卡券需先进行code导入后调用。
 * </p>
 * Created by mzlion on 2017/4/17.
 */
public class CardMpNewsGetHtmlRequest extends WxRequest<CardMpNewsGetHtmlResponse> {

    private static final long serialVersionUID = -5372071286834249383L;

    /**
     * 卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    public CardMpNewsGetHtmlRequest() {
    }

    public CardMpNewsGetHtmlRequest(String cardId) {
        this.cardId = cardId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/mpnews/gethtml";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardMpNewsGetHtmlResponse> responseClass() {
        return CardMpNewsGetHtmlResponse.class;
    }
}
