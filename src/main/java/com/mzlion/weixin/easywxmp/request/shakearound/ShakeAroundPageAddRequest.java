/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundPageAddResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 新增摇一摇出来的页面信息
 * <p>包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。其中，图片必须为用素材管理接口上传至微信侧服务器后返回的链接。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundPageAddRequest extends WxRequest<ShakeAroundPageAddResponse> {

    private static final long serialVersionUID = 5627497882731898281L;

    /**
     * 在摇一摇页面展示的主标题，不超过6个汉字或12个英文字母
     */
    private String title;
    /**
     * 在摇一摇页面展示的副标题，不超过7个汉字或14个英文字母
     */
    private String description;

    /**
     * 在摇一摇页面展示的图片。
     * 图片需先上传至微信侧服务器，用“素材管理-上传图片素材”接口上传图片，返回的图片URL再配置在此处
     */
    @ApiField("icon_url")
    private String iconUrl;

    /**
     * 页面的备注信息，不超过15个汉字或30个英文字母
     */
    private String comment;

    public ShakeAroundPageAddRequest() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/page/add";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundPageAddResponse> responseClass() {
        return ShakeAroundPageAddResponse.class;
    }
}
