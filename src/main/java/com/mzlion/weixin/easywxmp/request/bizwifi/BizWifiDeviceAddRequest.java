/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 添加密码型设备
 * <p>
 * 调用此接口向指定门店添加密码型设备的Wi-Fi信息。为保证门店下多台设备无缝漫游。
 * 1. 同一个门店可以添加多个ssid，最大添加100个ssid；
 * 2. 已添加过的ssid不能再重复添加；
 * 3. 一个门店只能拥有一种设备类型，只要调用此接口添加一个ssid后，该门店即为密码型设备，不能再添加portal型设备。
 * 调用清空门店Wi-Fi信息接口清空网络信息后，可再设置为其它类型设备。
 * </p>
 * <p>
 * 注意： 调用此接口后需要进行以下两步操作，才能正式启用设备
 * 1. 进入无线路由器的管理后台，修改待添加设备的ssid和密码，保证设备的ssid和密码与调用接口时填写的一致
 * 2. 请务必调用“获取物料二维码”接口，下载该门店二维码，张贴于店内。并确保有人（顾客或店员）用6.1以上安卓版微信或6.2.2以上IOS版微信扫码连接Wi-Fi，连网成功即表示设备添加成功。
 * </p>
 * Created by mzlion on 2017/4/18.
 */
public class BizWifiDeviceAddRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -8899732108666528074L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 无线网络设备的ssid。32个字符以内；ssid支持中文，但可能因设备兼容性问题导致显示乱码，或无法连接等问题，相关风险自行承担！
     * 当门店下是portal型设备时，ssid必填；当门店下是密码型设备时，ssid选填，且ssid和密码必须有一个以大写字母“WX”开头
     */
    private String ssid;

    /**
     * 无线网络设备的密码。8-24个字符；不能包含中文字符；
     * 当门店下是密码型设备时，才可填写password，且ssid和密码必须有一个以大写字母“WX”开头
     */
    private String password;

    public BizWifiDeviceAddRequest() {
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/device/add";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
