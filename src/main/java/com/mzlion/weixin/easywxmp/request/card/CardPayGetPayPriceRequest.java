/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardPayGetPayPriceResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 对优惠券批价
 * <p>本接口用于提前查询本次新增库存需要多少券点</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayGetPayPriceRequest extends WxRequest<CardPayGetPayPriceResponse> {

    private static final long serialVersionUID = -8294333230320391085L;

    /**
     * 要来配置库存的card_id
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 本次需要兑换的库存数目
     */
    private int quantity;

    public CardPayGetPayPriceRequest() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/pay/getpayprice";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayGetPayPriceResponse> responseClass() {
        return CardPayGetPayPriceResponse.class;
    }
}
