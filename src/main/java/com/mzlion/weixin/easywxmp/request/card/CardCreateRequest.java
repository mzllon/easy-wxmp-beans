/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.card.AbsCardModel;
import com.mzlion.weixin.easywxmp.response.card.CardCreateResponse;

/**
 * 创建卡券接口
 * <p>
 * 创建卡券接口是微信卡券的基础接口，用于创建一类新的卡券，获取card_id，创建成功并通过审核后，商家可以通过文档提供的其他接口将卡券下发给用户，每次成功领取，库存数量相应扣除。
 * </p>
 * <p>
 * 开发者须知
 * 1.需自定义Code码的商家必须在创建卡券时候，设定use_custom_code为true，且在调用投放卡券接口时填入指定的Code码。指定OpenID同理。特别注意：在公众平台创建的卡券均为非自定义Code类型。
 * 2.can_share字段指领取卡券原生页面是否可分享，建议指定Code码、指定OpenID等强限制条件的卡券填写false。
 * 3.特别注意：编码方式仅支持使用UTF-8，否则会报错。
 * 4.创建成功后该卡券会自动提交审核，审核结果将通过事件通知商户。开发者可调用设置白名单接口设置用户白名单，领取未通过审核的卡券，测试整个卡券的使用流程。
 * </p>
 *
 * @author mzlion on 2016/12/23.
 */
public class CardCreateRequest extends WxRequest<CardCreateResponse> {

    private final AbsCardModel card;

    public CardCreateRequest(AbsCardModel card) {
        this.card = card;
    }

    public AbsCardModel getCard() {
        return card;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/create";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardCreateResponse> responseClass() {
        return CardCreateResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
