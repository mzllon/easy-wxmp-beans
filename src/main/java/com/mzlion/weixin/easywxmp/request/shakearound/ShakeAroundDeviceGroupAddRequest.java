/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundDeviceGroupAddResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 新增分组
 * <p>新建设备分组，每个帐号下最多只有1000个分组。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceGroupAddRequest extends WxRequest<ShakeAroundDeviceGroupAddResponse> {

    private static final long serialVersionUID = 2110581094441791184L;

    /**
     * 分组名称，不超过100汉字或200个英文字母
     */
    @ApiField("group_name")
    private String groupName;

    public ShakeAroundDeviceGroupAddRequest() {
    }

    public ShakeAroundDeviceGroupAddRequest(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/group/add";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundDeviceGroupAddResponse> responseClass() {
        return ShakeAroundDeviceGroupAddResponse.class;
    }
}
