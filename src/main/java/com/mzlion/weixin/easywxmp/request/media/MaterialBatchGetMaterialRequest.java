/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.media;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.media.MaterialBatchGetMaterialResponse;

/**
 * 永久其他类型（图片、语音、视频）素材列表
 * <p>
 * 请注意：
 * 1、获取永久素材的列表，也包含公众号在公众平台官网素材管理模块中新建的图文消息、语音、视频等素材
 * 2、临时素材无法通过本接口获取
 * 3、调用该接口需https协议
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class MaterialBatchGetMaterialRequest extends WxRequest<MaterialBatchGetMaterialResponse> {

    private static final long serialVersionUID = -3720874846611942714L;

    /**
     * 素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
     */
    private String type;

    /**
     * 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
     */
    private int offset;

    /**
     * 返回素材的数量，取值在1到20之间
     */
    private int count = 1;

    public MaterialBatchGetMaterialRequest() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/material/batchget_material";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MaterialBatchGetMaterialResponse> responseClass() {
        return MaterialBatchGetMaterialResponse.class;
    }
}
