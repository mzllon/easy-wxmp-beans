/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.datacube;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.datacube.DataCubeGetCardBizuinInfoResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 拉取卡券概况数据接口
 * <p>支持调用该接口拉取本商户的总体数据情况，包括时间区间内的各指标总量。</p>
 * <p>
 * 1. 查询时间区间需&lt;=62天，否则报错{errcode: 61501，errmsg: "date range error"}；
 * 2. 传入时间格式需严格参照示例填写”2015-06-15”，否则报错{errcode":61500,"errmsg":"date format error"}
 * 3. 该接口只能拉取非当天的数据，不能拉取当天的卡券数据，否则报错。
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class DataCubeGetCardBizuinInfoRequest extends WxRequest<DataCubeGetCardBizuinInfoResponse> {

    /**
     * 获取数据的起始日期，begin_date和end_date的差值需小于“最大时间跨度”（比如最大时间跨度为1时，begin_date和end_date的差值只能为0，才能小于1），否则会报错
     */
    @ApiField("begin_date")
    private String beginDate;

    /**
     * 获取数据的结束日期，end_date允许设置的最大值为昨日
     */
    @ApiField("end_date")
    private String endDate;

    /**
     * 卡券来源
     * 0为公众平台创建的卡券数据
     * 1是API创建的卡券数据
     */
    @ApiField("cond_source")
    private int condSource;

    public DataCubeGetCardBizuinInfoRequest() {
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getCondSource() {
        return condSource;
    }

    public void setCondSource(int condSource) {
        this.condSource = condSource;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/datacube/getcardbizuininfo";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<DataCubeGetCardBizuinInfoResponse> responseClass() {
        return DataCubeGetCardBizuinInfoResponse.class;
    }
}
