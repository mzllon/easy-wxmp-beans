/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardPayGetOrderListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 查询券点流水详情接口
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayGetOrderListRequest extends WxRequest<CardPayGetOrderListResponse> {

    private static final long serialVersionUID = -8294333230320391085L;

    /**
     * 分批查询的起点，默认为0
     */
    private int offset;

    /**
     * 分批查询的数量，默认50条
     */
    private int count = 50;

    /**
     * 批量查询订单的起始事件，为时间戳，默认1周前
     */
    @ApiField("begin_time")
    private Long beginTime;

    /**
     * 批量查询订单的结束事件，为时间戳，默认为当前时间
     */
    @ApiField("end_time")
    private Long endTime;

    /**
     * 所要拉取的订单类型
     * ORDER_TYPE_SYS_ADD 平台赠送
     * ORDER_TYPE_WXPAY 充值
     * ORDER_TYPE_REFUND 库存回退券点
     * ORDER_TYPE_REDUCE 券点兑换库存
     * ORDER_TYPE_SYS_REDUCE 平台扣减
     */
    @ApiField("order_type")
    private String orderType;

    /**
     * 反选，不要拉取的订单
     */
    @ApiField("nor_filter")
    private NorFilter norFilter;

    /**
     * 对结果排序
     */
    @ApiField("sort_info")
    private SortInfo sortInfo;

    public CardPayGetOrderListRequest() {
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Long beginTime) {
        this.beginTime = beginTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public NorFilter getNorFilter() {
        return norFilter;
    }

    public void setNorFilter(NorFilter norFilter) {
        this.norFilter = norFilter;
    }

    public SortInfo getSortInfo() {
        return sortInfo;
    }

    public void setSortInfo(SortInfo sortInfo) {
        this.sortInfo = sortInfo;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/pay/getorderlist";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayGetOrderListResponse> responseClass() {
        return CardPayGetOrderListResponse.class;
    }

    public static class NorFilter implements Serializable {

        private static final long serialVersionUID = 1962174803560844383L;

        /**
         * 订单状态包括
         * ORDER_STATUS_WAITING 等待支付
         * ORDER_STATUS_SUCC 支付成功
         * ORDER_STATUS_FINANCE_SUCC 加代币成功
         * ORDER_STATUS_QUANTITY_SUCC 加库存成功
         * ORDER_STATUS_HAS_REFUND 已退币
         * ORDER_STATUS_REFUND_WAITING 等待退币确认
         * ORDER_STATUS_ROLLBACK 已回退,系统失败
         * ORDER_STATUS_HAS_RECEIPT 已开发票
         */
        private String status;

        public NorFilter() {
        }

        public NorFilter(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public static class SortInfo implements Serializable {

        private static final long serialVersionUID = -3426779071994511726L;

        /**
         * 排序依据，SORT_BY_TIME 以订单时间排序
         */
        @ApiField("sort_key")
        private String sortKey = "SORT_BY_TIME";

        /**
         * 排序规则，SORT_ASC 升序
         * SORT_DESC 降序
         */
        @ApiField("sort_type")
        private String sortType = "SORT_DESC";

        public SortInfo() {
        }

        public SortInfo(String sortKey, String sortType) {
            this.sortKey = sortKey;
            this.sortType = sortType;
        }

        public String getSortKey() {
            return sortKey;
        }

        public void setSortKey(String sortKey) {
            this.sortKey = sortKey;
        }

        public String getSortType() {
            return sortType;
        }

        public void setSortType(String sortType) {
            this.sortType = sortType;
        }
    }
}
