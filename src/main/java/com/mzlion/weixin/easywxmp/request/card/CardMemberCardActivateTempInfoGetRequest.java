/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardMemberCardActivateTempInfoGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 根据activate_ticket获取用户信息
 * <p>支持开发者根据activate_ticket获取到用户填写的信息。</p>
 *
 * @author kudo on 2017/6/8.
 */
public class CardMemberCardActivateTempInfoGetRequest extends WxRequest<CardMemberCardActivateTempInfoGetResponse> {

    @ApiField("activate_ticket")
    private String activateTicket;

    public String getActivateTicket() {
        return activateTicket;
    }

    public void setActivateTicket(String activateTicket) {
        this.activateTicket = activateTicket;
    }

    public CardMemberCardActivateTempInfoGetRequest() {
    }

    public CardMemberCardActivateTempInfoGetRequest(String activateTicket) {
        this.activateTicket = activateTicket;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/membercard/activatetempinfo/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardMemberCardActivateTempInfoGetResponse> responseClass() {
        return CardMemberCardActivateTempInfoGetResponse.class;
    }
}
