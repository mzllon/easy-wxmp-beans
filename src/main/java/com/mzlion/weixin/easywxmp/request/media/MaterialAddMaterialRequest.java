/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.media;

import com.mzlion.weixin.easywxmp.UploadWxRequest;
import com.mzlion.weixin.easywxmp.response.media.MaterialAddMaterialResponse;

/**
 * 新增其他类型永久素材
 * <p>
 * 通过POST表单来调用接口，表单id为media，包含需要上传的素材内容，有filename、filelength、content-type等信息。
 * 请注意：图片素材将进入公众平台官网素材管理模块中的默认分组。
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class MaterialAddMaterialRequest extends UploadWxRequest<MaterialAddMaterialResponse> {

    private static final long serialVersionUID = -6023720394284782204L;

    /**
     * 媒体文件类型，
     * 图片（image）
     * 语音（voice）
     * 视频（video）
     * 缩略图（thumb）
     */
    private String type;

    /**
     * 视频素材的标题
     */
    private String title;

    /**
     * 视频素材的描述
     */
    private String introduction;

    public MaterialAddMaterialRequest() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/material/add_material";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MaterialAddMaterialResponse> responseClass() {
        return MaterialAddMaterialResponse.class;
    }
}
