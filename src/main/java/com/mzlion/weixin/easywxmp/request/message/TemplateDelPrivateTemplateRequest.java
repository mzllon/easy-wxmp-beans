/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 删除模板
 * <p>删除模板可在MP中完成，为方便第三方开发者，提供通过接口调用的方式来删除某帐号下的模板</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateDelPrivateTemplateRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = 1125637776173677293L;

    /**
     * 模板ID
     */
    @ApiField("template_id")
    private String templateId;

    @Override
    public String serviceUrl() {
        return "/cgi-bin/template/del_private_template";
    }

    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

}
