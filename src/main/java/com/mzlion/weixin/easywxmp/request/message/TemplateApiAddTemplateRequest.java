/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.message.TemplateApiAddTemplateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获得模板ID
 * <p>从行业模板库选择模板到帐号后台，获得模板ID的过程可在MP中完成。为方便第三方开发者，提供通过接口调用的方式来获取模板ID</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateApiAddTemplateRequest extends WxRequest<TemplateApiAddTemplateResponse> {

    /**
     * 模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
     */
    @ApiField("template_id_short")
    private String templateIdShort;

    public TemplateApiAddTemplateRequest() {
    }

    public TemplateApiAddTemplateRequest(String templateIdShort) {
        this.templateIdShort = templateIdShort;
    }

    public String getTemplateIdShort() {
        return templateIdShort;
    }

    public void setTemplateIdShort(String templateIdShort) {
        this.templateIdShort = templateIdShort;
    }

    @Override
    public String serviceUrl() {
        return "/cgi-bin/template/api_add_template";
    }

    @Override
    public Class<TemplateApiAddTemplateResponse> responseClass() {
        return TemplateApiAddTemplateResponse.class;
    }

}
