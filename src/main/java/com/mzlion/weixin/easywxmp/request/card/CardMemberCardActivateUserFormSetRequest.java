/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.CommonFieldIdEnum;
import com.mzlion.weixin.easywxmp.constants.RichFieldIdEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 设置开卡字段接口
 * <p>开发者在创建时填入wx_activate字段后，需要调用该接口设置用户激活时需要填写的选项，否则一键开卡设置不生效。</p>
 *
 * @author mzlion on 2016/12/25.
 */
public class CardMemberCardActivateUserFormSetRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -4864180547392333804L;

    /**
     * 卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 服务声明，用于放置商户会员卡守则
     */
    @ApiField("service_statement")
    private ServiceStatement serviceStatement;

    /**
     * 绑定老会员链接
     */
    @ApiField("bind_old_card")
    private BindOldCardSupport bindOldCardSupport;

    /**
     * 会员卡激活时的必填选项
     */
    @ApiField("required_form")
    private UserForm requiredForm;

    /**
     * 会员卡激活时的选填项
     */
    @ApiField("optional_form")
    private UserForm optionalForm;

    public CardMemberCardActivateUserFormSetRequest() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public ServiceStatement getServiceStatement() {
        return serviceStatement;
    }

    public void setServiceStatement(ServiceStatement serviceStatement) {
        this.serviceStatement = serviceStatement;
    }

    public BindOldCardSupport getBindOldCardSupport() {
        return bindOldCardSupport;
    }

    public void setBindOldCardSupport(BindOldCardSupport bindOldCardSupport) {
        this.bindOldCardSupport = bindOldCardSupport;
    }

    public UserForm getRequiredForm() {
        return requiredForm;
    }

    public void setRequiredForm(UserForm requiredForm) {
        this.requiredForm = requiredForm;
    }

    public UserForm getOptionalForm() {
        return optionalForm;
    }

    public void setOptionalForm(UserForm optionalForm) {
        this.optionalForm = optionalForm;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/membercard/activateuserform/set";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }

    /**
     * 服务声明，用于放置商户会员卡守则
     */
    public static class ServiceStatement implements Serializable {

        private static final long serialVersionUID = -487011749472381778L;

        /**
         * 会员守则
         */
        private String name;

        /**
         * 守则地址
         */
        private String url;

        public ServiceStatement() {
        }

        public ServiceStatement(String name, String url) {
            this.name = name;
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }

    /**
     * 绑定老会员链接
     */
    public static class BindOldCardSupport implements Serializable {

        private static final long serialVersionUID = -6773209613026827391L;

        /**
         * 链接名称
         */
        private String name;

        /**
         * 地址
         */
        private String url;

        public BindOldCardSupport() {
        }

        public BindOldCardSupport(String name, String url) {
            this.name = name;
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }

    /**
     * 用户表单
     */
    public static class UserForm implements Serializable {

        private static final long serialVersionUID = 7514032829634038676L;

        /**
         * 当前结构（required_form或者optional_form ）内的字段是否允许用户激活后再次修改，
         * 商户设置为true时，需要接收相应事件通知处理修改事件
         */
        @ApiField("can_modify")
        private boolean canModify = false;

        /**
         * 微信格式化的选项类型
         */
        @ApiField("common_field_id_list")
        private List<CommonFieldIdEnum> commonFieldIdList;

        /**
         * 自定义选项名称，开发者可以分别在必填和选填中至多定义五个自定义选项
         */
        @ApiField("custom_field_list")
        private List<String> customFieldList;

        /**
         * 自定义富文本类型，包含以下三个字段，开发者可以分别在必填和选填中至多定义五个自定义选项
         */
        @ApiField("rich_field_list")
        private List<RichField> richFieldList;

        public UserForm() {
        }

        public boolean isCanModify() {
            return canModify;
        }

        public void setCanModify(boolean canModify) {
            this.canModify = canModify;
        }

        public List<CommonFieldIdEnum> getCommonFieldIdList() {
            return commonFieldIdList;
        }

        public void setCommonFieldIdList(List<CommonFieldIdEnum> commonFieldIdList) {
            this.commonFieldIdList = commonFieldIdList;
        }

        public List<String> getCustomFieldList() {
            return customFieldList;
        }

        public void setCustomFieldList(List<String> customFieldList) {
            this.customFieldList = customFieldList;
        }

        public List<RichField> getRichFieldList() {
            return richFieldList;
        }

        public void setRichFieldList(List<RichField> richFieldList) {
            this.richFieldList = richFieldList;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }

    /**
     * 自定义富文本
     */
    public static class RichField implements Serializable {

        private static final long serialVersionUID = 4933136509663669362L;

        /**
         * 富文本类型
         */
        @ApiField("type")
        private RichFieldIdEnum richFieldId;

        /**
         * 字段名
         */
        private String name;

        /**
         * 选择项
         */
        private List<String> values;

        public RichField() {
        }

        public RichField(RichFieldIdEnum richFieldId, String name, List<String> values) {
            this.richFieldId = richFieldId;
            this.name = name;
            this.values = values;
        }

        public RichFieldIdEnum getRichFieldId() {
            return richFieldId;
        }

        public void setRichFieldId(RichFieldIdEnum richFieldId) {
            this.richFieldId = richFieldId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getValues() {
            return values;
        }

        public void setValues(List<String> values) {
            this.values = values;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }
}
