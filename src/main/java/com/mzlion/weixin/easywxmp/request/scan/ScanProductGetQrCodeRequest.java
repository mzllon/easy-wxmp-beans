/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.scan;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.scan.ScanProductGetQrCodeResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取商品二维码,商户可获得由微信格式生成的商品二维码，用于印刷在包装上标识商品。
 * <p>
 * 商户可通过extinfo为同一款产品生成多个二维码，用于区分或作来源判断。
 * 例如一款产品虽然只有一个对应的商品ID，但可以为该款产品下每一个单独的商品分配一个extinfo参数用以标识。
 * 当用户打开商品主页，或者点击主页内的推广链接时，微信会将extinfo参数透传给商户，商户就能跟踪每一个商品和用户接触的情况。
 * 更多商品二维码能力请见“一物一码专区”。
 * </p>
 * Created by mzlion on 2017/4/18.
 */
public class ScanProductGetQrCodeRequest extends WxRequest<ScanProductGetQrCodeResponse> {

    private static final long serialVersionUID = -5996923280120445640L;

    /**
     * 商品编码标准，支持ean13、ean8和qrcode标准
     */
    @ApiField("keystandard")
    String keyStandard;

    /**
     * 商品编码内容。直接填写商品条码，
     * 标准是ean13，则直接填写商品条码，如“6901939621608”。
     * 标准是qrcode，二维码的内容可由商户自定义，建议使用商品条码，≤20个字符，由大小字母、数字、下划线和连字符组成。
     * 注意：编码标准是ean13时，编码内容必须在商户的号段之下，否则会报错
     */
    @ApiField("keystr")
    String keyStr;

    /**
     * 由商户自定义传入，建议仅使用大小写字母、数字及-_().*这6个常用字符
     */
    @ApiField("extinfo")
    String extInfo;

    /**
     * 二维码的尺寸（整型），数值代表边长像素数，不填写默认值为100
     */
    @ApiField("qrcode_size")
    int qrCodeSize = 100;

    public ScanProductGetQrCodeRequest() {
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public int getQrCodeSize() {
        return qrCodeSize;
    }

    public void setQrCodeSize(int qrCodeSize) {
        this.qrCodeSize = qrCodeSize;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/scan/product/getqrcode";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ScanProductGetQrCodeResponse> responseClass() {
        return ScanProductGetQrCodeResponse.class;
    }
}
