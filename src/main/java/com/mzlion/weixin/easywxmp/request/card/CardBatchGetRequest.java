/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.CardStatusVerifyEnum;
import com.mzlion.weixin.easywxmp.response.card.CardBatchGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 批量查询卡券列表
 *
 * @author mzlion on 2017/1/3.
 */
public class CardBatchGetRequest extends WxRequest<CardBatchGetResponse> {

    private static final long serialVersionUID = 6095383229642461317L;

    /**
     * 查询卡列表的起始偏移量，从0开始，即offset: 5是指从从列表里的第六个开始读取。
     */
    private int offset = 0;

    /**
     * 需要查询的卡片的数量（数量最大50）。
     */
    private int count = 10;

    @ApiField("status_list")
    private List<CardStatusVerifyEnum> cardStatusList;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        if (count > 50) this.count = 50;
        this.count = count;
    }

    public List<CardStatusVerifyEnum> getCardStatusList() {
        return cardStatusList;
    }

    public void setCardStatusList(List<CardStatusVerifyEnum> cardStatusList) {
        this.cardStatusList = cardStatusList;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/batchget";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardBatchGetResponse> responseClass() {
        return CardBatchGetResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
