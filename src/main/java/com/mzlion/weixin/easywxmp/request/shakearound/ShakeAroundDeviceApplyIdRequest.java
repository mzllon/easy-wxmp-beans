/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundDeviceApplyIdResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 申请设备ID
 * <p>
 * 申请配置设备所需的UUID、Major、Minor。申请成功后返回批次ID，可用返回的批次ID通过“查询设备ID申请状态”接口查询目前申请的审核状态。
 * 若单次申请的设备ID数量小于500个，系统会进行快速审核；若单次申请的设备ID数量大于等 500个 ，会在三个工作日内完成审核。
 * 如果已审核通过，可用返回的批次ID通过“查询设备列表”接口拉取本次申请的设备ID。 通过接口申请的设备ID，需先配置页面，若未配置页面，则摇不出页面信息。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceApplyIdRequest extends WxRequest<ShakeAroundDeviceApplyIdResponse> {

    private static final long serialVersionUID = -1903866082599636932L;

    /**
     * 申请的设备ID的数量，单次新增设备超过500个，需走人工审核流程
     */
    private int quantity;

    /**
     * 申请理由，不超过100个汉字或200个英文字母
     */
    @ApiField("apply_reason")
    private String applyReason;

    /**
     * 备注，不超过15个汉字或30个英文字母
     */
    private String comment;

    /**
     * 设备关联的门店ID，关联门店后，在门店1KM的范围内有优先摇出信息的机会。门店相关信息具体可查看门店相关的接口文档
     */
    @ApiField("poi_id")
    private String poiId;

    public ShakeAroundDeviceApplyIdRequest() {
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getApplyReason() {
        return applyReason;
    }

    public void setApplyReason(String applyReason) {
        this.applyReason = applyReason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/applyid";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundDeviceApplyIdResponse> responseClass() {
        return ShakeAroundDeviceApplyIdResponse.class;
    }
}
