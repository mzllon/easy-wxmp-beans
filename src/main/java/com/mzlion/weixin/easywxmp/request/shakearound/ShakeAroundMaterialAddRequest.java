/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.UploadWxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundMaterialAddResponse;

/**
 * 上传图片素材
 * <p>
 * 上传在摇一摇功能中需使用到的图片素材，素材保存在微信侧服务器上。图片格式限定为：jpg,jpeg,png,gif。
 * 若图片为在摇一摇页面展示的图片，则其素材为icon类型的图片，图片大小建议120px*120 px，限制不超过200 px *200 px，图片需为正方形。
 * 若图片为申请开通摇一摇周边功能需要上传的资质文件图片，则其素材为license类型的图片，图片的文件大小不超过2MB，尺寸不限，形状不限。
 * </p>
 * Created by mzlion on 2017/4/19.
 */
public class ShakeAroundMaterialAddRequest extends UploadWxRequest<ShakeAroundMaterialAddResponse> {

    private static final long serialVersionUID = -2113014858350817218L;

    /**
     * Icon：摇一摇页面展示的icon图；
     * License：申请开通摇一摇周边功能时需上传的资质文件；
     * 若不传type，则默认type=icon
     */
    private String type = "icon";

    public ShakeAroundMaterialAddRequest() {
    }



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/material/add";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundMaterialAddResponse> responseClass() {
        return ShakeAroundMaterialAddResponse.class;
    }
}
