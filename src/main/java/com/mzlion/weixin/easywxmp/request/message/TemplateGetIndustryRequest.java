/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ApiMethodEnum;
import com.mzlion.weixin.easywxmp.response.message.TemplateGetIndustryResponse;

/**
 * 获取设置的行业信息
 * <p>获取帐号设置的行业信息。可在MP官网中查看行业信息，为方便第三方开发者，提供通过接口调用的方式来获取帐号所设置的行业信息</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateGetIndustryRequest extends WxRequest<TemplateGetIndustryResponse> {

    private static final long serialVersionUID = 1125637776173677293L;

    /**
     * 接口请求方式，使用{@linkplain ApiMethodEnum#GET}请求
     *
     * @return {@link ApiMethodEnum}
     */
    @Override
    public ApiMethodEnum method() {
        return ApiMethodEnum.GET;
    }

    @Override
    public String serviceUrl() {
        return "/cgi-bin/template/get_industry";
    }

    @Override
    public Class<TemplateGetIndustryResponse> responseClass() {
        return TemplateGetIndustryResponse.class;
    }

}
