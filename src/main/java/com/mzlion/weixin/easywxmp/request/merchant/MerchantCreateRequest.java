/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.merchant.MerchantProductInfoModel;
import com.mzlion.weixin.easywxmp.response.merchant.MerchantCreateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 增加商品
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantCreateRequest extends WxRequest<MerchantCreateResponse> {

    /**
     * 商品基本信息
     */
    @ApiField("product_base")
    private MerchantProductInfoModel productInfo;

    /**
     * sku信息列表(可为多个)，每个sku信息串即为一个确定的商品，比如白色的37码的鞋子
     */
    @ApiField("sku_list")
    private List<MerchantProductInfoModel.SkuDetail> skuDetailList;

    /**
     * 商品其他属性
     */
    @ApiField("attrext")
    private MerchantProductInfoModel.AttrExtInfo attrExtInfo;

    /**
     * 运费信息
     */
    @ApiField("delivery_info")
    private MerchantProductInfoModel.DeliveryInfo deliveryInfo;

    public MerchantCreateRequest() {
    }

    public MerchantProductInfoModel getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(MerchantProductInfoModel productInfo) {
        this.productInfo = productInfo;
    }

    public List<MerchantProductInfoModel.SkuDetail> getSkuDetailList() {
        return skuDetailList;
    }

    public void setSkuDetailList(List<MerchantProductInfoModel.SkuDetail> skuDetailList) {
        this.skuDetailList = skuDetailList;
    }

    public MerchantProductInfoModel.AttrExtInfo getAttrExtInfo() {
        return attrExtInfo;
    }

    public void setAttrExtInfo(MerchantProductInfoModel.AttrExtInfo attrExtInfo) {
        this.attrExtInfo = attrExtInfo;
    }

    public MerchantProductInfoModel.DeliveryInfo getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(MerchantProductInfoModel.DeliveryInfo deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/create";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MerchantCreateResponse> responseClass() {
        return MerchantCreateResponse.class;
    }
}
