/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiShopGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询门店Wi-Fi信息
 * <p>通过此接口查询某一门店的详细Wi-Fi信息，包括门店内的设备类型、ssid、密码、设备数量、商家主页URL、顶部常驻入口文案。</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiShopGetRequest extends WxRequest<BizWifiShopGetResponse> {

    private static final long serialVersionUID = 1557827616625832743L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    public BizWifiShopGetRequest() {
    }

    public BizWifiShopGetRequest(String shopId) {
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/shop/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiShopGetResponse> responseClass() {
        return BizWifiShopGetResponse.class;
    }
}
