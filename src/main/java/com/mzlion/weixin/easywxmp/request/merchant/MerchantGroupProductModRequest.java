/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 修改分组商品
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantGroupProductModRequest extends WxRequest<WxResponse> {

    /**
     * 分组ID
     */
    @ApiField("group_id")
    private String groupId;

    /**
     * 分组名称
     */
    @ApiField("product")
    private List<ProductMod> productModList;

    public MerchantGroupProductModRequest() {
    }

    public MerchantGroupProductModRequest(String groupId, List<ProductMod> productModList) {
        this.groupId = groupId;
        this.productModList = productModList;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<ProductMod> getProductModList() {
        return productModList;
    }

    public void setProductModList(List<ProductMod> productModList) {
        this.productModList = productModList;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/group/productmod";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    /**
     * 商品修改规则
     */
    public static class ProductMod implements Serializable {

        /**
         * 商品ID
         */
        @ApiField("product_id")
        private String productId;

        /**
         * 修改操作
         *
         * @see MerchantGroupProductModRequest#MOD_ACTION$ADD
         * @see MerchantGroupProductModRequest#MOD_ACTION$DEL
         */
        @ApiField("mod_action")
        private Integer modAction;

        public ProductMod() {
        }

        public ProductMod(String productId, int modAction) {
            this.productId = productId;
            this.modAction = modAction;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public Integer getModAction() {
            return modAction;
        }

        public void setModAction(Integer modAction) {
            this.modAction = modAction;
        }
    }

    /**
     * 增加商品
     */
    public static final int MOD_ACTION$ADD = 1;

    /**
     * 删除商品
     */
    public static final int MOD_ACTION$DEL = 0;
}
