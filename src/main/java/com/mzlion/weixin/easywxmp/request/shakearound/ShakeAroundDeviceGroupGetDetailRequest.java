/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundDeviceGroupGetDetailResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询分组详情
 * <p>查询分组详情，包括分组名，分组id，分组里的设备列表。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceGroupGetDetailRequest extends WxRequest<ShakeAroundDeviceGroupGetDetailResponse> {

    private static final long serialVersionUID = -5993477349333845282L;

    /**
     * 分组唯一标识，全局唯一
     */
    @ApiField("group_id")
    private String groupId;

    /**
     * 分组列表的起始索引值
     */
    private int begin;

    /**
     * 待查询的分组数量，不能超过1000个
     */
    private int count;

    public ShakeAroundDeviceGroupGetDetailRequest() {
    }

    public ShakeAroundDeviceGroupGetDetailRequest(int begin, int count) {
        this.begin = begin;
        this.count = count;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/group/getdetail";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundDeviceGroupGetDetailResponse> responseClass() {
        return ShakeAroundDeviceGroupGetDetailResponse.class;
    }
}
