/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.card.CardSubMerchantModel;
import com.mzlion.weixin.easywxmp.response.card.CardSubMerchantUpdateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 子商户信息更新接口请求对象
 * <p>支持调用该接口更新子商户信息。</p>
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantUpdateRequest extends WxRequest<CardSubMerchantUpdateResponse> {

    /**
     * 子商户信息
     */
    @ApiField("info")
    private CardSubMerchantModel cardSubMerchant;

    public CardSubMerchantUpdateRequest() {
    }

    public CardSubMerchantUpdateRequest(CardSubMerchantModel cardSubMerchant) {
        this.cardSubMerchant = cardSubMerchant;
    }

    public CardSubMerchantModel getCardSubMerchant() {
        return cardSubMerchant;
    }

    public void setCardSubMerchant(CardSubMerchantModel cardSubMerchant) {
        this.cardSubMerchant = cardSubMerchant;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/submerchant/update";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardSubMerchantUpdateResponse> responseClass() {
        return CardSubMerchantUpdateResponse.class;
    }
}
