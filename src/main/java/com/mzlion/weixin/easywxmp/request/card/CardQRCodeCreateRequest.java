/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.constants.QrActionNameEnum;
import com.mzlion.weixin.easywxmp.request.BaseQRCodeCreateRequest;
import com.mzlion.weixin.easywxmp.response.card.CardQRCodeCreateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 创建二维码接口
 * <p>
 * 开发者可调用该接口生成一张卡券二维码供用户扫码后添加卡券到卡包。
 * 自定义Code码的卡券调用接口时，POST数据中需指定code，非自定义code不需指定，指定openid同理。指定后的二维码只能被用户扫描领取一次。
 * </p>
 * @author mzlion on 2017/4/17.
 */
public class CardQRCodeCreateRequest<T extends CardQRCodeCreateRequest.CardQRActionInfoMark> extends BaseQRCodeCreateRequest<T, CardQRCodeCreateResponse> {

    public CardQRCodeCreateRequest() {
        super.qrActionName = QrActionNameEnum.QR_CARD;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/qrcode/create";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardQRCodeCreateResponse> responseClass() {
        return CardQRCodeCreateResponse.class;
    }

    /**
     * 卡券二维码信息标记
     */
    public static class CardQRActionInfoMark extends BaseQRCodeCreateRequest.QRActionInfo {

    }

    /**
     * 扫描二维码领取单张卡券
     */
    public static class SingleCardQRActionInfo extends CardQRActionInfoMark {

        /**
         * 二维码信息
         */
        private QrCodeInfo card;

        public SingleCardQRActionInfo() {
        }

        public SingleCardQRActionInfo(QrCodeInfo card) {
            this.card = card;
        }

        public QrCodeInfo getCard() {
            return card;
        }

        public void setCard(QrCodeInfo card) {
            this.card = card;
        }

    }

    /**
     * 扫描二维码领取多张卡券
     */
    public static class MultipleCardQRActionInfo extends CardQRActionInfoMark {

        @ApiField("multiple_card")
        private MultipleCard multipleCard;

        public MultipleCardQRActionInfo() {
        }

        public MultipleCardQRActionInfo(MultipleCard multipleCard) {
            this.multipleCard = multipleCard;
        }

        public MultipleCard getMultipleCard() {
            return multipleCard;
        }

        public void setMultipleCard(MultipleCard multipleCard) {
            this.multipleCard = multipleCard;
        }
    }


    /**
     * 创建二维码接口的基本信息
     */
    public static class QrCodeInfo implements Serializable {

        /**
         * 券Code码,use_custom_code字段为true的卡券必须填写，非自定义code和导入code模式的卡券不必填写。
         */
        private String code;

        /**
         * 卡券ID。
         */
        @ApiField("card_id")
        private String cardId;

        /**
         * 指定领取者的openid，只有该用户能领取。bind_openid字段为true的卡券必须填写，非指定openid不必填写。
         */
        @ApiField("openid")
        private String openId;

        /**
         * 指定下发二维码，生成的二维码随机分配一个code，领取后不可再次扫描。
         * 填写true或false。默认false，注意填写该字段时，卡券须通过审核且库存不为0。
         */
        @ApiField("is_unique_code")
        private Boolean isUniqueCode;

        /**
         * 领取场景值，用于领取渠道的数据统计，默认值为0，字段类型为整型，长度限制为60位数字。
         * 用户领取卡券后触发的事件推送中会带上此自定义场景值。
         * outer_id字段升级版本，字符串类型，用户首次领卡时，会通过领取事件推送给商户；
         */
        @ApiField("outer_id")
        private String outerId;

        /**
         * 对于会员卡的二维码，用户每次扫码打开会员卡后点击任何url，会将该值拼入url中，方便开发者定位扫码来源
         */
        @ApiField("outer_str")
        private String outerStr;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getOpenId() {
            return openId;
        }

        public void setOpenId(String openId) {
            this.openId = openId;
        }

        public Boolean getUniqueCode() {
            return isUniqueCode;
        }

        public void setUniqueCode(Boolean uniqueCode) {
            isUniqueCode = uniqueCode;
        }

        public String getOuterId() {
            return outerId;
        }

        public void setOuterId(String outerId) {
            this.outerId = outerId;
        }

        public String getOuterStr() {
            return outerStr;
        }

        public void setOuterStr(String outerStr) {
            this.outerStr = outerStr;
        }
    }

    /**
     * 多张卡券包装对象
     */
    public static class MultipleCard implements Serializable {

        /**
         * 多张卡券
         */
        @ApiField("card_list")
        private List<QrCodeInfo> cardList;

        public MultipleCard() {
        }

        public MultipleCard(List<QrCodeInfo> cardList) {
            this.cardList = cardList;
        }

        public List<QrCodeInfo> getCardList() {
            return cardList;
        }

        public void setCardList(List<QrCodeInfo> cardList) {
            this.cardList = cardList;
        }
    }

}
