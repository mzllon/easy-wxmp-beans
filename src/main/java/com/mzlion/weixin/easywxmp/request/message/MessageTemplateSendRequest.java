/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.TemplateDynamicData;
import com.mzlion.weixin.easywxmp.response.message.MessageTemplateSendResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.Map;

/**
 * 发送模板消息
 * <p>
 * url和miniprogram都是非必填字段，若都不传则模板无跳转；若都传，会优先跳转至小程序。开发者可根据实际需要选择其中一种跳转方式即可。
 * 当用户的微信客户端版本不支持跳小程序时，将会跳转至url
 * </p>
 *
 * @author mzlion on 2017/4/20.
 */
public class MessageTemplateSendRequest extends WxRequest<MessageTemplateSendResponse> {

    private static final long serialVersionUID = 210827513536051667L;

    /**
     * 接收者openid
     */
    @ApiField("touser")
    private String toUser;
    /**
     * 模板消息id
     */
    @ApiField("template_id")
    private String templateId;

    /**
     * 用户点击模板消息进入的页面
     */
    private String url;

    /**
     * 跳小程序所需数据，不需跳小程序可不用传该数据
     */
    @ApiField("miniprogram")
    private MiniProgram miniProgram;


    /**
     * 发送给用户的模板数据
     */
    @ApiField("data")
    private Map<String, TemplateDynamicData> dataMap;

    public MessageTemplateSendRequest() {
        super();
    }


    public MessageTemplateSendRequest(String toUser, String templateId, String url, Map<String, TemplateDynamicData> dataMap) {
        this.toUser = toUser;
        this.templateId = templateId;
        this.url = url;
        this.dataMap = dataMap;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MiniProgram getMiniProgram() {
        return miniProgram;
    }

    public void setMiniProgram(MiniProgram miniProgram) {
        this.miniProgram = miniProgram;
    }

    public Map<String, TemplateDynamicData> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<String, TemplateDynamicData> dataMap) {
        this.dataMap = dataMap;
    }

    @Override
    public String serviceUrl() {
        return "/cgi-bin/message/template/send";
    }

    @Override
    public Class<MessageTemplateSendResponse> responseClass() {
        return MessageTemplateSendResponse.class;
    }

    public static class MiniProgram implements Serializable {

        private static final long serialVersionUID = -4274770405211153388L;

        /**
         * 所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系）
         */
        @ApiField("appid")
        private String appId;

        /**
         * 所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar）
         */
        @ApiField("pagepath")
        private String pagePath;

        public MiniProgram() {
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getPagePath() {
            return pagePath;
        }

        public void setPagePath(String pagePath) {
            this.pagePath = pagePath;
        }
    }
}
