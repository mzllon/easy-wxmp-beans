/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.UploadWxRequest;
import com.mzlion.weixin.easywxmp.response.merchant.MerchantOrderGetByFilterResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 根据订单状态/创建时间获取订单详情
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantOrderGetByFilterRequest extends UploadWxRequest<MerchantOrderGetByFilterResponse> {

    /**
     * 订单状态,默认查询全部状态订单
     * 2-待发货
     * 3-已发货
     * 5-已完成
     * 8-维权中
     *
     */
    private Integer status;

    /**
     * 订单创建时间起始时间(不带该字段则不按照时间做筛选)
     */
    @ApiField("begintime")
    private String beginTime;

    /**
     * 订单创建时间终止时间(不带该字段则不按照时间做筛选)
     */
    @ApiField("endtime")
    private String endTime;

    public MerchantOrderGetByFilterRequest() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/order/getbyfilter";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MerchantOrderGetByFilterResponse> responseClass() {
        return MerchantOrderGetByFilterResponse.class;
    }
}
