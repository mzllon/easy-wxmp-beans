/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.MerchantProductStatusEnum;
import com.mzlion.weixin.easywxmp.response.merchant.MerchantGetByStatusResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取指定状态的所有商品
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantGetByStatusRequest extends WxRequest<MerchantGetByStatusResponse> {

    /**
     * 商品状态
     */
    @ApiField("status")
    private MerchantProductStatusEnum productStatus = MerchantProductStatusEnum.ALL;

    public MerchantGetByStatusRequest() {
    }

    public MerchantGetByStatusRequest(MerchantProductStatusEnum productStatus) {
        this.productStatus = productStatus;
    }

    public MerchantProductStatusEnum getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(MerchantProductStatusEnum productStatus) {
        this.productStatus = productStatus;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/getbystatus";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MerchantGetByStatusResponse> responseClass() {
        return MerchantGetByStatusResponse.class;
    }

}
