/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.CardLandingPageSceneEnum;
import com.mzlion.weixin.easywxmp.response.card.CardLandingPageCreateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 创建货架接口
 * <p>
 * 开发者需调用该接口创建货架链接，用于卡券投放。创建货架时需填写投放路径的场景字段
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class CardLandingPageCreateRequest extends WxRequest<CardLandingPageCreateResponse> {

    private static final long serialVersionUID = 2342695616423626208L;

    /**
     * 页面的banner图片链接，须调用，建议尺寸为640*300
     */
    private String banner;

    /**
     * 页面的title
     */
    private String title;

    /**
     * 页面是否可以分享,填入true/false
     */
    @ApiField("can_share")
    private boolean canShare;

    /**
     * 投放页面的场景值；
     * SCENE_NEAR_BY 附近
     * SCENE_MENU	自定义菜单
     * SCENE_QRCODE	二维码
     * SCENE_ARTICLE	公众号文章
     * SCENE_H5	h5页面
     * SCENE_IVR	自动回复
     * SCENE_CARD_CUSTOM_CELL	卡券自定义cell
     */
    @ApiField("scene")
    private CardLandingPageSceneEnum cardLandingPageScene;

    /**
     * 投放的卡券列表
     */
    @ApiField("card_list")
    List<SimpleCard> cardList;

    public CardLandingPageCreateRequest() {
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCanShare() {
        return canShare;
    }

    public void setCanShare(boolean canShare) {
        this.canShare = canShare;
    }

    public CardLandingPageSceneEnum getCardLandingPageScene() {
        return cardLandingPageScene;
    }

    public void setCardLandingPageScene(CardLandingPageSceneEnum cardLandingPageScene) {
        this.cardLandingPageScene = cardLandingPageScene;
    }

    public List<SimpleCard> getCardList() {
        return cardList;
    }

    public void setCardList(List<SimpleCard> cardList) {
        this.cardList = cardList;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/landingpage/create";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardLandingPageCreateResponse> responseClass() {
        return CardLandingPageCreateResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }

    /**
     * 投放的卡券信息
     *
     * @author mzlion
     */
    public static class SimpleCard implements Serializable {

        /**
         * 所要在页面投放的card_id
         */
        @ApiField("card_id")
        private String cardId;

        /**
         * 缩略图url
         */
        @ApiField("thumb_url")
        private String thumbUrl;

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getThumbUrl() {
            return thumbUrl;
        }

        public void setThumbUrl(String thumbUrl) {
            this.thumbUrl = thumbUrl;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }
}
