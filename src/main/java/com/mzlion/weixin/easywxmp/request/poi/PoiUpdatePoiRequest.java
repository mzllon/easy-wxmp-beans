/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.poi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.WxPoiModel;
import com.mzlion.weixin.easywxmp.model.WxPoiWrapperModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 修改门店服务信息请求参数
 * <p>
 * 修改门店的服务信息，包括：sid、图片列表、营业时间、推荐、特色服务、简介、人均价格、电话8个字段（名称、坐标、地址等不可修改）修改后需要人工审核。
 * 以上8个字段，若有填写内容则为覆盖更新，若无内容则视为不修改，维持原有内容。
 * photo_list 字段为全列表覆盖，若需要增加图片，需将之前图片同样放入list 中，在其后增加新增图片。
 * 如：已有A、B、C 三张图片，又要增加D、E 两张图，则需要调用该接口，photo_list 传入A、B、C、D、E 五张图片的链接。
 * </p>
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiUpdatePoiRequest extends WxRequest<WxResponse> {

    @ApiField("business")
    private WxPoiWrapperModel<WxPoiModel> wxPoiWrapper;

    public PoiUpdatePoiRequest() {
    }

    public PoiUpdatePoiRequest(WxPoiWrapperModel<WxPoiModel> wxPoiWrapper) {
        this.wxPoiWrapper = wxPoiWrapper;
    }

    public WxPoiWrapperModel<WxPoiModel> getWxPoiWrapper() {
        return wxPoiWrapper;
    }

    public void setWxPoiWrapper(WxPoiWrapperModel<WxPoiModel> wxPoiWrapper) {
        this.wxPoiWrapper = wxPoiWrapper;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/poi/updatepoi";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
