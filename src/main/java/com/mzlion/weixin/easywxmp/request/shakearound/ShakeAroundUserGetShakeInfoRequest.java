/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundUserGetShakeInfoResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取设备及用户信息
 * <p>获取设备信息，包括UUID、major、minor，以及距离、openID等信息。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundUserGetShakeInfoRequest extends WxRequest<ShakeAroundUserGetShakeInfoResponse> {

    private static final long serialVersionUID = -8444634932727287941L;

    /**
     * 摇周边业务的ticket，可在摇到的URL中得到，ticket生效时间为30分钟，每一次摇都会重新生成新的ticket
     */
    private String ticket;

    /**
     * 是否需要返回门店poi_id，传1则返回，否则不返回；
     * 门店相关信息具体可查看门店相关的接口文档
     */
    @ApiField("need_poi")
    private int needPoi = 0;


    public ShakeAroundUserGetShakeInfoRequest() {
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getNeedPoi() {
        return needPoi;
    }

    public void setNeedPoi(int needPoi) {
        this.needPoi = needPoi;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/user/getshakeinfo";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundUserGetShakeInfoResponse> responseClass() {
        return ShakeAroundUserGetShakeInfoResponse.class;
    }
}
