/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 申请开通摇一摇周边功能
 * <p>
 * 成功提交申请请求后，工作人员会在三个工作日内完成审核。若审核不通过，可以重新提交申请请求。
 * 若是审核中，请耐心等待工作人员审核，在审核中状态不能再提交申请请求。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundAccountRegisterRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -991664942949690806L;

    /**
     * 联系人姓名，不超过20汉字或40个英文字母
     */
    private String name;

    /**
     * 联系人电话
     */
    @ApiField("phone_number")
    private String phoneNumber;

    /**
     * 联系人邮箱
     */
    private String email;

    /**
     * 平台定义的行业代号，具体请查看链接行业代号
     */
    @ApiField("industry_id")
    private String industryId;

    /**
     * 关资质文件的图片url，图片需先上传至微信侧服务器，用“素材管理-上传图片素材”接口上传图片，返回的图片URL再配置在此处；
     * 当不需要资质文件时，数组内可以不填写url
     */
    @ApiField("qualification_cert_urls")
    private List<String> qualificationCertUrls;

    /**
     * 申请理由，不超过250汉字或500个英文字母
     */
    @ApiField("apply_reason")
    private String applyReason;

    public ShakeAroundAccountRegisterRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public List<String> getQualificationCertUrls() {
        return qualificationCertUrls;
    }

    public void setQualificationCertUrls(List<String> qualificationCertUrls) {
        this.qualificationCertUrls = qualificationCertUrls;
    }

    public String getApplyReason() {
        return applyReason;
    }

    public void setApplyReason(String applyReason) {
        this.applyReason = applyReason;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/account/register";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
