/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 增加库存
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantStockAddRequest extends WxRequest<WxResponse> {

    /**
     * 商品ID
     */
    @ApiField("product_id")
    private String productId;

    /**
     * sku信息,格式"id1:vid1;id2:vid2",如商品为统一规格，则此处赋值为空字符串即可
     */
    @ApiField("sku_info")
    private String skuInfo;

    /**
     * 增加的库存数量
     */
    private int quantity;

    public MerchantStockAddRequest() {
    }

    public MerchantStockAddRequest(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSkuInfo() {
        if (this.skuInfo != null) {
            if (this.skuInfo.endsWith(";"))
                this.skuInfo = this.skuInfo.substring(0, this.skuInfo.length() - 2);
        }
        return skuInfo;
    }

    public void setSkuInfo(String skuInfo) {
        this.skuInfo = skuInfo;
    }

    /**
     * 增加一条SKU信息
     * @param sku SKU
     */
    public void addSku(Sku sku) {
        if (sku != null) {
            this.skuInfo = sku.toString() + ";" + this.skuInfo;
        }
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/stock/add";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    public static class Sku implements Serializable {

        /**
         * sku属性,SKU列表中id
         */
        String id;

        /**
         * sku值,SKU列表中vid
         */
        String vid;

        public Sku() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVid() {
            return vid;
        }

        public void setVid(String vid) {
            this.vid = vid;
        }

        @Override
        public String toString() {
            return String.format("%s:%s", this.id, this.vid);
        }
    }
}
