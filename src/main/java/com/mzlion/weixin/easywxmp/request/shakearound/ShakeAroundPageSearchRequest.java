/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundPageSearchResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 查询摇一摇页面信息列表
 * <p>包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。提供两种查询方式，可指定页面ID查询，也可批量拉取页面列表。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundPageSearchRequest extends WxRequest<ShakeAroundPageSearchResponse> {

    private static final long serialVersionUID = 5627497882731898281L;

    /**
     * 查询类型。
     * 1： 查询页面id列表中的页面信息；
     * 2：分页查询所有页面信息
     */
    private String type;

    /**
     * 新增页面的页面id
     */
    @ApiField("page_ids")
    private List<String> pageIdList;

    /**
     * 页面列表的起始索引值；当type为2时，此项为必填
     */
    private int begin;

    /**
     * 待查询的页面数量，不能超过50个；当type为2时，此项为必填
     */
    private int count;


    public ShakeAroundPageSearchRequest() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getPageIdList() {
        return pageIdList;
    }

    public void setPageIdList(List<String> pageIdList) {
        this.pageIdList = pageIdList;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/page/search";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundPageSearchResponse> responseClass() {
        return ShakeAroundPageSearchResponse.class;
    }
}
