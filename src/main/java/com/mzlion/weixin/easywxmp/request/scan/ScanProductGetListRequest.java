/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.scan;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.scan.ScanProductGetListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 批量查询商品信息
 * <p>调用该接口，商户可以批量查询创建成功的商品信息，查询维度有商品状态和编码内容。</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductGetListRequest extends WxRequest<ScanProductGetListResponse> {

    private static final long serialVersionUID = -5721662310910424330L;

    /**
     * 批量查询的起始位置，从0开始，包含该起始位置
     */
    private int offset;

    /**
     * 批量查询的数量
     */
    private int limit;

    /**
     * 支持按状态拉取。on为发布状态，off为未发布状态，check为审核中状态，reject为审核未通过状态，all为所有状态
     */
    private String status;

    /**
     * 支持按部分编码内容拉取。填写该参数后，可将编码内容中包含所传参数的商品信息拉出。类似关键词搜索
     */
    @ApiField("keystr")
    private String keyStr;

    public ScanProductGetListRequest() {
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/scan/product/getlist";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ScanProductGetListResponse> responseClass() {
        return ScanProductGetListResponse.class;
    }
}
