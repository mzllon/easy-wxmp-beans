/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.user;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.user.TagsGetIdListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取用户身上的标签列表
 *
 * @author mzlion on 2017/4/17.
 */
public class TagsGetIdListRequest extends WxRequest<TagsGetIdListResponse> {

    /**
     * openid
     */
    @ApiField("openid")
    private final String openId;

    public TagsGetIdListRequest(String openId) {
        this.openId = openId;
    }

    public String getOpenId() {
        return openId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/tags/getidlist";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<TagsGetIdListResponse> responseClass() {
        return TagsGetIdListResponse.class;
    }
}
