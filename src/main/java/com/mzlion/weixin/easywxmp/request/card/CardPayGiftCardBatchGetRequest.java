/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardPayGiftCardBatchGetResponse;

/**
 * 批量查询支付即会员规则接口
 * <p>可以批量查询某个商户支付即会员规则内容</p>
 *
 * @author mzlion on 2017/1/6.
 */
public class CardPayGiftCardBatchGetRequest extends WxRequest<CardPayGiftCardBatchGetResponse> {

    /**
     * 类型，此处填写 RULE_TYPE_PAY_MEMBER_CARD
     */
    private String type = "RULE_TYPE_PAY_MEMBER_CARD";

    /**
     * 是否仅查询生效的规则
     */
    private Boolean effective = Boolean.TRUE;

    /**
     * 起始偏移量
     */
    private int offset = 0;

    /**
     * 查询的数量
     */
    private int count = 10;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEffective() {
        return effective;
    }

    public void setEffective(boolean effective) {
        this.effective = effective;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/paygiftcard/batchget";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayGiftCardBatchGetResponse> responseClass() {
        return CardPayGiftCardBatchGetResponse.class;
    }
}
