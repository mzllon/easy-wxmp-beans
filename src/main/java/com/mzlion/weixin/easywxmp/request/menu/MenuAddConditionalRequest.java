/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.menu;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.MenuButtonModel;
import com.mzlion.weixin.easywxmp.model.MenuMatchRuleModel;
import com.mzlion.weixin.easywxmp.response.menu.MenuAddConditionalResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 创建个性化菜单请求参数
 *
 * @author mzlion on 2017/1/5.
 */
public class MenuAddConditionalRequest extends WxRequest<MenuAddConditionalResponse> {

    private static final long serialVersionUID = -3594418082769444481L;

    /**
     * 菜单列表
     */
    @ApiField(value = "button")
    private List<MenuButtonModel> buttonList;

    /**
     * 菜单匹配规则
     */
    @ApiField("matchrule")
    private MenuMatchRuleModel menuMatchRule;

    public MenuAddConditionalRequest() {
    }

    public List<MenuButtonModel> getButtonList() {
        return buttonList;
    }

    public void setButtonList(List<MenuButtonModel> buttonList) {
        this.buttonList = buttonList;
    }

    public MenuMatchRuleModel getMenuMatchRule() {
        return menuMatchRule;
    }

    public void setMenuMatchRule(MenuMatchRuleModel menuMatchRule) {
        this.menuMatchRule = menuMatchRule;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/menu/addconditional";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MenuAddConditionalResponse> responseClass() {
        return MenuAddConditionalResponse.class;
    }
}
