/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.scan;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.ProductBranInfoModel;
import com.mzlion.weixin.easywxmp.response.scan.ScanProductUpdateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 更新商品信息
 * <p>调用该接口，可对商品的基本信息（base_info）、详情信息（detail_info）、推广服务区 （action_info）和组件区（modul_info）四部分进行独立或整体的更新。</p>
 * <p>* 注意：对处于“发布状态（on）”的商品进行更新，调用接口成功后，新的商品信息会自动进入扫一扫审核系统。对“审核中（check）”的商品，不可再更新或取消发布，否则会报错。</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductUpdateRequest extends WxRequest<ScanProductUpdateResponse> {

    private static final long serialVersionUID = 6215093322063464189L;

    /**
     * 商品编码标准，支持ean13、ean8和qrcode标准
     */
    @ApiField("keystandard")
    String keyStandard;

    /**
     * 商品编码内容。直接填写商品条码，
     * 标准是ean13，则直接填写商品条码，如“6901939621608”。
     * 标准是qrcode，二维码的内容可由商户自定义，建议使用商品条码，≤20个字符，由大小字母、数字、下划线和连字符组成。
     * 注意：编码标准是ean13时，编码内容必须在商户的号段之下，否则会报错
     */
    @ApiField("keystr")
    String keyStr;

    /**
     * 商品信息
     */
    @ApiField("brand_info")
    ProductBranInfoModel productBranInfo;

    public ScanProductUpdateRequest() {
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public ProductBranInfoModel getProductBranInfo() {
        return productBranInfo;
    }

    public void setProductBranInfo(ProductBranInfoModel productBranInfo) {
        this.productBranInfo = productBranInfo;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/scan/product/update";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ScanProductUpdateResponse> responseClass() {
        return ScanProductUpdateResponse.class;
    }
}
