/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.card.PayGiftCardRuleInfoModel;
import com.mzlion.weixin.easywxmp.response.card.CardPayGiftCardAddResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 增加支付即会员规则接口
 * <p>
 * 开通微信支付的商户可以设置在用户微信支付后自动为用户发送一条领卡消息，用户点击消息即可领取会员卡。
 * 目前该功能仅支持微信支付商户号主体和制作会员卡公众号主体一致的情况下配置，否则报错。开发者可以登录“公众平台”-“公众号设置”、“微信支付商户平台首页”插卡企业主体信息是否一致。
 * </p>
 *
 * @author mzlion on 2017/1/6.
 */
public class CardPayGiftCardAddRequest extends WxRequest<CardPayGiftCardAddResponse> {

    /**
     * 规则信息
     */
    @ApiField("rule_info")
    private PayGiftCardRuleInfoModel payGiftCardRuleInfo;

    public CardPayGiftCardAddRequest() {
    }

    public CardPayGiftCardAddRequest(PayGiftCardRuleInfoModel payGiftCardRuleInfo) {
        this.payGiftCardRuleInfo = payGiftCardRuleInfo;
    }

    public PayGiftCardRuleInfoModel getPayGiftCardRuleInfo() {
        return payGiftCardRuleInfo;
    }

    public void setPayGiftCardRuleInfo(PayGiftCardRuleInfoModel payGiftCardRuleInfo) {
        this.payGiftCardRuleInfo = payGiftCardRuleInfo;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/paygiftcard/add";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayGiftCardAddResponse> responseClass() {
        return CardPayGiftCardAddResponse.class;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CardPayGiftCardAddRequest{");
        sb.append("payGiftCardRuleInfo=").append(payGiftCardRuleInfo);
        sb.append('}');
        return sb.toString();
    }
}
