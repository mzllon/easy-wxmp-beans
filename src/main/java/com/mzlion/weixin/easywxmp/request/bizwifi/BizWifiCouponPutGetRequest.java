package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiCouponPutGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询门店卡券投放信息
 * <p>通过此接口查询某一门店的详细卡券投放信息，包括卡券投放状态，卡券ID，卡券的投放时间等信息。</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiCouponPutGetRequest extends WxRequest<BizWifiCouponPutGetResponse> {

    private static final long serialVersionUID = -7359637315745894254L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;


    public BizWifiCouponPutGetRequest() {
    }

    public BizWifiCouponPutGetRequest(String shopId) {
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/couponput/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiCouponPutGetResponse> responseClass() {
        return BizWifiCouponPutGetResponse.class;
    }
}
