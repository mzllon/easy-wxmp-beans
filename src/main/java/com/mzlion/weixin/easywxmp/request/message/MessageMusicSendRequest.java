/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 发送音乐消息
 *
 * @author mzlion on 2017/4/16.
 */
public class MessageMusicSendRequest extends AbsMessageCustomSendRequest {

    /**
     * 音乐消息
     */
    @ApiField("music")
    private MusicMessage musicMessage;

    public MessageMusicSendRequest() {
        super.msgType = MsgTypeEnum.MUSIC;
    }

    public MusicMessage getMusicMessage() {
        return musicMessage;
    }

    public void setMusicMessage(MusicMessage musicMessage) {
        this.musicMessage = musicMessage;
    }

    /**
     * 音乐消息
     *
     * @author mzlion
     */
    public static class MusicMessage implements Serializable {

        /**
         * 图文消息/视频消息/音乐消息的标题
         */
        private String title;

        /**
         * 图文消息/视频消息/音乐消息的描述
         */
        private String description;

        /**
         * 音乐链接
         */
        @ApiField("musicurl")
        private String musicUrl;

        /**
         * 高品质音乐链接，wifi环境优先使用该链接播放音乐
         */
        @ApiField("hqmusicurl")
        private String hqMusicUrl;

        /**
         * 缩略图的媒体ID
         */
        @ApiField("thumb_media_id")
        private String thumbMediaId;

        public MusicMessage() {
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMusicUrl() {
            return musicUrl;
        }

        public void setMusicUrl(String musicUrl) {
            this.musicUrl = musicUrl;
        }

        public String getHqMusicUrl() {
            return hqMusicUrl;
        }

        public void setHqMusicUrl(String hqMusicUrl) {
            this.hqMusicUrl = hqMusicUrl;
        }

        public String getThumbMediaId() {
            return thumbMediaId;
        }

        public void setThumbMediaId(String thumbMediaId) {
            this.thumbMediaId = thumbMediaId;
        }
    }
}
