/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardPayGiftCardGetByIdResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询支付即会员规则详情接口
 * <p>
 * 可以查询某个支付即会员规则内容
 * </p>
 * Created by TM on 2017/1/6.
 */
public class CardPayGiftCardGetByIdRequest extends WxRequest<CardPayGiftCardGetByIdResponse> {

    /**
     * 要查询规则id
     */
    @ApiField("rule_id")
    private String ruleId;

    public CardPayGiftCardGetByIdRequest() {
    }

    public CardPayGiftCardGetByIdRequest(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/paygiftcard/getbyid";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayGiftCardGetByIdResponse> responseClass() {
        return CardPayGiftCardGetByIdResponse.class;
    }
}
