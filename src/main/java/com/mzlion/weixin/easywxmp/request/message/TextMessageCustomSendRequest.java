/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 发送文本消息
 *
 * @author mzlion on 2017/4/16.
 */
public class TextMessageCustomSendRequest extends MessageCustomSendRequest {

    private static final long serialVersionUID = 2215255297025677071L;

    /**
     * 文本消息
     */
    @ApiField("text")
    private TextMessage textMessage;

    public TextMessageCustomSendRequest() {
        super.msgType = MsgTypeEnum.TEXT;
    }

    public TextMessage getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(TextMessage textMessage) {
        this.textMessage = textMessage;
    }

    /**
     * 发送文本消息
     *
     * @author mzlion
     */
    public static class TextMessage implements Serializable {

        private static final long serialVersionUID = -4175428497385189760L;

        /**
         * 文本消息内容
         */
        private String content;

        public TextMessage(String content) {
            this.content = content;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }


}
