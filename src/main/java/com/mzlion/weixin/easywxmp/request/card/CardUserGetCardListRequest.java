/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardUserGetCardListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取用户已领取卡券接口
 * <p>* 用于获取用户卡包里的，属于该appid下所有可用卡券，包括正常状态和未生效状态。</p>
 *
 * @author mzlion by 2017/1/6.
 */
public class CardUserGetCardListRequest extends WxRequest<CardUserGetCardListResponse> {

    private static final long serialVersionUID = -6391032470716100809L;

    /**
     * 需要查询的用户openid
     */
    @ApiField("openid")
    private String openId;

    /**
     * 卡券ID。不填写时默认查询当前appid下的卡券。
     */
    @ApiField("card_id")
    private String cardId;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/user/getcardlist";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardUserGetCardListResponse> responseClass() {
        return CardUserGetCardListResponse.class;
    }
}
