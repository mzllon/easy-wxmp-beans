/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiApPortalRegisterResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 添加portal型设备
 * <p>
 * 调用下述接口可以添加portal型设备的网络信息，并获得secretkey。secretkey为加密字符串参数，是portal设备改造流程中的重要参数。
 * 为防止secretkey泄露，可通过此接口重置刷新，重置后之前生成的secretkey将会失效。需注意：
 * 1. 同一个门店可以添加多个ssid，最大添加100个ssid；
 * 2. 一个门店只能拥有一种设备类型，只要调用此接口添加一个ssid后，该门店即为portal型改造设备。如果门店下已有非portal型设备时，无法调用此接口。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiApPortalRegisterRequest extends WxRequest<BizWifiApPortalRegisterResponse> {

    private static final long serialVersionUID = 2923561361169438147L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 无线网络设备的ssid。32个字符以内；ssid支持中文，但可能因设备兼容性问题导致显示乱码，或无法连接等问题，相关风险自行承担！
     */
    private String ssid;

    /**
     * 重置secretkey，false-不重置，true-重置，默认为false
     */
    private Boolean reset = Boolean.FALSE;

    public BizWifiApPortalRegisterRequest() {
    }

    public BizWifiApPortalRegisterRequest(String shopId, String ssid, Boolean reset) {
        this.shopId = shopId;
        this.ssid = ssid;
        this.reset = reset;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public Boolean getReset() {
        return reset;
    }

    public void setReset(Boolean reset) {
        this.reset = reset;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/apportal/register";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiApPortalRegisterResponse> responseClass() {
        return BizWifiApPortalRegisterResponse.class;
    }
}
