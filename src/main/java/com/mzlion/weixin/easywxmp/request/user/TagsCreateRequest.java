/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.user;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.TagModel;
import com.mzlion.weixin.easywxmp.response.user.TagsCreateResponse;

/**
 * 创建标签
 * <p>
 * 一个公众号，最多可以创建100个标签
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class TagsCreateRequest extends WxRequest<TagsCreateResponse> {

    /**
     * 标签，仅输入标签名即可
     */
    private final TagModel tag;

    public TagsCreateRequest(TagModel tag) {
        this.tag = tag;
    }

    public TagModel getTag() {
        return tag;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/tags/create";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<TagsCreateResponse> responseClass() {
        return TagsCreateResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
