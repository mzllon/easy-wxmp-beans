/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 客服接口-发消息
 *
 * @author mzlion on 2017/4/16.
 */
public abstract class MessageCustomSendRequest extends WxRequest<WxResponse> {

    /**
     * 普通用户openid
     */
    @ApiField("touser")
    private String toUser;

    /**
     * 消息类型
     */
    @ApiField("msgtype")
    protected MsgTypeEnum msgType;

    /**
     * 客服帐号来发消息
     */
    @ApiField("customservice")
    private CustomService customService;

    public MessageCustomSendRequest() {
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public CustomService getCustomService() {
        return customService;
    }

    public void setCustomService(CustomService customService) {
        this.customService = customService;
    }

    public MsgTypeEnum getMsgType() {
        return msgType;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/message/custom/send";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    /**
     * 客服帐号来发消息
     *
     * @author mzlion
     */
    public static class CustomService implements Serializable {

        /**
         * 账号
         */
        @ApiField("kf_account")
        private String kfAccount;

        public CustomService(String kfAccount) {
            this.kfAccount = kfAccount;
        }

        public String getKfAccount() {
            return kfAccount;
        }

        public void setKfAccount(String kfAccount) {
            this.kfAccount = kfAccount;
        }
    }
}
