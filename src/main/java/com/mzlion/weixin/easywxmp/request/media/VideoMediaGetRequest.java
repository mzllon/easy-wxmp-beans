/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.media;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.media.VideoMediaGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取临时素材
 *
 * @author mzlion on 2017/4/20.
 */
public class VideoMediaGetRequest extends WxRequest<VideoMediaGetResponse> {

    private static final long serialVersionUID = 8525527532536644218L;

    /**
     * 媒体文件ID
     */
    @ApiField("media_id")
    private String mediaId;

    public VideoMediaGetRequest() {
    }

    public VideoMediaGetRequest(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/media/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<VideoMediaGetResponse> responseClass() {
        return VideoMediaGetResponse.class;
    }
}
