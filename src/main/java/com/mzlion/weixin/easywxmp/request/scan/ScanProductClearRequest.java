/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.scan;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 清除商品信息
 * <p>调用该接口，商户可以清除创建成功的商品信息</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductClearRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -5164550028921543870L;

    /**
     * 商品编码标准，支持ean13、ean8和qrcode标准
     */
    @ApiField("keystandard")
    String keyStandard;

    /**
     * 商品编码内容。直接填写商品条码，
     * 标准是ean13，则直接填写商品条码，如“6901939621608”。
     * 标准是qrcode，二维码的内容可由商户自定义，建议使用商品条码，≤20个字符，由大小字母、数字、下划线和连字符组成。
     * 注意：编码标准是ean13时，编码内容必须在商户的号段之下，否则会报错
     */
    @ApiField("keystr")
    String keyStr;

    public ScanProductClearRequest() {
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/scan/product/clear";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
