/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.user;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.user.TagsMembersGetBlackListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取公众号的黑名单列表
 * <p>
 * 公众号可通过该接口来获取帐号的黑名单列表，黑名单列表由一串 OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
 * 该接口每次调用最多可拉取 10000 个OpenID，当列表数较多时，可以通过多次拉取的方式来满足需求。。
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class TagsMembersGetBlackListRequest extends WxRequest<TagsMembersGetBlackListResponse> {

    /**
     * 当 begin_openid 为空时，默认从开头拉取。
     */
    @ApiField("begin_openid")
    private Integer tagBeginOpenId;

    public TagsMembersGetBlackListRequest() {
    }

    public Integer getTagBeginOpenId() {
        return tagBeginOpenId;
    }

    public void setTagBeginOpenId(Integer tagBeginOpenId) {
        this.tagBeginOpenId = tagBeginOpenId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/tags/members/getblacklist";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<TagsMembersGetBlackListResponse> responseClass() {
        return TagsMembersGetBlackListResponse.class;
    }
}
