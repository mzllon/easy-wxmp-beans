/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardCodeGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询Code接口
 * <p>我们强烈建议开发者在调用核销code接口之前调用查询code接口，并在核销之前对非法状态的code(如转赠中、已删除、已核销等)做出处理</p>
 * <p>
 * 注意事项：
 * 1.固定时长有效期会根据用户实际领取时间转换，如用户2013年10月1日领取，固定时长有效期为90天，即有效时间为2013年10月1日-12月29日有效。
 * 2.无论check_consume填写的是true还是false,当code未被添加或者code被转赠领取是统一报错：invalid serial code
 * </p>
 *
 * @author mzlion on 2016/12/30.
 */
public class CardCodeGetRequest extends WxRequest<CardCodeGetResponse> {

    private static final long serialVersionUID = 6321720679229785663L;

    /**
     * 卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 单张卡券的唯一标准
     */
    private String code;

    /**
     * 是否校验code核销状态，填入true和false时的code异常状态返回数据不同
     */
    @ApiField("check_consume")
    private Boolean checkConsume;

    /**
     * 卡券ID
     *
     * @return 卡券ID
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * 卡券ID
     *
     * @param cardId 卡券ID
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getCheckConsume() {
        return checkConsume;
    }

    public void setCheckConsume(Boolean checkConsume) {
        this.checkConsume = checkConsume;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/code/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardCodeGetResponse> responseClass() {
        return CardCodeGetResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
