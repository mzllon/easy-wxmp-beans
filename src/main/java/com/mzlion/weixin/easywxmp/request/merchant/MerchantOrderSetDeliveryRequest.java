/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.UploadWxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.DeliveryCompanyEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 设置订单发货信息
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantOrderSetDeliveryRequest extends UploadWxRequest<WxResponse> {

    /**
     * 订单ID
     */
    @ApiField("order_id")
    private String orderId;

    /**
     * 商品是否需要物流
     *
     * @see #NEED_DELIVERY$YES
     * @see #NEED_DELIVERY$NO
     */
    @ApiField("need_delivery")
    private int needDelivery = NEED_DELIVERY$YES;

    /**
     * 物流公司ID
     * 当{@code needDelivery}={@linkplain #NEED_DELIVERY$YES}此字段不可空
     * 当属性{@linkplain #isOthers}的值为{@linkplain #IS_OTHERS$NO}则此属性的取值范围{@linkplain DeliveryCompanyEnum}
     * 当属性{@linkplain #isOthers}的值为{@linkplain #IS_OTHERS$YES}则此属性的值可自定义
     *
     * @see #needDelivery
     * @see #isOthers
     */
    @ApiField("delivery_company")
    private String deliveryCompany;

    /**
     * 运单ID
     * 当{@code needDelivery}={@linkplain #NEED_DELIVERY$YES}此字段不可空
     */
    @ApiField("delivery_track_no")
    private String deliveryTrackNo;

    /**
     * 当物流公司不在{@linkplain DeliveryCompanyEnum}定义范围之内此属性为{@linkplain #IS_OTHERS$YES}，否则此属性的值为{@linkplain #IS_OTHERS$NO}
     *
     * @see #IS_OTHERS$NO
     * @see #IS_OTHERS$YES
     */
    @ApiField("is_others")
    private int isOthers = 0;

    public MerchantOrderSetDeliveryRequest() {
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getNeedDelivery() {
        return needDelivery;
    }

    public void setNeedDelivery(int needDelivery) {
        this.needDelivery = needDelivery;
    }

    public String getDeliveryCompany() {
        return deliveryCompany;
    }

    public void setDeliveryCompany(String deliveryCompany) {
        this.deliveryCompany = deliveryCompany;
    }

    public String getDeliveryTrackNo() {
        return deliveryTrackNo;
    }

    public void setDeliveryTrackNo(String deliveryTrackNo) {
        this.deliveryTrackNo = deliveryTrackNo;
    }

    public int getIsOthers() {
        return isOthers;
    }

    public void setIsOthers(int isOthers) {
        this.isOthers = isOthers;
    }

    /**
     * 设置系统提供的物流公司
     * @param deliveryCompany 物流公司
     */
    public void setSysDeliveryCompany(DeliveryCompanyEnum deliveryCompany) {
        this.deliveryCompany = deliveryCompany.getDeliveryId();
        this.needDelivery = NEED_DELIVERY$YES;
        this.isOthers = IS_OTHERS$NO;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/order/close";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    /**
     * 需要物流
     */
    public static final int NEED_DELIVERY$YES = 1;

    /**
     * 不需要物流
     */
    public static final int NEED_DELIVERY$NO = 0;

    /**
     * 其它物流公司，即非系统指定的物流公司
     */
    public static final int IS_OTHERS$YES = 1;

    /**
     * 系统指定的物流公司
     */
    public static final int IS_OTHERS$NO = 0;
}
