/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundStatisticsPageResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 以页面为维度的数据统计接口
 * <p>
 * 查询单个页面通过摇周边摇出来的人数、次数，点击摇周边页面的人数、次数；查询的最长时间跨度为30天。只能查询最近90天的数据。
 * 此接口无法获取当天的数据，最早只能获取前一天的数据。由于系统在凌晨处理前一天的数据，太早调用此接口可能获取不到数据，建议在早上8：00之后调用此接口。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundStatisticsPageRequest extends WxRequest<ShakeAroundStatisticsPageResponse> {

    private static final long serialVersionUID = 7498880738209073646L;

    /**
     * 新增页面的页面id
     */
    @ApiField("page_id")
    private String pageId;

    /**
     * 起始日期时间戳，最长时间跨度为30天，单位为秒
     */
    @ApiField("begin_date")
    private Long beginDate;

    /**
     * 结束日期时间戳，最长时间跨度为30天，单位为秒
     */
    @ApiField("end_date")
    private Long endDate;


    public ShakeAroundStatisticsPageRequest() {
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public Long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Long beginDate) {
        this.beginDate = beginDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/statistics/page";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundStatisticsPageResponse> responseClass() {
        return ShakeAroundStatisticsPageResponse.class;
    }
}
