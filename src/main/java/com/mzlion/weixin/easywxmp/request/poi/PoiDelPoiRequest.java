/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.poi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 删除门店请求参数
 * <p>商户可以通过该接口，删除已经成功创建的门店。请商户慎重调用该接口。</p>
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiDelPoiRequest extends WxRequest<WxResponse> {

    /**
     * 门店ID
     */
    @ApiField("poi_id")
    private String poiId;

    /**
     * 门店ID
     */
    public String getPoiId() {
        return poiId;
    }

    /**
     * 门店ID
     */
    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/poi/delpoi";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
