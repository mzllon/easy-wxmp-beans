/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 设置微信首页欢迎语
 * <p>
 * 设置微信首页欢迎语，可选择“欢迎光临XXX”或“已连接XXXWiFi”，XXX为公众号名称或门店名称。
 * 注：设置微信首页欢迎语的接口，未审核的门店不能设置包含门店名称（bar_type为1、3）的欢迎语内容。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiBarSetRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -1257375168343881531L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 微信首页欢迎语的文本内容：
     * 0--欢迎光临+公众号名称；
     * 1--欢迎光临+门店名称；
     * 2--已连接+公众号名称+WiFi；
     * 3--已连接+门店名称+Wi-Fi。
     */
    @ApiField("bar_type")
    private Integer barType;

    public BizWifiBarSetRequest() {
    }

    public BizWifiBarSetRequest(String shopId, Integer barType) {
        this.shopId = shopId;
        this.barType = barType;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public Integer getBarType() {
        return barType;
    }

    public void setBarType(Integer barType) {
        this.barType = barType;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/bar/set";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
