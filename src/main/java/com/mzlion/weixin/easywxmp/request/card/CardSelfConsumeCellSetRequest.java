/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 设置自助核销接口
 * <p>
 * 创建卡券之后，开发者可以通过设置微信买单接口设置该card_id支持自助核销功能。
 * 值得开发者注意的是，设置自助核销的card_id必须已经配置了门店，否则会报错。
 * </p>
 *
 * @author mzlion on 2016/12/23.
 */
public class CardSelfConsumeCellSetRequest extends WxRequest<WxResponse> {

    /**
     * 卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 是否开启自助核销功能，填true/false，默认为false
     */
    @ApiField("is_open")
    private boolean isOpen;

    //虽然接口文档有这几个参数，但是提交的时候不支持
    /**
     * 用户核销时是否需要输入验证码，填true/false，默认为false
     */
    @ApiField("need_verify_cod")
    private Boolean needVerifyCod;
    /**
     * 用户核销时是否需要备注核销金额，填true/false，默认为false
     */
    @ApiField("need_remark_amount")
    private Boolean needRemarkAmount;

    public CardSelfConsumeCellSetRequest() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public Boolean getNeedVerifyCod() {
        return needVerifyCod;
    }

    public void setNeedVerifyCod(Boolean needVerifyCod) {
        this.needVerifyCod = needVerifyCod;
    }

    public Boolean getNeedRemarkAmount() {
        return needRemarkAmount;
    }

    public void setNeedRemarkAmount(Boolean needRemarkAmount) {
        this.needRemarkAmount = needRemarkAmount;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/selfconsumecell/set";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
