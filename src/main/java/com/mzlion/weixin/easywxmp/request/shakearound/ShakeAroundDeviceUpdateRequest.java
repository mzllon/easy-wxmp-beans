/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 编辑设备信息
 * <p>编辑设备的备注信息。可用设备ID或完整的UUID、Major、Minor指定设备，二者选其一。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceUpdateRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -8444634932727287941L;

    /**
     * 申请的批次ID
     */
    @ApiField("device_identifier")
    private ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier;

    /**
     * 设备的备注信息，不超过15个汉字或30个英文字母。
     */
    private String comment;


    public ShakeAroundDeviceUpdateRequest() {
    }

    public ShakeAroundDeviceIdentifierModel getShakeAroundDeviceIdentifier() {
        return shakeAroundDeviceIdentifier;
    }

    public void setShakeAroundDeviceIdentifier(ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier) {
        this.shakeAroundDeviceIdentifier = shakeAroundDeviceIdentifier;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/update";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
