/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardMemberCardUserInfoGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 拉取会员信息（积分查询）接口
 * <p>支持开发者根据card_id和Code查询会员信息,包括激活资料、积分信息以及余额等信息。</p>
 *
 * @author mzlion on 2016/12/25.
 */
public class CardMemberCardUserInfoGetRequest extends WxRequest<CardMemberCardUserInfoGetResponse> {

    private static final long serialVersionUID = 4503485223212374686L;

    /**
     * 查询会员卡的cardid
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 所查询用户领取到的code值
     */
    private String code;

    public CardMemberCardUserInfoGetRequest() {
    }

    public CardMemberCardUserInfoGetRequest(String cardId, String code) {
        this.cardId = cardId;
        this.code = code;
    }


    /**
     * 查询会员卡的cardid
     */
    public String getCardId() {
        return cardId;
    }


    /**
     * 查询会员卡的cardid
     *
     * @param cardId 卡券ID
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 所查询用户领取到的code值
     */
    public String getCode() {
        return code;
    }

    /**
     * 所查询用户领取到的code值
     *
     * @param code 会员卡的code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/membercard/userinfo/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardMemberCardUserInfoGetResponse> responseClass() {
        return CardMemberCardUserInfoGetResponse.class;
    }
}
