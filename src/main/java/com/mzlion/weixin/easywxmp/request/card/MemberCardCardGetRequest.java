/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.model.card.MemberCardCardModel;
import com.mzlion.weixin.easywxmp.response.card.MemberCardCardGetResponse;

/**
 * 查看卡券(会员卡)详情
 * <p>
 * 开发者可以调用该接口查询某个card_id的创建信息、审核状态以及库存数量。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class MemberCardCardGetRequest extends AbsCardGetRequest<MemberCardCardModel, MemberCardCardGetResponse> {

    private static final long serialVersionUID = 7980712337565245757L;

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MemberCardCardGetResponse> responseClass() {
        return MemberCardCardGetResponse.class;
    }

}
