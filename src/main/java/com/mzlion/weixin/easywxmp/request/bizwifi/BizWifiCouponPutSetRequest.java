package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 设置门店卡券投放信息
 * <p>
 * 调用设置门店卡劵投放信息接口后，用户可在连网流程中关注商家公众号之后，领取配置的卡券。需设置卡劵ID，投放的有效时间等。
 * 注：需要先创建卡劵后，才能获取卡劵ID。创建并获取的方法有：
 * 1、在公众平台创建。登录公众平台，在卡券功能插件中创建卡券，再在优惠券列表中选择卡券点击详情，在详情页中可获取到卡券ID。
 * 2、调用微信卡劵“创建卡劵”接口，设置卡劵信息，获取卡券ID。
 * </p>
 * Created by mzlion on 2017/4/18.
 */
public class BizWifiCouponPutSetRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = 1802009083110548106L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 卡券描述，不能超过18个字符
     */
    @ApiField("card_describe")
    private String cardDescribe;

    /**
     * 卡券投放开始时间（单位是秒）
     */
    @ApiField("start_time")
    private Long startTime;

    /**
     * 卡券投放结束时间（单位是秒）
     * 注：不能超过卡券的有效期时间
     */
    @ApiField("end_time")
    private Long endTime;

    public BizWifiCouponPutSetRequest() {
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardDescribe() {
        return cardDescribe;
    }

    public void setCardDescribe(String cardDescribe) {
        this.cardDescribe = cardDescribe;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/couponput/set";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
