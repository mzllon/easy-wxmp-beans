/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.media;

import com.mzlion.core.http.ContentType;
import com.mzlion.core.io.FilenameUtils;
import com.mzlion.core.lang.Assert;
import com.mzlion.weixin.easywxmp.UploadWxRequest;
import com.mzlion.weixin.easywxmp.constants.MediaFileTypeEnum;
import com.mzlion.weixin.easywxmp.response.media.MediaUploadResponse;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

/**
 * 新增临时素材请求对象
 * <p>
 * 公众号经常有需要用到一些临时性的多媒体素材的场景，例如在使用接口特别是发送消息时，对多媒体文件、多媒体消息的获取和调用等操作，是通过media_id来进行的。
 * 素材管理接口对所有认证的订阅号和服务号开放。通过本接口，公众号可以新增临时素材（即上传临时多媒体文件）。
 * 注意点：
 * 1、临时素材media_id是可复用的。
 * 2、媒体文件在微信后台保存时间为3天，即3天后media_id失效。
 * 3、上传临时素材的格式、大小限制与公众平台官网一致。
 * 图片（image）: 2M，支持PNG\JPEG\JPG\GIF格式
 * 语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式
 * 视频（video）：10MB，支持MP4格式
 * 缩略图（thumb）：64KB，支持JPG格式
 * 4、需使用https调用本接口。
 * </p>
 *
 * @author mzlion on 2016/12/22.
 */
public class MediaUploadRequest extends UploadWxRequest<MediaUploadResponse> {

    private transient MediaFileTypeEnum mediaFileType;

    public MediaUploadRequest(File uploadFile) {
        this(uploadFile, null);
    }

    public MediaUploadRequest(File uploadFile, String contentType) {
        this.isFileValid(uploadFile);
        this.uploadFile = uploadFile;
        this.contentType = contentType;
        if (this.contentType == null) {
            String fileExt = FilenameUtils.getFileExt(uploadFile);
            Assert.hasLength(fileExt, "Cannot parse file extension.");
            this.contentType = ContentType.parseByFileExt(fileExt).toString();
        }
    }

    public MediaUploadRequest(InputStream uploadStream, long contentLength, String filename, String contentType) {
        this.uploadStream = uploadStream;
        this.contentLength = contentLength;
        this.filename = filename;
        this.contentType = contentType;
    }

    public MediaFileTypeEnum getMediaFileType() {
        return mediaFileType;
    }

    public void setMediaFileType(MediaFileTypeEnum mediaFileType) {
        this.mediaFileType = mediaFileType;
    }

    private void isFileValid(File uploadFile) {
        if (!uploadFile.exists())
            throw new IllegalArgumentException(String.format("UploadFile[=%s] does not exists.", uploadFile.toString()));
        if (uploadFile.isDirectory())
            throw new IllegalArgumentException(String.format("UploadFile[=%s] exists but is a directory.", uploadFile.toString()));
        if (!uploadFile.canRead())
            throw new IllegalArgumentException(String.format("UploadFile[=%s] exists but cannot read.", uploadFile.toString()));
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/media/upload";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MediaUploadResponse> responseClass() {
        return MediaUploadResponse.class;
    }

    /**
     * 返回URL中携带的参数
     *
     * @return 参数
     */
    @Override
    public Map<String, String> getQueryString() {
        return Collections.singletonMap("type", this.mediaFileType.getEnumValue());
    }
}
