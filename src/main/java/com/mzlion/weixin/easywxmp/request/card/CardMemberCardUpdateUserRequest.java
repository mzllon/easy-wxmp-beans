/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardMemberCardUpdateUserResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 更新会员信息
 * <p>当会员持卡消费后，支持开发者调用该接口更新会员信息。
 * 会员卡交易后的每次信息变更需通过该接口通知微信，便于后续消息通知及其他扩展功能。</p>
 *
 * @author mzlion on 2017/05/25.
 */
public class CardMemberCardUpdateUserRequest extends WxRequest<CardMemberCardUpdateUserResponse> {

    private static final long serialVersionUID = 4503485223212374686L;

    /**
     * 查询会员卡的cardid
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 所查询用户领取到的code值
     */
    private String code;

    /**
     * 支持商家激活时针对单个会员卡分配自定义的会员卡背景。
     */
    @ApiField("background_pic_url")
    private String backgroudPicUrl;

    /**
     * 需要设置的积分全量值，传入的数值会直接显示
     */
    private Long bonus;

    /**
     * 本次积分变动值，传负数代表减少
     */
    @ApiField("add_bonus")
    private Long addBonus;

    /**
     * 商家自定义积分消耗记录，不超过14个汉字
     */
    @ApiField("record_bonus")
    private String recordBonus;

    /**
     * 需要设置的余额全量值，传入的数值会直接显示在卡面
     */
    private Long balance;

    /**
     * 本次余额变动值，传负数代表减少
     */
    @ApiField("add_balance")
    private Long addBalance;

    /**
     * 商家自定义金额消耗记录，不超过14个汉字。
     */
    @ApiField("record_balance")
    private String recordBalance;

    /**
     * 创建时字段custom_field1定义类型的最新数值，限制为4个汉字，12字节。
     */
    @ApiField("custom_field_value1")
    private String customFieldValue1;

    /**
     * 创建时字段custom_field2定义类型的最新数值，限制为4个汉字，12字节。
     */
    @ApiField("custom_field_value2")
    private String customFieldValue2;

    /**
     * 创建时字段custom_field3定义类型的最新数值，限制为4个汉字，12字节。
     */
    @ApiField("custom_field_value3")
    private String customFieldValue3;

    /**
     * 一些消息通知配置
     */
    @ApiField("notify_optional")
    private NotifyOptional notifyOptional;


    public CardMemberCardUpdateUserRequest() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBackgroudPicUrl() {
        return backgroudPicUrl;
    }

    public void setBackgroudPicUrl(String backgroudPicUrl) {
        this.backgroudPicUrl = backgroudPicUrl;
    }

    public Long getBonus() {
        return bonus;
    }

    public void setBonus(Long bonus) {
        this.bonus = bonus;
    }

    public Long getAddBonus() {
        return addBonus;
    }

    public void setAddBonus(Long addBonus) {
        this.addBonus = addBonus;
    }

    public String getRecordBonus() {
        return recordBonus;
    }

    public void setRecordBonus(String recordBonus) {
        this.recordBonus = recordBonus;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getAddBalance() {
        return addBalance;
    }

    public void setAddBalance(Long addBalance) {
        this.addBalance = addBalance;
    }

    public String getRecordBalance() {
        return recordBalance;
    }

    public void setRecordBalance(String recordBalance) {
        this.recordBalance = recordBalance;
    }

    public String getCustomFieldValue1() {
        return customFieldValue1;
    }

    public void setCustomFieldValue1(String customFieldValue1) {
        this.customFieldValue1 = customFieldValue1;
    }

    public String getCustomFieldValue2() {
        return customFieldValue2;
    }

    public void setCustomFieldValue2(String customFieldValue2) {
        this.customFieldValue2 = customFieldValue2;
    }

    public String getCustomFieldValue3() {
        return customFieldValue3;
    }

    public void setCustomFieldValue3(String customFieldValue3) {
        this.customFieldValue3 = customFieldValue3;
    }

    public NotifyOptional getNotifyOptional() {
        return notifyOptional;
    }

    public void setNotifyOptional(NotifyOptional notifyOptional) {
        this.notifyOptional = notifyOptional;
    }

    /**
     * 查询会员卡的cardid
     */
    public String getCardId() {
        return cardId;
    }


    /**
     * 查询会员卡的cardid
     *
     * @param cardId 卡券ID
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 所查询用户领取到的code值
     */
    public String getCode() {
        return code;
    }

    /**
     * 所查询用户领取到的code值
     *
     * @param code 会员卡的code
     */
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/membercard/updateuser";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardMemberCardUpdateUserResponse> responseClass() {
        return CardMemberCardUpdateUserResponse.class;
    }

    /**
     * 控制原生消息结构体，包含各字段的消息控制字段
     */
    public static class NotifyOptional implements Serializable {

        /**
         * 积分变动时是否触发系统模板消息，默认为true
         */
        @ApiField("is_notify_bonus")
        private Boolean isNotifyBonus;

        /**
         * 余额变动时是否触发系统模板消息，默认为true
         */
        @ApiField("is_notify_balance")
        private Boolean isNotifyBalance;

        /**
         * 自定义group1变动时是否触发系统模板消息，默认为false。（2、3同理）
         */
        @ApiField("is_notify_custom_field1")
        private Boolean isNotifyCustomField1;
        @ApiField("is_notify_custom_field2")
        private Boolean isNotifyCustomField2;
        @ApiField("is_notify_custom_field3")
        private Boolean isNotifyCustomField3;

        public Boolean getIsNotifyBonus() {
            return isNotifyBonus;
        }

        public void setIsNotifyBonus(Boolean isNotifyBonus) {
            this.isNotifyBonus = isNotifyBonus;
        }

        public Boolean getIsNotifyBalance() {
            return isNotifyBalance;
        }

        public void setIsNotifyBalance(Boolean isNotifyBalance) {
            this.isNotifyBalance = isNotifyBalance;
        }

        public Boolean getIsNotifyCustomField1() {
            return isNotifyCustomField1;
        }

        public void setIsNotifyCustomField1(Boolean isNotifyCustomField1) {
            this.isNotifyCustomField1 = isNotifyCustomField1;
        }

        public Boolean getIsNotifyCustomField2() {
            return isNotifyCustomField2;
        }

        public void setIsNotifyCustomField2(Boolean isNotifyCustomField2) {
            this.isNotifyCustomField2 = isNotifyCustomField2;
        }

        public Boolean getIsNotifyCustomField3() {
            return isNotifyCustomField3;
        }

        public void setIsNotifyCustomField3(Boolean isNotifyCustomField3) {
            this.isNotifyCustomField3 = isNotifyCustomField3;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("NotifyOptional{");
            sb.append("isNotifyBonus=").append(isNotifyBonus);
            sb.append(", isNotifyBalance=").append(isNotifyBalance);
            sb.append(", isNotifyCustomField1=").append(isNotifyCustomField1);
            sb.append(", isNotifyCustomField2=").append(isNotifyCustomField2);
            sb.append(", isNotifyCustomField3=").append(isNotifyCustomField3);
            sb.append('}');
            return sb.toString();
        }
    }

}
