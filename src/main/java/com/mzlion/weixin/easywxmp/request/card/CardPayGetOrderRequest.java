/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardPayGetOrderResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询订单详情接口
 * <p>本接口用于查询充值订单的状态</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayGetOrderRequest extends WxRequest<CardPayGetOrderResponse> {

    private static final long serialVersionUID = -8294333230320391085L;

    /**
     * 一步中获得的订单号，作为一次交易的唯一凭证
     */
    @ApiField("order_id")
    private String orderId;


    public CardPayGetOrderRequest() {
    }

    public CardPayGetOrderRequest(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/pay/getorder";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayGetOrderResponse> responseClass() {
        return CardPayGetOrderResponse.class;
    }
}
