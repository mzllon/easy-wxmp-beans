/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 添加设备到分组
 * <p>每个分组能够持有的设备上限为10000，并且每次添加操作的添加上限为1000。只有在摇周边申请的设备才能添加到分组。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceGroupAddDeviceRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -8444634932727287941L;

    /**
     * 申请的批次ID
     */
    @ApiField("device_identifier")
    private ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier;

    /**
     * 分组唯一标识，全局唯一
     */
    @ApiField("group_id")
    private String groupId;


    public ShakeAroundDeviceGroupAddDeviceRequest() {
    }

    public ShakeAroundDeviceIdentifierModel getShakeAroundDeviceIdentifier() {
        return shakeAroundDeviceIdentifier;
    }

    public void setShakeAroundDeviceIdentifier(ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier) {
        this.shakeAroundDeviceIdentifier = shakeAroundDeviceIdentifier;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/adddevice";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
