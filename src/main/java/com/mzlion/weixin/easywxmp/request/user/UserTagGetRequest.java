/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.user;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.user.UserTagGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取标签下粉丝列表
 *
 * @author mzlion on 2017/4/17.
 */
public class UserTagGetRequest extends WxRequest<UserTagGetResponse> {

    /**
     * 标签ID
     */
    @ApiField("tagid")
    private Integer tagId;

    /**
     * 第一个拉取的OPENID，不填默认从头开始拉取
     */
    @ApiField("next_openid")
    private String nextOpenId;

    public UserTagGetRequest() {
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public String getNextOpenId() {
        return nextOpenId;
    }

    public void setNextOpenId(String nextOpenId) {
        this.nextOpenId = nextOpenId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/user/tag/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<UserTagGetResponse> responseClass() {
        return UserTagGetResponse.class;
    }
}
