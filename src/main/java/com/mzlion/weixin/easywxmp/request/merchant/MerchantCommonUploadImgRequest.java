/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.UploadWxRequest;
import com.mzlion.weixin.easywxmp.response.merchant.MerchantCommonUploadImgResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信小店上传图片
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantCommonUploadImgRequest extends UploadWxRequest<MerchantCommonUploadImgResponse> {

    /**
     * 图片文件名
     */
    @ApiField(expose = true)
    private String filename;


    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * 返回URL中携带的参数
     *
     * @return 参数
     */
    @Override
    public Map<String, String> getQueryString() {
        Map<String, String> params = new HashMap<>();
        params.put("filename", filename);
        return params;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/common/upload_img";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MerchantCommonUploadImgResponse> responseClass() {
        return MerchantCommonUploadImgResponse.class;
    }
}
