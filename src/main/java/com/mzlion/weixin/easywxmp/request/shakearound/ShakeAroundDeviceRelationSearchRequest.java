/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundDeviceRelationSearchResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询设备与页面的关联关系
 * <p>
 * 查询设备与页面的关联关系。提供两种查询方式，可指定页面ID分页查询该页面所关联的所有的设备信息；
 * 也可根据设备ID或完整的UUID、Major、Minor查询该设备所关联的所有页面信息。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceRelationSearchRequest extends WxRequest<ShakeAroundDeviceRelationSearchResponse> {

    private static final long serialVersionUID = -4536584943585556621L;

    /**
     * 查询方式
     * 1： 查询设备的关联关系；
     * 2：查询页面的关联关系
     */
    private String type;

    /**
     * 申请的批次ID
     */
    @ApiField("device_identifier")
    private ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier;

    /**
     * 指定的页面id；当type为2时，此项为必填
     */
    @ApiField("page_id")
    private String pageId;

    /**
     * 关联关系列表的起始索引值；当type为2时，此项为必填
     */
    private int begin;

    /**
     * 待查询的关联关系数量，不能超过50个；当type为2时，此项为必填
     */
    private int count;


    public ShakeAroundDeviceRelationSearchRequest() {
    }

    public ShakeAroundDeviceIdentifierModel getShakeAroundDeviceIdentifier() {
        return shakeAroundDeviceIdentifier;
    }

    public void setShakeAroundDeviceIdentifier(ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier) {
        this.shakeAroundDeviceIdentifier = shakeAroundDeviceIdentifier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/relation/search";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundDeviceRelationSearchResponse> responseClass() {
        return ShakeAroundDeviceRelationSearchResponse.class;
    }
}
