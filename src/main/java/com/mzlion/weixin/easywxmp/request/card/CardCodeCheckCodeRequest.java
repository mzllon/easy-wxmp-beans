/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ApiProtocolEnum;
import com.mzlion.weixin.easywxmp.response.card.CardCodeCheckCodeResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 核查code接口
 * <p>为了避免出现导入差错，强烈建议开发者在查询完code数目的时候核查code接口校验code导入微信后台的情况</p>
 *
 * @author mzlion on 2017/4/17.
 */
public class CardCodeCheckCodeRequest extends WxRequest<CardCodeCheckCodeResponse> {

    /**
     * 需要进行导入code的卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 需导入微信卡券后台的自定义code，上限为100个
     */
    @ApiField("code")
    private List<String> codeList;

    public CardCodeCheckCodeRequest() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public List<String> getCodeList() {
        return codeList;
    }

    public void setCodeList(List<String> codeList) {
        this.codeList = codeList;
    }

    /**
     * 接口协议，使用{@linkplain ApiProtocolEnum#HTTP}协议
     *
     * @return {@linkplain ApiProtocolEnum}
     */
    @Override
    public ApiProtocolEnum protocol() {
        return ApiProtocolEnum.HTTP;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/code/checkcode";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardCodeCheckCodeResponse> responseClass() {
        return CardCodeCheckCodeResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
