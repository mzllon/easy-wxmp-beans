/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.QrActionNameEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 基础的二维码参数
 *
 * @author mzlion on 2017/4/17.
 */
public abstract class BaseQRCodeCreateRequest<Qr extends BaseQRCodeCreateRequest.QRActionInfo, Resp extends WxResponse> extends WxRequest<Resp> {


    /**
     * 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）
     * 此字段如果不填，则默认有效期为30秒。
     */
    @ApiField("expire_seconds")
    private Integer expireSeconds;

    @ApiField("action_name")
    protected QrActionNameEnum qrActionName;

    /**
     * 当前二维码携带的数据内容
     */
    @ApiField("action_info")
    private Qr qrActionInfo;

    public Integer getExpireSeconds() {
        return expireSeconds;
    }

    public void setExpireSeconds(Integer expireSeconds) {
        this.expireSeconds = expireSeconds;
    }

    public QrActionNameEnum getQrActionName() {
        return qrActionName;
    }

    public void setQrActionName(QrActionNameEnum qrActionName) {
        this.qrActionName = qrActionName;
    }

    public Qr getQrActionInfo() {
        return qrActionInfo;
    }

    public void setQrActionInfo(Qr qrActionInfo) {
        this.qrActionInfo = qrActionInfo;
    }

    /**
     * 二维码数据内容
     */
    public static abstract class QRActionInfo implements Serializable {

    }

}
