/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 接口激活会员卡
 * <p>
 * 接口激活通常需要开发者开发用户填写资料的网页。通常有两种激活流程：
 * 1. 用户必须在填写资料后才能领卡，领卡后开发者调用激活接口为用户激活会员卡；
 * 2. 是用户可以先领取会员卡，点击激活会员卡跳转至开发者设置的资料填写页面，填写完成后开发者调用激活接口为用户激活会员卡。
 * </p>
 *
 * @author mzlion on 2016/12/25.
 */
public class CardMemberCardActivateRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = 3142426998086293923L;

    /**
     * 会员卡编号，由开发者填入，作为序列号显示在用户的卡包里。
     * 可与Code码保持等值。
     */
    @ApiField("membership_number")
    private String membershipNumber;

    /**
     * 领取会员卡用户获得的code
     */
    private String code;

    /**
     * 卡券ID,自定义code卡券必填
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 商家自定义会员卡背景图，须先调用上传图片接口将背景图上传至CDN，否则报错，卡面设计请遵循微信会员卡自定义背景设计规范
     */
    @ApiField("background_pic_url")
    private String backgroundPicUrl;

    /**
     * 激活后的有效起始时间。若不填写默认以创建时的 data_info 为准。Unix时间戳格式。
     */
    @ApiField("activate_begin_time")
    private long activateBeginTime;

    /**
     * 激活后的有效截至时间。若不填写默认以创建时的 data_info 为准。Unix时间戳格式。
     */
    @ApiField("activate_end_time")
    private long activateEndTime;

    /**
     * 初始积分，不填为0。
     */
    @ApiField("init_bonus")
    private int initBonus;

    /**
     * 积分同步说明。
     */
    @ApiField("init_bonus_record")
    private String initBonusRecord;

    /**
     * 初始余额，不填为0。
     */
    @ApiField("init_balance")
    private int initBalance;

    /**
     * 创建时字段custom_field1定义类型的初始值，限制为4个汉字，12字节。
     */
    @ApiField("init_custom_field_value1")
    private String initCustomFieldValue1;

    /**
     * 创建时字段custom_field2定义类型的初始值，限制为4个汉字，12字节。
     */
    @ApiField("init_custom_field_value2")
    private String initCustomFieldValue2;

    /**
     * 创建时字段custom_field3定义类型的初始值，限制为4个汉字，12字节。
     */
    @ApiField("init_custom_field_value3")
    private String initCustomFieldValue3;

    public CardMemberCardActivateRequest() {
    }

    public String getMembershipNumber() {
        return membershipNumber;
    }

    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = membershipNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getBackgroundPicUrl() {
        return backgroundPicUrl;
    }

    public void setBackgroundPicUrl(String backgroundPicUrl) {
        this.backgroundPicUrl = backgroundPicUrl;
    }

    public long getActivateBeginTime() {
        return activateBeginTime;
    }

    public void setActivateBeginTime(long activateBeginTime) {
        this.activateBeginTime = activateBeginTime;
    }

    public long getActivateEndTime() {
        return activateEndTime;
    }

    public void setActivateEndTime(long activateEndTime) {
        this.activateEndTime = activateEndTime;
    }

    public int getInitBonus() {
        return initBonus;
    }

    public void setInitBonus(int initBonus) {
        this.initBonus = initBonus;
    }

    public String getInitBonusRecord() {
        return initBonusRecord;
    }

    public void setInitBonusRecord(String initBonusRecord) {
        this.initBonusRecord = initBonusRecord;
    }

    public int getInitBalance() {
        return initBalance;
    }

    public void setInitBalance(int initBalance) {
        this.initBalance = initBalance;
    }

    public String getInitCustomFieldValue1() {
        return initCustomFieldValue1;
    }

    public void setInitCustomFieldValue1(String initCustomFieldValue1) {
        this.initCustomFieldValue1 = initCustomFieldValue1;
    }

    public String getInitCustomFieldValue2() {
        return initCustomFieldValue2;
    }

    public void setInitCustomFieldValue2(String initCustomFieldValue2) {
        this.initCustomFieldValue2 = initCustomFieldValue2;
    }

    public String getInitCustomFieldValue3() {
        return initCustomFieldValue3;
    }

    public void setInitCustomFieldValue3(String initCustomFieldValue3) {
        this.initCustomFieldValue3 = initCustomFieldValue3;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/membercard/activate";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
