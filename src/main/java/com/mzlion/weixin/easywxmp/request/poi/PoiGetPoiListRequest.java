/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.poi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.poi.PoiGetPoiListResponse;

/**
 * 查询门店列表请求参数
 * <p>
 * 商户可以通过该接口，批量查询自己名下的门店list，并获取已审核通过的poiid、商户自身sid 用于对应、商户名、分店名、地址字段。
 * </p>
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiGetPoiListRequest extends WxRequest<PoiGetPoiListResponse> {

    /**
     * 开始位置，0 即为从第一条开始查询
     */
    private int begin = 0;

    /**
     * 返回数据条数，最大允许50，默认为20
     */
    private int limit = 20;

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        if (limit > 50) {
            this.limit = 50;
            return;
        }
        this.limit = limit;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/poi/getpoilist";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<PoiGetPoiListResponse> responseClass() {
        return PoiGetPoiListResponse.class;
    }
}
