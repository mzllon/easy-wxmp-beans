/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundDeviceApplyStatusResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询设备ID申请审核状态
 * <p>
 * 查询设备ID申请的审核状态。若单次申请的设备ID数量小于等于500个，系统会进行快速审核；
 * 若单次申请的设备ID数量大于500个，则在三个工作日内完成审核。</p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceApplyStatusRequest extends WxRequest<ShakeAroundDeviceApplyStatusResponse> {

    private static final long serialVersionUID = 6295944412152076169L;

    /**
     * 申请的批次ID
     */
    @ApiField("apply_id")
    private String applyId;


    public ShakeAroundDeviceApplyStatusRequest() {
    }

    public ShakeAroundDeviceApplyStatusRequest(String applyId) {
        this.applyId = applyId;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/applystatus";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundDeviceApplyStatusResponse> responseClass() {
        return ShakeAroundDeviceApplyStatusResponse.class;
    }
}
