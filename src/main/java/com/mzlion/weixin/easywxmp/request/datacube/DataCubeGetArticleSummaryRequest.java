/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.datacube;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.datacube.DataCubeGetArticleSummaryResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取图文群发每日数据
 *
 * @author mzlion on 2017/4/17.
 */
public class DataCubeGetArticleSummaryRequest extends WxRequest<DataCubeGetArticleSummaryResponse> {

    /**
     * 获取数据的起始日期，begin_date和end_date的差值需小于“最大时间跨度”（比如最大时间跨度为1时，begin_date和end_date的差值只能为0，才能小于1），否则会报错
     */
    @ApiField("begin_date")
    private String beginDate;

    /**
     * 获取数据的结束日期，end_date允许设置的最大值为昨日
     */
    @ApiField("end_date")
    private String endDate;

    public DataCubeGetArticleSummaryRequest() {
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/datacube/getarticlesummary";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<DataCubeGetArticleSummaryResponse> responseClass() {
        return DataCubeGetArticleSummaryResponse.class;
    }
}
