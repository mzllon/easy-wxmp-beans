/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.card.CardSubMerchantModel;
import com.mzlion.weixin.easywxmp.response.card.CardSubMerchantSubmitResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 创建子商户接口
 * <p>
 * 支持母商户调用该接口传入子商户的相关资料，并获取子商户ID，用于子商户的卡券功能管理。
 * 子商户的资质包括：商户名称、商户logo（图片）、卡券类目、授权函（扫描件或彩照）、授权函有效期截止时间
 * </p>
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantSubmitRequest extends WxRequest<CardSubMerchantSubmitResponse> {

    /**
     * 子商户信息
     */
    @ApiField("info")
    private CardSubMerchantModel cardSubMerchant;

    public CardSubMerchantSubmitRequest() {
    }

    public CardSubMerchantSubmitRequest(CardSubMerchantModel cardSubMerchant) {
        this.cardSubMerchant = cardSubMerchant;
    }

    public CardSubMerchantModel getCardSubMerchant() {
        return cardSubMerchant;
    }

    public void setCardSubMerchant(CardSubMerchantModel cardSubMerchant) {
        this.cardSubMerchant = cardSubMerchant;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/submerchant/submit";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardSubMerchantSubmitResponse> responseClass() {
        return CardSubMerchantSubmitResponse.class;
    }
}
