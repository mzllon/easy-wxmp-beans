/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.menu;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.ApiMethodEnum;

/**
 * 自定义菜单删除接口请求对象
 * <p>
 * 使用接口创建自定义菜单后，开发者还可使用接口删除当前使用的自定义菜单。
 * 另请注意，在个性化菜单时，调用此接口会删除默认菜单及全部个性化菜单。
 * </p>
 *
 * @author mzlion on 2017/1/5.
 */
public class MenuDeleteRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -7204989403625092879L;

    /**
     * 接口请求方式，使用{@linkplain ApiMethodEnum#GET}请求
     *
     * @return {@link ApiMethodEnum}
     */
    @Override
    public ApiMethodEnum method() {
        return ApiMethodEnum.GET;
    }

    @Override
    public String serviceUrl() {
        return "/cgi-bin/menu/delete";
    }

    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

}
