/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 确认兑换库存接口
 * <p>本接口用于确认兑换库存，确认后券点兑换为库存，过程不可逆</p>
 * <p>上一步获得的order_id须在60s内使用，否则确认兑换库存接口将会失效</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayConfirmRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -8294333230320391085L;

    /**
     * 要来配置库存的card_id
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 仅可以使用上面得到的订单号，保证批价有效性
     */
    @ApiField("order_id")
    private String orderId;

    /**
     * 本次需要兑换的库存数目
     */
    private int quantity;

    public CardPayConfirmRequest() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/pay/confirm";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
