/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 修改库存接口
 * <p>* 调用修改库存接口增减某张卡券的库存。</p>
 *
 * @author mzlion on 2017/1/6.
 */
public class CardModifyStockRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -1940924279056189780L;
    /**
     * 卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 增加多少库存，支持不填或填0
     */
    @ApiField("increase_stock_value")
    private Integer increaseStockValue;

    /**
     * 减少多少库存，可以不填或填0
     */
    @ApiField("reduce_stock_value")
    private Integer reduceStockValue;


    /**
     * 卡券ID
     *
     * @return 卡券ID
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * 卡券ID
     *
     * @param cardId 卡券ID
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Integer getIncreaseStockValue() {
        return increaseStockValue;
    }

    public void setIncreaseStockValue(Integer increaseStockValue) {
        this.increaseStockValue = increaseStockValue;
    }

    public Integer getReduceStockValue() {
        return reduceStockValue;
    }

    public void setReduceStockValue(Integer reduceStockValue) {
        this.reduceStockValue = reduceStockValue;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/modifystock";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
