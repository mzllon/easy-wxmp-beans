/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardSubMerchantGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 子商户信息查询接口请求对象
 * <p>通过指定的子商户appid，拉取该子商户的基础信息。 注意，用母商户去调用接口，但接口内传入的是子商户的appid。</p>
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantGetRequest extends WxRequest<CardSubMerchantGetResponse> {

    private static final long serialVersionUID = 2966509620031274202L;

    /**
     * 商户号
     */
    @ApiField("merchant_id")
    private String merchantId;

    public CardSubMerchantGetRequest() {
    }

    public CardSubMerchantGetRequest(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/submerchant/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardSubMerchantGetResponse> responseClass() {
        return CardSubMerchantGetResponse.class;
    }
}
