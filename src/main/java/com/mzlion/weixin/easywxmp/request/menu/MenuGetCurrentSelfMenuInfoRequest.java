/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.menu;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ApiMethodEnum;
import com.mzlion.weixin.easywxmp.response.menu.MenuGetCurrentSelfMenuInfoResponse;

/**
 * 获取自定义菜单配置接口请求参数
 * <p>
 * 本接口将会提供公众号当前使用的自定义菜单的配置，如果公众号是通过API调用设置的菜单，则返回菜单的开发配置，
 * 而如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回运营者设置的菜单配置。
 * </p>
 *
 * @author mzlion on 2017-1-5
 */
public class MenuGetCurrentSelfMenuInfoRequest extends WxRequest<MenuGetCurrentSelfMenuInfoResponse> {

    private static final long serialVersionUID = -241791383021328248L;

    /**
     * 接口请求方式，GET请求
     *
     * @return {@link ApiMethodEnum}
     */
    @Override
    public ApiMethodEnum method() {
        return ApiMethodEnum.GET;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/get_current_selfmenu_info";
    }

    @Override
    public Class<MenuGetCurrentSelfMenuInfoResponse> responseClass() {
        return MenuGetCurrentSelfMenuInfoResponse.class;
    }

}
