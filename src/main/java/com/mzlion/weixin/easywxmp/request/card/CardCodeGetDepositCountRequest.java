/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ApiProtocolEnum;
import com.mzlion.weixin.easywxmp.response.card.CardCodeGetDepositCountResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询导入code数目接口
 *
 * @author mzlion on 2017/4/17.
 */
public class CardCodeGetDepositCountRequest extends WxRequest<CardCodeGetDepositCountResponse> {

    /**
     * 需要进行导入code的卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    public CardCodeGetDepositCountRequest() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 接口协议，使用{@linkplain ApiProtocolEnum#HTTP}协议
     *
     * @return {@linkplain ApiProtocolEnum}
     */
    @Override
    public ApiProtocolEnum protocol() {
        return ApiProtocolEnum.HTTP;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/code/getdepositcount";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardCodeGetDepositCountResponse> responseClass() {
        return CardCodeGetDepositCountResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
