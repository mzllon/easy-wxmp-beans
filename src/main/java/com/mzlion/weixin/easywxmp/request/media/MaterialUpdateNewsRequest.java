/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.media;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.MaterialArticleModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 修改永久图文素材
 * <p>
 * 请注意：
 * 1、也可以在公众平台官网素材管理模块中保存的图文消息（永久图文素材）
 * 2、调用该接口需https协议
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class MaterialUpdateNewsRequest extends WxRequest<WxResponse> {

    /**
     * 要修改的素材的media_id
     */
    @ApiField("media_id")
    private String mediaId;

    /**
     * 要更新的文章在图文消息中的位置（多图文消息时，此字段才有意义），第一篇为0
     */
    private int index;

    /**
     * 图文列表
     */
    @ApiField("articles")
    private List<MaterialArticleModel> materialArticleList;

    public MaterialUpdateNewsRequest() {
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<MaterialArticleModel> getMaterialArticleList() {
        return materialArticleList;
    }

    public void setMaterialArticleList(List<MaterialArticleModel> materialArticleList) {
        this.materialArticleList = materialArticleList;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/material/update_news";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
