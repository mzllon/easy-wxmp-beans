/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundDeviceSearchResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 查询设备列表
 * <p>
 * 查询已有的设备ID、UUID、Major、Minor、激活状态、备注信息、关联门店、关联页面等信息。可指定设备ID或完整的UUID、Major、Minor查询，也可批量拉取设备信息列表。
 * 查询所返回的设备列表按设备ID正序排序。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceSearchRequest extends WxRequest<ShakeAroundDeviceSearchResponse> {

    private static final long serialVersionUID = 7961182803131253614L;

    /**
     * 查询类型。
     * 1：查询设备id列表中的设备；
     * 2：分页查询所有设备信息；
     * 3：分页查询某次申请的所有设备信息
     */
    private String type;

    /**
     * 申请的批次ID
     */
    @ApiField("device_identifiers")
    private List<ShakeAroundDeviceIdentifierModel> shakeAroundDeviceIdentifierList;

    /**
     * 前一次查询列表末尾的设备ID，第一次查询last_seen 为0
     */
    @ApiField("last_seen")
    private String lastSeen;

    /**
     * 关联门店所归属的公众账号的APPID
     */
    private int count;

    /**
     * 申请的批次ID
     */
    @ApiField("apply_id")
    private String applyId;

    public ShakeAroundDeviceSearchRequest() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ShakeAroundDeviceIdentifierModel> getShakeAroundDeviceIdentifierList() {
        return shakeAroundDeviceIdentifierList;
    }

    public void setShakeAroundDeviceIdentifierList(List<ShakeAroundDeviceIdentifierModel> shakeAroundDeviceIdentifierList) {
        this.shakeAroundDeviceIdentifierList = shakeAroundDeviceIdentifierList;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/search";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundDeviceSearchResponse> responseClass() {
        return ShakeAroundDeviceSearchResponse.class;
    }
}
