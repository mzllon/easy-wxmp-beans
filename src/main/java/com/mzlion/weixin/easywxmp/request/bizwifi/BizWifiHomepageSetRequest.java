/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 设置商家主页
 * <p>
 * 设置商户主页后，点击微信聊首页欢迎语，即可进入设置的商户主页。可以设置默认模板和自定义url模板。
 * 注：第三方平台和认证公众号才能调用该接口设置商家主页。
 * </p>
 * @author mzlion on 2017/4/18.
 */
public class BizWifiHomepageSetRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -1353333357419044169L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 模板ID，
     * 0-默认模板，
     * 1-自定义url
     */
    @ApiField("template_id")
    private int templateId;

    /**
     * 模板结构
     */
    @ApiField("struct")
    private TemplateStruct templateStruct;

    public BizWifiHomepageSetRequest() {
    }

    public BizWifiHomepageSetRequest(String shopId, int templateId, TemplateStruct templateStruct) {
        this.shopId = shopId;
        this.templateId = templateId;
        this.templateStruct = templateStruct;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public TemplateStruct getTemplateStruct() {
        return templateStruct;
    }

    public void setTemplateStruct(TemplateStruct templateStruct) {
        this.templateStruct = templateStruct;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/homepage/set";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    /**
     * 模板结构
     */
    public static class TemplateStruct implements Serializable {

        private static final long serialVersionUID = 366512573113609342L;

        /**
         * 自定义链接，当template_id为1时必填
         */
        private String url;

        public TemplateStruct() {
        }

        public TemplateStruct(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
