/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 发送视频消息
 *
 * @author mzlion on 2017/4/16.
 */
public class VideoMessageCustomSendRequest extends MessageCustomSendRequest {

    /**
     * 视频消息
     */
    @ApiField("video")
    private VideoMessage videoMessage;

    public VideoMessageCustomSendRequest() {
        super.msgType = MsgTypeEnum.VIDEO;
    }

    public VideoMessage getVideoMessage() {
        return videoMessage;
    }

    public void setVideoMessage(VideoMessage videoMessage) {
        this.videoMessage = videoMessage;
    }

    /**
     * 视频消息
     *
     * @author mzlion
     */
    public static class VideoMessage implements Serializable {

        private static final long serialVersionUID = 1794362180876557106L;

        /**
         * 发送的图片/语音/视频/图文消息（点击跳转到图文消息页）的媒体ID
         */
        @ApiField("media_id")
        private String mediaId;

        /**
         * 缩略图的媒体ID
         */
        @ApiField("thumb_media_id")
        private String thumbMediaId;

        /**
         * 图文消息/视频消息/音乐消息的标题
         */
        private String title;

        /**
         * 图文消息/视频消息/音乐消息的描述
         */
        private String description;

        public VideoMessage() {
        }

        public VideoMessage(String mediaId, String thumbMediaId, String title, String description) {
            this.mediaId = mediaId;
            this.thumbMediaId = thumbMediaId;
            this.title = title;
            this.description = description;
        }

        public VideoMessage(String mediaId, String thumbMediaId) {
            this.mediaId = mediaId;
            this.thumbMediaId = thumbMediaId;
        }

        public String getMediaId() {
            return mediaId;
        }

        public void setMediaId(String mediaId) {
            this.mediaId = mediaId;
        }

        public String getThumbMediaId() {
            return thumbMediaId;
        }

        public void setThumbMediaId(String thumbMediaId) {
            this.thumbMediaId = thumbMediaId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
