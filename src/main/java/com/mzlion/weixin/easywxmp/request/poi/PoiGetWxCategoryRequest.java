/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.poi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ApiMethodEnum;
import com.mzlion.weixin.easywxmp.response.poi.PoiGetWxCategoryResponse;

/**
 * 门店类目请求参数
 * <p>类目名称接口是为商户提供自己门店类型信息的接口。门店类目定位的越规范，能够精准的吸引更多用户，提高曝光率。</p>
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiGetWxCategoryRequest extends WxRequest<PoiGetWxCategoryResponse> {
    /**
     * 接口请求方式，使用{@linkplain ApiMethodEnum#GET}请求
     *
     * @return {@link ApiMethodEnum}
     */
    @Override
    public ApiMethodEnum method() {
        return ApiMethodEnum.GET;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/poi/getwxcategory";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<PoiGetWxCategoryResponse> responseClass() {
        return PoiGetWxCategoryResponse.class;
    }

}
