/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 发送图文消息（点击跳转到图文消息页面） 图文消息条数限制在8条以内，注意，如果图文数超过8，则将会无响应
 *
 * @author mzlion on 2017/4/16.
 */
public class MpNewsMessageCustomSendRequest extends MessageCustomSendRequest {

    /**
     * 图文消息页面
     */
    @ApiField("mpnews")
    private MpNewsMessage mpNewsMessage;

    public MpNewsMessageCustomSendRequest() {
        super.msgType = MsgTypeEnum.MP_NEWS;
    }

    public MpNewsMessage getMpNewsMessage() {
        return mpNewsMessage;
    }

    public void setMpNewsMessage(MpNewsMessage mpNewsMessage) {
        this.mpNewsMessage = mpNewsMessage;
    }

    /**
     * 图文消息页面
     *
     * @author mzlion
     */
    public static class MpNewsMessage implements Serializable {


        /**
         * 发送的图片/语音/视频/图文消息（点击跳转到图文消息页）的媒体ID
         */
        @ApiField("media_id")
        private String mediaId;

        public MpNewsMessage(String mediaId) {
            this.mediaId = mediaId;
        }

        public String getMediaId() {
            return mediaId;
        }

        public void setMediaId(String mediaId) {
            this.mediaId = mediaId;
        }
    }


}
