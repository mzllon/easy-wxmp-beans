/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.card.AbsCardModel;
import com.mzlion.weixin.easywxmp.response.card.AbsCardGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查看卡券详情
 * <p>
 * 开发者可以调用该接口查询某个card_id的创建信息、审核状态以及库存数量。
 * </p>
 *
 * @author mzlion on 2016/12/30.
 */
public abstract class AbsCardGetRequest<Card extends AbsCardModel, Resp extends AbsCardGetResponse<Card>> extends WxRequest<Resp> {

    /**
     * 卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 卡券ID
     *
     * @return 卡券ID
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * 卡券ID
     *
     * @param cardId 卡券ID
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/get";
    }

}
