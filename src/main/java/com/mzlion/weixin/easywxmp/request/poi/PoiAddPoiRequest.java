/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.poi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.WxPoiModel;
import com.mzlion.weixin.easywxmp.model.WxPoiWrapperModel;
import com.mzlion.weixin.easywxmp.response.poi.PoiAddPoiResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 创建门店请求参数
 * <p>
 * 创建门店接口是为商户提供创建自己门店数据的接口，门店数据字段越完整，商户页面展示越丰富，越能够吸引更多用户，并提高曝光度。
 * 创建门店接口调用成功后会返回errcode 0、errmsg ok，会实时返回唯一的poiid。
 * </p>
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiAddPoiRequest extends WxRequest<PoiAddPoiResponse> {

    /**
     * 门店信息
     */
    @ApiField("business")
    private WxPoiWrapperModel<WxPoiModel> wxPoiWrapper;

    public PoiAddPoiRequest() {
    }

    public PoiAddPoiRequest(WxPoiWrapperModel<WxPoiModel> wxPoiWrapper) {
        this.wxPoiWrapper = wxPoiWrapper;
    }

    public PoiAddPoiRequest(WxPoiModel wxPoi) {
        this.wxPoiWrapper = new WxPoiWrapperModel<>(wxPoi);
    }

    public WxPoiWrapperModel<WxPoiModel> getWxPoiWrapper() {
        return wxPoiWrapper;
    }

    public void setWxPoiWrapper(WxPoiWrapperModel<WxPoiModel> wxPoiWrapper) {
        this.wxPoiWrapper = wxPoiWrapper;
    }

    public WxPoiModel getWxPoi() {
        return this.wxPoiWrapper == null ? null : this.wxPoiWrapper.getPoiInfo();
    }

    public void setWxPoi(WxPoiModel wxPoi) {
        if (this.wxPoiWrapper == null) {
            this.wxPoiWrapper = new WxPoiWrapperModel<>();
        }
        this.wxPoiWrapper.setPoiInfo(wxPoi);
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/poi/addpoi";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<PoiAddPoiResponse> responseClass() {
        return PoiAddPoiResponse.class;
    }

}
