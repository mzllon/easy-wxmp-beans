/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.card.CardCodeDecryptResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * Code解码接口
 * <p>
 * code解码接口支持两种场景：
 * 1.商家获取choos_card_info后，将card_id和encrypt_code字段通过解码接口，获取真实code。
 * 2.卡券内跳转外链的签名中会对code进行加密处理，通过调用解码接口获取真实code。
 * </p>
 * Created by mzlion on 2017/4/17.
 */
public class CardCodeDecryptRequest extends WxRequest<CardCodeDecryptResponse> {

    /**
     * 经过加密的Code码
     */
    @ApiField("encrypt_code")
    private String encryptCode;

    public CardCodeDecryptRequest() {
    }

    public CardCodeDecryptRequest(String encryptCode) {
        this.encryptCode = encryptCode;
    }

    public String getEncryptCode() {
        return encryptCode;
    }

    public void setEncryptCode(String encryptCode) {
        this.encryptCode = encryptCode;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/code/decrypt";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardCodeDecryptResponse> responseClass() {
        return CardCodeDecryptResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
