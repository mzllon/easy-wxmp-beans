/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request;

import com.mzlion.weixin.easywxmp.constants.QrActionNameEnum;
import com.mzlion.weixin.easywxmp.response.QRCodeCreateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 生成带参数的二维码
 * <p>
 * 为了满足用户渠道推广分析和用户帐号绑定等场景的需要，公众平台提供了生成带参数二维码的接口。使用该接口可以获得多个带不同场景值的二维码，用户扫描后，公众号可以接收到事件推送。
 * 目前有2种类型的二维码：
 * 1、临时二维码，是有过期时间的，最长可以设置为在二维码生成后的30天（即2592000秒）后过期，但能够生成较多数量。临时二维码主要用于帐号绑定等不要求二维码永久保存的业务场景
 * 2、永久二维码，是无过期时间的，但数量较少（目前为最多10万个）。永久二维码主要用于适用于帐号绑定、用户来源统计等场景。
 * 用户扫描带场景值二维码时，可能推送以下两种事件：
 * 如果用户还未关注公众号，则用户可以关注公众号，关注后微信会将带场景值关注事件推送给开发者。
 * 如果用户已经关注公众号，在用户扫描后会自动进入会话，微信也会将带场景值扫描事件推送给开发者。
 * 获取带参数的二维码的过程包括两步，首先创建二维码ticket，然后凭借ticket到指定URL换取二维码。
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class QRCodeCreateRequest extends BaseQRCodeCreateRequest<QRCodeCreateRequest.ParamQRActionInfo, QRCodeCreateResponse> {

    public QRCodeCreateRequest() {
        super.qrActionName = QrActionNameEnum.QR_SCENE;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/qrcode/create";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<QRCodeCreateResponse> responseClass() {
        return QRCodeCreateResponse.class;
    }

    /**
     * 带参数的二维码
     */
    public static class ParamQRActionInfo extends BaseQRCodeCreateRequest.QRActionInfo {

        private static final long serialVersionUID = 6413561348047299156L;

        /**
         * 二维码场景信息
         */
        private final Scene scene;

        public ParamQRActionInfo(Scene scene) {
            this.scene = scene;
        }

        public Scene getScene() {
            return scene;
        }

    }

    /**
     * 二维码场景信息
     */
    public static class Scene implements Serializable {

        private static final long serialVersionUID = 7166215023660597286L;

        @ApiField("scene_id")
        private Long sceneId;

        @ApiField("scene_str")
        private String sceneStr;

        public Scene() {
        }

        public Scene(Long sceneId) {
            this.sceneId = sceneId;
        }

        public Scene(String sceneStr) {
            this.sceneStr = sceneStr;
        }

        public Long getSceneId() {
            return sceneId;
        }

        public void setSceneId(Long sceneId) {
            this.sceneId = sceneId;
        }

        public String getSceneStr() {
            return sceneStr;
        }

        public void setSceneStr(String sceneStr) {
            this.sceneStr = sceneStr;
        }
    }
}
