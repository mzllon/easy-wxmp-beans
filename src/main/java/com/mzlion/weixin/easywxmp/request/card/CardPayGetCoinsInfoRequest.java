/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ApiMethodEnum;
import com.mzlion.weixin.easywxmp.response.card.CardPayGetCoinsInfoResponse;

/**
 * 查询券点余额接口
 * <p>本接口用于开发者查询当前券点账户中的免费券点/付费券点数目以及总额</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayGetCoinsInfoRequest extends WxRequest<CardPayGetCoinsInfoResponse> {

    private static final long serialVersionUID = -8294333230320391085L;

    public CardPayGetCoinsInfoRequest() {
    }

    /**
     * 接口请求方式，使用{@linkplain ApiMethodEnum#GET}请求
     *
     * @return {@link ApiMethodEnum}
     */
    @Override
    public ApiMethodEnum method() {
        return ApiMethodEnum.GET;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/pay/getcoinsinfo";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardPayGetCoinsInfoResponse> responseClass() {
        return CardPayGetCoinsInfoResponse.class;
    }
}
