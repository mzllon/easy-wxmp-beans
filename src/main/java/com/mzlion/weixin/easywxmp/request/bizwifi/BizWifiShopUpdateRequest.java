/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 修改门店网络信息
 * <p>
 * 通过此接口修改门店的网络信息，包括网络名称（ssid）或密码。需注意：
 * 1. 只有门店下已添加Wi-Fi网络信息，才能调用此接口修改网络信息；添加方式请参考“添加密码型设备”和“添加portal型设备”接口文档。
 * 2. 网络信息修改后，密码型设备需同步修改所有设备的ssid或密码；portal型设备需修改所有设备的ssid，
 * 并按照《硬件鉴权协议接口》修改“第二步：改造移动端portal页面”中的ssid参数，否则将无法正常连网。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiShopUpdateRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = 8153393444059456486L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 老的无线网络设备的ssid
     */
    @ApiField("old_ssid")
    private String oldSsid;

    /**
     * 无线网络设备的ssid。32个字符以内；ssid支持中文，但可能因设备兼容性问题导致显示乱码，或无法连接等问题，相关风险自行承担！
     * 当门店下是portal型设备时，ssid必填；当门店下是密码型设备时，ssid选填，且ssid和密码必须有一个以大写字母“WX”开头
     */
    private String ssid;

    /**
     * 无线网络设备的密码。8-24个字符；不能包含中文字符；
     * 当门店下是密码型设备时，才可填写password，且ssid和密码必须有一个以大写字母“WX”开头
     */
    private String password;

    public BizWifiShopUpdateRequest() {
    }

    public BizWifiShopUpdateRequest(String shopId) {
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getOldSsid() {
        return oldSsid;
    }

    public void setOldSsid(String oldSsid) {
        this.oldSsid = oldSsid;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/shop/update";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
