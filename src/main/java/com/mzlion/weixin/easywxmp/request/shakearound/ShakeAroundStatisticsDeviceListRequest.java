/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.shakearound.ShakeAroundStatisticsDeviceListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 批量查询设备统计数据接口
 * <p>
 * 查询指定时间商家帐号下的每个设备进行摇周边操作的人数、次数，点击摇周边消息的人数、次数。
 * 只能查询最近90天内的数据，且一次只能查询一天。
 * 此接口无法获取当天的数据，最早只能获取前一天的数据。
 * 由于系统在凌晨处理前一天的数据，太早调用此接口可能获取不到数据，建议在早上8：00之后调用此接口。
 * 注意：对于摇周边人数、摇周边次数、点击摇周边消息的人数、点击摇周边消息的次数都为0的设备，不在结果列表中返回。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundStatisticsDeviceListRequest extends WxRequest<ShakeAroundStatisticsDeviceListResponse> {

    private static final long serialVersionUID = -1421789911122825216L;

    /**
     * 指定查询日期时间戳，单位为秒
     */
    private Long date;

    /**
     * 指定查询的结果页序号；返回结果按摇周边人数降序排序，每50条记录为一页
     */
    @ApiField("page_index")
    private int pageIndex = 1;


    public ShakeAroundStatisticsDeviceListRequest() {
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/statistics/devicelist";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShakeAroundStatisticsDeviceListResponse> responseClass() {
        return ShakeAroundStatisticsDeviceListResponse.class;
    }
}
