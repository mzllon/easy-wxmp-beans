/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.shakearound;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 配置设备与门店的关联关系
 * <p>支持创建门店后直接关联在设备上，无需为审核通过状态，摇周边后台自动更新门店的最新信息和状态。</p>
 * <p>
 * 支持设备关联其他公众账号的门店，门店需为审核通过状态。
 * 因为第三方门店不归属本公众账号，所以未保存到设备详情中，查询设备列表接口与获取摇周边的设备及用户信息接口不会返回第三方门店。如需验证请使用摇一摇关注JS API验证。
 * 若设备上绑定自己门店，则自动屏蔽第三方门店相关功能。
 * </p>
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceBindLocationRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -1433290695641350575L;

    /**
     * 申请的批次ID
     */
    @ApiField("device_identifier")
    private ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier;

    /**
     * 设备关联的门店ID，关联门店后，在门店1KM的范围内有优先摇出信息的机会。
     * 当值为0时，将清除设备已关联的门店ID。门店相关信息具体可查看门店相关的接口文档
     */
    @ApiField("poi_id")
    private String poiId;

    /**
     * 为1时，关联的门店和设备归属于同一公众账号；
     * 为2时，关联的门店为其他公众账号的门店
     */
    private String type = "1";

    /**
     * 关联门店所归属的公众账号的APPID
     */
    @ApiField("poi_appid")
    private String poiAppId;

    public ShakeAroundDeviceBindLocationRequest() {
    }

    public ShakeAroundDeviceIdentifierModel getShakeAroundDeviceIdentifier() {
        return shakeAroundDeviceIdentifier;
    }

    public void setShakeAroundDeviceIdentifier(ShakeAroundDeviceIdentifierModel shakeAroundDeviceIdentifier) {
        this.shakeAroundDeviceIdentifier = shakeAroundDeviceIdentifier;
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPoiAppId() {
        return poiAppId;
    }

    public void setPoiAppId(String poiAppId) {
        this.poiAppId = poiAppId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/shakearound/device/bindlocation";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
