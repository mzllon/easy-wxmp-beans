/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiDeviceListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询设备
 * <p>可通过指定分页或具体门店ID的方式，查询当前MP账号下指定门店连网成功的设备信息。一次最多能查询20个门店的设备信息。</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiDeviceListRequest extends WxRequest<BizWifiDeviceListResponse> {

    private static final long serialVersionUID = 6221497365253266367L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 分页下标，默认从1开始
     */
    @ApiField("pageindex")
    private int pageIndex = 1;

    /**
     * 每页的个数，默认10个，最大20个
     */
    @ApiField("pagesize")
    private int pageSize = 10;


    public BizWifiDeviceListRequest() {
    }

    public BizWifiDeviceListRequest(int pageIndex, int pageSize, String shopId) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/device/list";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiDeviceListResponse> responseClass() {
        return BizWifiDeviceListResponse.class;
    }
}
