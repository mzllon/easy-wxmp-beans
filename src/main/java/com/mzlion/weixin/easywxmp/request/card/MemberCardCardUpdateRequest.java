/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.model.card.MemberCardCardModel;
import com.mzlion.weixin.easywxmp.response.card.MemberCardCardUpdateResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 更改会员卡信息接口
 * <p>* 支持更改会员卡卡面信息以及卡券属性信息。</p>
 *
 * @author mzlion on 2016/12/25.
 */
public class MemberCardCardUpdateRequest extends WxRequest<MemberCardCardUpdateResponse> {

    /**
     * 查询会员卡的cardid
     */
    @ApiField( "card_id")
    private String cardId;

    /**
     * 卡种信息
     */
    @ApiField( "member_card")
    private MemberCardCardModel.MemberCard memberCard;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public MemberCardCardModel.MemberCard getMemberCard() {
        return memberCard;
    }

    public void setMemberCard(MemberCardCardModel.MemberCard memberCard) {
        this.memberCard = memberCard;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/update";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MemberCardCardUpdateResponse> responseClass() {
        return MemberCardCardUpdateResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
