/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 设置连网完成页
 * <p>
 * 当顾客使用微信连Wi-Fi方式连网成功时，点击页面右上角“完成”按钮，即可进入已设置的连网完成页。
 * 注：此接口只对公众号第三方平台和认证公众号开放，非认证公众号无法调用接口设置连网成功页。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiFinishPageSetRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -4409565122579974041L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 连网完成页URL
     */
    @ApiField("finishpage_url")
    private Integer finishPageUrl;

    public BizWifiFinishPageSetRequest() {
    }

    public BizWifiFinishPageSetRequest(String shopId, Integer finishPageUrl) {
        this.shopId = shopId;
        this.finishPageUrl = finishPageUrl;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public Integer getFinishPageUrl() {
        return finishPageUrl;
    }

    public void setFinishPageUrl(Integer finishPageUrl) {
        this.finishPageUrl = finishPageUrl;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/finishpage/set";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
