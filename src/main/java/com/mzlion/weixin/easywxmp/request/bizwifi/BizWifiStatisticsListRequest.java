/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiStatisticsListResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * Wi-Fi数据统计
 * <p>
 * 查询一定时间范围内的WiFi连接总人数、微信方式连Wi-Fi人数、商家主页访问人数、连网后消息发送人数、新增公众号关注人数和累计公众号关注人数。
 * 查询的最长时间跨度为30天。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiStatisticsListRequest extends WxRequest<BizWifiStatisticsListResponse> {

    private static final long serialVersionUID = 3590091678682903502L;

    /**
     * 起始日期时间，格式yyyy-mm-dd，最长时间跨度为30天
     */
    @ApiField("begin_date")
    private String beginDate;

    /**
     * 结束日期时间戳，格式yyyy-mm-dd，最长时间跨度为30天
     */
    @ApiField("end_date")
    private String endDate;

    /**
     * 按门店ID搜索，-1为总统计
     */
    @ApiField("shop_id")
    private String shopId = "-1";

    public BizWifiStatisticsListRequest() {
    }

    public BizWifiStatisticsListRequest(String beginDate, String endDate) {
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public BizWifiStatisticsListRequest(String beginDate, String endDate, String shopId) {
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.shopId = shopId;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/statistics/list";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiStatisticsListResponse> responseClass() {
        return BizWifiStatisticsListResponse.class;
    }
}
