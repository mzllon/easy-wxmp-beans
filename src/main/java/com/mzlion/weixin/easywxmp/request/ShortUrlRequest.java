/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ShortUrlActionEnum;
import com.mzlion.weixin.easywxmp.response.ShortUrlResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 长链接转短链接接口
 * <p>
 * 主要使用场景： 开发者用于生成二维码的原链接（商品、支付二维码等）太长导致扫码速度和成功率下降，将原长链接通过此接口转成短链接再生成二维码将大大提升扫码速度和成功率。
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class ShortUrlRequest extends WxRequest<ShortUrlResponse> {

    /**
     * 此处填long2short，代表长链接转短链接
     */
    @ApiField("action")
    private ShortUrlActionEnum shortUrlAction;

    /**
     * 需要转换的长链接，支持http://、https://、weixin://wxpay 格式的url
     */
    @ApiField("long_url")
    private String longUrl;

    public ShortUrlRequest() {
    }

    public ShortUrlActionEnum getShortUrlAction() {
        return shortUrlAction;
    }

    public void setShortUrlAction(ShortUrlActionEnum shortUrlAction) {
        this.shortUrlAction = shortUrlAction;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/shorturl";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ShortUrlResponse> responseClass() {
        return ShortUrlResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
