/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 商品上下架
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantModProductStatusRequest extends WxRequest<WxResponse> {

    /**
     * 商品ID
     */
    @ApiField("product_id")
    private String productId;

    /**
     * 商品上下架标识(0-下架, 1-上架)
     *
     * @see #STATUS$ON_SHELVES
     * @see #STATUS$OFF_SHELVES
     */
    private Integer status;

    public MerchantModProductStatusRequest() {
    }

    public MerchantModProductStatusRequest(String productId, int status) {
        this.productId = productId;
        this.status = status;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/modproductstatus";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

    /**
     * 下架
     */
    public static final int STATUS$OFF_SHELVES = 0;

    /**
     * 上架
     */
    public static final int STATUS$ON_SHELVES = 1;


}
