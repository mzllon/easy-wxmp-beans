/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 清空门店网络及设备
 * <p>通过此接口清空门店的网络配置及所有设备，恢复空门店状态</p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiShopCleanRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = 1557827616625832743L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 无线网络设备的ssid。若不填写ssid，默认为清空门店下所有设备；
     * 填写ssid则为清空该ssid下的所有设备
     */
    private String ssid;

    public BizWifiShopCleanRequest() {
    }

    public BizWifiShopCleanRequest(String shopId, String ssid) {
        this.shopId = shopId;
        this.ssid = ssid;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/shop/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
