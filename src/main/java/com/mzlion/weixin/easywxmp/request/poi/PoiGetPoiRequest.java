/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.poi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.poi.PoiGetPoiResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询门店信息请求参数
 * <p>
 * 创建门店后获取poi_id 后，商户可以利用poi_id，查询具体某条门店的信息。
 * 若在查询时，update_status 字段为1，表明在5 个工作日内曾用update 接口修改过门店扩展字段，该扩展字段为最新的修改字段，尚未经过审核采纳，因此不是最终结果。
 * 最终结果会在5 个工作日内，最终确认是否采纳，并前端生效（但该扩展字段的采纳过程不影响门店的可用性，即available_state仍为审核通过状态）
 * 注：修改扩展字段将会推送审核，但不会影响该门店的生效可用状态。
 * </p>
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiGetPoiRequest extends WxRequest<PoiGetPoiResponse> {

    /**
     * 微信的门店ID
     */
    @ApiField("poi_id")
    private String poiId;

    /**
     * 微信的门店ID
     */
    public String getPoiId() {
        return poiId;
    }

    /**
     * 微信的门店ID
     */
    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/cgi-bin/poi/getpoi";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<PoiGetPoiResponse> responseClass() {
        return PoiGetPoiResponse.class;
    }
}
