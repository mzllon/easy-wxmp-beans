/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiQRCodeGetResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取物料二维码
 * <p>
 * 添加设备后，通过此接口可以获取物料，包括二维码和桌贴两种样式。将物料铺设在线下门店里，可供用户扫码上网。
 * 注：只有门店下已添加Wi-Fi网络信息，才能调用此接口获取二维码，添加方式请参考“添加密码型设备”和“添加portal型设备”接口文档。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiQRCodeGetRequest extends WxRequest<BizWifiQRCodeGetResponse> {

    private static final long serialVersionUID = 722828204252265060L;

    /**
     * 门店ID
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 无线网络设备的ssid。
     */
    private String ssid;

    /**
     * 物料样式编号：
     * 0-纯二维码，可用于自由设计宣传材料；
     * 1-二维码物料，155mm×215mm(宽×高)，可直接张贴
     */
    private int imgId = 0;

    public BizWifiQRCodeGetRequest() {
    }

    public BizWifiQRCodeGetRequest(String shopId, String ssid, int imgId) {
        this.shopId = shopId;
        this.ssid = ssid;
        this.imgId = imgId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/qrcode/get";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiQRCodeGetResponse> responseClass() {
        return BizWifiQRCodeGetResponse.class;
    }
}
