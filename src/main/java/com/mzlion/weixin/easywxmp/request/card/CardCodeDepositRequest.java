/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.constants.ApiProtocolEnum;
import com.mzlion.weixin.easywxmp.response.card.CardCodeDepositResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 导入code接口
 * <p>在自定义code卡券成功创建并且通过审核后，必须将自定义code按照与发券方的约定数量调用导入code接口导入微信后台</p>
 * <p>
 * 开发者可调用该接口将自定义code导入微信卡券后台，由微信侧代理存储并下发code。
 * 注：
 * 1）单次调用接口传入code的数量上限为100个。
 * 2）每一个 code 均不能为空串。
 * 3）导入结束后系统会自动判断提供方设置库存与实际导入code的量是否一致。
 * 4）导入失败支持重复导入，提示成功为止。
 * </p>
 *
 * @author mzlion on 2017/4/17.
 */
public class CardCodeDepositRequest extends WxRequest<CardCodeDepositResponse> {

    /**
     * 需要进行导入code的卡券ID
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * 需导入微信卡券后台的自定义code，上限为100个
     */
    @ApiField("code")
    private List<String> codeList;

    public CardCodeDepositRequest() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public List<String> getCodeList() {
        return codeList;
    }

    public void setCodeList(List<String> codeList) {
        this.codeList = codeList;
    }

    /**
     * 接口协议，使用{@linkplain ApiProtocolEnum#HTTP}协议
     *
     * @return {@linkplain ApiProtocolEnum}
     */
    @Override
    public ApiProtocolEnum protocol() {
        return ApiProtocolEnum.HTTP;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/card/code/deposit";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<CardCodeDepositResponse> responseClass() {
        return CardCodeDepositResponse.class;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
