/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.scan;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.scan.ScanScanTicketCheckResponse;

/**
 * 检查wxticket参数
 * <p>
 * 调用该接口，可检查当前访问用户来源的商品主页，以及在网页前端获取该用户的身份信息。
 * 为保证场景的正确性，wxticket参数作为临时签名仅在20分钟内有效，超过有效时长会检查失败。
 * </p>
 * Created by mzlion on 2017/4/18.
 */
public class ScanScanTicketCheckRequest extends WxRequest<ScanScanTicketCheckResponse> {

    private static final long serialVersionUID = 1893526600734879417L;

    /**
     * 请求URL中带上的wxticket参数
     */
    private String ticket;

    public ScanScanTicketCheckRequest() {
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/scan/scanticket/check";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<ScanScanTicketCheckResponse> responseClass() {
        return ScanScanTicketCheckResponse.class;
    }
}
