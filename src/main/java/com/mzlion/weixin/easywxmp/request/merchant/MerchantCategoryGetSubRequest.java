/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.merchant;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.merchant.MerchantCategoryGetSubResponse;

/**
 * 获取指定分类的所有子分类
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantCategoryGetSubRequest extends WxRequest<MerchantCategoryGetSubResponse> {

    /**
     * 大分类ID(根节点分类id为1)
     */
    private String cateId = "1";

    public MerchantCategoryGetSubRequest() {
    }

    public MerchantCategoryGetSubRequest(String cateId) {
        this.cateId = cateId;
    }

    public String getCateId() {
        return cateId;
    }

    public void setCateId(String cateId) {
        this.cateId = cateId;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/merchant/category/getsub";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<MerchantCategoryGetSubResponse> responseClass() {
        return MerchantCategoryGetSubResponse.class;
    }
}
