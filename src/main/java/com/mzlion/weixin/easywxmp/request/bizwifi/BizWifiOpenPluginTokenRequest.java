/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.bizwifi;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.response.bizwifi.BizWifiOpenPluginTokenResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 第三方平台获取开插件wifi_token
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiOpenPluginTokenRequest extends WxRequest<BizWifiOpenPluginTokenResponse> {

    private static final long serialVersionUID = 3594710430371209579L;

    /**
     * 回调URL，开通插件成功后的跳转页面。
     * 注：该参数域名必须与跳转进开通插件页面的页面域名保持一致，建议均采用第三方平台域名。
     */
    @ApiField("callback_url")
    private String callbackUrl;

    public BizWifiOpenPluginTokenRequest() {
    }

    public BizWifiOpenPluginTokenRequest(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/bizwifi/openplugin/token";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<BizWifiOpenPluginTokenResponse> responseClass() {
        return BizWifiOpenPluginTokenResponse.class;
    }
}
