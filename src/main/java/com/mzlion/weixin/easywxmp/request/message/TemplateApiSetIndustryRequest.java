/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 设置所属行业
 * <p>设置行业可在MP中完成，每月可修改行业1次，账号仅可使用所属行业中相关的模板，为方便第三方开发者，提供通过接口调用的方式来修改账号所属行业</p>
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateApiSetIndustryRequest extends WxRequest<WxResponse> {

    /**
     * 公众号模板消息所属行业编号
     */
    @ApiField("industry_id1")
    private String industryId1;

    /**
     * 公众号模板消息所属行业编号
     */
    @ApiField("industry_id2")
    private String industryId2;

    public TemplateApiSetIndustryRequest() {
    }

    public TemplateApiSetIndustryRequest(String industryId1, String industryId2) {
        this.industryId1 = industryId1;
        this.industryId2 = industryId2;
    }

    public String getIndustryId1() {
        return industryId1;
    }

    public void setIndustryId1(String industryId1) {
        this.industryId1 = industryId1;
    }

    public String getIndustryId2() {
        return industryId2;
    }

    public void setIndustryId2(String industryId2) {
        this.industryId2 = industryId2;
    }

    @Override
    public String serviceUrl() {
        return "/cgi-bin/template/api_set_industry";
    }

    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }

}
