/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.request.message;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.model.ArticleItemModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 发送图文消息（点击跳转到外链） 图文消息条数限制在8条以内，注意，如果图文数超过8，则将会无响应
 *
 * @author mzlion on 2017/4/16.
 */
public class NewsMessageCustomSendRequest extends MessageCustomSendRequest {

    /**
     * 图文列表
     */
    @ApiField(expose = true)
    private List<ArticleItemModel> articleItemList;


    /**
     * 图文消息
     */
    @ApiField("news")
    private NewsMessage newsMessage;

    public NewsMessageCustomSendRequest() {
        super.msgType = MsgTypeEnum.NEWS;
    }

    public List<ArticleItemModel> getArticleItemList() {
        return articleItemList;
    }

    public void setArticleItemList(List<ArticleItemModel> articleItemList) {
        this.articleItemList = articleItemList;
        this.newsMessage = new NewsMessage();
        this.newsMessage.articleItemList = this.articleItemList;
    }

    public NewsMessage getNewsMessage() {
        return newsMessage;
    }

    /**
     * 图文消息
     *
     * @author mzlion
     */
    public static class NewsMessage implements Serializable {

        /**
         * 图文列表
         */
        @ApiField("articles")
        private List<ArticleItemModel> articleItemList;

        public List<ArticleItemModel> getArticleItemList() {
            return articleItemList;
        }

        public void setArticleItemList(List<ArticleItemModel> articleItemList) {
            this.articleItemList = articleItemList;
        }
    }
}
