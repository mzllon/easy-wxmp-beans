package com.mzlion.weixin.easywxmp.events;

import com.mzlion.core.reflect.ReflectionUtils;

/**
 * 会员卡激活事件推送
 * <p>当用户通过一键激活的方式提交信息并点击激活或者用户修改会员卡信息后，商户会收到用户激活的事件推送</p>
 *
 * @author mzlion on 2016/12/28.
 */
public class SubmitMemberUserInfoEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 卡券Code码
     */
    private String userCardCode;

    /**
     * 保留默认构造函数
     */
    public SubmitMemberUserInfoEvent() {
    }

    public SubmitMemberUserInfoEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
