/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

import com.mzlion.weixin.easywxmp.model.ScanCodeInfoModel;

/**
 * 扫码推事件的事件推送
 *
 * @author mzlion on 2017/1/5.
 */
public class ScanCodePushEvent extends BaseEventMsg {

    /**
     * 事件KEY值，设置的跳转URL
     */
    private String eventKey;

    /**
     * 扫描信息
     */
    private ScanCodeInfoModel scanCodeInfo;

    /**
     * 保留默认构造函数
     */
    public ScanCodePushEvent() {
    }

    public ScanCodePushEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    /**
     * 事件KEY值，设置的跳转URL
     *
     * @return 事件KEY值
     */
    public String getEventKey() {
        return eventKey;
    }

    /**
     * 事件KEY值，设置的跳转URL
     *
     * @param eventKey 事件KEY值
     */
    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public ScanCodeInfoModel getScanCodeInfo() {
        return scanCodeInfo;
    }

    public void setScanCodeInfo(ScanCodeInfoModel scanCodeInfo) {
        this.scanCodeInfo = scanCodeInfo;
    }
}
