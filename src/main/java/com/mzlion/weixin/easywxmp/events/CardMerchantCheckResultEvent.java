/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

import com.mzlion.core.reflect.ReflectionUtils;

/**
 * 子商户审核事件推送
 * <p>开发者所代理的子商户审核通过后，会收到微信服务器发送的事件推送。</p>
 *
 * @author mzlion on 2016/12/28.
 */
public class CardMerchantCheckResultEvent extends BaseEventMsg {

    /**
     * 保留默认构造函数
     */
    public CardMerchantCheckResultEvent() {
    }

    public CardMerchantCheckResultEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    /**
     * 子商户ID
     */
    private String merchantId;

    /**
     * 是否通过，为1时审核通过
     *
     * @see #IS_PASS$YES
     */
    private Integer isPass;
    /**
     * 驳回的原因
     */
    private String reason;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getIsPass() {
        return isPass;
    }

    public void setIsPass(Integer isPass) {
        this.isPass = isPass;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * 审核通过
     */
    public static final int IS_PASS$YES = 1;

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
