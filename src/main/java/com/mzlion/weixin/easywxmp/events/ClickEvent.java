/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 自定义菜单事件,
 * 用户点击自定义菜单后，微信会把点击事件推送给开发者，请注意，点击菜单弹出子菜单，不会产生上报。
 *
 * @author mzlion on 2017/1/5.
 */
public class ClickEvent extends BaseEventMsg {

    /**
     * 事件KEY值，与自定义菜单接口中KEY值对应
     */
    private String eventKey;

    /**
     * 保留默认构造函数
     */
    public ClickEvent() {
    }

    public ClickEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    /**
     * 事件KEY值，与自定义菜单接口中KEY值对应
     *
     * @return 事件KEY值
     */
    public String getEventKey() {
        return eventKey;
    }

    /**
     * 事件KEY值，与自定义菜单接口中KEY值对应
     *
     * @param eventKey 事件KEY值
     */
    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }
}
