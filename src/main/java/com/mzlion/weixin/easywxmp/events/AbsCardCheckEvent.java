/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 生成的卡券通过审核时，微信会把这个事件推送到开发者填写的URL。
 *
 * @author mzlion on 2016/12/27.
 */
abstract class AbsCardCheckEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    protected String cardId;

    /**
     * 保留默认构造函数
     */
    AbsCardCheckEvent() {
    }

    public AbsCardCheckEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }


}
