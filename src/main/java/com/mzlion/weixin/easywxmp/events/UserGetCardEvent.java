/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

import com.mzlion.core.reflect.ReflectionUtils;

/**
 * 用户在领取卡券时，微信会把这个事件推送到开发者填写的URL
 *
 * @author mzlion on 2016/12/27.
 */
public class UserGetCardEvent extends BaseEventMsg {

    /**
     * 保留默认构造函数
     */
    public UserGetCardEvent() {
    }

    public UserGetCardEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 是否为转赠领取，1代表是，0代表否。
     */
    private Integer isGiveByFriend;

    /**
     * 当IsGiveByFriend为1时填入的字段，表示发起转赠用户的openid
     */
    private String friendUserName;

    /**
     * code序列号
     */
    private String userCardCode;

    /**
     * 为保证安全，微信会在转赠发生后变更该卡券的code号，该字段表示转赠前的code
     */
    private String oldUserCardCode;

    /**
     * 领取场景值，用于领取渠道数据统计。可在生成二维码接口及添加Addcard接口中自定义该字段的字符串值
     */
    private String outerStr;

    /**
     * 用户删除会员卡后可重新找回，当用户本次操作为找回时，该值为1，否则为0
     */
    private Integer isRestoreMemberCard;

    /**
     * 渠道场景值
     */
    private String outerId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Integer getIsGiveByFriend() {
        return isGiveByFriend;
    }

    public void setIsGiveByFriend(Integer isGiveByFriend) {
        this.isGiveByFriend = isGiveByFriend;
    }

    public String getFriendUserName() {
        return friendUserName;
    }

    public void setFriendUserName(String friendUserName) {
        this.friendUserName = friendUserName;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }

    public String getOldUserCardCode() {
        return oldUserCardCode;
    }

    public void setOldUserCardCode(String oldUserCardCode) {
        this.oldUserCardCode = oldUserCardCode;
    }

    public String getOuterStr() {
        return outerStr;
    }

    public void setOuterStr(String outerStr) {
        this.outerStr = outerStr;
    }

    public Integer getIsRestoreMemberCard() {
        return isRestoreMemberCard;
    }

    public void setIsRestoreMemberCard(Integer isRestoreMemberCard) {
        this.isRestoreMemberCard = isRestoreMemberCard;
    }

    public String getOuterId() {
        return outerId;
    }

    public void setOuterId(String outerId) {
        this.outerId = outerId;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }

    /**
     * 判断用户是否重新找回了卡
     *
     * @return 返回{@code true}则表示此卡为从删除中找回的，否则则不是
     */
    public boolean isRestoreMemberCard() {
        return this.isRestoreMemberCard == IS_RESTORE_MEMBER_CARD$YES;
    }

    /**
     * 判断卡的来源方式是否为转赠
     *
     * @return 返回{@code true}则表示转赠领取，否则则不是
     */
    public boolean isGiveByFriend() {
        return this.isGiveByFriend == IS_GIVE_BY_FRIEND$YES;
    }

    /**
     * 是转赠领取
     */
    public static final int IS_GIVE_BY_FRIEND$YES = 1;

    /**
     * 不是转赠领取
     */
    public static final int IS_GIVE_BY_FRIEND$NO = 0;

    /**
     * 删除找回的卡
     */
    public static final int IS_RESTORE_MEMBER_CARD$YES = 1;

    /**
     * 不是删除找回的卡
     */
    public static final int IS_RESTORE_MEMBER_CARD$NO = 1;
}
