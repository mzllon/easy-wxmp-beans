package com.mzlion.weixin.easywxmp.events;

/**
 * 库存报警事件
 * <p>
 * 当某个card_id的初始库存数大于200且当前库存小于等于100时，用户尝试领券会触发发送事件给商户，事件每隔12h发送一次。
 * </p>
 *
 * @author mzlion on 2016/12/27.
 */
public class CardSkuRemindEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 报警详细信息
     */
    private String detail;

    /**
     * 保留默认构造函数
     */
    public CardSkuRemindEvent() {
    }

    public CardSkuRemindEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
