/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

import java.util.Objects;

/**
 * 门店审核事件推送
 *
 * @author mzlion on 2017/4/23.
 */
public class PoiCheckNotifyEvent extends BaseEventMsg {

    /**
     * 保留默认构造函数
     */
    public PoiCheckNotifyEvent() {
    }

    public PoiCheckNotifyEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    /**
     * 商户自己内部ID，即字段中的sid
     */
    private String uniqId;

    /**
     * 微信的门店ID，微信内门店唯一标示ID
     */
    private String poiId;

    /**
     * 审核结果，成功succ 或失败fail
     */
    private String result;

    /**
     * 成功的通知信息，或审核失败的驳回理由
     */
    private String msg;

    public String getUniqId() {
        return uniqId;
    }

    public void setUniqId(String uniqId) {
        this.uniqId = uniqId;
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 审核通过则返回{@code true},否则返回{@code false}
     *
     * @return 审核结果
     */
    public boolean isSuccess() {
        return Objects.equals("succ", result);
    }
}
