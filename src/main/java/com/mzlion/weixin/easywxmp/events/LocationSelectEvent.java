/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

import java.io.Serializable;

/**
 * location_select：弹出地理位置选择器的事件推送
 *
 * @author mzlion on 2017/1/5.
 */
public class LocationSelectEvent extends BaseEventMsg {

    /**
     * 事件KEY值，由开发者在创建菜单时设定
     */
    private String eventKey;

    /**
     * 发送的位置信息
     */
    private SendLocationInfoWrapper sendLocationInfo;

    /**
     * 保留默认构造函数
     */
    public LocationSelectEvent() {
    }

    public LocationSelectEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public SendLocationInfoWrapper getSendLocationInfo() {
        return sendLocationInfo;
    }

    public void setSendLocationInfo(SendLocationInfoWrapper sendLocationInfo) {
        this.sendLocationInfo = sendLocationInfo;
    }


    /**
     * 发送的位置信息
     *
     * @author mzlion on 2017/1/5.
     */
    public static class SendLocationInfoWrapper implements Serializable {

        /**
         * X坐标信息
         */
        private String locationX;

        /**
         * Y坐标信息
         */
        private String locationY;

        /**
         * 精度，可理解为精度或者比例尺、越精细的话 scale越高
         */
        private String scale;

        /**
         * 地理位置的字符串信息
         */
        private String label;

        /**
         * 朋友圈POI的名字，可能为空
         */
        private String poiName;

        public String getLocationX() {
            return locationX;
        }

        public void setLocationX(String locationX) {
            this.locationX = locationX;
        }

        public String getLocationY() {
            return locationY;
        }

        public void setLocationY(String locationY) {
            this.locationY = locationY;
        }

        public String getScale() {
            return scale;
        }

        public void setScale(String scale) {
            this.scale = scale;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getPoiName() {
            return poiName;
        }

        public void setPoiName(String poiName) {
            this.poiName = poiName;
        }
    }
}
