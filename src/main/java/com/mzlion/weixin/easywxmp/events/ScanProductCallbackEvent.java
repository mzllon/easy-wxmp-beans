/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 商品扫码推送事件
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductCallbackEvent extends BaseEventMsg {

    private static final long serialVersionUID = 6716253484698937000L;

    /**
     * 商品编码标准
     */
    private String keyStandard;

    /**
     * 商品编码内容
     */
    private String keyStr;

    /**
     * 调用“获取商品二维码接口”时传入的extinfo，为标识参数
     */
    private String extInfo;

    /**
     * 请是否使用微信提供的弹窗页面展示防伪结果，true为使用，false为未使用。
     */
    private Boolean needAntiFake;

    /**
     * 保留默认构造函数
     */
    public ScanProductCallbackEvent() {
    }

    public ScanProductCallbackEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public Boolean getNeedAntiFake() {
        return needAntiFake;
    }

    public void setNeedAntiFake(Boolean needAntiFake) {
        this.needAntiFake = needAntiFake;
    }
}
