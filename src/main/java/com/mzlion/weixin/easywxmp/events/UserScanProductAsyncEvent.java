/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 地理位置信息异步推送
 * <p>
 * 当用户打开商品主页，微信会将该用户实时的地理位置信息以异步事件的形式推送到商户填写的URL。商户可利用该信息做数据分析，形成差异化运营方案或指导生产。
 * 推送的地理位置信息为“省”一级，如广东省。由于用户的网速影响，异步推送的响应速度可能较慢。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class UserScanProductAsyncEvent extends BaseEventMsg {

    private static final long serialVersionUID = 819663436585819434L;

    /**
     * 商品编码标准
     */
    private String keyStandard;

    /**
     * 商品编码内容
     */
    private String keyStr;

    /**
     * 调用“获取商品二维码接口”时传入的extinfo，为标识参数
     */
    private String extInfo;

    /**
     * 用户的实时地理位置信息（目前只精确到省一级），可在国家统计局网站查到对应明细：http://www.stats.gov.cn/tjsj/tjbz/xzqhdm/201504/t20150415_712722.html
     */
    private String regionCode;

    /**
     * 保留默认构造函数
     */
    public UserScanProductAsyncEvent() {
    }

    public UserScanProductAsyncEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
}
