package com.mzlion.weixin.easywxmp.events;

/**
 * 券点流水详情事件
 *
 * @author mzlion on 2016/12/28.
 */
public class CardPayOrderEvent extends BaseEventMsg {

    /**
     * 本次推送对应的订单号
     */
    private String orderId;

    /**
     * 本次订单号的状态
     * ORDER_STATUS_WAITING 等待支付
     * ORDER_STATUS_SUCC 支付成功
     * ORDER_STATUS_FINANCE_SUCC 加代币成功
     * ORDER_STATUS_QUANTITY_SUCC 加库存成功
     * ORDER_STATUS_HAS_REFUND 已退币
     * ORDER_STATUS_REFUND_WAITING 等待退币确认
     * ORDER_STATUS_ROLLBACK 已回退,系统失败
     * ORDER_STATUS_HAS_RECEIPT 已开发票
     */
    private String status;

    /**
     * 购买券点时，支付二维码的生成时间
     */
    private Long createOrderTime;

    /**
     * 购买券点时，实际支付成功的时间
     */
    private Long payFinishTime;

    /**
     * 支付方式，一般为微信支付充值
     */
    private String desc;

    /**
     * 剩余免费券点数量
     */
    private Integer freeCoinCount;

    /**
     * 剩余付费券点数量
     */
    private Integer payCoinCount;

    /**
     * 本次变动的免费券点数量
     */
    private Integer refundFreeCoinCount;

    /**
     * 本次变动的付费券点数量
     */
    private Integer refundPayCoinCount;

    /**
     * 所要拉取的订单类型
     * ORDER_TYPE_SYS_ADD 平台赠送券点
     * ORDER_TYPE_WXPAY 充值券点
     * ORDER_TYPE_REFUND 库存未使用回退券点
     * ORDER_TYPE_REDUCE 券点兑换库存
     * ORDER_TYPE_SYS_REDUCE 平台扣减
     */
    private String orderType;

    /**
     * 系统备注，说明此次变动的缘由，如开通账户奖励、门店奖励、核销奖励以及充值、扣减。
     */
    private String memo;

    /**
     * 所开发票的详情
     */
    private String receiptInfo;

    /**
     * 保留默认构造函数
     */
    public CardPayOrderEvent() {
    }

    public CardPayOrderEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreateOrderTime() {
        return createOrderTime;
    }

    public void setCreateOrderTime(Long createOrderTime) {
        this.createOrderTime = createOrderTime;
    }

    public Long getPayFinishTime() {
        return payFinishTime;
    }

    public void setPayFinishTime(Long payFinishTime) {
        this.payFinishTime = payFinishTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getFreeCoinCount() {
        return freeCoinCount;
    }

    public void setFreeCoinCount(Integer freeCoinCount) {
        this.freeCoinCount = freeCoinCount;
    }

    public Integer getPayCoinCount() {
        return payCoinCount;
    }

    public void setPayCoinCount(Integer payCoinCount) {
        this.payCoinCount = payCoinCount;
    }

    public Integer getRefundFreeCoinCount() {
        return refundFreeCoinCount;
    }

    public void setRefundFreeCoinCount(Integer refundFreeCoinCount) {
        this.refundFreeCoinCount = refundFreeCoinCount;
    }

    public Integer getRefundPayCoinCount() {
        return refundPayCoinCount;
    }

    public void setRefundPayCoinCount(Integer refundPayCoinCount) {
        this.refundPayCoinCount = refundPayCoinCount;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getReceiptInfo() {
        return receiptInfo;
    }

    public void setReceiptInfo(String receiptInfo) {
        this.receiptInfo = receiptInfo;
    }
}
