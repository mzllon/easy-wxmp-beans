/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 转赠事件推送
 *
 * @author mzlion on 2016/12/27.
 */
public class UserGiftingCardEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 接收卡券用户的openid
     */
    private String friendUserName;

    /**
     * code序列号
     */
    private String userCardCode;

    /**
     * 是否转赠退回，0代表不是，1代表是
     */
    private Integer isReturnBack;

    /**
     * 是否是群转赠
     */
    private Integer isChatRoom;

    /**
     * 保留默认构造函数
     */
    public UserGiftingCardEvent() {
    }

    public UserGiftingCardEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getFriendUserName() {
        return friendUserName;
    }

    public void setFriendUserName(String friendUserName) {
        this.friendUserName = friendUserName;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }

    public Integer getIsReturnBack() {
        return isReturnBack;
    }

    public void setIsReturnBack(Integer isReturnBack) {
        this.isReturnBack = isReturnBack;
    }

    public Integer getIsChatRoom() {
        return isChatRoom;
    }

    public void setIsChatRoom(Integer isChatRoom) {
        this.isChatRoom = isChatRoom;
    }

}
