/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 名称认证失败（这时虽然客户端不打勾，但仍有接口权限）
 *
 * @author mzlion on 2017/4/17.
 */
public class NamingVerifyFailEvent extends BaseEventMsg {

    /**
     * 失败发生时间 (整形)，时间戳
     */
    private long failTime;

    /**
     * 认证失败的原因
     */
    private String failReason;

    /**
     * 保留默认构造函数
     */
    public NamingVerifyFailEvent() {
    }

    public NamingVerifyFailEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public long getFailTime() {
        return failTime;
    }

    public void setFailTime(long failTime) {
        this.failTime = failTime;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }
}
