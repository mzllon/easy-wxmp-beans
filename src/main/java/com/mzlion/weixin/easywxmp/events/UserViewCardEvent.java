package com.mzlion.weixin.easywxmp.events;

/**
 *  进入会员卡事件推送
 *  
 * @author mzlion on 2016/12/27.
 */
public class UserViewCardEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 卡券Code码
     */
    private String userCardCode;

    /**
     * 开发者发起核销时传入的自定义参数，用于进行核销渠道统计
     */
    private String outerStr;

    /**
     * 保留默认构造函数
     */
    public UserViewCardEvent() {
    }

    public UserViewCardEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }

    public String getOuterStr() {
        return outerStr;
    }

    public void setOuterStr(String outerStr) {
        this.outerStr = outerStr;
    }
}
