/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

import com.mzlion.weixin.easywxmp.constants.SexEnum;

/**
 * 打开商品主页事件推送
 * <p>
 * 当用户打开商品主页，无论是通过扫码，还是从其他场景（会话、收藏或朋友圈）打开，微信均会推送该事件到商户填写的URL。
 * 推送的内容包括用户基本信息，以及商品主页对应的码信息。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class UserScanProductEvent extends BaseEventMsg {

    private static final long serialVersionUID = 1564177612799980845L;

    /**
     * 商品编码标准
     */
    private String keyStandard;

    /**
     * 商品编码内容
     */
    private String keyStr;

    /**
     * 用户在微信内设置的国家
     */
    private String country;

    /**
     * 用户在微信内设置的省份
     */
    private String province;

    /**
     * 用户在微信内设置的城市
     */
    private String city;

    /**
     * 用户的性别，1为男性，2为女性，0代表未知
     */
    private SexEnum sex;

    /**
     * 打开商品主页的场景，1为扫码，2为其他打开场景（如会话、收藏或朋友圈）
     */
    private String scene;

    /**
     * 调用“获取商品二维码接口”时传入的extinfo，为标识参数
     */
    private String extInfo;

    /**
     * 保留默认构造函数
     */
    public UserScanProductEvent() {
    }

    public UserScanProductEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }
}
