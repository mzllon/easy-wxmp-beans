package com.mzlion.weixin.easywxmp.events;

import java.io.Serializable;
import java.util.List;

/**
 * 摇一摇事件通知
 * <p>
 * 用户进入摇一摇界面，在“周边”页卡下摇一摇时，微信会把这个事件推送到开发者填写的URL（登录公众平台进入开发者中心设置）。
 * 推送内容包含摇一摇时“周边”页卡展示出来的页面所对应的设备信息，以及附近最多五个属于该公众账号的设备的信息。 当摇出列表时，此事件不推送。
 * 微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次。关于重试的消息排重，推荐使用FromUserName + CreateTime 排重。
 * 假如服务器无法保证在五秒内处理并回复，可以直接回复空串，微信服务器不会对此作任何处理，并且不会发起重试。
 * </p>
 * Created by mzlion on 2017/4/19.
 */
public class ShakeAroundUserShakeEvent extends BaseEventMsg {

    private static final long serialVersionUID = -6331737917561823499L;

    /**
     * 摇一摇时“周边”页卡展示出来的页面所对应的设备信息
     */
    private Beacon choseBeacon;

    /**
     * 附近最多五个属于该公众账号的设备的信息
     */
    private List<Beacon> aroundBeaconList;

    /**
     * 保留默认构造函数
     */
    public ShakeAroundUserShakeEvent() {
    }

    public ShakeAroundUserShakeEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public Beacon getChoseBeacon() {
        return choseBeacon;
    }

    public void setChoseBeacon(Beacon choseBeacon) {
        this.choseBeacon = choseBeacon;
    }

    public List<Beacon> getAroundBeaconList() {
        return aroundBeaconList;
    }

    public void setAroundBeaconList(List<Beacon> aroundBeaconList) {
        this.aroundBeaconList = aroundBeaconList;
    }

    /**
     * 设备信息
     */
    public static class Beacon implements Serializable {

        private static final long serialVersionUID = 3094427432051700664L;

        /**
         * uuid
         */
        private String uuid;

        /**
         * major
         */
        private String major;

        /**
         * minor
         */
        private String minor;

        private double distance;

        public Beacon() {
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getMajor() {
            return major;
        }

        public void setMajor(String major) {
            this.major = major;
        }

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }
    }
}
