/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 点击菜单跳转链接时的事件推送
 *
 * @author mzlion on 2017/1/5.
 */
public class ViewEvent extends BaseEventMsg {

    /**
     * 事件KEY值，设置的跳转URL
     */
    private String eventKey;

    /**
     * 指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。
     */
    private String menuId;

    /**
     * 保留默认构造函数
     */
    public ViewEvent() {
    }

    public ViewEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    /**
     * 事件KEY值，设置的跳转URL
     *
     * @return 事件KEY值
     */
    public String getEventKey() {
        return eventKey;
    }

    /**
     * 事件KEY值，设置的跳转URL
     *
     * @param eventKey 事件KEY值
     */
    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    /**
     * 指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。
     *
     * @return 菜单ID
     */
    public String getMenuId() {
        return menuId;
    }

    /**
     * 指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。
     *
     * @param menuId 菜单ID
     */
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }
}
