/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 买单事件推送
 *
 * @author mzlion on 2016/12/27.
 */
public class UserPayFromPayCellEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 卡券Code码
     */
    private String userCardCode;

    /**
     * 微信支付交易订单号（只有使用买单功能核销的卡券才会出现）
     */
    private String transId;

    /**
     * 门店ID，当前卡券核销的门店ID（只有通过卡券商户助手和买单核销时才会出现）
     */
    private String locationId;

    /**
     * 实付金额，单位为分
     */
    private Long fee;

    /**
     * 应付金额，单位为分
     */
    private Long originalFee;

    /**
     * 保留默认构造函数
     */
    public UserPayFromPayCellEvent() {
    }

    public UserPayFromPayCellEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public Long getFee() {
        return fee;
    }

    public void setFee(Long fee) {
        this.fee = fee;
    }

    public Long getOriginalFee() {
        return originalFee;
    }

    public void setOriginalFee(Long originalFee) {
        this.originalFee = originalFee;
    }

}
