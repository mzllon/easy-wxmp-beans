package com.mzlion.weixin.easywxmp.events;

/**
 * 会员卡内容更新事件
 * <p>当用户的会员卡积分余额发生变动时，微信会推送事件告知开发者。</p>
 *
 * @author mzlion on 2016/12/27.
 */
public class UpdateMemberCardEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 卡券Code码
     */
    private String userCardCode;

    /**
     * 变动的积分值
     */
    private Long modifyBonus;

    /**
     * 变动的余额值
     */
    private Long modifyBalance;

    /**
     * 保留默认构造函数
     */
    public UpdateMemberCardEvent() {
    }

    public UpdateMemberCardEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }

    public Long getModifyBonus() {
        return modifyBonus;
    }

    public void setModifyBonus(Long modifyBonus) {
        this.modifyBonus = modifyBonus;
    }

    public Long getModifyBalance() {
        return modifyBalance;
    }

    public void setModifyBalance(Long modifyBalance) {
        this.modifyBalance = modifyBalance;
    }
}
