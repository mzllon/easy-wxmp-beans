package com.mzlion.weixin.easywxmp.events;

/**
 * 从卡券进入公众号会话事件推送
 *
 * @author mzlion on 2016/12/27.
 */
public class UserEnterSessionFromCardEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * 卡券Code码
     */
    private String userCardCode;

    /**
     * 保留默认构造函数
     */
    public UserEnterSessionFromCardEvent() {
    }

    public UserEnterSessionFromCardEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }
}
