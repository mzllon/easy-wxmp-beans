package com.mzlion.weixin.easywxmp.events;

import com.mzlion.core.reflect.ReflectionUtils;

/**
 * 删除事件推送
 *
 * @author mzlion on 2016/12/27.
 */
public class UserDelCardEvent extends BaseEventMsg {

    /**
     * 卡券ID
     */
    private String cardId;

    /**
     * code序列号。自定义code及非自定义code的卡券被领取后都支持事件推送
     */
    private String userCardCode;

    /**
     * 保留默认构造函数
     */
    public UserDelCardEvent() {
    }

    public UserDelCardEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserCardCode() {
        return userCardCode;
    }

    public void setUserCardCode(String userCardCode) {
        this.userCardCode = userCardCode;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
