/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

/**
 * 连网后下发消息
 * <p>
 * 顾客到店并连接Wi-Fi后，会触发连网成功事件推送。开发者接收到此事件后，可以调用“客服接口”通过公众号向连网用户发送消息，包括文字、卡券等。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class WifiConnectedEvent extends BaseEventMsg {

    private static final long serialVersionUID = -8121145923188002912L;

    /**
     * 连网时间（整型）
     */
    private Long connectTime;

    /**
     * 系统保留字段，固定值
     */
    private String expireTime;

    /**
     * 系统保留字段，固定值
     */
    private String vendorId;

    /**
     * 门店ID，即shop_id
     */
    private String shopId;

    /**
     * 连网的设备无线mac地址，对应bssid
     */
    private String deviceNo;

    /**
     * 保留默认构造函数
     */
    public WifiConnectedEvent() {
    }

    public WifiConnectedEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    public Long getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(Long connectTime) {
        this.connectTime = connectTime;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }
}
