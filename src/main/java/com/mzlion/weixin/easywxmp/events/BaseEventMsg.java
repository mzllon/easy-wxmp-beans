/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;


import com.mzlion.weixin.easywxmp.constants.EventTypeEnum;
import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.InMsg;

/**
 * 事件的基础信息
 *
 * @author mzlion on 2016/12/27.
 */
public abstract class BaseEventMsg extends InMsg {

    private static final long serialVersionUID = -7526681775653636227L;

    /**
     * 事件类型
     */
    protected EventTypeEnum event;

    /**
     * 保留默认构造函数
     */
    public BaseEventMsg() {
        super();
    }

    public BaseEventMsg(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId);
        this.event = EventTypeEnum.parse(event);
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.EVENT;
    }

    public EventTypeEnum getEvent() {
        return event;
    }

    public void setEvent(EventTypeEnum event) {
        this.event = event;
    }
}
