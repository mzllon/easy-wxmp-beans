/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.events;

import com.mzlion.core.reflect.ReflectionUtils;

/**
 * 审核事件推送(卡券通过审核/卡券未通过审核)
 *
 * @author mzlion on 2017/4/19.
 */
public class CardPassCheckEvent extends AbsCardCheckEvent {

    /**
     * 审核结果，返回{@code true}则通过，否则不通过
     */
    private boolean passCheck;

    /**
     * 审核不通过原因
     */
    private String refuseReason;

    public CardPassCheckEvent() {
    }

    public boolean isPassCheck() {
        return passCheck;
    }

    public void setPassCheck(boolean passCheck) {
        this.passCheck = passCheck;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason;
    }

    public CardPassCheckEvent(String fromUserName, String toUserName, Long createTime, Long msgId, String event) {
        super(fromUserName, toUserName, createTime, msgId, event);
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
