/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 所有接口响应结果的接口，定义了所有接口通用信息
 * <p>
 * 返回-9000表示获取AccessToken失败
 * </p>
 *
 * @author mzlion on 2016/12/22.
 */
public class WxResponse extends WxObject {

    private static final long serialVersionUID = -7137737430640351882L;

    /**
     * 错误代码
     */
    @ApiField("errcode")
    protected int errCode;

    /**
     * 错误消息
     */
    @ApiField("errmsg")
    protected String errMsg;

    public WxResponse() {
    }


    /**
     * 错误代码
     * <p>返回-9000表示获取AccessToken失败</p>
     *
     * @return 错误代码
     * @see #ERR_CODE$ACCESS_TOKEN_IS_NULL
     */
    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    /**
     * 错误消息
     *
     * @return 错误消息
     */
    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }

    public boolean isSuccess() {
        return 0 == this.errCode;
    }

    /**
     * 自定义错误码：获取AccessToken失败
     */
    public static final int ERR_CODE$ACCESS_TOKEN_IS_NULL = -9000;
    public static final String ERR_MSG$ACCESS_TOKEN_IS_NULL = "获取AccessToken失败";
}
