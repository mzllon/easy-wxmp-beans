/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp;

import com.mzlion.weixin.easywxmp.constants.ApiMethodEnum;
import com.mzlion.weixin.easywxmp.constants.ApiProtocolEnum;

import java.util.Map;

/**
 * 微信请求接口描述
 *
 * @author mzlion on 2017/04/16.
 */
public abstract class WxRequest<Resp extends WxResponse> extends WxObject {

    /**
     * 接口协议，默认使用{@linkplain ApiProtocolEnum#HTTPS}协议
     *
     * @return {@linkplain ApiProtocolEnum}
     */
    public ApiProtocolEnum protocol() {
        return ApiProtocolEnum.HTTPS;
    }

    /**
     * 接口请求方式，默认应该使用{@linkplain ApiMethodEnum#POST}请求
     *
     * @return {@link ApiMethodEnum}
     */
    public ApiMethodEnum method() {
        return ApiMethodEnum.POST;
    }

    /**
     * 返回URL中携带的参数
     *
     * @return 参数
     */
    public Map<String, String> getQueryString() {
        return null;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    public abstract String serviceUrl();

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    public abstract Class<? extends Resp> responseClass();

}
