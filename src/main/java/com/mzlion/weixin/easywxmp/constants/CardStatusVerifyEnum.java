/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 卡券状态
 *
 * @author mzlion on 2017/1/3.
 */
public enum CardStatusVerifyEnum implements ApiEnum {

    /**
     * 待审核
     */
    NOT_VERIFY("CARD_STATUS_NOT_VERIFY", "待审核"),

    /**
     * 审核失败
     */
    VERIFY_FAIL("CARD_STATUS_VERIFY_FAIL", "审核失败"),

    /**
     * 通过审核
     */
    VERIFY_OK("CARD_STATUS_VERIFY_OK", "通过审核"),

    /**
     * 卡券被商户删除
     */
    DELETE("CARD_STATUS_DELETE", "卡券被商户删除"),

    /**
     * 在公众平台投放过的卡券
     */
    DISPATCH("CARD_STATUS_DISPATCH", "在公众平台投放过的卡券"),;

    /**
     * 卡券状态
     */
    private String status;

    /**
     * 卡券状态说明
     */
    private String desc;

    /**
     * 卡券状态
     *
     * @return 卡券状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 卡券状态说明
     *
     * @return 状态说明
     */
    public String getDesc() {
        return desc;
    }

    CardStatusVerifyEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.status;
    }
}
