/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 短连接操作类型
 *
 * @author mzlion on 2017/4/17.
 */
public enum ShortUrlActionEnum implements ApiEnum {

    /**
     * 长链接转短链接
     */
    LONG_2_SHORT("long2short", "长链接转短链接"),;

    private String action;
    private String desc;

    ShortUrlActionEnum(String action, String desc) {
        this.action = action;
        this.desc = desc;
    }

    public String getAction() {
        return action;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.action;
    }
}
