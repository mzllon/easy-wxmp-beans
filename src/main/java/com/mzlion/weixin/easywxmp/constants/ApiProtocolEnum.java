/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

/**
 * 接口协议
 *
 * @author mzlion on 2017/4/17.
 */
public enum ApiProtocolEnum {

    /**
     * HTTP协议
     */
    HTTP("http://", "HTTP协议"),

    /**
     * HTTPS协议
     */
    HTTPS("https://", "HTTPS协议"),;

    /**
     * 协议
     */
    private String protocol;

    /**
     * 协议描述
     */
    private String desc;

    ApiProtocolEnum(String protocol, String desc) {
        this.protocol = protocol;
        this.desc = desc;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getDesc() {
        return desc;
    }

}
