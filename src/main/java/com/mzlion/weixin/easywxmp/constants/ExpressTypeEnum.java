/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 快递类型列表
 *
 * @author mzlion on 2017/4/24.
 */
public enum ExpressTypeEnum implements ApiEnum {

    /**
     * 平邮
     */
    SURFACE_MAIL("10000027", "平邮"),

    /**
     * 快递
     */
    EXPRESS_MAIL("10000028", "快递"),

    /**
     * EMS
     */
    EMS("10000029", "EMS"),;

    /**
     * 快递ID
     */
    private String typeName;

    /**
     * 快递说明
     */
    private String typeDesc;

    ExpressTypeEnum(String typeName, String typeDesc) {
        this.typeName = typeName;
        this.typeDesc = typeDesc;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.typeName;
    }
}
