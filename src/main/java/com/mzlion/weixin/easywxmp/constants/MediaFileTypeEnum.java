/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 素材的文件格式
 *
 * @author mzlion on 2017-06-05.
 */
public enum MediaFileTypeEnum implements ApiEnum {

    IMAGE("image", "图片（image）: 2M，支持PNG\\JPEG\\JPG\\GIF格式"),
    VOICE("voice", "语音（voice）：2M，播放长度不超过60s，支持AMR\\MP3格式"),
    VIDEO("video", "  视频（video）：10MB，支持MP4格式"),
    THUMB("thumb", "缩略图（thumb）：64KB，支持JPG格式"),;

    private String type;
    private String desc;

    MediaFileTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.type;
    }
}
