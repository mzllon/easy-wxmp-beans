/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 卡券类型
 *
 * @author mzlion on 2016/12/22.
 */
public enum CardTypeEnum implements ApiEnum {

    /**
     * 团购券
     */
    GROUPON,
    /**
     * 代金券
     */
    CASH,
    /**
     * 折扣券
     */
    DISCOUNT,
    /**
     * 兑换券
     */
    GIFT,
    /**
     * 通用券
     */
    GENERAL_COUPON,
    /**
     * 会员卡
     */
    MEMBER_CARD,
    /**
     * 景点门票
     */
    SCENIC_TICKET,
    /**
     * 电影票
     */
    MOVIE_TICKET,
    /**
     * 飞机票
     */
    BOARDING_PASS,;

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.name();
    }
}
