/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 二维码类型
 *
 * @author mzlion on 2017/2/16.
 */
public enum QrActionNameEnum implements ApiEnum {

    /**
     * 临时二维码
     */
    QR_SCENE("QR_SCENE", "临时二维码"),

    /**
     * 永久二维码
     */
    QR_LIMIT_SCENE("QR_LIMIT_SCENE", "永久二维码"),

    /**
     * 永久的字符串
     */
    QR_LIMIT_STR_SCENE("QR_LIMIT_STR_SCENE", "永久的字符串"),

    /**
     * 单张卡券二维码
     */
    QR_CARD("QR_CARD", "单张卡券二维码"),

    /**
     * 多张卡券二维码
     */
    QR_MULTIPLE_CARD("QR_MULTIPLE_CARD", "多张卡券二维码"),;
    private String actionName;
    private String actionMsg;

    QrActionNameEnum(String actionName, String actionMsg) {
        this.actionName = actionName;
        this.actionMsg = actionMsg;
    }

    public String getActionName() {
        return actionName;
    }

    public String getActionMsg() {
        return actionMsg;
    }


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.actionName;
    }
}
