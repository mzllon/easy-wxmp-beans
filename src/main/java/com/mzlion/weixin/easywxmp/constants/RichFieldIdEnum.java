package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 富文本选项
 *
 * @author mzlion on 2017/4/20.
 */
public enum RichFieldIdEnum implements ApiEnum {

    /**
     * 自定义单选
     */
    RADIO("FORM_FIELD_RADIO", "自定义单选"),

    /**
     * 自定义选择项
     */
    SELECT("FORM_FIELD_SELECT", "自定义选择项"),

    /**
     * 自定义多选
     */
    CHECK_BOX("FORM_FIELD_CHECK_BOX", "自定义多选"),;

    private String fieldName;
    private String fieldDesc;

    RichFieldIdEnum(String fieldName, String fieldDesc) {
        this.fieldName = fieldName;
        this.fieldDesc = fieldDesc;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldDesc() {
        return fieldDesc;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return null;
    }
}
