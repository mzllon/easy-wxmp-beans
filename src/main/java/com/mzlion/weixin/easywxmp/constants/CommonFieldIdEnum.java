/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 微信固定的卡券激活选项
 *
 * @author mzlin on 2017/3/22.
 */
public enum CommonFieldIdEnum implements ApiEnum {

    /**
     * 手机号
     */
    MOBILE("USER_FORM_INFO_FLAG_MOBILE", "手机号"),

    /**
     * 性别
     */
    SEX("USER_FORM_INFO_FLAG_SEX", "性别"),

    /**
     * 姓名
     */
    NAME("USER_FORM_INFO_FLAG_NAME", "姓名"),

    /**
     * 生日
     */
    BIRTHDAY("USER_FORM_INFO_FLAG_BIRTHDAY", "生日"),

    /**
     * 身份证
     */
    ID_CARD("USER_FORM_INFO_FLAG_IDCARD", "身份证"),

    /**
     *
     */
    EMAIL("USER_FORM_INFO_FLAG_EMAIL", "邮箱"),
    LOCATION("USER_FORM_INFO_FLAG_LOCATION", "详细地址"),
    EDUCATION_BG("USER_FORM_INFO_FLAG_EDUCATION_BACKGRO", "教育背景"),
    INDUSTRY("USER_FORM_INFO_FLAG_INDUSTRY", "行业"),
    INCOME("USER_FORM_INFO_FLAG_INCOME", "收入"),
    HABIT("USER_FORM_INFO_FLAG_HABIT", "兴趣爱好"),;

    private String fieldName;
    private String fieldDesc;

    CommonFieldIdEnum(String fieldName, String fieldDesc) {
        this.fieldName = fieldName;
        this.fieldDesc = fieldDesc;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldDesc() {
        return fieldDesc;
    }


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.fieldName;
    }

    /**
     * 增加一个通过{@code fieldName}解析
     *
     * @param fieldName 字段名
     * @return {@linkplain CommonFieldIdEnum}
     */
    public static CommonFieldIdEnum parse(String fieldName) {
        for (CommonFieldIdEnum commonFieldIdEnum : CommonFieldIdEnum.values()) {
            if (commonFieldIdEnum.getFieldName().equals(fieldName)) {
                return commonFieldIdEnum;
            }
        }
        return null;
    }
}
