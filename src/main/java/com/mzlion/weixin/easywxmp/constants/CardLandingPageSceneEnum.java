/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 货架投放页面的场景值
 *
 * @author mzlion on 2017/4/17.
 */
public enum CardLandingPageSceneEnum implements ApiEnum {

    /**
     * 附近
     */
    SCENE_NEAR_BY,

    /**
     * 自定义菜单
     */
    SCENE_MENU,
    /**
     * 二维码
     */
    SCENE_QRCODE,

    /**
     * 公众号文章
     */
    SCENE_ARTICLE,

    /**
     * h5页面
     */
    SCENE_H5,

    /**
     * 自动回复
     */
    SCENE_IVR,

    /**
     * 卡券自定义cell
     */
    SCENE_CARD_CUSTOM_CELL;


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.name();
    }
}
