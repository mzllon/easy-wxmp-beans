/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 微信小店，商品状态码
 *
 * @author mzlion on 2017/4/24.
 */
public enum MerchantProductStatusEnum implements ApiEnum {

    /**
     * 全部
     */
    ALL(0, "全部"),

    /**
     * 上架
     */
    ON_SHELVES(1, "上架"),

    /**
     * 下架
     */
    OFF_SHELVES(2, "下架"),;

    /**
     * 状态值
     */
    private int value;

    /**
     * 状态描述
     */
    private String desc;

    MerchantProductStatusEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public int getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return String.valueOf(this.value);
    }
}
