/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

/**
 * 微信发出的事件类型
 *
 * @author mzlion on 2017/4/16.
 */
public enum EventTypeEnum {

    /**
     * 卡券审核事件推送
     */
    CARD_PASS_CHECK("card_pass_check", "卡券审核事件推送"),

    /**
     * 卡券领取事件推送
     */
    USER_GET_CARD("user_get_card", "卡券领取事件推送"),

    /**
     * 卡券转赠事件推送
     */
    USER_GIFTING_CARD("user_gifting_card", "卡券转赠事件推送"),

    /**
     * 卡券删除事件推送
     */
    USER_DEL_CARD("user_del_card", "卡券删除事件推送"),

    /**
     * 卡券核销事件推送
     */
    USER_CONSUME_CARD("user_consume_card", "卡券核销事件推送"),

    /**
     * 卡券买单事件推送
     */
    USER_PAY_FROM_PAY_CELL("user_pay_from_pay_cell", "卡券买单事件推送"),

    /**
     * 用户进入会员卡事件推送
     */
    USER_VIEW_CARD("user_view_card", "用户进入会员卡事件推送"),

    /**
     * 从卡券进入公众号会话事件推送
     */
    USER_ENTER_SESSION_FROM_CARD("user_enter_session_from_card", "从卡券进入公众号会话事件推送"),

    /**
     * 会员卡内容更新事件
     */
    UPDATE_MEMBER_CARD("update_member_card", "会员卡内容更新事件"),

    /**
     * 库存报警事件
     */
    CARD_SKU_REMIND("card_sku_remind", "库存报警事件"),

    /**
     * 用户进入会员卡事件推送
     */
    CARD_PAY_ORDER("card_pay_order", "用户进入会员卡事件推送"),

    /**
     * 会员卡激活事件推送
     */
    SUBMIT_MEMBER_CARD_USER_INFO("submit_membercard_user_info", "会员卡激活事件推送"),

    /**
     * 子商户审核事件推送
     */
    CARD_MERCHANT_CHECK_RESULT("card_merchant_check_result", "子商户审核事件推送"),

    /**
     * 点击菜单跳转链接时的事件推送
     */
    VIEW("VIEW", "点击菜单跳转链接时的事件推送"),

    /**
     * 点击菜单拉取消息时的事件推送
     */
    CLICK("CLICK", "点击菜单拉取消息时的事件推送"),

    /**
     * 扫码推事件且弹出“消息接收中”提示框的事件推送
     */
    SCAN_CODE_WAIT_MSG("scancode_waitmsg", "扫码推事件且弹出“消息接收中”提示框的事件推送"),

    /**
     * 弹出系统拍照发图的事件推送
     */
    PIC_SYS_PHOTO("pic_sysphoto", "弹出系统拍照发图的事件推送"),

    /**
     * 弹出拍照或者相册发图的事件推送
     */
    SCAN_CODE_PUSH("scancode_push", "弹出拍照或者相册发图的事件推送"),

    /**
     * 扫码推事件的事件推送
     */
    PIC_PHOTO_OR_ALBUM("pic_photo_or_album", "扫码推事件的事件推送"),

    /**
     * 弹出微信相册发图器的事件推送
     */
    PIC_WEI_XIN("pic_weixin", "弹出微信相册发图器的事件推送"),

    /**
     * 弹出地理位置选择器的事件推送
     */
    LOCATION_SELECT("location_select", "弹出地理位置选择器的事件推送"),

    /**
     * 模版消息发送任务事件推送
     */
    TEMPLATE_SEND_JOB_FINISH("TEMPLATESENDJOBFINISH", "模版消息发送任务事件推送"),

    /**
     * 关注公众号事件
     */
    SUBSCRIBE("subscribe", "关注公众号事件"),

    /**
     * 取消关注公众号事件
     */
    UNSUBSCRIBE("unsubscribe", "取消关注公众号事件"),

    /**
     * 扫描带参数二维码事件
     */
    SCAN("SCAN", "扫描带参数二维码事件"),

    /**
     * 上报地理位置事件
     */
    LOCATION("LOCATION", "上报地理位置事件"),

    ;

    /**
     * 事件名
     */
    private String eventName;

    /**
     * 事件描述
     */
    private String eventDesc;

    EventTypeEnum(String eventName, String eventDesc) {
        this.eventName = eventName;
        this.eventDesc = eventDesc;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public static EventTypeEnum parse(String eventName) {
        for (EventTypeEnum eventTypeEnum : EventTypeEnum.values()) {
            if (eventTypeEnum.getEventName().equals(eventName)) {
                return eventTypeEnum;
            }
        }
        throw new IllegalArgumentException("The eventName [" + eventName + "] cannot find!");
    }
}
