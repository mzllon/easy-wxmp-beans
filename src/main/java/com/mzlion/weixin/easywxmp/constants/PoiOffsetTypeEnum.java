/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 门店坐标类型
 *
 * @author mzlion on 2017/1/5.
 */
public enum PoiOffsetTypeEnum implements ApiEnum {

    /**
     * 未知坐标类型
     */
    UNKNOWN_COORDINATES(-1, "未知坐标类型"),

    /**
     * 火星坐标
     */
    MARS_COORDINATES(1, "火星坐标"),

    /**
     * sogou经纬度
     */
    SOGOU_COORDINATES(2, "sogou经纬度"),

    /**
     * 百度经纬度
     */
    BAIDU_COORDINATES(3, "百度经纬度"),

    /**
     * mapbar经纬度
     */
    MAPBAR_COORDINATES(4, "mapbar经纬度"),
    /**
     * GPS坐标
     */
    GPS_COORDINATES(5, "GPS坐标"),
    /**
     * sogou墨卡托坐标
     */
    SOGOU_MERCATOR_COORDINATES(6, "sogou墨卡托坐标"),;

    /**
     * 门店坐标类型
     */
    private int offsetType;

    /**
     * 说明
     */
    private String desc;

    /**
     * 门店坐标类型
     */
    public int getOffsetType() {
        return offsetType;
    }

    /**
     * 说明
     */
    public String getDesc() {
        return desc;
    }

    PoiOffsetTypeEnum(int offsetType, String desc) {
        this.offsetType = offsetType;
        this.desc = desc;
    }

    public static PoiOffsetTypeEnum parse(int offsetType) {
        for (PoiOffsetTypeEnum poiOffsetTypeEnum : PoiOffsetTypeEnum.values()) {
            if (poiOffsetTypeEnum.offsetType == offsetType) return poiOffsetTypeEnum;
        }
        return PoiOffsetTypeEnum.UNKNOWN_COORDINATES;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return String.valueOf(this.offsetType);
    }
}
