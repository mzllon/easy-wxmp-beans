/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 用户卡券状态
 *
 * @author mzlion on 2017/4/17.
 */
public enum UserCardStatusEnum implements ApiEnum {

    /**
     * 正常
     */
    NORMAL,

    /**
     * 已核销
     */
    CONSUMED,

    /**
     * 已过期
     */
    EXPIRE,

    /**
     * 转赠中
     */
    GIFTING,

    /**
     * 转赠超时
     */
    GIFT_TIMEOUT,

    /**
     * 已删除
     */
    DELETE,

    /**
     * 已失效
     */
    UNAVAILABLE,;


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.name();
    }
}
