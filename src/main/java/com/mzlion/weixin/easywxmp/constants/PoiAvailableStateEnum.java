/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 门店可用状态
 *
 * @author mzlion on 2017/4/23.
 */
public enum PoiAvailableStateEnum implements ApiEnum {

    /**
     * 系统错误
     */
    ERROR(1, "系统错误"),

    /**
     * 审核中
     */
    CHECKING(2, "审核中"),

    /**
     * 审核通过
     */
    APPROVED(3, "审核通过"),

    /**
     * 审核驳回
     */
    REJECTED(4, "审核驳回"),;
    /**
     * 门店可用状态值
     */
    private int state;

    /**
     * 门店可用状态说明
     */
    private String desc;

    PoiAvailableStateEnum(int state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    public int getState() {
        return state;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return String.valueOf(this.state);
    }
}
