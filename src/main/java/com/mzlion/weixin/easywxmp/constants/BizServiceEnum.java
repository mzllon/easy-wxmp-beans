/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 商家服务类型
 *
 * @author mzlion on 2017/3/10.
 */
public enum BizServiceEnum implements ApiEnum {

    /**
     * 外卖服务
     */
    BIZ_SERVICE_DELIVER("BIZ_SERVICE_DELIVER", "外卖服务"),

    /**
     * 停车位
     */
    BIZ_SERVICE_FREE_PARK("BIZ_SERVICE_FREE_PARK", "停车位"),

    /**
     * 可带宠物
     */
    BIZ_SERVICE_WITH_PET("BIZ_SERVICE_WITH_PET", "可带宠物"),

    /**
     * 免费wifi
     */
    BIZ_SERVICE_FREE_WIFI("BIZ_SERVICE_FREE_WIFI", "免费WIFI"),;

    private String bizService;
    private String desc;

    public String getBizService() {
        return bizService;
    }

    public String getDesc() {
        return desc;
    }

    BizServiceEnum(String bizService, String desc) {
        this.bizService = bizService;
        this.desc = desc;
    }


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.bizService;
    }
}
