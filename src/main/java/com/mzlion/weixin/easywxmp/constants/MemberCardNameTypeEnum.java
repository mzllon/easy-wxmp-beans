package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 会员卡预定义类目，这些类目会触发系统系统模板消息
 *
 * @author mzlion on 2017/3/12.
 */
public enum MemberCardNameTypeEnum implements ApiEnum {

    /**
     * 等级
     */
    LEVEL("FIELD_NAME_TYPE_LEVEL", "等级"),


    COUPON("FIELD_NAME_TYPE_COUPON", "优惠券"),
    STAMP("FIELD_NAME_TYPE_STAMP", "印花"),
    DISCOUNT("FIELD_NAME_TYPE_DISCOUNT", "折扣"),
    ACHIEVEMENT("FIELD_NAME_TYPE_ACHIEVEMEN", "成就"),
    MILEAGE("FIELD_NAME_TYPE_MILEAGE", "里程"),
    SET_POINTS("FIELD_NAME_TYPE_SET_POINT", "集点"),
    TIMES("FIELD_NAME_TYPE_TIMS", "次数"),

    NONE("", ""),//清空
    ;


    private String nameType;
    private String nameDesc;

    MemberCardNameTypeEnum(String nameType, String nameDesc) {
        this.nameType = nameType;
        this.nameDesc = nameDesc;
    }

    public String getNameType() {
        return nameType;
    }

    public String getNameDesc() {
        return nameDesc;
    }


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.nameType;
    }
}
