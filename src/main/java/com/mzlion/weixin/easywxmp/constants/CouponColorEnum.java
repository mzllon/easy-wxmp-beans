/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 卡券背景色
 *
 * @author mzlion on 2017/4/24.
 */
public enum CouponColorEnum implements ApiEnum {
    COLOR010("Color010", "#63b359", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR020("Color020", "#2c9f67", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR030("Color030", "#509fc9", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR040("Color040", "#5885cf", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR050("Color050", "#9062c0", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR060("Color060", "#d09a45", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR070("Color070", "#e4b138", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR080("Color080", "#ee903c", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR081("Color081", "#f08500", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR082("Color082", "#a9d92d", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR090("Color090", "#dd6549", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR100("Color100", "#cc463d", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR101("Color101", "#cf3e36", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),
    COLOR102("Color102", "#5E6671", "http://mmbiz.qpic.cn/mmbiz/PiajxSqBRaEIQxibpLbyuSK8jCzLMb60WcvO4beTEEmdDcCBOHtpYAH8oh7BpRrYU6qeCLXjAPBIe4LicZ3FvO6lw/0?wx_fmt=png"),;

    CouponColorEnum(String colorName, String colorValue, String desc) {
        this.colorName = colorName;
        this.colorValue = colorValue;
        this.desc = desc;
    }

    /**
     * 卡券背景色名称
     */
    private String colorName;

    /**
     * 色值
     */
    private String colorValue;

    /**
     * 描述
     */
    private String desc;

    public String getColorName() {
        return colorName;
    }

    public String getColorValue() {
        return colorValue;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 根据背景色名称解析对应的枚举实例
     *
     * @param colorName 背景色名称
     * @return {@linkplain CouponColorEnum}
     */
    public static CouponColorEnum parse(String colorName) {
        for (CouponColorEnum couponColorEnum : CouponColorEnum.values()) {
            if (couponColorEnum.colorName.equals(colorName)) return couponColorEnum;
        }
        return null;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.colorName;
    }
}
