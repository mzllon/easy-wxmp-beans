/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 物流公司列表
 *
 * @author mzlion on 2017/4/24.
 */
public enum DeliveryCompanyEnum implements ApiEnum {

    /**
     * 邮政EMS
     */
    EMS("Fsearch_code", "邮政EMS"),

    /**
     * 申通快递
     */
    SHEN_TONG_EXPRESS("002shentong", "申通快递"),

    /**
     * 中通速递
     */
    ZHONG_TONG_EXPRESS("066zhongtong", "中通速递"),

    /**
     * 圆通速递
     */
    YUAN_TONG_EXPRESS("056yuantong", "圆通速递"),

    /**
     * 天天快递
     */
    TIAN_TIAN_EXPRESS("042tiantian", "天天快递"),;

    private String deliveryId;
    private String deliveryName;

    DeliveryCompanyEnum(String deliveryId, String deliveryName) {
        this.deliveryId = deliveryId;
        this.deliveryName = deliveryName;
    }

    public String getDeliveryId() {
        return deliveryId;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.deliveryId;
    }
}
