/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 消息类型
 *
 * @author mzlion on 2016/12/26.
 */
public enum MsgTypeEnum implements ApiEnum {

    /**
     * 文本消息
     */
    TEXT("text", "文本消息"),

    /**
     * 图片消息
     */
    IMAGE("image", "图片消息"),

    /**
     * 语音消息
     */
    VOICE("voice", "语音消息"),

    /**
     * 视频消息
     */
    VIDEO("video", "视频消息"),

    /**
     * 小视频消息
     */
    SHORT_VIDEO("shortvideo", "小视频消息"),

    /**
     * 地理位置消息
     */
    LOCATION("location", "地理位置消息"),

    /**
     * 链接消息
     */
    LINK("link", "链接消息"),

    /**
     * 事件消息
     */
    EVENT("event", "事件消息"),

    /**
     * 音乐消息
     */
    MUSIC("music", "音乐消息"),

    /**
     * 图文消息（点击跳转到外链）
     */
    NEWS("news", "外链消息"),

    /**
     * 图文消息（点击跳转到图文消息页面）
     */
    MP_NEWS("mpnews", "图文消息"),

    /**
     * 卡券消息
     */
    WX_CARD("wxcard", "卡券消息"),

    /**
     * 未知消息类型
     */
    UNKOWN("", "未知消息类型"),

    /**
     * 商品扫码结果
     */
    SCAN_PRODUCT("scanproduct", "商品扫码结果");

    private String msgType;

    private String msgDesc;

    MsgTypeEnum(String msgType, String msgDesc) {
        this.msgDesc = msgDesc;
        this.msgType = msgType;
    }

    public String getMsgType() {
        return msgType;
    }

    public String getMsgDesc() {
        return msgDesc;
    }

    /**
     * 根据消息类型值解析为对应的枚举
     *
     * @param msgType 消息类型值
     * @return {@linkplain MsgTypeEnum}
     */
    public static MsgTypeEnum parse(String msgType) {
        if (msgType == null) return UNKOWN;
        for (MsgTypeEnum mt : MsgTypeEnum.values()) {
            if (mt.getMsgType().equals(msgType)) {
                return mt;
            }
        }
        return UNKOWN;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.msgType;
    }
}
