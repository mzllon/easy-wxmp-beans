/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * Code展示类型
 *
 * @author mzlion on 2016/12/29.
 */
public enum CardCodeTypeEnum implements ApiEnum {

    /**
     * 文本
     */
    TEXT("CODE_TYPE_TEXT", "文本"),
    /**
     * 一维码
     */
    BARCODE("CODE_TYPE_BARCODE", "一维码"),
    /**
     * 二维码
     */
    QRCODE("CODE_TYPE_QRCODE", "二维码"),

    /**
     * 仅显示二维码
     */
    ONLY_QRCODE("CODE_TYPE_ONLY_QRCODE", "仅显示二维码"),
    /**
     * 仅显示一维码
     */
    ONLY_BARCODE("CODE_TYPE_ONLY_BARCODE", "仅显示一维码"),
    /**
     * 不显示任何码型
     */
    NONE("CODE_TYPE_NONE", "不显示任何码型"),;

    /**
     * Code展示类型
     */
    private String type;

    /**
     * 说明
     */
    private String desc;

    CardCodeTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    /**
     * Code展示类型
     *
     * @return Code展示类型
     */
    public String getType() {
        return type;
    }

    /**
     * 说明
     *
     * @return Code说明
     */
    public String getDesc() {
        return desc;
    }


    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.type;
    }

    /**
     * 转为指定的CodeType枚举
     *
     * @param codeType 按钮类型
     * @return {@linkplain CardCodeTypeEnum}
     */
    public static CardCodeTypeEnum parse(String codeType) {
        for (CardCodeTypeEnum cardCodeTypeEnum : CardCodeTypeEnum.values()) {
            if (cardCodeTypeEnum.type.equals(codeType)) {
                return cardCodeTypeEnum;
            }
        }
        return null;
    }

}
