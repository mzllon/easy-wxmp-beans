/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.constants;

import com.mzlion.weixin.easywxmp.support.mapping.ApiEnum;

/**
 * 子商户审核状态
 *
 * @author mzlion on 2017/1/16.
 */
public enum CardSubMerchantStatusEnum implements ApiEnum {

    /**
     * 审核中
     */
    CHECKING("CHECKING", "审核中"),

    /**
     * 已通过
     */
    APPROVED("APPROVED", "已通过"),

    /**
     * 被驳回
     */
    REJECTED("REJECTED", "被驳回"),

    /**
     * 协议已过期
     */
    EXPIRED("EXPIRED", "协议已过期"),;

    private String status;

    private String msg;

    CardSubMerchantStatusEnum(String status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    /**
     * 获取枚举对应的值
     *
     * @return 转换后的值
     */
    @Override
    public String getEnumValue() {
        return this.status;
    }
}
