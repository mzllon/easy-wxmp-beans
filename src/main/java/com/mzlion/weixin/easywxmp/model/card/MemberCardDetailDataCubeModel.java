/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 单张会员卡数据统计
 *
 * @author mzlion on 2017/4/19.
 */
public class MemberCardDetailDataCubeModel extends MemberCardSummaryDataCubeModel {

    /**
     * 激活人数
     */
    @ApiField("merchanttype")
    private String merchantType;

    /**
     * 子商户ID
     */
    @ApiField("submerchantid")
    private String subMerchantId;

    /**
     * 新用户数
     */
    @ApiField("new_user")
    private int newUser;

    /**
     * 应收金额（仅限使用快速买单的会员卡）
     */
    private long payOriginalFee;

    /**
     * 实收金额（仅限使用快速买单的会员卡）
     */
    private long fee;

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public String getSubMerchantId() {
        return subMerchantId;
    }

    public void setSubMerchantId(String subMerchantId) {
        this.subMerchantId = subMerchantId;
    }

    public int getNewUser() {
        return newUser;
    }

    public void setNewUser(int newUser) {
        this.newUser = newUser;
    }

    public long getPayOriginalFee() {
        return payOriginalFee;
    }

    public void setPayOriginalFee(long payOriginalFee) {
        this.payOriginalFee = payOriginalFee;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }
}
