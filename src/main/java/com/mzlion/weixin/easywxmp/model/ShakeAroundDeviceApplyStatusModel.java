package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 摇一摇设备审核结果
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceApplyStatusModel extends WxObject {

    private static final long serialVersionUID = -1914603024370110662L;

    /**
     * 申请的批次ID，可用在“查询设备列表”接口按批次查询本次申请成功的设备ID。
     */
    @ApiField("apply_id")
    private String applyId;

    /**
     * 提交申请的时间戳
     */
    @ApiField("apply_time")
    private Long applyTime;

    /**
     * 审核状态。
     * 0：审核未通过、
     * 1：审核中、
     * 2：审核已通过；
     * 若单次申请的设备ID数量小于等于500个，系统会进行快速审核；
     * 若单次申请的设备ID数量大于500个，会在三个工作日内完成审核；
     */
    @ApiField("audit_status")
    private String auditStatus;

    /**
     * 审核备注，对审核状态的文字说明
     */
    @ApiField("audit_comment")
    private String auditComment;

    /**
     * 确定审核结果的时间戳，若状态为审核中，则该时间值为0
     */
    @ApiField("audit_time")
    private Long auditTime;

    public ShakeAroundDeviceApplyStatusModel() {
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    public Long getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Long applyTime) {
        this.applyTime = applyTime;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditComment() {
        return auditComment;
    }

    public void setAuditComment(String auditComment) {
        this.auditComment = auditComment;
    }

    public Long getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Long auditTime) {
        this.auditTime = auditTime;
    }
}
