package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * WIFI门店信息
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiShopModel extends WxObject {

    private static final long serialVersionUID = 6816804246898817403L;

    /**
     * 门店ID（适用于微信连Wi-Fi业务）
     */
    @ApiField("shop_id")
    private String shopId;

    /**
     * 门店名称
     */
    @ApiField("shop_name")
    private String shopName;

    /**
     * 无线网络设备的ssid，未添加设备为空，多个ssid时显示第一个
     */
    private String ssid;

    /**
     * 设备密码，当设备类型为密码型时返回
     */
    private String password;

    /**
     * 无线网络设备的ssid列表，返回数组格式
     */
    @ApiField("ssid_list")
    private List<String> ssidList;

    /**
     * ssid和密码的列表，数组格式。当为密码型设备时，密码才有值
     */
    @ApiField("ssid_password_list")
    private List<Ssid> ssidPasswordList;

    /**
     * 门店内设备的设备类型，
     * 0-未添加设备，
     * 1-专业型设备，
     * 4-密码型设备，
     * 5-portal自助型设备，
     * 31-portal改造型设备
     */
    @ApiField("protocol_type")
    private String protocolType;

    /**
     * 门店内设备总数
     */
    @ApiField("ap_count")
    private int apCount;

    /**
     * 商家主页模板类型
     */
    @ApiField("template_id")
    private String templateId;

    /**
     * 商家主页链接
     */
    @ApiField("homepage_url")
    private String homepageUrl;

    /**
     * 顶部常驻入口上显示的文本内容：
     * 0--欢迎光临+公众号名称；
     * 1--欢迎光临+门店名称；
     * 2--已连接+公众号名称+WiFi；
     * 3--已连接+门店名称+Wi-Fi
     */
    @ApiField("bar_type")
    private String barType;

    /**
     * 连网完成页链接
     */
    @ApiField("finishpage_url")
    private String finishPageUrl;

    /**
     * 商户自己的id，与门店poi_id对应关系，建议在添加门店时候建立关联关系，具体请参考“微信门店接口”
     */
    private String sid;

    /**
     * 门店ID（适用于微信卡券、微信门店业务），具体定义参考微信门店，与shop_id一一对应
     */
    @ApiField("poi_id")
    private List<String> poiId;

    public BizWifiShopModel() {
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public List<String> getSsidList() {
        return ssidList;
    }

    public void setSsidList(List<String> ssidList) {
        this.ssidList = ssidList;
    }

    public String getProtocolType() {
        return protocolType;
    }

    public void setProtocolType(String protocolType) {
        this.protocolType = protocolType;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public List<String> getPoiId() {
        return poiId;
    }

    public void setPoiId(List<String> poiId) {
        this.poiId = poiId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Ssid> getSsidPasswordList() {
        return ssidPasswordList;
    }

    public void setSsidPasswordList(List<Ssid> ssidPasswordList) {
        this.ssidPasswordList = ssidPasswordList;
    }

    public int getApCount() {
        return apCount;
    }

    public void setApCount(int apCount) {
        this.apCount = apCount;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getHomepageUrl() {
        return homepageUrl;
    }

    public void setHomepageUrl(String homepageUrl) {
        this.homepageUrl = homepageUrl;
    }

    public String getBarType() {
        return barType;
    }

    public void setBarType(String barType) {
        this.barType = barType;
    }

    public String getFinishPageUrl() {
        return finishPageUrl;
    }

    public void setFinishPageUrl(String finishPageUrl) {
        this.finishPageUrl = finishPageUrl;
    }

    /**
     * 无线网络设备
     */
    public static class Ssid implements Serializable {

        private static final long serialVersionUID = 7663594340276801583L;

        /**
         * 无线网络设备的ssid
         */
        private String ssid;

        /**
         * 设备密码
         */
        private String password;

        public Ssid() {
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
