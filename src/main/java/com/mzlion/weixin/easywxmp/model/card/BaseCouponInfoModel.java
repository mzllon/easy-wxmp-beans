/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.constants.CardCodeTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 卡券基础信息字段（重要）
 *
 * @author mzlion on 2016/12/22.
 */
public class BaseCouponInfoModel extends WxObject {

    //region=============== base_info（卡券基础信息）字段-必填字段===============
    /**
     * 卡券的商户logo，建议像素为300*300。
     */
    @ApiField("logo_url")
    private String logoUrl;

    /**
     * 码型
     */
    @ApiField("code_type")
    private CardCodeTypeEnum codeType;
    /**
     * 商户名字,字数上限为12个汉字。
     */
    @ApiField("brand_name")
    private String brandName;

    /**
     * 卡券名，字数上限为9个汉字。(建议涵盖卡券属性、服务及金额)。
     */
    private String title;

    /**
     * 券颜色。按色彩规范标注填写Color010-Color100。
     */
    private String color;

    /**
     * 卡券使用提醒，字数上限为16个汉字
     */
    private String notice;

    /**
     * 卡券使用说明，字数上限为1024个汉字。
     */
    private String description;

    /**
     * 商品信息。
     */
    private QuantityWrapper sku;

    /**
     * 使用日期，有效期的信息。
     */
    @ApiField("date_info")
    private DateInfoWrapper dateInfo;
    //endregion=============== base_info（卡券基础信息）字段-必填字段===============


    //region=============== base_info（卡券基础信息）字段-非必填字段===============
    /**
     * 是否自定义Code码。填写true或false，默认为false。通常自有优惠码系统的开发者选择自定义Code码，并在卡券投放时带入Code码
     */
    @ApiField("use_custom_code")
    private Boolean useCustomCode;

    /**
     * 填入GET_CUSTOM_CODE_MODE_DEPOSIT表示该卡券为预存code模式卡券，
     * 须导入超过库存数目的自定义code后方可投放，填入该字段后，quantity字段须为0,须导入code后再增加库存
     */
    @ApiField("get_custom_code_mode")
    private String getCustomCodeMode;

    /**
     * 是否指定用户领取，填写true或false，默认为false。
     * 通常指定特殊用户群体投放卡券或防止刷券时选择指定用户领取。
     */
    @ApiField("bind_openid")
    private Boolean bindOpenId;

    /**
     * 客服电话。
     */
    @ApiField("service_phone")
    private String servicePhone;

    /**
     * 门店位置poiid
     */
    @ApiField("location_id_list")
    private List<String> locationIdList;

    /**
     * 设置本卡券支持全部门店，与location_id_list互斥
     */
    @ApiField("use_all_locations")
    private Boolean useAllLocations;

    /**
     * 第三方来源名，例如同程旅游、大众点评。
     */
    private String source;

    /**
     * 自定义跳转外链的入口名字
     */
    @ApiField("custom_url_name")
    private String customUrlName;

    /**
     * 自定义跳转的URL。
     */
    @ApiField("custom_url")
    private String customUrl;

    /**
     * 显示在入口右侧的提示语。
     */
    @ApiField("custom_url_sub_title")
    private String customUrlSubTitle;

    /**
     * 卡券顶部居中的按钮，仅在卡券状态正常(可以核销)时显示
     */
    @ApiField("center_title")
    private String centerTitle;

    /**
     * 显示在入口下方的提示语，仅在卡券状态正常(可以核销)时显示。
     */
    @ApiField("center_sub_title")
    private String centerSubTitle;

    /**
     * 顶部居中的url，仅在卡券状态正常(可以核销)时显示。
     */
    @ApiField("center_url")
    private String centerUrl;

    /**
     * 营销场景的自定义入口名称。
     */
    @ApiField("promotion_url_name")
    private String promotionUrlName;

    /**
     * 入口跳转外链的地址链接。
     */
    @ApiField("promotion_url")
    private String promotionUrl;

    /**
     * 显示在营销入口右侧的提示语。
     */
    @ApiField("promotion_url_sub_title")
    private String promotionUrlSubTitle;

    /**
     * 每人可领券的数量限制,不填写默认为50。
     */
    @ApiField("get_limit")
    private Integer getLimit = 1;

    /**
     * 卡券领取页面是否可分享。
     */
    @ApiField("can_share")
    private Boolean canShare;

    /**
     * 卡券是否可转赠。
     */
    @ApiField("can_give_friend")
    private Boolean canGiveFriend;

    /**
     * 填写true为用户点击进入会员卡时推送事件，默认为false。详情见进入会员卡事件推送
     */
    @ApiField("need_push_on_view")
    private Boolean needPushOnView;
    //endregion=============== base_info（卡券基础信息）字段-非必填字段===============


    //region========================创建子商户卡券接口========================

    @ApiField("sub_merchant_info")
    private SubMerchantInfo subMerchantInfo;

    //endregion

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public CardCodeTypeEnum getCodeType() {
        return codeType;
    }

    public void setCodeType(CardCodeTypeEnum codeType) {
        this.codeType = codeType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public QuantityWrapper getSku() {
        return sku;
    }

    public void setSku(QuantityWrapper sku) {
        this.sku = sku;
    }

    public DateInfoWrapper getDateInfo() {
        return dateInfo;
    }

    public void setDateInfo(DateInfoWrapper dateInfo) {
        this.dateInfo = dateInfo;
    }

    public Boolean getUseCustomCode() {
        return useCustomCode;
    }

    public void setUseCustomCode(Boolean useCustomCode) {
        this.useCustomCode = useCustomCode;
    }

    public String getGetCustomCodeMode() {
        return getCustomCodeMode;
    }

    public void setGetCustomCodeMode(String getCustomCodeMode) {
        this.getCustomCodeMode = getCustomCodeMode;
    }

    public Boolean getBindOpenId() {
        return bindOpenId;
    }

    public void setBindOpenId(Boolean bindOpenId) {
        this.bindOpenId = bindOpenId;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    public List<String> getLocationIdList() {
        return locationIdList;
    }

    public void setLocationIdList(List<String> locationIdList) {
        this.locationIdList = locationIdList;
    }

    public Boolean getUseAllLocations() {
        return useAllLocations;
    }

    public void setUseAllLocations(Boolean useAllLocations) {
        this.useAllLocations = useAllLocations;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCustomUrlName() {
        return customUrlName;
    }

    public void setCustomUrlName(String customUrlName) {
        this.customUrlName = customUrlName;
    }

    public String getCustomUrl() {
        return customUrl;
    }

    public void setCustomUrl(String customUrl) {
        this.customUrl = customUrl;
    }

    public String getCustomUrlSubTitle() {
        return customUrlSubTitle;
    }

    public void setCustomUrlSubTitle(String customUrlSubTitle) {
        this.customUrlSubTitle = customUrlSubTitle;
    }

    public String getCenterTitle() {
        return centerTitle;
    }

    public void setCenterTitle(String centerTitle) {
        this.centerTitle = centerTitle;
    }

    public String getCenterSubTitle() {
        return centerSubTitle;
    }

    public void setCenterSubTitle(String centerSubTitle) {
        this.centerSubTitle = centerSubTitle;
    }

    public String getCenterUrl() {
        return centerUrl;
    }

    public void setCenterUrl(String centerUrl) {
        this.centerUrl = centerUrl;
    }

    public String getPromotionUrlName() {
        return promotionUrlName;
    }

    public void setPromotionUrlName(String promotionUrlName) {
        this.promotionUrlName = promotionUrlName;
    }

    public String getPromotionUrl() {
        return promotionUrl;
    }

    public void setPromotionUrl(String promotionUrl) {
        this.promotionUrl = promotionUrl;
    }

    public String getPromotionUrlSubTitle() {
        return promotionUrlSubTitle;
    }

    public void setPromotionUrlSubTitle(String promotionUrlSubTitle) {
        this.promotionUrlSubTitle = promotionUrlSubTitle;
    }

    public Integer getGetLimit() {
        return getLimit;
    }

    public void setGetLimit(Integer getLimit) {
        this.getLimit = getLimit;
    }

    public Boolean getCanShare() {
        return canShare;
    }

    public void setCanShare(Boolean canShare) {
        this.canShare = canShare;
    }

    public Boolean getCanGiveFriend() {
        return canGiveFriend;
    }

    public void setCanGiveFriend(Boolean canGiveFriend) {
        this.canGiveFriend = canGiveFriend;
    }

    public Boolean getNeedPushOnView() {
        return needPushOnView;
    }

    public void setNeedPushOnView(Boolean needPushOnView) {
        this.needPushOnView = needPushOnView;
    }

    public SubMerchantInfo getSubMerchantInfo() {
        return subMerchantInfo;
    }

    public void setSubMerchantInfo(SubMerchantInfo subMerchantInfo) {
        this.subMerchantInfo = subMerchantInfo;
    }


    /**
     * 卡券的数量
     */
    public static class QuantityWrapper implements Serializable {

        /**
         * 数量
         */
        private int quantity;

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public QuantityWrapper() {
        }

        public QuantityWrapper(int quantity) {
            this.quantity = quantity;
        }
    }

    /**
     * 使用日期包装类
     */
    public static class DateInfoWrapper implements Serializable {

        /**
         * 使用时间的类型
         */
        private String type;

        /**
         * type为DATE_TYPE_FIX_TIME_RANGE时专用，表示起用时间。
         * 从1970年1月1日00:00:00至起用时间的秒数，最终需转换为字符串形态传入。（东八区时间,UTC+8，单位为秒）
         */
        @ApiField("begin_timestamp")
        private Long beginTimestamp;

        /**
         * 表示结束时间，建议设置为截止日期的23:59:59过期。（东八区时间,UTC+8，单位为秒）
         */
        @ApiField("end_timestamp")
        private Long endTimestamp;


        /**
         * type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天内有效，不支持填写0。
         */
        @ApiField("fixed_term")
        private Integer fixedTerm;

        /**
         * type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天开始生效，领取后当天生效填写0。（单位为天）
         */
        @ApiField("fixed_begin_term")
        private Integer fixedBeginTerm;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Long getBeginTimestamp() {
            return beginTimestamp;
        }

        public void setBeginTimestamp(Long beginTimestamp) {
            this.beginTimestamp = beginTimestamp;
        }

        public Long getEndTimestamp() {
            return endTimestamp;
        }

        public void setEndTimestamp(Long endTimestamp) {
            this.endTimestamp = endTimestamp;
        }

        public Integer getFixedTerm() {
            return fixedTerm;
        }

        public void setFixedTerm(Integer fixedTerm) {
            this.fixedTerm = fixedTerm;
        }

        public Integer getFixedBeginTerm() {
            return fixedBeginTerm;
        }

        public void setFixedBeginTerm(Integer fixedBeginTerm) {
            this.fixedBeginTerm = fixedBeginTerm;
        }
    }


    //region========================创建子商户卡券接口========================
    //若母商户需创建自己的卡券，参考原有卡券创建接口
    public static class SubMerchantInfo implements Serializable {

        @ApiField("merchant_id")
        private String merchantId;

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }
    }

    //endregion
}
