/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 会员卡概况数据统计
 *
 * @author mzlion on 2017/4/19.
 */
public class MemberCardSummaryDataCubeModel extends BaseCouponDataCubeModel {

    /**
     * 激活人数
     */
    @ApiField("active_user")
    private int activeUser;

    /**
     * 有效会员总人数
     */
    @ApiField("total_user")
    private int totalUser;

    /**
     * 历史领取会员卡总人数
     */
    @ApiField("total_receive_user")
    private int totalReceiveUser;

    public int getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(int activeUser) {
        this.activeUser = activeUser;
    }

    public int getTotalUser() {
        return totalUser;
    }

    public void setTotalUser(int totalUser) {
        this.totalUser = totalUser;
    }

    public int getTotalReceiveUser() {
        return totalReceiveUser;
    }

    public void setTotalReceiveUser(int totalReceiveUser) {
        this.totalReceiveUser = totalReceiveUser;
    }
}
