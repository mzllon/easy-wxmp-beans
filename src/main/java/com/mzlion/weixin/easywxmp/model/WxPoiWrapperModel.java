/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 门店信息的包装类
 *
 * @author mzlion on 2017/4/23.
 */
public class WxPoiWrapperModel<POI extends WxPoiModel> extends WxObject {

    /**
     * 门店信息
     */
    @ApiField("base_info")
    private POI poiInfo;

    public WxPoiWrapperModel() {
    }

    public WxPoiWrapperModel(POI poiInfo) {
        this.poiInfo = poiInfo;
    }

    public POI getPoiInfo() {
        return poiInfo;
    }

    public void setPoiInfo(POI poiInfo) {
        this.poiInfo = poiInfo;
    }
}
