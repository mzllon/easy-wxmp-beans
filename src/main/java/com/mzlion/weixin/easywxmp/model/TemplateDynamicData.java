/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;

/**
 * 模板中的动态数据
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateDynamicData extends WxObject {

    private static final long serialVersionUID = 4603127425466210528L;
    /**
     * 数据值
     */
    private String value;

    /**
     * 数据色块
     */
    private String color;

    public TemplateDynamicData() {
    }

    public TemplateDynamicData(String value) {
        this.value = value;
    }

    public TemplateDynamicData(String value, String color) {
        this.value = value;
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
