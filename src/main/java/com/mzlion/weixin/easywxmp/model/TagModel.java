/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;

/**
 * 微信的标签信息
 *
 * @author mzlion on 2017/4/17.
 */
public class TagModel extends WxObject {

    private static final long serialVersionUID = 630035482749426480L;

    /**
     * 标签id，由微信分配
     */
    private Integer id;

    /**
     * 标签名，UTF8编码
     */
    private String name;

    /**
     * 此标签下粉丝数
     */
    private int count;

    public TagModel() {
    }

    public TagModel(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
