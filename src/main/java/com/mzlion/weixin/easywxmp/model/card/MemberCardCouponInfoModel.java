/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 会员卡扩展的卡券信息
 *
 * @author mzlion on 2017/4/20.
 */
public class MemberCardCouponInfoModel extends BaseCouponInfoModel {

    private static final long serialVersionUID = 5692944949232283232L;

    //商户可以创建一张会员卡支持微信支付刷卡，须在创建会员卡接口的JSON中加入以下字段

    /**
     * 创建会员卡支持微信支付刷卡
     */
    @ApiField("pay_info")
    private PayInfo payInfo;

    /**
     * 是否设置该会员卡中部的按钮同时支持微信支付刷卡和会员卡二维码
     */
    @ApiField("is_pay_and_qrcode")
    private Boolean beIsPayAndQrcode;

    //endregion

    public PayInfo getPayInfo() {
        return payInfo;
    }

    public void setPayInfo(PayInfo payInfo) {
        this.payInfo = payInfo;
    }

    public Boolean getBeIsPayAndQrcode() {
        return beIsPayAndQrcode;
    }

    public void setBeIsPayAndQrcode(Boolean beIsPayAndQrcode) {
        this.beIsPayAndQrcode = beIsPayAndQrcode;
    }

    /**
     * 支付及会员结构体
     */
    public static class PayInfo implements Serializable {

        private static final long serialVersionUID = -7138654753200793459L;

        /**
         * 刷卡结构体
         */
        @ApiField("swipe_card")
        private SwipeCard swipeCard;

        public PayInfo() {
            this(null);
        }

        public PayInfo(Boolean isSwipeCard) {
            this.swipeCard = new SwipeCard();
            this.swipeCard.beIsSwipeCard = isSwipeCard;
        }

        public SwipeCard getSwipeCard() {
            return swipeCard;
        }

        public void setSwipeCard(SwipeCard swipeCard) {
            this.swipeCard = swipeCard;
        }

    }

    public static class SwipeCard implements Serializable {

        public SwipeCard() {
        }

        /**
         * 是否设置该会员卡支持拉出微信支付刷卡界面
         */
        @ApiField("is_swipe_card")
        private Boolean beIsSwipeCard;

        public Boolean getBeIsSwipeCard() {
            return beIsSwipeCard;
        }

        public void setBeIsSwipeCard(Boolean beIsSwipeCard) {
            this.beIsSwipeCard = beIsSwipeCard;
        }

    }


}
