/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 公众号卡券总体统计结果
 *
 * @author mzlion on 2017/4/19.
 */
public class SummaryCouponDataCubeModel extends BaseCouponDataCubeModel {


    /**
     * 转赠次数
     */
    @ApiField("given_cnt")
    private int givenCnt;

    /**
     * 转赠人数
     */
    @ApiField("given_user")
    private int givenUser;

    /**
     * 过期次数
     */
    @ApiField("expire_cnt")
    private int expireCnt;

    /**
     * 过期人数
     */
    @ApiField("expire_user")
    private int expireUser;

    public int getGivenCnt() {
        return givenCnt;
    }

    public void setGivenCnt(int givenCnt) {
        this.givenCnt = givenCnt;
    }

    public int getGivenUser() {
        return givenUser;
    }

    public void setGivenUser(int givenUser) {
        this.givenUser = givenUser;
    }

    public int getExpireCnt() {
        return expireCnt;
    }

    public void setExpireCnt(int expireCnt) {
        this.expireCnt = expireCnt;
    }

    public int getExpireUser() {
        return expireUser;
    }

    public void setExpireUser(int expireUser) {
        this.expireUser = expireUser;
    }
}
