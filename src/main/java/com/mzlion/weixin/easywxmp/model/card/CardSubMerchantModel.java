/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.constants.CardSubMerchantStatusEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 卡券子商户信息
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantModel extends WxObject {

    private static final long serialVersionUID = -5079051195895731975L;

    /**
     * 子商户id，一个母商户公众号下唯一。
     */
    @ApiField("merchant_id")
    private String merchantId;

    /**
     * 商户名称（12个汉字内），该名称将在制券时填入并显示在卡券页面上
     */
    @ApiField("brand_name")
    private String brandName;
    /**
     * 商户的公众号app_id
     * 配置后子商户卡券券面上的app_id为该app_id。注意：该app_id须经过认证
     */
    @ApiField("app_id")
    private String appId;

    /**
     * 子商户logo，可通过上传图片接口获取。该logo将在制券时填入并显示在卡券页面上
     */
    @ApiField("logo_url")
    private String logoUrl;

    /**
     * 授权函ID，即通过上传临时素材接口上传授权函后获得的meida_id
     */
    private String protocol;

    /**
     * 授权函有效期截止时间（东八区时间，单位为秒），需要与提交的扫描件一致
     */
    @ApiField("end_time")
    private Long endTime;

    /**
     * 一级类目id,可以通过本文档中接口查询
     */
    @ApiField("primary_category_id")
    private int primaryCategoryId;

    /**
     * 二级类目id，可以通过本文档中接口查询
     */
    @ApiField("secondary_category_id")
    private int secondaryCategoryId;

    /**
     * 营业执照或个体工商户营业执照彩照或扫描件
     */
    @ApiField("agreement_media_id")
    private String agreementMediaId;

    /**
     * 营业执照内登记的经营者身份证彩照或扫描件
     */
    @ApiField("operator_media_id")
    private String operatorMediaId;

    /**
     * 子商户信息创建时间
     */
    @ApiField("create_time")
    private Long createTime;

    /**
     * 子商户信息更新时间
     */
    @ApiField("update_time")
    private Long updateTime;

    /**
     * 子商户状态
     */
    @ApiField("status")
    private CardSubMerchantStatusEnum cardSubMerchantStatus;

    /**
     * 创建时间（非协议开始时间）
     */
    @ApiField("begin_time")
    private String beginTime;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public int getPrimaryCategoryId() {
        return primaryCategoryId;
    }

    public void setPrimaryCategoryId(int primaryCategoryId) {
        this.primaryCategoryId = primaryCategoryId;
    }

    public int getSecondaryCategoryId() {
        return secondaryCategoryId;
    }

    public void setSecondaryCategoryId(int secondaryCategoryId) {
        this.secondaryCategoryId = secondaryCategoryId;
    }

    public String getAgreementMediaId() {
        return agreementMediaId;
    }

    public void setAgreementMediaId(String agreementMediaId) {
        this.agreementMediaId = agreementMediaId;
    }

    public String getOperatorMediaId() {
        return operatorMediaId;
    }

    public void setOperatorMediaId(String operatorMediaId) {
        this.operatorMediaId = operatorMediaId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public CardSubMerchantStatusEnum getCardSubMerchantStatus() {
        return cardSubMerchantStatus;
    }

    public void setCardSubMerchantStatus(CardSubMerchantStatusEnum cardSubMerchantStatus) {
        this.cardSubMerchantStatus = cardSubMerchantStatus;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CardSubMerchantModel{");
        sb.append("merchantId='").append(merchantId).append('\'');
        sb.append(", brandName='").append(brandName).append('\'');
        sb.append(", appId='").append(appId).append('\'');
        sb.append(", logoUrl='").append(logoUrl).append('\'');
        sb.append(", protocol='").append(protocol).append('\'');
        sb.append(", endTime=").append(endTime);
        sb.append(", primaryCategoryId=").append(primaryCategoryId);
        sb.append(", secondaryCategoryId=").append(secondaryCategoryId);
        sb.append(", agreementMediaId='").append(agreementMediaId).append('\'');
        sb.append(", operatorMediaId='").append(operatorMediaId).append('\'');
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", cardSubMerchantStatus=").append(cardSubMerchantStatus);
        sb.append(", beginTime='").append(beginTime).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
