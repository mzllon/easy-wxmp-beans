/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.constants.MenuButtonTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 自定义菜单信息
 *
 * @author mzlion on 2017/4/16.
 */
public class MenuButtonModel extends WxObject {

    /**
     * 二级菜单数组
     */
    @ApiField("sub_button")
    private List<MenuButtonModel> subMenuButtonList;

    /**
     * 菜单的响应动作类型
     */
    @ApiField("type")
    private MenuButtonTypeEnum buttonType;

    /**
     * 菜单标题，不超过16个字节，子菜单不超过60个字节
     */
    private String name;

    /**
     * click等点击类型必须
     * 菜单KEY值，用于消息接口推送，不超过128字节
     */
    private String key;

    /**
     * view、miniprogram类型必须
     * 网页链接，用户点击菜单可打开链接，不超过1024字节。
     * type为miniprogram时，不支持小程序的老版本客户端将打开本url。
     */
    private String url;

    /**
     * media_id类型和view_limited类型必须
     * 调用新增永久素材接口返回的合法media_id
     */
    @ApiField("media_id")
    private String mediaId;    //media_id类型和view_limited类型必须	调用新增永久素材接口返回的合法media_id

    /**
     * miniprogram类型必须
     * 小程序的appid
     */
    @ApiField("appid")
    private String appId;

    /**
     * miniprogram类型必须
     * 小程序的页面路径
     */
    @ApiField("pagepath")
    private String pagePath;

    //----------以下属于通过设置数据存储字段----------
    /**
     * 文字/mediaID/视频下载链接
     */
    private String value;
    //----------以下属于通过设置数据存储字段----------


    public List<MenuButtonModel> getSubMenuButtonList() {
        return subMenuButtonList;
    }

    public void setSubMenuButtonList(List<MenuButtonModel> subMenuButtonList) {
        this.subMenuButtonList = subMenuButtonList;
    }

    public MenuButtonTypeEnum getButtonType() {
        return buttonType;
    }

    public void setButtonType(MenuButtonTypeEnum buttonType) {
        this.buttonType = buttonType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
