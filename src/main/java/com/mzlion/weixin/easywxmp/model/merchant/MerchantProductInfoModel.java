/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.merchant;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.constants.ExpressTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 商品/产品信息
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantProductInfoModel extends WxObject {

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品分类id
     */
    @ApiField("category_id")
    private List<String> categoryIdList;

    /**
     * 商品主图(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品。
     * 图片分辨率推荐尺寸为640×600)
     */
    @ApiField("main_img")
    private String mainImg;

    /**
     * 商品图片列表(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品。
     * 图片分辨率推荐尺寸为640×600)
     */
    @ApiField("img")
    private List<String> imgList;

    /**
     * 商品详情列表，显示在客户端的商品详情页内
     */
    @ApiField("detail")
    private List<ProductDetail> productDetailList;

    /**
     * 商品属性列表
     *
     * @see
     */
    @ApiField("property")
    private List<ProductProperty> productPropertyList;

    /**
     * 用户商品限购数量
     */
    @ApiField("buy_limit")
    private Integer buyLimit;

    /**
     * 商品sku列表
     */
    @ApiField("sku_info")
    private List<SkuSimpleInfo> skuInfoList;

    public MerchantProductInfoModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCategoryIdList() {
        return categoryIdList;
    }

    public void setCategoryIdList(List<String> categoryIdList) {
        this.categoryIdList = categoryIdList;
    }

    public String getMainImg() {
        return mainImg;
    }

    public void setMainImg(String mainImg) {
        this.mainImg = mainImg;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    public List<ProductDetail> getProductDetailList() {
        return productDetailList;
    }

    public void setProductDetailList(List<ProductDetail> productDetailList) {
        this.productDetailList = productDetailList;
    }

    public List<ProductProperty> getProductPropertyList() {
        return productPropertyList;
    }

    public void setProductPropertyList(List<ProductProperty> productPropertyList) {
        this.productPropertyList = productPropertyList;
    }

    public Integer getBuyLimit() {
        return buyLimit;
    }

    public void setBuyLimit(Integer buyLimit) {
        this.buyLimit = buyLimit;
    }

    public List<SkuSimpleInfo> getSkuInfoList() {
        return skuInfoList;
    }

    public void setSkuInfoList(List<SkuSimpleInfo> skuInfoList) {
        this.skuInfoList = skuInfoList;
    }

    /**
     * 商品属性
     */
    public static class ProductProperty implements Serializable {

        /**
         * 属性id
         */
        private String id;

        /**
         * 属性值id
         */
        private String vid;

        public ProductProperty() {
        }

        public ProductProperty(String id, String vid) {
            this.id = id;
            this.vid = vid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVid() {
            return vid;
        }

        public void setVid(String vid) {
            this.vid = vid;
        }
    }

    /**
     * 商品详情
     */
    public static class ProductDetail implements Serializable {

        /**
         * 文字描述
         */
        private String text;

        /**
         * 图片(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品)
         */
        private String img;

        public ProductDetail() {
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }

    /**
     * 商品sku定义
     */
    public static class SkuSimpleInfo implements Serializable {

        /**
         * sku属性(SKU列表中id, 支持自定义SKU，格式为"$xxx"，xxx即为显示在客户端中的字符串)
         */
        private String id;

        /**
         * sku值(SKU列表中vid, 如需自定义SKU，格式为"$xxx"，xxx即为显示在客户端中的字符串)
         */
        private List<String> vid;

        public SkuSimpleInfo() {
        }

        public SkuSimpleInfo(String id, List<String> vid) {
            this.id = id;
            this.vid = vid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<String> getVid() {
            return vid;
        }

        public void setVid(List<String> vid) {
            this.vid = vid;
        }
    }


    public static class AttrExtInfo implements Serializable {

        /**
         * 是否包邮(0-否, 1-是),
         * 如果包邮delivery_info字段可省略
         *
         * @see #IS_POST_FREE$YES
         * @see #IS_POST_FREE$NO
         */
        private Integer isPostFree;

        /**
         * 是否提供发票(0-否, 1-是)
         *
         * @see #IS_HAS_RECEIPT$YES
         * @see #IS_HAS_RECEIPT$NO
         */
        private Integer isHasReceipt;

        /**
         * 是否保修(0-否, 1-是)
         *
         * @see #IS_UNDER_GUARANTY$YES
         * @see #IS_UNDER_GUARANTY$NO
         */
        private Integer isUnderGuaranty;

        /**
         * 是否支持退换货(0-否, 1-是)
         *
         * @see #is_Support_Replace$YES
         * @see #is_Support_Replace$NO
         */
        private Integer isSupportReplace;

        /**
         * 商品所在地地址
         */
        private Location location;

        public AttrExtInfo() {
        }

        public Integer getIsPostFree() {
            return isPostFree;
        }

        public void setIsPostFree(Integer isPostFree) {
            this.isPostFree = isPostFree;
        }

        public Integer getIsHasReceipt() {
            return isHasReceipt;
        }

        public void setIsHasReceipt(Integer isHasReceipt) {
            this.isHasReceipt = isHasReceipt;
        }

        public Integer getIsUnderGuaranty() {
            return isUnderGuaranty;
        }

        public void setIsUnderGuaranty(Integer isUnderGuaranty) {
            this.isUnderGuaranty = isUnderGuaranty;
        }

        public Integer getIsSupportReplace() {
            return isSupportReplace;
        }

        public void setIsSupportReplace(Integer isSupportReplace) {
            this.isSupportReplace = isSupportReplace;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        /**
         * 包邮
         */
        public static final int IS_POST_FREE$YES = 1;

        /**
         * 不包邮
         */
        public static final int IS_POST_FREE$NO = 0;

        /**
         * 需要发票
         */
        public static final int IS_HAS_RECEIPT$YES = 1;

        /**
         * 不需要发票
         */
        public static final int IS_HAS_RECEIPT$NO = 0;

        /**
         * 需要保修
         */
        public static final int IS_UNDER_GUARANTY$YES = 1;

        /**
         * 不需要保修
         */
        public static final int IS_UNDER_GUARANTY$NO = 0;

        /**
         * 支持退货
         */
        public static final int is_Support_Replace$YES = 1;

        /**
         * 不支持退货
         */
        public static final int is_Support_Replace$NO = 0;

    }

    public static class DeliveryInfo implements Serializable {

        /**
         * 运费类型
         * 0-使用下面express字段的默认模板,
         * 1-使用template_id代表的邮费模板, 详见邮费模板相关API
         */
        @ApiField("delivery_type")
        private Integer deliveryType;

        /**
         * 邮费模板ID
         */
        @ApiField("template_id")
        private String templateId;

        /**
         * 快递及其运费列表
         */
        @ApiField("express")
        private List<Express> expressList;

        public DeliveryInfo() {
        }

        public Integer getDeliveryType() {
            return deliveryType;
        }

        public void setDeliveryType(Integer deliveryType) {
            this.deliveryType = deliveryType;
        }

        public String getTemplateId() {
            return templateId;
        }

        public void setTemplateId(String templateId) {
            this.templateId = templateId;
        }

        public List<Express> getExpressList() {
            return expressList;
        }

        public void setExpressList(List<Express> expressList) {
            this.expressList = expressList;
        }
    }

    /**
     * 快递运费
     */
    public static class Express implements Serializable {

        /**
         * 快递类型
         */
        private ExpressTypeEnum expressId;

        /**
         * 运费(单位:分)
         */
        private int price;

        public Express() {
        }

        public Express(ExpressTypeEnum expressId, int price) {
            this.expressId = expressId;
            this.price = price;
        }

        public ExpressTypeEnum getExpressId() {
            return expressId;
        }

        public void setExpressId(ExpressTypeEnum expressId) {
            this.expressId = expressId;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }
    }

    public static class Location implements Serializable {

        /**
         * 国家(详见《地区列表》说明)
         */
        private String country;

        /**
         * 省份(详见《地区列表》说明)
         */
        private String province;

        /**
         * 城市(详见《地区列表》说明)
         */
        private String city;

        /**
         * 地址
         */
        private String address;

        public Location() {
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }


    public static class SkuDetail implements Serializable {

        /**
         * sku信息, 参照上述sku_table的定义;
         * 格式 : "id1:vid1;id2:vid2"
         * 规则 : id_info的组合个数必须与sku_table个数一致(若商品无sku信息, 即商品为统一规格，则此处赋值为空字符串即可)
         */
        @ApiField("sku_id")
        private String skuId;

        /**
         * sku原价(单位 : 分)
         */
        @ApiField("ori_price")
        private long orPrice;

        /**
         * sku iconurl(图片需调用图片上传接口获得图片Url)
         */
        @ApiField("icon_url")
        private String iconUrl;

        /**
         * sku库存
         */
        private int quantity;

        /**
         * sku微信价(单位 : 分, 微信价必须比原价小, 否则添加商品失败)
         */
        @ApiField("price")
        private long price;

        /**
         * 商家商品编码
         */
        @ApiField("product_code")
        private String productCode;

        public SkuDetail() {
        }

        public String getSkuId() {
            return skuId;
        }

        public void setSkuId(String skuId) {
            this.skuId = skuId;
        }

        public long getOrPrice() {
            return orPrice;
        }

        public void setOrPrice(long orPrice) {
            this.orPrice = orPrice;
        }

        public long getPrice() {
            return price;
        }

        public void setPrice(long price) {
            this.price = price;
        }

        public String getIconUrl() {
            return iconUrl;
        }

        public void setIconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }
    }


}
