/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 摇一摇设备信息
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceIdentifierModel extends WxObject {

    private static final long serialVersionUID = -1914603024370110662L;

    /**
     * 设备编号
     */
    @ApiField("device_id")
    private String deviceId;

    /**
     * uuid
     */
    private String uuid;

    /**
     * major
     */
    private String major;

    /**
     * minor
     */
    private String minor;

    /**
     * 设备的备注信息，不超过15个汉字或30个英文字母。
     */
    private String comment;

    /**
     * 激活状态
     * 0：未激活
     * 1：已激活
     */
    private String status;

    /**
     * 设备最近一次被摇到的日期（最早只能获取前一天的数据）；新申请的设备该字段值为0
     */
    @ApiField("last_active_time")
    private Long lastActiveTime;

    /**
     * 若配置了设备与其他公众账号门店关联关系，则返回配置门店归属的公众账号appid。查看配置设备与其他公众账号门店关联关系接口
     */
    @ApiField("poi_appid")
    private String poiAppId;

    /**
     * 设备关联的门店ID，关联门店后，在门店1KM的范围内有优先摇出信息的机会。门店相关信息具体可查看门店相关的接口文档
     */
    @ApiField("poi_id")
    private String poiId;

    public ShakeAroundDeviceIdentifierModel() {
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLastActiveTime() {
        return lastActiveTime;
    }

    public void setLastActiveTime(Long lastActiveTime) {
        this.lastActiveTime = lastActiveTime;
    }

    public String getPoiAppId() {
        return poiAppId;
    }

    public void setPoiAppId(String poiAppId) {
        this.poiAppId = poiAppId;
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

}
