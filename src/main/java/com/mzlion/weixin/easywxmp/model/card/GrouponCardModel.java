/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.constants.CardTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 团购券
 *
 * @author mzlion on 2016/12/22.
 */
public class GrouponCardModel extends AbsCardModel {

    /**
     * 团购券的基本信息
     */
    private Groupon groupon;

    public GrouponCardModel() {
        super(CardTypeEnum.GROUPON);
    }

    public Groupon getGroupon() {
        return groupon;
    }

    public void setGroupon(Groupon groupon) {
        this.groupon = groupon;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GrouponCard{");
        sb.append("groupon=").append(groupon);
        sb.append('}');
        return sb.toString();
    }

    /**
     * 团购券的基本信息
     *
     * @author mzlion on 2016/12/22.
     */
    public static class Groupon implements Serializable {

        /**
         * 卡券基础信息字段（重要）
         */
        @ApiField("base_info")
        private BaseCouponInfoModel baseCouponInfo;

        /**
         * Advanced_info（卡券高级信息）字段
         */
        @ApiField("advanced_info")
        private AdvancedCouponInfoModel advancedCouponInfo;

        /**
         * 团购详情。
         */
        @ApiField("deal_detail")
        private String dealDetail;

        public BaseCouponInfoModel getBaseCouponInfo() {
            return baseCouponInfo;
        }

        public void setBaseCouponInfo(BaseCouponInfoModel baseCouponInfo) {
            this.baseCouponInfo = baseCouponInfo;
        }

        public AdvancedCouponInfoModel getAdvancedCouponInfo() {
            return advancedCouponInfo;
        }

        public void setAdvancedCouponInfo(AdvancedCouponInfoModel advancedCouponInfo) {
            this.advancedCouponInfo = advancedCouponInfo;
        }

        public String getDealDetail() {
            return dealDetail;
        }

        public void setDealDetail(String dealDetail) {
            this.dealDetail = dealDetail;
        }

    }
}
