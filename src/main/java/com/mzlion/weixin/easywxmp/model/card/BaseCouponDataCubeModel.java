/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 卡券统计基类
 *
 * @author mzlion on 2017/4/19.
 */
public class BaseCouponDataCubeModel extends WxObject {

    /**
     * 数据的日期，需在begin_date和end_date之间
     */
    @ApiField("ref_date")
    private String refDate;

    /**
     * 浏览次数
     */
    @ApiField("view_cnt")
    private int viewCnt;

    /**
     * 浏览人数
     */
    @ApiField("view_user")
    private int viewUser;

    /**
     * 领取次数
     */
    @ApiField("receive_cnt")
    private int receiveCnt;

    /**
     * 领取人数
     */
    @ApiField("receive_user")
    private int receiveUser;

    /**
     * 使用次数
     */
    @ApiField("verify_cnt")
    private int verifyCnt;

    /**
     * 使用人数
     */
    @ApiField("verify_user")
    private int verifyUser;

    public String getRefDate() {
        return refDate;
    }

    public void setRefDate(String refDate) {
        this.refDate = refDate;
    }

    public int getViewCnt() {
        return viewCnt;
    }

    public void setViewCnt(int viewCnt) {
        this.viewCnt = viewCnt;
    }

    public int getViewUser() {
        return viewUser;
    }

    public void setViewUser(int viewUser) {
        this.viewUser = viewUser;
    }

    public int getReceiveCnt() {
        return receiveCnt;
    }

    public void setReceiveCnt(int receiveCnt) {
        this.receiveCnt = receiveCnt;
    }

    public int getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(int receiveUser) {
        this.receiveUser = receiveUser;
    }

    public int getVerifyCnt() {
        return verifyCnt;
    }

    public void setVerifyCnt(int verifyCnt) {
        this.verifyCnt = verifyCnt;
    }

    public int getVerifyUser() {
        return verifyUser;
    }

    public void setVerifyUser(int verifyUser) {
        this.verifyUser = verifyUser;
    }

}
