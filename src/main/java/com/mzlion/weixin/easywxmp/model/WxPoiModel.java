/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.constants.PoiOffsetTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 门店服务信息字段(包含门店基础字段)
 *
 * @author mzlion on 2017/1/5.
 */
public class WxPoiModel extends WxObject {

    //region ==== 门店基础信息

    @ApiField("poi_id")
    private String poiId;

    /**
     * 商户自己的id，用于后续审核通过收到poi_id 的通知时，做对应关系。请商户自己保证唯一识别性
     */
    private String sid;

    /**
     * 门店名称（仅为商户名，如：国美、麦当劳，不应包含地区、地址、分店名等信息，错误示例：北京国美）
     * 不能为空，15个汉字或30个英文字符内
     */
    @ApiField("business_name")
    private String businessName;

    /**
     * 分店名称（不应包含地区信息，不应与门店名有重复，错误示例：北京王府井店）
     * 20个字以内
     */
    @ApiField("branch_name")
    private String branchName;

    /**
     * 门店所在的省份（直辖市填城市名,如：北京市）
     * 10个字以内
     */
    private String province;

    /**
     * 门店所在的城市
     * 10个字以内
     */
    private String city;

    /**
     * 门店所在地区
     * 10个字以内
     */
    private String district;

    /**
     * 门店所在的详细街道地址（不要填写省市信息）
     * （东莞等没有“区”行政区划的城市，该字段可不必填写。其余城市必填。）
     */
    private String address;

    /**
     * 门店的电话（纯数字，区号、分机号均由“-”隔开）
     */
    private String telephone;

    /**
     * 门店的类型（不同级分类用“,”隔开，如：美食，川菜，火锅。详细分类参见附件：微信门店类目表）
     */
    private List<String> categories;

    /**
     * 坐标类型：
     *
     */
    @ApiField("offset_type")
    private PoiOffsetTypeEnum offsetType;

    /**
     * 门店所在地理位置的经度
     */
    private double longitude;

    /**
     * 门店所在地理位置的纬度（经纬度均为火星坐标，最好选用腾讯地图标记的坐标）
     */
    private double latitude;

    //endregion 
    
    /**
     * 图片列表，url 形式，可以有多张图片，尺寸为640*340px。必须为上一接口生成的url。
     * 图片内容不允许与门店不相关，不允许为二维码、员工合照（或模特肖像）、营业执照、无门店正门的街景、地图截图、公交地铁站牌、菜单截图等
     */
    @ApiField("photo_list")
    private List<Photo> photoList;

    /**
     * 推荐品，餐厅可为推荐菜；酒店为推荐套房；景点为推荐游玩景点等，针对自己行业的推荐内容
     * 200字以内
     */
    private String recommend;

    /**
     * 特色服务，如免费wifi，免费停车，送货上门等商户能提供的特色功能或服务
     */
    private String special;

    /**
     * 商户简介，主要介绍商户信息等
     * 300字以内
     */
    private String introduction;

    /**
     * 营业时间，24小时制表示，用“-”连接，如8:00-20:00
     */
    @ApiField("open_time")
    private String openTime;

    /**
     * 人均价格，大于0 的整数
     */
    @ApiField("avg_price")
    private int avgPrice;

    public WxPoiModel() {
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public PoiOffsetTypeEnum getOffsetType() {
        return offsetType;
    }

    public void setOffsetType(PoiOffsetTypeEnum offsetType) {
        this.offsetType = offsetType;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public List<Photo> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public int getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(int avgPrice) {
        this.avgPrice = avgPrice;
    }

    /**
     * 门店图片
     *
     * @author mzlion
     */
    public static class Photo implements Serializable {

        public Photo() {
        }

        public Photo(String photoUrl) {
            this.photoUrl = photoUrl;
        }

        @ApiField("photo_url")
        private String photoUrl;

        public String getPhotoUrl() {
            return photoUrl;
        }

        public void setPhotoUrl(String photoUrl) {
            this.photoUrl = photoUrl;
        }
    }


}
