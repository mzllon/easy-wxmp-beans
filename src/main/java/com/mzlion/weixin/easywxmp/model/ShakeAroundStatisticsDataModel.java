/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 摇一摇数据统计
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundStatisticsDataModel extends WxObject {

    private static final long serialVersionUID = -7807165083567068773L;

    /**
     * 当天0点对应的时间戳
     */
    private Long ftime;

    /**
     * 点击摇周边消息的次数
     */
    @ApiField("click_pv")
    private int clickPv;

    /**
     * 点击摇周边消息的人数
     */
    @ApiField("click_uv")
    private int clickUv;

    /**
     * 摇周边的次数
     */
    @ApiField("shake_pv")
    private int shakePv;

    /**
     * 摇周边的人数
     */
    @ApiField("shake_uv")
    private int shakeUv;

    public ShakeAroundStatisticsDataModel() {
    }

    public Long getFtime() {
        return ftime;
    }

    public void setFtime(Long ftime) {
        this.ftime = ftime;
    }

    public int getClickPv() {
        return clickPv;
    }

    public void setClickPv(int clickPv) {
        this.clickPv = clickPv;
    }

    public int getClickUv() {
        return clickUv;
    }

    public void setClickUv(int clickUv) {
        this.clickUv = clickUv;
    }

    public int getShakePv() {
        return shakePv;
    }

    public void setShakePv(int shakePv) {
        this.shakePv = shakePv;
    }

    public int getShakeUv() {
        return shakeUv;
    }

    public void setShakeUv(int shakeUv) {
        this.shakeUv = shakeUv;
    }

}
