/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.constants.CardTypeEnum;
import com.mzlion.weixin.easywxmp.constants.MemberCardNameTypeEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 会员卡
 *
 * @author mzlion on 2016/12/22.
 */
public class MemberCardCardModel extends AbsCardModel {

    private static final long serialVersionUID = -4455659118836832653L;

    @ApiField("member_card")
    private MemberCard memberCard;

    public MemberCard getMemberCard() {
        return memberCard;
    }

    public void setMemberCard(MemberCard memberCard) {
        this.memberCard = memberCard;
    }

    public MemberCardCardModel() {
        super(CardTypeEnum.MEMBER_CARD);
    }


    /**
     * 会员卡信息
     *
     * @author mzlion on 2016/12/30.
     */
    public static class MemberCard implements Serializable {

        /**
         * 商家自定义会员卡背景图，须先调用上传图片接口将背景图上传至CDN，否则报错，
         * 卡面设计请遵循微信会员卡自定义背景设计规范  ,像素大小控制在1000像素*600像素以下
         */
        @ApiField("background_pic_url")
        private String backgroundPicUrl;

        @ApiField("base_info")
        private MemberCardCouponInfoModel baseCouponInfo;

        @ApiField("advanced_info")
        private AdvancedCouponInfoModel advancedInfo;

        /**
         * 会员卡特权说明。
         */
        private String prerogative;

        /**
         * 设置为true时用户领取会员卡后系统自动将其激活，无需调用激活接口，详情见自动激活。
         */
        @ApiField("auto_activate")
        private Boolean autoActivate;

        /**
         * 设置为true时会员卡支持一键开卡，不允许同时传入activate_url字段，否则设置wx_activate失效。
         * 填入该字段后仍需调用接口设置开卡项方可生效，详情见一键开卡。
         */
        @ApiField("wx_activate")
        private Boolean wxActivate;

        /**
         * 是否支持跳转型一键激活，填true或false
         */
        @ApiField("wx_activate_after_submit")
        private Boolean wxActivateAfterSubmit;

        /**
         * 跳转型一键激活跳转的地址链接，请填写http://或者https://开头的链接
         */
        @ApiField("wx_activate_after_submit_url")
        private String wxActivateAfterSubmitUrl;

        /**
         * 显示积分，填写true或false，如填写true，积分相关字段均为必填。
         */
        @ApiField("supply_bonus")
        private Boolean supplyBonus;

        /**
         * 设置跳转外链查看积分详情。仅适用于积分无法通过激活接口同步的情况下使用该字段。
         */
        @ApiField("bonus_url")
        private String bonusUrl;

        /**
         * 是否支持储值，填写true或false。如填写true，储值相关字段均为必填。
         */
        @ApiField("supply_balance")
        private Boolean supplyBalance;

        /**
         * 设置跳转外链查看余额详情。仅适用于余额无法通过激活接口同步的情况下使用该字段。
         */
        @ApiField("balance_url")
        private String balanceUrl;

        /**
         * 自定义会员信息类目，会员卡激活后显示,包含name_type(name)和url字段
         */
        @ApiField("custom_field1")
        private MemberCustomField customField1;

        /**
         * 自定义会员信息类目，会员卡激活后显示,包含name_type(name)和url字段
         */
        @ApiField("custom_field2")
        private MemberCustomField customField2;

        /**
         * 自定义会员信息类目，会员卡激活后显示,包含name_type(name)和url字段
         */
        @ApiField("custom_field3")
        private MemberCustomField customField3;

        /**
         * 积分清零规则。
         */
        @ApiField("bonus_cleared")
        private String bonusCleared;

        /**
         * 积分规则。
         */
        @ApiField("bonus_rules")
        private String bonusRules;

        /**
         * 储值说明。
         */
        @ApiField("balance_rules")
        private String balanceRules;

        /**
         * 激活会员卡的url。
         */
        @ApiField("activate_url")
        private String activateUrl;

        /**
         * 自定义会员信息类目
         */
        @ApiField("custom_cell1")
        private MemberCustomCell customCell1;

        /**
         * 自定义会员信息类目
         */
        @ApiField("custom_cell2")
        private MemberCustomCell customCell2;

        /**
         * 积分规则
         */
        @ApiField("bonus_rule")
        private MemberBonusRule bonusRule;

        /**
         * 折扣，该会员卡享受的折扣优惠,填10就是九折。
         */
        private Integer discount;

        //region========================设置跟随推荐接口========================
        //调用更新卡券信息接口将增推荐位字段 update到已成功通过审核的卡券。 同时支持在创建卡券时填入相应字段。

        /**
         * 推荐类型，代表积分余额等变动消息赠券
         */
        @ApiField("modify_msg_operation")
        private MsgOperation modifyMsgOperation;

        /**
         * 推荐类型，代表会员卡激活消息赠券
         */
        @ApiField("activate_msg_operation")
        private MsgOperation activateMsgOperation;
        //endregion

        public AdvancedCouponInfoModel getAdvancedInfo() {
            return advancedInfo;
        }

        public void setAdvancedInfo(AdvancedCouponInfoModel advancedInfo) {
            this.advancedInfo = advancedInfo;
        }

        public String getPrerogative() {
            return prerogative;
        }

        public void setPrerogative(String prerogative) {
            this.prerogative = prerogative;
        }

        public Boolean getAutoActivate() {
            return autoActivate;
        }

        public void setAutoActivate(Boolean autoActivate) {
            this.autoActivate = autoActivate;
        }

        public Boolean getWxActivate() {
            return wxActivate;
        }

        public void setWxActivate(Boolean wxActivate) {
            this.wxActivate = wxActivate;
        }

        public Boolean getWxActivateAfterSubmit() {
            return wxActivateAfterSubmit;
        }

        public void setWxActivateAfterSubmit(Boolean wxActivateAfterSubmit) {
            this.wxActivateAfterSubmit = wxActivateAfterSubmit;
        }

        public String getWxActivateAfterSubmitUrl() {
            return wxActivateAfterSubmitUrl;
        }

        public void setWxActivateAfterSubmitUrl(String wxActivateAfterSubmitUrl) {
            this.wxActivateAfterSubmitUrl = wxActivateAfterSubmitUrl;
        }

        public Boolean getSupplyBonus() {
            return supplyBonus;
        }

        public void setSupplyBonus(Boolean supplyBonus) {
            this.supplyBonus = supplyBonus;
        }

        public String getBonusUrl() {
            return bonusUrl;
        }

        public void setBonusUrl(String bonusUrl) {
            this.bonusUrl = bonusUrl;
        }

        public Boolean getSupplyBalance() {
            return supplyBalance;
        }

        public void setSupplyBalance(Boolean supplyBalance) {
            this.supplyBalance = supplyBalance;
        }

        public String getBalanceUrl() {
            return balanceUrl;
        }

        public void setBalanceUrl(String balanceUrl) {
            this.balanceUrl = balanceUrl;
        }

        public MemberCustomField getCustomField1() {
            return customField1;
        }

        public void setCustomField1(MemberCustomField customField1) {
            this.customField1 = customField1;
        }

        public MemberCustomField getCustomField2() {
            return customField2;
        }

        public void setCustomField2(MemberCustomField customField2) {
            this.customField2 = customField2;
        }

        public MemberCustomField getCustomField3() {
            return customField3;
        }

        public void setCustomField3(MemberCustomField customField3) {
            this.customField3 = customField3;
        }

        public String getBonusCleared() {
            return bonusCleared;
        }

        public void setBonusCleared(String bonusCleared) {
            this.bonusCleared = bonusCleared;
        }

        public String getBonusRules() {
            return bonusRules;
        }

        public void setBonusRules(String bonusRules) {
            this.bonusRules = bonusRules;
        }

        public String getBalanceRules() {
            return balanceRules;
        }

        public void setBalanceRules(String balanceRules) {
            this.balanceRules = balanceRules;
        }

        public String getActivateUrl() {
            return activateUrl;
        }

        public void setActivateUrl(String activateUrl) {
            this.activateUrl = activateUrl;
        }

        public MemberCustomCell getCustomCell1() {
            return customCell1;
        }

        public void setCustomCell1(MemberCustomCell customCell1) {
            this.customCell1 = customCell1;
        }

        public MemberBonusRule getBonusRule() {
            return bonusRule;
        }

        public void setBonusRule(MemberBonusRule bonusRule) {
            this.bonusRule = bonusRule;
        }

        public Integer getDiscount() {
            return discount;
        }

        public void setDiscount(Integer discount) {
            this.discount = discount;
        }

        public String getBackgroundPicUrl() {
            return backgroundPicUrl;
        }

        public void setBackgroundPicUrl(String backgroundPicUrl) {
            this.backgroundPicUrl = backgroundPicUrl;
        }

        public MemberCardCouponInfoModel getBaseCouponInfo() {
            return baseCouponInfo;
        }

        public void setBaseCouponInfo(MemberCardCouponInfoModel baseCouponInfo) {
            this.baseCouponInfo = baseCouponInfo;
        }

        public MsgOperation getModifyMsgOperation() {
            return modifyMsgOperation;
        }

        public void setModifyMsgOperation(MsgOperation modifyMsgOperation) {
            this.modifyMsgOperation = modifyMsgOperation;
        }

        public MsgOperation getActivateMsgOperation() {
            return activateMsgOperation;
        }

        public void setActivateMsgOperation(MsgOperation activateMsgOperation) {
            this.activateMsgOperation = activateMsgOperation;
        }

        public MemberCustomCell getCustomCell2() {
            return customCell2;
        }

        public void setCustomCell2(MemberCustomCell customCell2) {
            this.customCell2 = customCell2;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }

    /**
     * 自定义会员信息类目，会员卡激活后显示,包含name_type(name)和url字段
     *
     * @author mzlion
     */
    public static class MemberCustomField implements Serializable {

        private static final long serialVersionUID = 473577792466020706L;

        /**
         * 会员信息类目半自定义名称，当开发者变更这类类目信息的value值时可以选择触发系统模板消息通知用户。
         */
        @ApiField("name_type")
        private MemberCardNameTypeEnum nameType;

        /**
         * 会员信息类目自定义名称，当开发者变更这类类目信息的value值时不会触发系统模板消息通知用户
         */
        private String name;

        /**
         * 点击类目跳转外链url
         */
        private String url;

        public MemberCardNameTypeEnum getNameType() {
            return nameType;
        }

        public void setNameType(MemberCardNameTypeEnum nameType) {
            this.nameType = nameType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("MemberCustomField{");
            sb.append("nameType='").append(nameType).append('\'');
            sb.append(", name='").append(name).append('\'');
            sb.append(", url='").append(url).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    /**
     * 自定义会员信息类目，会员卡激活后显示。
     *
     * @author mzlion
     */
    public static class MemberCustomCell implements Serializable {

        private static final long serialVersionUID = 1792806653483304316L;

        /**
         * 入口名称。
         */
        private String name;

        /**
         * 入口右侧提示语，6个汉字内。
         */
        private String tips;

        /**
         * 入口跳转链接。
         */
        private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("MemberCustomCell{");
            sb.append("name='").append(name).append('\'');
            sb.append(", tips='").append(tips).append('\'');
            sb.append(", url='").append(url).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    public static class MemberBonusRule implements Serializable {

        private static final long serialVersionUID = 1735052942180740346L;

        /**
         * 消费金额。以分为单位。
         */
        @ApiField("cost_money_unit")
        private Integer costMoneyUnit;

        /**
         * 对应增加的积分。
         */
        @ApiField("increase_bonus")
        private Integer increaseBonus;

        /**
         * 用户单次可获取的积分上限。
         */
        @ApiField("max_increase_bonus")
        private Integer maxIncreaseBonus;

        /**
         * 初始设置积分。
         */
        @ApiField("init_increase_bonus")
        private Integer initIncreaseBonus;

        /**
         * 每使用5积分。
         */
        @ApiField("cost_bonus_unit")
        private Integer costBonusUnit;

        /**
         * 抵扣xx元，（这里以分为单位）
         */
        @ApiField("reduce_money")
        private Integer reduceMoney;

        /**
         * 抵扣条件，满xx元（这里以分为单位）可用。
         */
        @ApiField("least_money_to_use_bonus")
        private Integer leastMoneyToUseBonus;

        /**
         * 抵扣条件，单笔最多使用xx积分。
         */
        @ApiField("max_reduce_bonus")
        private Integer maxReduceBonus;

        public Integer getCostMoneyUnit() {
            return costMoneyUnit;
        }

        public void setCostMoneyUnit(Integer costMoneyUnit) {
            this.costMoneyUnit = costMoneyUnit;
        }

        public Integer getIncreaseBonus() {
            return increaseBonus;
        }

        public void setIncreaseBonus(Integer increaseBonus) {
            this.increaseBonus = increaseBonus;
        }

        public Integer getMaxIncreaseBonus() {
            return maxIncreaseBonus;
        }

        public void setMaxIncreaseBonus(Integer maxIncreaseBonus) {
            this.maxIncreaseBonus = maxIncreaseBonus;
        }

        public Integer getInitIncreaseBonus() {
            return initIncreaseBonus;
        }

        public void setInitIncreaseBonus(Integer initIncreaseBonus) {
            this.initIncreaseBonus = initIncreaseBonus;
        }

        public Integer getCostBonusUnit() {
            return costBonusUnit;
        }

        public void setCostBonusUnit(Integer costBonusUnit) {
            this.costBonusUnit = costBonusUnit;
        }

        public Integer getReduceMoney() {
            return reduceMoney;
        }

        public void setReduceMoney(Integer reduceMoney) {
            this.reduceMoney = reduceMoney;
        }

        public Integer getLeastMoneyToUseBonus() {
            return leastMoneyToUseBonus;
        }

        public void setLeastMoneyToUseBonus(Integer leastMoneyToUseBonus) {
            this.leastMoneyToUseBonus = leastMoneyToUseBonus;
        }

        public Integer getMaxReduceBonus() {
            return maxReduceBonus;
        }

        public void setMaxReduceBonus(Integer maxReduceBonus) {
            this.maxReduceBonus = maxReduceBonus;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("MemberBonusRule{");
            sb.append("costMoneyUnit=").append(costMoneyUnit);
            sb.append(", increaseBonus=").append(increaseBonus);
            sb.append(", maxIncreaseBonus=").append(maxIncreaseBonus);
            sb.append(", initIncreaseBonus=").append(initIncreaseBonus);
            sb.append(", costBonusUnit=").append(costBonusUnit);
            sb.append(", reduceMoney=").append(reduceMoney);
            sb.append(", leastMoneyToUseBonus=").append(leastMoneyToUseBonus);
            sb.append(", maxReduceBonus=").append(maxReduceBonus);
            sb.append('}');
            return sb.toString();
        }
    }

    /**
     * 数据变动赠券
     */
    public static class MsgOperation implements Serializable {

        private static final long serialVersionUID = 7753147722597060955L;

        @ApiField("url_cell")
        private UrlCellData urlCellData;

        public UrlCellData getUrlCellData() {
            return urlCellData;
        }

        public void setUrlCellData(UrlCellData urlCellData) {
            this.urlCellData = urlCellData;
        }

        /**
         * 推荐内容结构体，如示例
         *
         * @author mzlion
         */
        public static class UrlCellData implements Serializable {

            /**
             * 送券的card_id列表，与url字段互斥，不支持普通券和朋友的券混合使用，最多填写10个card_id
             */
            @ApiField("card_id_list")
            private List<String> cardIdList;

            /**
             * 推荐位展示的截止时间
             */
            @ApiField("end_time")
            private long endTime;

            /**
             * 文本内容
             */
            private String text;

            /**
             * 跳转链接，与card_id_list互斥，若设置了跳转url，用户点击模板消息详情后将跳转至该链接领券
             */
            private String url;

            public List<String> getCardIdList() {
                return cardIdList;
            }

            public void setCardIdList(List<String> cardIdList) {
                this.cardIdList = cardIdList;
            }

            public long getEndTime() {
                return endTime;
            }

            public void setEndTime(long endTime) {
                this.endTime = endTime;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }


}
