/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 支付后营销规则结构体
 *
 * @author mzlion on 2017/1/6.
 */
public class PayGiftCardRuleInfoModel extends WxObject {

    /**
     * 营销规则类型，支付即会员填写RULE_TYPE_PAY_MEMBER_CARD
     */
    private String type = "RULE_TYPE_PAY_MEMBER_CARD";

    /**
     * 营销规则结构体
     */
    @ApiField("base_info")
    private BaseInfo baseInfo;

    /**
     * 会员卡结构体
     */
    @ApiField("member_rule")
    private MemberRule memberRule;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BaseInfo getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(BaseInfo baseInfo) {
        this.baseInfo = baseInfo;
    }

    public MemberRule getMemberRule() {
        return memberRule;
    }

    public void setMemberRule(MemberRule memberRule) {
        this.memberRule = memberRule;
    }

    /**
     * 营销规则结构体
     *
     * @author mzlion
     */
    public static class BaseInfo implements Serializable {

        private static final long serialVersionUID = -6819660882509279455L;

        /**
         * 商户号列表，是一个数组结构，建议单次请求100个以下商户号
         */
        @ApiField("mchid_list")
        private List<String> mchIdList;

        /**
         * 规则开始时间
         */
        @ApiField("begin_time")
        private long beginTime;

        /**
         * 规则结束时间
         */
        @ApiField("end_time")
        private long endTime;

        /**
         * 状态
         */
        private String status;

        /**
         * 创建时间
         */
        private Long createTime;

        /**
         * 更新时间
         */
        private Long updateTime;

        public List<String> getMchIdList() {
            return mchIdList;
        }

        public void setMchIdList(List<String> mchIdList) {
            this.mchIdList = mchIdList;
        }

        public long getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(long beginTime) {
            this.beginTime = beginTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("BaseInfo{");
            sb.append("mchIdList=").append(mchIdList);
            sb.append(", beginTime=").append(beginTime);
            sb.append(", endTime=").append(endTime);
            sb.append(", status='").append(status).append('\'');
            sb.append(", createTime=").append(createTime);
            sb.append(", updateTime=").append(updateTime);
            sb.append('}');
            return sb.toString();
        }
    }

    /**
     * 会员卡结构体
     *
     * @author mzlion
     */
    public static class MemberRule implements Serializable {

        /**
         * 要赠送的会员卡card_id
         */
        @ApiField("card_id")
        private String cardId;

        /**
         * 单次消费送会员卡的金额下限，以分为单位
         */
        @ApiField("least_cost")
        private int leastCost;

        /**
         * 单次消费送会员卡的金额上限，以分为单位
         */
        @ApiField("max_cost")
        private Integer maxCost;

        /**
         * 商户自定义领卡网页链接，填入后点击支付即会员消息会跳转至商户网页领卡
         */
        @ApiField("jump_url")
        private String jumpUrl;

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public int getLeastCost() {
            return leastCost;
        }

        public void setLeastCost(int leastCost) {
            this.leastCost = leastCost;
        }

        public Integer getMaxCost() {
            return maxCost;
        }

        public void setMaxCost(Integer maxCost) {
            this.maxCost = maxCost;
        }

        public String getJumpUrl() {
            return jumpUrl;
        }

        public void setJumpUrl(String jumpUrl) {
            this.jumpUrl = jumpUrl;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("MemberRule{");
            sb.append("cardId='").append(cardId).append('\'');
            sb.append(", leastCost=").append(leastCost);
            sb.append(", maxCost=").append(maxCost);
            sb.append(", jumpUrl='").append(jumpUrl).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PayGiftCardRuleInfoModel{");
        sb.append("type='").append(type).append('\'');
        sb.append(", baseInfo=").append(baseInfo);
        sb.append(", memberRule=").append(memberRule);
        sb.append('}');
        return sb.toString();
    }
}

