/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.constants.PoiAvailableStateEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 门店信息扩展了其他信息，扩展的信息均在这里
 *
 * @author mzlion on 2017/4/25.
 */
public class ExtendWxPoiModel extends WxPoiModel {

    /**
     * 门店是否可用状态。1 表示系统错误、2 表示审核中、3 审核通过、4 审核驳回。
     * 当该字段为1、2、4 状态时，poi_id 为空
     */
    @ApiField("available_state")
    private PoiAvailableStateEnum poiAvailableState;

    /**
     * 扩展字段是否正在更新中
     * 1 表示扩展字段正在更新中，尚未生效，不允许再次更新；
     * 0 表示扩展字段没有在更新中或更新已生效，可以再次更新
     *
     * @see #UPDATE_STATUS$UPDATING
     * @see #UPDATE_STATUS$EFFECT
     */
    @ApiField("update_status")
    private Integer updateStatus;

    public ExtendWxPoiModel() {
    }

    public PoiAvailableStateEnum getPoiAvailableState() {
        return poiAvailableState;
    }

    public void setPoiAvailableState(PoiAvailableStateEnum poiAvailableState) {
        this.poiAvailableState = poiAvailableState;
    }

    public Integer getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(Integer updateStatus) {
        this.updateStatus = updateStatus;
    }

    /**
     * 表示扩展字段正在更新中，尚未生效，不允许再次更新
     */
    public static final int UPDATE_STATUS$UPDATING = 1;

    /**
     * 表示扩展字段没有在更新中或更新已生效，可以再次更新
     */
    public static final int UPDATE_STATUS$EFFECT = 0;
}