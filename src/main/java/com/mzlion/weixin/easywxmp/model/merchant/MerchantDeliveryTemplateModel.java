/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.merchant;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 微信小店邮费模板
 *
 * @author mzlion on 2017/4/25.
 */
public class MerchantDeliveryTemplateModel extends WxObject {

    /**
     * 模板ID
     */
    @ApiField("Id")
    private String id;

    /**
     * 邮费模板名称
     */
    @ApiField("Name")
    String name;

    /**
     * 支付方式(0-买家承担运费, 1-卖家承担运费)
     */
    @ApiField("Assumer")
    Integer assumer;

    /**
     * 计费单位
     * 0-按件计费,
     * 1-按重量计费,
     * 2-按体积计费，目前只支持按件计费，默认为0
     */
    @ApiField("Valuation")
    int valuation = 0;

    /**
     * 具体运费计算
     */
    @ApiField("TopFee")
    List<TopFee> topFeeList;

    public MerchantDeliveryTemplateModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAssumer() {
        return assumer;
    }

    public void setAssumer(Integer assumer) {
        this.assumer = assumer;
    }

    public int getValuation() {
        return valuation;
    }

    public void setValuation(int valuation) {
        this.valuation = valuation;
    }

    public List<TopFee> getTopFeeList() {
        return topFeeList;
    }

    public void setTopFeeList(List<TopFee> topFeeList) {
        this.topFeeList = topFeeList;
    }

    public static class TopFee implements Serializable {

        /**
         * 快递类型ID(参见增加商品/快递列表)
         */
        @ApiField("Type")
        private String type;


        /**
         * 默认邮费计算规则
         */
        @ApiField("Normal")
        private NormalFee normalFee;

        /**
         * 指定地区邮费计算方法
         */
        @ApiField("Custom")
        private CustomFee customFee;

        public TopFee() {
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public NormalFee getNormalFee() {
            return normalFee;
        }

        public void setNormalFee(NormalFee normalFee) {
            this.normalFee = normalFee;
        }

        public CustomFee getCustomFee() {
            return customFee;
        }

        public void setCustomFee(CustomFee customFee) {
            this.customFee = customFee;
        }
    }

    /**
     * 默认邮费计算规则
     */
    public static class NormalFee implements Serializable {

        /**
         * 起始计费数量(比如计费单位是按件, 填2代表起始计费为2件)
         */
        @ApiField("StartStandards")
        private int startStandardCount;

        /**
         * 起始计费金额(单位: 分）
         */
        @ApiField("StartFees")
        private int StartFee;

        /**
         * 递增计费数量
         */
        @ApiField("AddStandards")
        private int addStandardCount;

        /**
         * 递增计费金额(单位 : 分)
         */
        @ApiField("AddFees")
        private int addFee;

        public NormalFee() {
        }

        public int getStartStandardCount() {
            return startStandardCount;
        }

        public void setStartStandardCount(int startStandardCount) {
            this.startStandardCount = startStandardCount;
        }

        public int getStartFee() {
            return StartFee;
        }

        public void setStartFee(int startFee) {
            StartFee = startFee;
        }

        public int getAddStandardCount() {
            return addStandardCount;
        }

        public void setAddStandardCount(int addStandardCount) {
            this.addStandardCount = addStandardCount;
        }

        public int getAddFee() {
            return addFee;
        }

        public void setAddFee(int addFee) {
            this.addFee = addFee;
        }
    }

    /**
     * 自定义邮费计算规则
     */
    public static class CustomFee extends NormalFee {

        /**
         * 指定国家(详见《地区列表》说明)
         */
        @ApiField("DestCountry")
        private int destCountry;

        /**
         * 指定省份(详见《地区列表》说明)
         */
        @ApiField("DestProvince")
        private int destProvince;

        /**
         * 指定城市(详见《地区列表》说明)
         */
        @ApiField("DestCity")
        private int destCity;

        public CustomFee() {
        }

        public int getDestCountry() {
            return destCountry;
        }

        public void setDestCountry(int destCountry) {
            this.destCountry = destCountry;
        }

        public int getDestProvince() {
            return destProvince;
        }

        public void setDestProvince(int destProvince) {
            this.destProvince = destProvince;
        }

        public int getDestCity() {
            return destCity;
        }

        public void setDestCity(int destCity) {
            this.destCity = destCity;
        }
    }
}
