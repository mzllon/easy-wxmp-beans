/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;

import java.io.Serializable;
import java.util.List;

/**
 * 发送的图片信息
 *
 * @author mzlion on 2017/1/5.
 */
public class SendPicsInfoModel extends WxObject {

    /**
     * 发送的图片数量
     */
    private Integer count;

    /**
     * 图片列表
     */
    private List<Pic> picList;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Pic> getPicList() {
        return picList;
    }

    public void setPicList(List<Pic> picList) {
        this.picList = picList;
    }

    /**
     * 图片包装
     *
     * @author mzlion
     */
    public static class Pic implements Serializable {

        /**
         * 图片信息
         */
        private PicItem item;

        public PicItem getItem() {
            return item;
        }

        public void setItem(PicItem item) {
            this.item = item;
        }

        public Pic() {
        }

        public Pic(PicItem item) {
            this.item = item;
        }
    }

    /**
     * 图片信息
     *
     * @author mzlion
     */
    public static class PicItem implements Serializable {

        /**
         * 图片的MD5值，开发者若需要，可用于验证接收到图片
         */
        private String picMd5Sum;

        public PicItem() {
        }

        public PicItem(String picMd5Sum) {
            this.picMd5Sum = picMd5Sum;
        }

        /**
         * 图片的MD5值，开发者若需要，可用于验证接收到图片
         *
         * @return 图片的MD5值
         */
        public String getPicMd5Sum() {
            return picMd5Sum;
        }

        /**
         * 图片的MD5值，开发者若需要，可用于验证接收到图片
         *
         * @param picMd5Sum 图片的MD5值
         */
        public void setPicMd5Sum(String picMd5Sum) {
            this.picMd5Sum = picMd5Sum;
        }
    }

}
