package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 商品信息
 *
 * @author mzlion on 2017/4/18.
 */
public class ProductBranInfoModel extends WxObject {

    private static final long serialVersionUID = -7980863595866813L;

    /**
     * 商品的基本信息
     */
    @ApiField("base_info")
    BaseInfo baseInfo;

    /**
     * 商品的详细描述信息
     */
    @ApiField("detail_info")
    DetailInfo detailInfo;

    /**
     * 商品的推广服务区信息
     */
    @ApiField("action_info")
    ActionInfo actionInfo;

    /**
     * 商品的组件信息
     */
    @ApiField("module_info")
    ModuleInfo moduleInfo;

    public ProductBranInfoModel() {
    }

    public BaseInfo getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(BaseInfo baseInfo) {
        this.baseInfo = baseInfo;
    }

    public DetailInfo getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(DetailInfo detailInfo) {
        this.detailInfo = detailInfo;
    }

    public ActionInfo getActionInfo() {
        return actionInfo;
    }

    public void setActionInfo(ActionInfo actionInfo) {
        this.actionInfo = actionInfo;
    }

    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    public void setModuleInfo(ModuleInfo moduleInfo) {
        this.moduleInfo = moduleInfo;
    }


    /**
     * 商品的基本信息
     *
     * @author mzlion
     */
    public static class BaseInfo implements Serializable {

        private static final long serialVersionUID = -6873229366623976852L;
        /**
         * 商品名称，建议不超过15个字，超过部分在客户端上以省略号显示
         */
        String title;

        /**
         * 商品缩略图，推荐尺寸为180px*180px，大小不超过50k，支持jpg、png、gif、jpeg格式
         */
        @ApiField("thumb_url")
        String thumbUrl;

        /**
         * 品牌字段，如“宝洁海飞丝”、“宝洁飘柔”
         */
        @ApiField("brand_tag")
        String brandTag;

        /**
         * 商品类目ID，通过“获取商户信息”接口获取
         */
        @ApiField("category_id")
        String verifiedCateId;

        /**
         * 是否展示有该商品的电商渠道，识别条件是编码内容。auto为自动，由微信识别展示渠道；custom为自定义，商户可指定store_vendorid_list内的渠道出现
         */
        @ApiField("store_mgr_type")
        String storeMgrType;

        /**
         * 电商渠道，如果store_mgr_type为custom，则可从以下电商渠道进行选择：2为亚马逊，3为当当网，4为京东，9为一号店，11为聚美优品，19为酒仙网
         */
        @ApiField("store_vendorid_list")
        List<String> storeVendoridList;

        /**
         * 主页头部背景色。设置“auto”或不填则自动取色；也支持传入十六进制颜色码自定义背景色。比如，“FFFFFF”代表纯白色。注意：颜色码不识别大小写，也不需要传入“#”
         */
        String color;

        /**
         * 商品状态
         */
        String status;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumbUrl() {
            return thumbUrl;
        }

        public void setThumbUrl(String thumbUrl) {
            this.thumbUrl = thumbUrl;
        }

        public String getBrandTag() {
            return brandTag;
        }

        public void setBrandTag(String brandTag) {
            this.brandTag = brandTag;
        }

        public String getVerifiedCateId() {
            return verifiedCateId;
        }

        public void setVerifiedCateId(String verifiedCateId) {
            this.verifiedCateId = verifiedCateId;
        }

        public String getStoreMgrType() {
            return storeMgrType;
        }

        public void setStoreMgrType(String storeMgrType) {
            this.storeMgrType = storeMgrType;
        }

        public List<String> getStoreVendoridList() {
            return storeVendoridList;
        }

        public void setStoreVendoridList(List<String> storeVendoridList) {
            this.storeVendoridList = storeVendoridList;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    /**
     * 商品的详细描述信息
     */
    public static class DetailInfo implements Serializable {

        private static final long serialVersionUID = -8433003338689277573L;
        /**
         * 商品详情页中图文详情可设置多张图片，最多可上传6张
         */
        @ApiField("banner_list")
        List<Banner> bannerList;

        /**
         * 商品详情页中可设置多组商品属性
         */
        @ApiField("detail_list")
        List<Detail> detailList;

        public List<Banner> getBannerList() {
            return bannerList;
        }

        public void setBannerList(List<Banner> bannerList) {
            this.bannerList = bannerList;
        }

        public List<Detail> getDetailList() {
            return detailList;
        }

        public void setDetailList(List<Detail> detailList) {
            this.detailList = detailList;
        }
    }

    /**
     * 商品详情页中图文详情
     */
    public static class Banner implements Serializable {
        private static final long serialVersionUID = 3497042007137148008L;
        /**
         * 商品详情页中图文详情的图片，640px*320px，单张≤200k，支持jpg、png、gif、jpeg格式
         */
        String link;

        /**
         * 商品详情页中图文详情的描述，≤80个汉字
         */
        String desc;

        public Banner() {
        }

        public Banner(String link, String desc) {
            this.link = link;
            this.desc = desc;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

    }

    /**
     * 商品详细介绍
     */
    public static class Detail implements Serializable {

        private static final long serialVersionUID = 8434115641510656299L;
        /**
         * 商品详情页中商品属性名称，≤6个汉字
         */
        String detail;

        /**
         * 商品详情页中商品属性内容，≤80个汉字
         */
        String desc;

        public Detail() {
        }

        public Detail(String detail, String desc) {
            this.detail = detail;
            this.desc = desc;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    /**
     * 商品的推广服务区信息
     */
    public static class ActionInfo implements Serializable {

        private static final long serialVersionUID = 7028887858279069556L;

        /**
         * 商品主页中可设置多个服务栏
         */
        @ApiField("action_list")
        List<Action> actionList;

        public ActionInfo() {
        }

        public ActionInfo(List<Action> actionList) {
            this.actionList = actionList;
        }

        public List<Action> getActionList() {
            return actionList;
        }

        public void setActionList(List<Action> actionList) {
            this.actionList = actionList;
        }
    }

    public static class Action implements Serializable {

        private static final long serialVersionUID = 2069612310258245213L;
        /**
         * 服务栏的类型，
         * Media,视频播放；
         * Text，文字介绍；
         * Link，图片跳转；
         * Link，普通链接；
         * User，公众号；
         * Card，微信卡券；
         * Price，建议零售价；
         * Product，微信小店；
         * Store，电商链接；
         * recommend，商品推荐
         */
        String type;

        //region====类型：视频播放====
        /**
         * 对应的视频链接，仅支持在v.qq.com上传的视频内容，格式请按JSON示例拼接
         */
        String link;

        /**
         * 对应视频的封面，推荐尺寸690px*320px，大小不超过200k，支持jpg、png、gif、jpeg格式
         */
        String image;

        //endregion

        //region====类型：文字介绍====
        /**
         * 对应文字介绍的标题
         */
        String name;

        /**
         * 对应文字介绍的内容
         */
        String text;
        //endregion

        //region====类型：图片跳转====

        // 共享
        //link	是	对应图片跳转后的网页链接
        //image	是	对应跳转入口的图片链接，请参考JSON示例

        /**
         * 值为banner，设置图片跳转类型的服务栏时必填
         */
        @ApiField("showtype")
        String showType;
        //endregion

        //region====类型：普通链接====
        /**
         * 服务栏右侧的引导语，不超过5个汉字
         */
        String digest;
        //endregion

        //region====类型：微信卡券====
        /**
         * 卡券必须为非自定义code（概念说明见微信卡券接口文档）
         */
        @ApiField("cardid")
        String cardId;
        //endregion

        //region====类型：建议零售价====
        /**
         * 表示商品的建议零售价，以“元”为单位
         */
        @ApiField("retail_price")
        String retailPrice;
        //endregion

        //region====类型：微信小店====
        /**
         * 对应小店商品的id，需保证有效
         */
        @ApiField("productid")
        String productId;
        //endregion

        //region====类型：电商链接====
        /**
         * 对应商品的价格，单位元
         */
        @ApiField("sale_price")
        String salePrice;
        //endregion

        //region====类型：商品推荐====
        /**
         * 示商品推荐的方式，目前只支持指定，值为appointed
         */
        @ApiField("recommend_type")
        String recommendType;

        /**
         * 表示指定要推荐的商品列表
         */
        @ApiField("recommend_list")
        List<Recommend> recommendList;

        public Action() {
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getShowType() {
            return showType;
        }

        public void setShowType(String showType) {
            this.showType = showType;
        }

        public String getDigest() {
            return digest;
        }

        public void setDigest(String digest) {
            this.digest = digest;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getRetailPrice() {
            return retailPrice;
        }

        public void setRetailPrice(String retailPrice) {
            this.retailPrice = retailPrice;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(String salePrice) {
            this.salePrice = salePrice;
        }

        public String getRecommendType() {
            return recommendType;
        }

        public void setRecommendType(String recommendType) {
            this.recommendType = recommendType;
        }

        public List<Recommend> getRecommendList() {
            return recommendList;
        }

        public void setRecommendList(List<Recommend> recommendList) {
            this.recommendList = recommendList;
        }
    }

    /**
     * 推荐的商品信息
     */
    public static class Recommend implements Serializable {

        private static final long serialVersionUID = 7505045627551777277L;
        /**
         * 表示被推荐的商品编码格式
         */
        @ApiField("keystandard")
        String keyStandard;

        /**
         * 表示被推荐商品的编码内容
         */
        @ApiField("keystr")
        String keyStr;

        public Recommend() {
        }

        public Recommend(String keyStandard, String keyStr) {
            this.keyStandard = keyStandard;
            this.keyStr = keyStr;
        }

        public String getKeyStandard() {
            return keyStandard;
        }

        public void setKeyStandard(String keyStandard) {
            this.keyStandard = keyStandard;
        }

        public String getKeyStr() {
            return keyStr;
        }

        public void setKeyStr(String keyStr) {
            this.keyStr = keyStr;
        }
    }

    /**
     * 商品的组件信息
     */
    public static class ModuleInfo implements Serializable {

        private static final long serialVersionUID = 6772577514885951084L;
        /**
         * 未来可设置多个组件，目前仅支持防伪组件
         */
        @ApiField("module_list")
        List<Module> moduleList;

        public ModuleInfo() {
        }

        public ModuleInfo(List<Module> moduleList) {
            this.moduleList = moduleList;
        }

        public List<Module> getModuleList() {
            return moduleList;
        }

        public void setModuleList(List<Module> moduleList) {
            this.moduleList = moduleList;
        }
    }

    /**
     * 组件
     */
    public static class Module implements Serializable {

        private static final long serialVersionUID = 5822376448794429360L;
        /**
         * 组件的类型，目前仅包括防伪组件anti_fake
         */
        String type;

        /**
         * 设置为true时，防伪结果使用微信提供的弹窗页面展示，商户仅需调用“商品管理”部分的组件消息接口回传产品真假信息。设置为false时，无防伪弹窗效果
         */
        @ApiField("native_show")
        Boolean nativeShow;

        /**
         * 商户提供的防伪查询链接，当native_show设置为false时必填
         */
        @ApiField("anti_fake_url")
        String antiFakeUrl;

        public Module() {
        }

        public Module(Boolean nativeShow, String type, String antiFakeUrl) {
            this.nativeShow = nativeShow;
            this.type = type;
            this.antiFakeUrl = antiFakeUrl;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean getNativeShow() {
            return nativeShow;
        }

        public void setNativeShow(Boolean nativeShow) {
            this.nativeShow = nativeShow;
        }

        public String getAntiFakeUrl() {
            return antiFakeUrl;
        }

        public void setAntiFakeUrl(String antiFakeUrl) {
            this.antiFakeUrl = antiFakeUrl;
        }
    }
}
