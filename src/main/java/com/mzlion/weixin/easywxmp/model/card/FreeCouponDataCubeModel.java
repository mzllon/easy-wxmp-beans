/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 免费券统计结果
 *
 * @author mzlion on 2017/4/19.
 */
public class FreeCouponDataCubeModel extends SummaryCouponDataCubeModel {

    /**
     * 卡券ID。填写后，指定拉出该卡券的相关数据。
     */
    @ApiField("card_id")
    private String cardId;

    /**
     * cardtype
     * 0：折扣券，
     * 1：代金券，
     * 2：礼品券，
     * 3：优惠券，
     * 4：团购券（暂不支持拉取特殊票券类型数据，电影票、飞机票、会议门票、景区门票）
     */
    @ApiField("card_type")
    private int cardType;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }
}
