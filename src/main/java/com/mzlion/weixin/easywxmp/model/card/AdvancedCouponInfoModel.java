/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model.card;

import com.mzlion.weixin.easywxmp.constants.BizServiceEnum;
import com.mzlion.weixin.easywxmp.constants.WeekEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * Advanced_info（卡券高级信息）字段
 *
 * @author mzlion on 2016/12/23.
 */
public class AdvancedCouponInfoModel implements Serializable {

    /**
     * 使用门槛（条件）字段，若不填写使用条件则在券面拼写：无最低消费限制，全场通用，不限品类；并在使用说明显示：可与其他优惠共享
     *
     * @see UseConditionWrapper
     */
    @ApiField("use_condition")
    private UseConditionWrapper useCondition;

    /**
     * 封面摘要结构体名称
     */
    @ApiField("abstract")
    private AbstractWrapper abstractCover;

    /**
     * 图文列表，显示在详情内页，优惠券券开发者须至少传入一组图文列表
     */
    @ApiField(value = "text_image_list")
    private List<TextImage> textImageList;

    /**
     * 商家服务类型：
     * BIZ_SERVICE_DELIVER 外卖服务；
     * BIZ_SERVICE_FREE_PARK 停车位；
     * BIZ_SERVICE_WITH_PET 可带宠物；
     * BIZ_SERVICE_FREE_WIFI 免费wifi，
     * 可多选
     */
    @ApiField(value = "business_service")
    private List<BizServiceEnum> bizServiceList;

    /**
     * 使用时段限制
     */
    @ApiField(value = "time_limit")
    private List<TimeLimit> timeLimitList;

    public UseConditionWrapper getUseCondition() {
        return useCondition;
    }

    public void setUseCondition(UseConditionWrapper useCondition) {
        this.useCondition = useCondition;
    }

    public AbstractWrapper getAbstractCover() {
        return abstractCover;
    }

    public void setAbstractCover(AbstractWrapper abstractCover) {
        this.abstractCover = abstractCover;
    }

    public List<TextImage> getTextImageList() {
        return textImageList;
    }

    public void setTextImageList(List<TextImage> textImageList) {
        this.textImageList = textImageList;
    }

    public List<BizServiceEnum> getBizServiceList() {
        return bizServiceList;
    }

    public void setBizServiceList(List<BizServiceEnum> bizServiceList) {
        this.bizServiceList = bizServiceList;
    }

    public List<TimeLimit> getTimeLimitList() {
        return timeLimitList;
    }

    public void setTimeLimitList(List<TimeLimit> timeLimitList) {
        this.timeLimitList = timeLimitList;
    }


    /**
     * 使用门槛（条件）字段，若不填写使用条件则在券面拼写：无最低消费限制，全场通用，不限品类；并在使用说明显示：可与其他优惠共享
     */
    public static class UseConditionWrapper implements Serializable {

        /**
         * 指定可用的商品类目，仅用于代金券类型，填入后将在券面拼写适用于xxx
         */
        @ApiField("accept_category")
        private String acceptCategory;

        /**
         * 指定不可用的商品类目，仅用于代金券类型，填入后将在券面拼写不适用于xxxx
         */
        @ApiField("reject_category")
        private String rejectCategory;

        /**
         * 满减门槛字段，可用于兑换券和代金券，填入后将在全面拼写消费满xx元可用。
         */
        @ApiField("least_cost")
        private int leastCost;

        /**
         * 购买xx可用类型门槛，仅用于兑换，填入后自动拼写购买xxx可用。
         */
        @ApiField("object_use_for")
        private String objectUseFor;

        /**
         * 不可以与其他类型共享门槛，填写false时系统将在使用须知里拼写“不可与其他优惠共享”，
         * 填写true时系统将在使用须知里拼写“可与其他优惠共享”，默认为true
         */
        @ApiField("can_use_with_other_discount")
        private Boolean canUseWithOtherDiscount;

        public String getAcceptCategory() {
            return acceptCategory;
        }

        public void setAcceptCategory(String acceptCategory) {
            this.acceptCategory = acceptCategory;
        }

        public String getRejectCategory() {
            return rejectCategory;
        }

        public void setRejectCategory(String rejectCategory) {
            this.rejectCategory = rejectCategory;
        }

        public int getLeastCost() {
            return leastCost;
        }

        public void setLeastCost(int leastCost) {
            this.leastCost = leastCost;
        }

        public String getObjectUseFor() {
            return objectUseFor;
        }

        public void setObjectUseFor(String objectUseFor) {
            this.objectUseFor = objectUseFor;
        }

        public Boolean getCanUseWithOtherDiscount() {
            return canUseWithOtherDiscount;
        }

        public void setCanUseWithOtherDiscount(Boolean canUseWithOtherDiscount) {
            this.canUseWithOtherDiscount = canUseWithOtherDiscount;
        }
    }

    public static class AbstractWrapper {

        /**
         * 封面摘要简介。
         */
        @ApiField("abstract")
        private String abstractDesc;

        /**
         * 封面图片列表，仅支持填入一个封面图片链接，上传图片接口上传获取图片获得链接，填写非CDN链接会报错，并在此填入。
         * 建议图片尺寸像素850*350
         */
        @ApiField("icon_url_list")
        private List<String> iconUrlList;

        public String getAbstractDesc() {
            return abstractDesc;
        }

        public void setAbstractDesc(String abstractDesc) {
            this.abstractDesc = abstractDesc;
        }

        public List<String> getIconUrlList() {
            return iconUrlList;
        }

        public void setIconUrlList(List<String> iconUrlList) {
            this.iconUrlList = iconUrlList;
        }
    }

    /**
     * 图文列表，显示在详情内页，优惠券券开发者须至少传入一组图文列表
     */
    public static class TextImage implements Serializable {

        /**
         * 图片链接，必须调用上传图片接口上传图片获得链接，并在此填入，否则报错
         */
        @ApiField(value = "image_url")
        private String imageUrl;

        /**
         * 图文描述
         */
        private String text;

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("TextImage{");
            sb.append("imageUrl='").append(imageUrl).append('\'');
            sb.append(", text='").append(text).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }


    public static class TimeLimit implements Serializable {

        /**
         * 限制类型枚举值：支持填入{@linkplain WeekEnum}
         * 此处只控制显示，不控制实际使用逻辑，不填默认不显示
         */
        @ApiField(value = "type")
        private WeekEnum week;

        /**
         * 当前type类型下的起始时间（小时），如当前结构体内填写了MONDAY，此处填写了10，则此处表示周一 10:00可用
         */
        @ApiField(value = "begin_hour")
        private Integer beginHour;

        /**
         * 当前type类型下的起始时间（分钟），如当前结构体内填写了MONDAY，begin_hour填写10，此处填写了59，则此处表示周一 10:59可用
         */
        @ApiField(value = "begin_minute")
        private Integer beginMinute;

        /**
         * 当前type类型下的结束时间（小时），如当前结构体内填写了MONDAY，此处填写了20，则此处表示周一 10:00-20:00可用
         */
        @ApiField(value = "end_hour")
        private Integer endHour;

        /**
         * 当前type类型下的结束时间（分钟），如当前结构体内填写了MONDAY，begin_hour填写10，此处填写了59，则此处表示周一 10:59-00:59可用
         */
        @ApiField(value = "end_minute")
        private Integer endMinute;

        public WeekEnum getWeek() {
            return week;
        }

        public void setWeek(WeekEnum week) {
            this.week = week;
        }

        public Integer getBeginHour() {
            return beginHour;
        }

        public void setBeginHour(Integer beginHour) {
            this.beginHour = beginHour;
        }

        public Integer getBeginMinute() {
            return beginMinute;
        }

        public void setBeginMinute(Integer beginMinute) {
            this.beginMinute = beginMinute;
        }

        public Integer getEndHour() {
            return endHour;
        }

        public void setEndHour(Integer endHour) {
            this.endHour = endHour;
        }

        public Integer getEndMinute() {
            return endMinute;
        }

        public void setEndMinute(Integer endMinute) {
            this.endMinute = endMinute;
        }
    }
}
