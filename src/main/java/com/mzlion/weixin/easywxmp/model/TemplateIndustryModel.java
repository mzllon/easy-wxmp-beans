/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.model;

import com.mzlion.weixin.easywxmp.WxObject;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 消息模板行业信息
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateIndustryModel extends WxObject {

    private static final long serialVersionUID = 8343475085629015803L;

    /**
     * 主行业
     */
    @ApiField("first_class")
    private String firstClass;

    /**
     * 副行业
     */
    @ApiField("second_class")
    private String secondClass;

    public String getFirstClass() {
        return firstClass;
    }

    public void setFirstClass(String firstClass) {
        this.firstClass = firstClass;
    }

    public String getSecondClass() {
        return secondClass;
    }

    public void setSecondClass(String secondClass) {
        this.secondClass = secondClass;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Industry:");
        sb.append("firstClass=").append(firstClass);
        sb.append(", secondClass=").append(secondClass);
        return sb.toString();
    }

}
