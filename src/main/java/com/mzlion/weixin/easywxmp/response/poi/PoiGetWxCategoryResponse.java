/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.poi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 门店类目响应参数
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiGetWxCategoryResponse extends WxResponse {

    /**
     * 类目列表
     */
    @ApiField("category_list")
    private List<String> categoryList;

    public List<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<String> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append("PoiGetWxCategoryResponse{");
        sb.append("categoryList=").append(categoryList);
        sb.append('}');
        return sb.toString();
    }
//    @Override
//    public boolean isSuccess() {
//        return this.errCode == null;
//    }
}
