/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.user;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.TagModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 获取公众号已创建的标签
 *
 * @author mzlion on 2017/4/17.
 */
public class TagsGetResponse extends WxResponse {

    /**
     * 标签列表
     */
    @ApiField("tags")
    private List<TagModel> tagList;

    public List<TagModel> getTagList() {
        return tagList;
    }

    public void setTagList(List<TagModel> tagList) {
        this.tagList = tagList;
    }
}
