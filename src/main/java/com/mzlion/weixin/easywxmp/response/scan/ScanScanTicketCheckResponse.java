/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.scan;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 检查wxticket参数
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanScanTicketCheckResponse extends WxResponse {

    private static final long serialVersionUID = 873397892459159151L;

    /**
     * 商品编码标准，支持ean13、ean8和qrcode标准
     */
    @ApiField("keystandard")
    private String keyStandard;

    /**
     * 商品编码内容。直接填写商品条码，
     * 标准是ean13，则直接填写商品条码，如“6901939621608”。
     * 标准是qrcode，二维码的内容可由商户自定义，建议使用商品条码，≤20个字符，由大小字母、数字、下划线和连字符组成。
     * 注意：编码标准是ean13时，编码内容必须在商户的号段之下，否则会报错
     */
    @ApiField("keystr")
    private String keyStr;

    /**
     * 当前访问者的openid，可唯一标识用户
     */
    @ApiField("openid")
    private String openId;

    /**
     * 打开商品主页的场景，scan为扫码，others为其他场景，可能是会话、收藏或朋友圈
     */
    private String scene;

    /**
     * 该条码（二维码）是否被扫描，true为是，false为否
     */
    @ApiField("is_check")
    private boolean isCheck;

    /**
     * 是否关注公众号，true为已关注，false为未关注
     */
    @ApiField("is_contact")
    private boolean isContact;

    public ScanScanTicketCheckResponse() {
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setIsCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }

    public boolean isContact() {
        return isContact;
    }

    public void setIsContact(boolean isContact) {
        this.isContact = isContact;
    }
}
