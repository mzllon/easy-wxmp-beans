/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 查询页面列表
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundPageSearchResponse extends WxResponse {

    private static final long serialVersionUID = 6230428895023289138L;

    /**
     * 查询已有的页面
     */
    @ApiField("data")
    private ShakeAroundPageData shakeAroundPageData;

    public ShakeAroundPageSearchResponse() {
    }

    public ShakeAroundPageData getShakeAroundPageData() {
        return shakeAroundPageData;
    }

    public void setShakeAroundPageData(ShakeAroundPageData shakeAroundPageData) {
        this.shakeAroundPageData = shakeAroundPageData;
    }

    public static class ShakeAroundPageData implements Serializable {

        private static final long serialVersionUID = 7873042464494278209L;

        /**
         * 商户名下的页面总数
         */
        @ApiField("total_count")
        private int totalCount;

        /**
         * 页面列表
         */
        @ApiField("pages")
        private List<ShakeAroundPage> shakeAroundPageList;

        public ShakeAroundPageData() {
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<ShakeAroundPage> getShakeAroundPageList() {
            return shakeAroundPageList;
        }

        public void setShakeAroundPageList(List<ShakeAroundPage> shakeAroundPageList) {
            this.shakeAroundPageList = shakeAroundPageList;
        }
    }

    public static class ShakeAroundPage implements Serializable {

        private static final long serialVersionUID = 5709679389263383983L;

        /**
         * 新增页面的页面id
         */
        @ApiField("page_id")
        private String pageId;

        /**
         * 在摇一摇页面展示的主标题，不超过6个汉字或12个英文字母
         */
        private String title;
        /**
         * 在摇一摇页面展示的副标题，不超过7个汉字或14个英文字母
         */
        private String description;

        /**
         * 在摇一摇页面展示的图片。
         * 图片需先上传至微信侧服务器，用“素材管理-上传图片素材”接口上传图片，返回的图片URL再配置在此处
         */
        @ApiField("icon_url")
        private String iconUrl;

        /**
         * 页面的备注信息，不超过15个汉字或30个英文字母
         */
        private String comment;

        /**
         * 跳转链接
         */
        @ApiField("page_url")
        private String pageUrl;

        public String getPageId() {
            return pageId;
        }

        public void setPageId(String pageId) {
            this.pageId = pageId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIconUrl() {
            return iconUrl;
        }

        public void setIconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getPageUrl() {
            return pageUrl;
        }

        public void setPageUrl(String pageUrl) {
            this.pageUrl = pageUrl;
        }
    }
}
