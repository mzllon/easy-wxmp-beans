/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 对优惠券批价
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayGetPayPriceResponse extends WxResponse {

    private static final long serialVersionUID = 2126132157595470396L;

    /**
     * 本次需要支付的券点总额度
     */
    private double price;

    /**
     * 本次需要支付的免费券点额度
     */
    @ApiField("free_coin")
    private double freeCoin;

    /**
     * 本次需要支付的付费券点额度
     */
    @ApiField("pay_coin")
    private double payCoin;

    public CardPayGetPayPriceResponse() {
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getFreeCoin() {
        return freeCoin;
    }

    public void setFreeCoin(double freeCoin) {
        this.freeCoin = freeCoin;
    }

    public double getPayCoin() {
        return payCoin;
    }

    public void setPayCoin(double payCoin) {
        this.payCoin = payCoin;
    }

}
