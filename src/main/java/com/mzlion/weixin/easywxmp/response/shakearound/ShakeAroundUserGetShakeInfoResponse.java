/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 获取设备及用户信息
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundUserGetShakeInfoResponse extends WxResponse {

    private static final long serialVersionUID = -7396672312241487006L;

    /**
     * 统计数据
     */
    @ApiField("data")
    private ShakeInfoData shakeInfoData;

    public ShakeAroundUserGetShakeInfoResponse() {
    }

    public ShakeInfoData getShakeInfoData() {
        return shakeInfoData;
    }

    public void setShakeInfoData(ShakeInfoData shakeInfoData) {
        this.shakeInfoData = shakeInfoData;
    }

    public static class ShakeInfoData implements Serializable {

        private static final long serialVersionUID = 8014838390370989348L;

        /**
         * 新增页面的页面id
         */
        @ApiField("page_id")
        private String pageId;

        /**
         * 设备信息，包括UUID、major、minor，以及距离
         */
        @ApiField("beacon_info")
        private BeaconInfo beaconInfo;

        /**
         * 商户AppID下用户的唯一标识
         */
        @ApiField("openid")
        private String openId;

        /**
         * 门店ID，有的话则返回，反之不会在JSON格式内
         */
        @ApiField("poi_id")
        private String poiId;

        public ShakeInfoData() {
        }

        public String getPageId() {
            return pageId;
        }

        public void setPageId(String pageId) {
            this.pageId = pageId;
        }

        public BeaconInfo getBeaconInfo() {
            return beaconInfo;
        }

        public void setBeaconInfo(BeaconInfo beaconInfo) {
            this.beaconInfo = beaconInfo;
        }

        public String getOpenId() {
            return openId;
        }

        public void setOpenId(String openId) {
            this.openId = openId;
        }

        public String getPoiId() {
            return poiId;
        }

        public void setPoiId(String poiId) {
            this.poiId = poiId;
        }
    }


    /**
     * 设备信息，包括UUID、major、minor，以及距离
     */
    public static class BeaconInfo implements Serializable {

        private static final long serialVersionUID = 3094427432051700664L;

        /**
         * uuid
         */
        private String uuid;

        /**
         * major
         */
        private String major;

        /**
         * minor
         */
        private String minor;

        private double distance;

        public BeaconInfo() {
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getMajor() {
            return major;
        }

        public void setMajor(String major) {
            this.major = major;
        }

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }
    }
}
