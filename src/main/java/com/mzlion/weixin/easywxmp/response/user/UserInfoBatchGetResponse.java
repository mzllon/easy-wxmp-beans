/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.user;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.UserInfoModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 批量获取用户基本信息
 *
 * @author mzlion on 2017/4/17.
 */
public class UserInfoBatchGetResponse extends WxResponse {

    /**
     * 用户列表
     */
    @ApiField("user_info_list")
    private List<UserInfoModel> userInfoList;

    public List<UserInfoModel> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<UserInfoModel> userInfoList) {
        this.userInfoList = userInfoList;
    }
}
