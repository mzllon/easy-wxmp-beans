/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 充值券点接口
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayRechargeResponse extends WxResponse {

    private static final long serialVersionUID = 2126132157595470396L;

    /**
     * 本次支付的订单号，用于查询订单状态
     */
    @ApiField("order_id")
    private String orderId;

    /**
     * 支付二维码的的链接，开发者可以调用二维码生成的公开库转化为二维码显示在网页上，微信扫码支付
     */
    @ApiField("qrcode_url")
    private String qrCodeUrl;

    /**
     * 二维码的数据流，开发者可以使用写入一个文件的方法显示该二维码
     */
    @ApiField("qrcode_buffer")
    private String qrCodeBuffer;

    public CardPayRechargeResponse() {
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }

    public String getQrCodeBuffer() {
        return qrCodeBuffer;
    }

    public void setQrCodeBuffer(String qrCodeBuffer) {
        this.qrCodeBuffer = qrCodeBuffer;
    }

}
