/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 核查code接口
 *
 * @author mzlion on 2017/4/17.
 */
public class CardCodeCheckCodeResponse extends WxResponse {

    /**
     * 已经成功存入的code
     */
    @ApiField("exist_code")
    private List<String> existCodeList;

    /**
     * 没有存入的code
     */
    @ApiField("not_exist_code")
    private List<String> notExistCodeList;

    public List<String> getExistCodeList() {
        return existCodeList;
    }

    public void setExistCodeList(List<String> existCodeList) {
        this.existCodeList = existCodeList;
    }

    public List<String> getNotExistCodeList() {
        return notExistCodeList;
    }

    public void setNotExistCodeList(List<String> notExistCodeList) {
        this.notExistCodeList = notExistCodeList;
    }
}
