/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 添加portal型设备
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiApPortalRegisterResponse extends WxResponse {

    private static final long serialVersionUID = -6113180357278049366L;

    /**
     * portal设备配置信息
     */
    @ApiField("data")
    private ApPortalConfig apPortalConfig;

    public BizWifiApPortalRegisterResponse() {
    }

    public ApPortalConfig getApPortalConfig() {
        return apPortalConfig;
    }

    public void setApPortalConfig(ApPortalConfig apPortalConfig) {
        this.apPortalConfig = apPortalConfig;
    }

    /**
     * portal设备配置信息
     */
    public static class ApPortalConfig implements Serializable {

        private static final long serialVersionUID = 2240764636145402846L;

        /**
         * 改造portal页面所需参数，该参数用于触发呼起微信的JSAPI接口的sign参数值的计算
         */
        @ApiField("secretkey")
        private String secretKey;

        public ApPortalConfig() {
        }

        public String getSecretKey() {
            return secretKey;
        }

        public void setSecretKey(String secretKey) {
            this.secretKey = secretKey;
        }
    }
}
