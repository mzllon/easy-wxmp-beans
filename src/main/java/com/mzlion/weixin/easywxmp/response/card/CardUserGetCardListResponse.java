/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取用户已领取卡券接口响应参数
 *
 * @author mzlion on 2017/1/6.
 */
public class CardUserGetCardListResponse extends WxResponse {

    /**
     * 卡券列表
     */
    @ApiField("card_list")
    private List<SimpleCard> cardList;

    /**
     * 是否有可用的朋友的券
     */
    @ApiField("has_share_card")
    private Boolean hasShareCard;

    public List<SimpleCard> getCardList() {
        return cardList;
    }

    public void setCardList(List<SimpleCard> cardList) {
        this.cardList = cardList;
    }

    public Boolean getHasShareCard() {
        return hasShareCard;
    }

    public void setHasShareCard(Boolean hasShareCard) {
        this.hasShareCard = hasShareCard;
    }

    /**
     * 简易卡券信息
     */
    public static class SimpleCard implements Serializable {

        /**
         * code
         */
        private String code;

        /**
         * 卡券ID
         */
        @ApiField("card_id")
        private String cardId;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }
    }

}
