/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundStatisticsDataModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 以设备为维度的数据统计接口
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundStatisticsDeviceResponse extends WxResponse {

    private static final long serialVersionUID = 2684512772983419816L;

    /**
     * 统计数据
     */
    @ApiField("data")
    private List<ShakeAroundStatisticsDataModel> statisticsDataList;

    public ShakeAroundStatisticsDeviceResponse() {
    }

    public List<ShakeAroundStatisticsDataModel> getStatisticsDataList() {
        return statisticsDataList;
    }

    public void setStatisticsDataList(List<ShakeAroundStatisticsDataModel> statisticsDataList) {
        this.statisticsDataList = statisticsDataList;
    }

}
