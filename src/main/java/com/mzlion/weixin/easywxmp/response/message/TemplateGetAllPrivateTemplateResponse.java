/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.message;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取模板列表
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateGetAllPrivateTemplateResponse extends WxResponse {

    private static final long serialVersionUID = 7155654701357772024L;

    /**
     * 模板ID
     */
    @ApiField("template_id")
    private String templateId;

    /**
     *模板标题
     */
    private String title;

    /**
     * 模板所属行业的一级行业
     */
    @ApiField("primary_industry")
    private String primaryIndustry;

    /**
     * 模板所属行业的二级行业
     */
    @ApiField("deputy_industry")
    private String deputyIndustry;

    /**
     *模板内容
     */
    private String content;

    /**
     *模板示例
     */
    private String example;

    public TemplateGetAllPrivateTemplateResponse() {
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrimaryIndustry() {
        return primaryIndustry;
    }

    public void setPrimaryIndustry(String primaryIndustry) {
        this.primaryIndustry = primaryIndustry;
    }

    public String getDeputyIndustry() {
        return deputyIndustry;
    }

    public void setDeputyIndustry(String deputyIndustry) {
        this.deputyIndustry = deputyIndustry;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }
}
