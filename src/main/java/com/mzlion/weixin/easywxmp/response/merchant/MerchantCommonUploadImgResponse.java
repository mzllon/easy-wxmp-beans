package com.mzlion.weixin.easywxmp.response.merchant;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 微信小店上传图片
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantCommonUploadImgResponse extends WxResponse {

    /**
     * 图片Url
     */
    @ApiField("image_url")
    private String imageUrl;

    public MerchantCommonUploadImgResponse() {
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
