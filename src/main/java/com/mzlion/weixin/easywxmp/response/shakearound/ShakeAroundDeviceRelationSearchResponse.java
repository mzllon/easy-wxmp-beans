/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 查询设备与页面的关联关系
 *
 * @author on 2017/4/19.
 */
public class ShakeAroundDeviceRelationSearchResponse extends WxResponse {

    private static final long serialVersionUID = 879065952291170979L;

    /**
     * 数据
     */
    @ApiField("data")
    private DeviceRelationToPageDataWrapper deviceRelationToPageData;

    public ShakeAroundDeviceRelationSearchResponse() {
    }

    public DeviceRelationToPageDataWrapper getDeviceRelationToPageData() {
        return deviceRelationToPageData;
    }

    public void setDeviceRelationToPageData(DeviceRelationToPageDataWrapper deviceRelationToPageData) {
        this.deviceRelationToPageData = deviceRelationToPageData;
    }

    public static class DeviceRelationToPageDataWrapper implements Serializable {

        private static final long serialVersionUID = 580037634226946257L;

        /**
         * 关联关系列表
         */
        @ApiField("relations")
        private List<DeviceRelationToPage> deviceRelationToPageList;

        /**
         * 设备或页面的关联关系总数
         */
        @ApiField("total_count")
        private int totalCount;

        public DeviceRelationToPageDataWrapper() {
        }

        public List<DeviceRelationToPage> getDeviceRelationToPageList() {
            return deviceRelationToPageList;
        }

        public void setDeviceRelationToPageList(List<DeviceRelationToPage> deviceRelationToPageList) {
            this.deviceRelationToPageList = deviceRelationToPageList;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }
    }

    public static class DeviceRelationToPage implements Serializable {

        private static final long serialVersionUID = 9094846138670679511L;

        /**
         * 设备编号
         */
        @ApiField("device_id")
        private String deviceId;

        /**
         * uuid
         */
        private String uuid;

        /**
         * major
         */
        private String major;

        /**
         * minor
         */
        private String minor;

        /**
         * 摇周边页面唯一ID
         */
        @ApiField("page_id")
        private String pageId;

        public DeviceRelationToPage() {
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getMajor() {
            return major;
        }

        public void setMajor(String major) {
            this.major = major;
        }

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }

        public String getPageId() {
            return pageId;
        }

        public void setPageId(String pageId) {
            this.pageId = pageId;
        }
    }
}
