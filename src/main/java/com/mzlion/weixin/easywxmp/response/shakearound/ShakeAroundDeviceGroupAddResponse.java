/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceGroupDataModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 新增分组
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceGroupAddResponse extends WxResponse {

    private static final long serialVersionUID = -2221202576052095002L;

    /**
     * 申请结果
     */
    @ApiField("data")
    private ShakeAroundDeviceGroupDataModel groupData;

    public ShakeAroundDeviceGroupAddResponse() {
    }

    public ShakeAroundDeviceGroupDataModel getGroupData() {
        return groupData;
    }

    public void setGroupData(ShakeAroundDeviceGroupDataModel groupData) {
        this.groupData = groupData;
    }

}
