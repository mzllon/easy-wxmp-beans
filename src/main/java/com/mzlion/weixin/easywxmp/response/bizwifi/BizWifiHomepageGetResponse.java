/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 查询商家主页
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiHomepageGetResponse extends WxResponse {

    private static final long serialVersionUID = -7754959626152480033L;

    /**
     * 商家主页
     */
    @ApiField("data")
    private Homepage homepage;

    public BizWifiHomepageGetResponse() {
    }

    public Homepage getHomepage() {
        return homepage;
    }

    public void setHomepage(Homepage homepage) {
        this.homepage = homepage;
    }

    /**
     * 商家主页
     */
    public static class Homepage implements Serializable {

        private static final long serialVersionUID = -3632080706014843492L;

        /**
         * 门店ID
         */
        @ApiField("shop_id")
        private String shopId;

        /**
         * 模板ID，
         * 0-默认模板，
         * 1-自定义url
         */
        @ApiField("template_id")
        private int templateId;

        /**
         * 自定义链接，当template_id为1时必填
         */
        private String url;

        public Homepage() {
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public int getTemplateId() {
            return templateId;
        }

        public void setTemplateId(int templateId) {
            this.templateId = templateId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
