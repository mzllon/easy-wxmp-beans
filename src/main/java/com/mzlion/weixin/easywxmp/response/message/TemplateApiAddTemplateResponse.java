/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.message;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.TemplateIndustryModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获得模板ID
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateApiAddTemplateResponse extends WxResponse {

    private static final long serialVersionUID = 7155654701357772024L;

    /**
     * 帐号设置的主营行业
     */
    @ApiField("template_id")
    private TemplateIndustryModel templateId;


    public TemplateApiAddTemplateResponse() {
    }

    public TemplateIndustryModel getTemplateId() {
        return templateId;
    }

    public void setTemplateId(TemplateIndustryModel templateId) {
        this.templateId = templateId;
    }
}
