/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 更新会员信息
 *
 * @author mzlion on 2017/05/25.
 */
public class CardMemberCardUpdateUserResponse extends WxResponse {

    /**
     * 用户在本公众号内唯一识别码
     */
    @ApiField("openid")
    private String openId;

    /**
     * 当前用户积分总额
     */
    @ApiField("result_bonus")
    private Integer resultBonus;

    /**
     * 当前用户预存总金额
     */
    private Integer resultBalance;


    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Integer getResultBonus() {
        return resultBonus;
    }

    public void setResultBonus(Integer resultBonus) {
        this.resultBonus = resultBonus;
    }

    public Integer getResultBalance() {
        return resultBalance;
    }

    public void setResultBalance(Integer resultBalance) {
        this.resultBalance = resultBalance;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }

}
