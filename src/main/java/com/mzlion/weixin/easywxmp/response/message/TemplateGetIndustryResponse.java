/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.message;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.TemplateIndustryModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取设置的行业信息
 *
 * @author mzlion on 2017/4/20.
 */
public class TemplateGetIndustryResponse extends WxResponse {

    private static final long serialVersionUID = 7155654701357772024L;

    /**
     * 帐号设置的主营行业
     */
    @ApiField("primary_industry")
    private TemplateIndustryModel primaryIndustry;

    /**
     * 帐号设置的副营行业
     */
    @ApiField("secondary_industry")
    private TemplateIndustryModel secondaryIndustry;

    public TemplateGetIndustryResponse() {
    }

    public TemplateIndustryModel getPrimaryIndustry() {
        return primaryIndustry;
    }

    public void setPrimaryIndustry(TemplateIndustryModel primaryIndustry) {
        this.primaryIndustry = primaryIndustry;
    }

    public TemplateIndustryModel getSecondaryIndustry() {
        return secondaryIndustry;
    }

    public void setSecondaryIndustry(TemplateIndustryModel secondaryIndustry) {
        this.secondaryIndustry = secondaryIndustry;
    }
}
