/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 批量查询页面统计数据接口
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundStatisticsPageListResponse extends WxResponse {

    private static final long serialVersionUID = -7396672312241487006L;

    /**
     * 所查询的日期时间戳
     */
    private Long date;

    /**
     * 设备总数
     */
    @ApiField("total_count")
    private int totalCount;

    /**
     * 指定查询的结果页序号；返回结果按摇周边人数降序排序，每50条记录为一页
     */
    @ApiField("page_index")
    private Integer pageIndex;

    /**
     * 统计数据
     */
    @ApiField("data")
    private StatisticsPageListDataWrapper statisticsPageListData;

    public ShakeAroundStatisticsPageListResponse() {
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public StatisticsPageListDataWrapper getStatisticsPageListData() {
        return statisticsPageListData;
    }

    public void setStatisticsPageListData(StatisticsPageListDataWrapper statisticsPageListData) {
        this.statisticsPageListData = statisticsPageListData;
    }

    public static class StatisticsPageListDataWrapper implements Serializable {

        private static final long serialVersionUID = -8032224740090636084L;

        /**
         * 页面统计
         */
        private List<StatisticsPageListData> statisticsPageListDataList;

        public StatisticsPageListDataWrapper() {
        }

        public List<StatisticsPageListData> getStatisticsPageListDataList() {
            return statisticsPageListDataList;
        }

        public void setStatisticsPageListDataList(List<StatisticsPageListData> statisticsPageListDataList) {
            this.statisticsPageListDataList = statisticsPageListDataList;
        }
    }

    public static class StatisticsPageListData implements Serializable {

        private static final long serialVersionUID = 8713303506326956314L;

        /**
         * 页面ID
         */
        @ApiField("page_id")
        private String pageId;

        /**
         * 点击摇周边消息的次数
         */
        @ApiField("click_pv")
        private int clickPv;

        /**
         * 点击摇周边消息的人数
         */
        @ApiField("click_uv")
        private int clickUv;

        /**
         * 摇周边的次数
         */
        @ApiField("shake_pv")
        private int shakePv;

        /**
         * 摇周边的人数
         */
        @ApiField("shake_uv")
        private int shakeUv;

        public StatisticsPageListData() {
        }

        public String getPageId() {
            return pageId;
        }

        public void setPageId(String pageId) {
            this.pageId = pageId;
        }

        public int getClickPv() {
            return clickPv;
        }

        public void setClickPv(int clickPv) {
            this.clickPv = clickPv;
        }

        public int getClickUv() {
            return clickUv;
        }

        public void setClickUv(int clickUv) {
            this.clickUv = clickUv;
        }

        public int getShakePv() {
            return shakePv;
        }

        public void setShakePv(int shakePv) {
            this.shakePv = shakePv;
        }

        public int getShakeUv() {
            return shakeUv;
        }

        public void setShakeUv(int shakeUv) {
            this.shakeUv = shakeUv;
        }
    }
}
