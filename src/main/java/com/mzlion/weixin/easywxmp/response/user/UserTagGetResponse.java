/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.user;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.UserOpenIdsModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取标签下粉丝列表
 *
 * @author mzlion on 2017/4/17.
 */
public class UserTagGetResponse extends WxResponse {

    /**
     * 这次获取的粉丝数量
     */
    private int count;

    /**
     * 拉取列表最后一个用户的openid
     */
    @ApiField("next_openid")
    private String nextOpenId;

    /**
     * 粉丝信息
     */
    @ApiField("data")
    private UserOpenIdsModel userFans;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNextOpenId() {
        return nextOpenId;
    }

    public void setNextOpenId(String nextOpenId) {
        this.nextOpenId = nextOpenId;
    }

    public UserOpenIdsModel getUserFans() {
        return userFans;
    }

    public void setUserFans(UserOpenIdsModel userFans) {
        this.userFans = userFans;
    }

}
