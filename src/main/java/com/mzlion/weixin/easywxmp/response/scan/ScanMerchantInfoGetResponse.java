/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.scan;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取商户信息
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanMerchantInfoGetResponse extends WxResponse {

    private static final long serialVersionUID = -6946005105090752852L;

    /**
     * 品牌标签列表，创建商品时传入，商户自定义生成的品牌标识字段
     */
    @ApiField("brand_tag_list")
    List<String> brandTagList;

    @ApiField("verified_list")
    List<Verifiability> verifiedList;

    public List<String> getBrandTagList() {
        return brandTagList;
    }

    public void setBrandTagList(List<String> brandTagList) {
        this.brandTagList = brandTagList;
    }

    public List<Verifiability> getVerifiedList() {
        return verifiedList;
    }

    public void setVerifiedList(List<Verifiability> verifiedList) {
        this.verifiedList = verifiedList;
    }

    /**
     * 权限实体
     */
    public static class Verifiability implements Serializable {

        private static final long serialVersionUID = -1219842482536775581L;

        /**
         * 商户号段，表示该商户下有资质的条码号段
         */
        @ApiField("verified_firm_code")
        String verifiedFirmCode;

        @ApiField("verified_cate_list")
        List<VerifiedCare> verifiedCateList;

        public String getVerifiedFirmCode() {
            return verifiedFirmCode;
        }

        public void setVerifiedFirmCode(String verifiedFirmCode) {
            this.verifiedFirmCode = verifiedFirmCode;
        }

        public List<VerifiedCare> getVerifiedCateList() {
            return verifiedCateList;
        }

        public void setVerifiedCateList(List<VerifiedCare> verifiedCateList) {
            this.verifiedCateList = verifiedCateList;
        }
    }

    /**
     * 商户类目
     */
    public static class VerifiedCare implements Serializable {

        private static final long serialVersionUID = -5084005994977725618L;

        /**
         * 商户类目ID，表示该商户下可用于创建商的类目ID
         */
        @ApiField("verified_cate_id")
        String verifiedCateId;

        /**
         * 商户类目名称，对应类目ID的名称
         */
        @ApiField("verified_cate_name")
        String verifiedCateName;

        public String getVerifiedCateId() {
            return verifiedCateId;
        }

        public void setVerifiedCateId(String verifiedCateId) {
            this.verifiedCateId = verifiedCateId;
        }

        public String getVerifiedCateName() {
            return verifiedCateName;
        }

        public void setVerifiedCateName(String verifiedCateName) {
            this.verifiedCateName = verifiedCateName;
        }
    }
}
