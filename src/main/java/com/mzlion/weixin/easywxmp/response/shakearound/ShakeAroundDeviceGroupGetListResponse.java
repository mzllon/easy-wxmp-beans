/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceGroupDataModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 查询分组列表
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceGroupGetListResponse extends WxResponse {

    private static final long serialVersionUID = 5624035042638597337L;

    /**
     * 申请结果
     */
    @ApiField("data")
    private ShakeAroundDeviceGroupGetListDataWrapper groupGetListData;

    public ShakeAroundDeviceGroupGetListResponse() {
    }

    public ShakeAroundDeviceGroupGetListDataWrapper getGroupGetListData() {
        return groupGetListData;
    }

    public void setGroupGetListData(ShakeAroundDeviceGroupGetListDataWrapper groupGetListData) {
        this.groupGetListData = groupGetListData;
    }

    public static class ShakeAroundDeviceGroupGetListDataWrapper implements Serializable {

        private static final long serialVersionUID = 5522098591822390696L;

        /**
         * 此账号下现有的总分组数
         */
        @ApiField("total_count")
        private int totalCount;

        /**
         * 申请结果
         */
        @ApiField("groups")
        private List<ShakeAroundDeviceGroupDataModel> groupList;

        public ShakeAroundDeviceGroupGetListDataWrapper() {
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<ShakeAroundDeviceGroupDataModel> getGroupList() {
            return groupList;
        }

        public void setGroupList(List<ShakeAroundDeviceGroupDataModel> groupList) {
            this.groupList = groupList;
        }
    }
}
