/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.user;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.UserOpenIdsModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取公众号的黑名单列表
 *
 * @author mzlion on 2017/4/17.
 */
public class TagsMembersGetBlackListResponse extends WxResponse {

    /**
     * 黑名单用户总数
     */
    private int total;

    /**
     * 拉取的OPENID个数，最大值为10000
     */
    private int count;

    /**
     * 拉取列表最后一个用户的openid
     */
    @ApiField("next_openid")
    private String nextOpenId;

    /**
     * 粉丝信息
     */
    @ApiField("data")
    private UserOpenIdsModel userOpenIds;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNextOpenId() {
        return nextOpenId;
    }

    public void setNextOpenId(String nextOpenId) {
        this.nextOpenId = nextOpenId;
    }

    public UserOpenIdsModel getUserOpenIds() {
        return userOpenIds;
    }

    public void setUserOpenIds(UserOpenIdsModel userOpenIds) {
        this.userOpenIds = userOpenIds;
    }
}
