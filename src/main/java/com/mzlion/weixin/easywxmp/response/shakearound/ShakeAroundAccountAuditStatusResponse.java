/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 查询摇一摇审核状态
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundAccountAuditStatusResponse extends WxResponse {

    private static final long serialVersionUID = -775448671439529442L;

    /**
     * 摇一摇审核结果
     */
    @ApiField("data")
    private AccountAudit accountAudit;

    public ShakeAroundAccountAuditStatusResponse() {
    }

    public AccountAudit getAccountAudit() {
        return accountAudit;
    }

    public void setAccountAudit(AccountAudit accountAudit) {
        this.accountAudit = accountAudit;
    }

    /**
     * 摇一摇审核结果
     */
    public static class AccountAudit implements Serializable {

        private static final long serialVersionUID = -3276326397695903460L;

        /**
         * 提交申请的时间戳
         */
        @ApiField("apply_time")
        private Long applyTime;
        /**
         * 审核状态。0：审核未通过、1：审核中、2：审核已通过；审核会在三个工作日内完成
         */
        @ApiField("audit_status")
        private String auditStatus;
        /**
         * 审核备注，包括审核不通过的原因
         */
        @ApiField("audit_comment")
        private String auditComment;
        /**
         * 确定审核结果的时间戳；若状态为审核中，则该时间值为0
         */
        @ApiField("audit_time")
        private Long auditTime;

        public AccountAudit() {
        }

        public Long getApplyTime() {
            return applyTime;
        }

        public void setApplyTime(Long applyTime) {
            this.applyTime = applyTime;
        }

        public String getAuditStatus() {
            return auditStatus;
        }

        public void setAuditStatus(String auditStatus) {
            this.auditStatus = auditStatus;
        }

        public String getAuditComment() {
            return auditComment;
        }

        public void setAuditComment(String auditComment) {
            this.auditComment = auditComment;
        }

        public Long getAuditTime() {
            return auditTime;
        }

        public void setAuditTime(Long auditTime) {
            this.auditTime = auditTime;
        }
    }
}
