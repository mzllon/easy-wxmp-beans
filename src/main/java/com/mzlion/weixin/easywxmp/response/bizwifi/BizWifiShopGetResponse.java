/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.BizWifiShopModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询门店Wi-Fi信息
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiShopGetResponse extends WxResponse {

    private static final long serialVersionUID = 4086325191627900261L;

    /**
     * 门店Wi-Fi信息
     */
    @ApiField("data")
    private BizWifiShopModel bizWifiShop;

    public BizWifiShopGetResponse() {
    }

    public BizWifiShopModel getBizWifiShop() {
        return bizWifiShop;
    }

    public void setBizWifiShop(BizWifiShopModel bizWifiShop) {
        this.bizWifiShop = bizWifiShop;
    }
}
