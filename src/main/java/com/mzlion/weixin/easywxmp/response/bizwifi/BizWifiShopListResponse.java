/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.BizWifiShopModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取Wi-Fi门店列表
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiShopListResponse extends WxResponse {

    private static final long serialVersionUID = 3970467906185298114L;

    /**
     * WIFI门店
     */
    @ApiField("data")
    private ShopDataWrapper shopData;

    public BizWifiShopListResponse() {
    }

    public BizWifiShopListResponse(ShopDataWrapper shopData) {
        this.shopData = shopData;
    }

    public ShopDataWrapper getShopData() {
        return shopData;
    }

    public void setShopData(ShopDataWrapper shopData) {
        this.shopData = shopData;
    }

    /**
     * WIFI门店
     */
    public static class ShopDataWrapper implements Serializable {

        private static final long serialVersionUID = -2328590974168923205L;

        /**
         * 总数
         */
        @ApiField("totalcount")
        private int totalCount;

        /**
         * 分页下标，默认从1开始
         */
        @ApiField("pageindex")
        private int pageIndex = 1;

        /**
         * 分页页数
         */
        @ApiField("pagecount")
        private int pageCount;

        /**
         * 门店列表
         */
        @ApiField("records")
        private List<BizWifiShopModel> shopList;

        public ShopDataWrapper() {
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public List<BizWifiShopModel> getShopList() {
            return shopList;
        }

        public void setShopList(List<BizWifiShopModel> shopList) {
            this.shopList = shopList;
        }
    }

}
