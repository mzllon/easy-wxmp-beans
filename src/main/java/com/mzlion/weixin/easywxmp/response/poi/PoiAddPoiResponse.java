/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.poi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 创建门店响应参数
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiAddPoiResponse extends WxResponse {

    /**
     * 微信的门店ID，微信内门店唯一标示ID
     */
    @ApiField("poi_id")
    private String poiId;

    /**
     * 微信的门店ID，微信内门店唯一标示ID
     */
    public String getPoiId() {
        return poiId;
    }

    /**
     * 微信的门店ID，微信内门店唯一标示ID
     */
    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }
}
