/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.merchant;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.MerchantProductStatusEnum;
import com.mzlion.weixin.easywxmp.model.merchant.MerchantProductInfoModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 查询商品
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantGetByStatusResponse extends WxResponse {

    /**
     * 商品列表
     */
    @ApiField("products_info")
    private List<ProductInfoData> productInfoDataList;

    public MerchantGetByStatusResponse() {
    }

    public List<ProductInfoData> getProductInfoDataList() {
        return productInfoDataList;
    }

    public void setProductInfoDataList(List<ProductInfoData> productInfoDataList) {
        this.productInfoDataList = productInfoDataList;
    }

    /**
     * 商品详情
     */
    public static class ProductInfoData implements Serializable {

        /**
         * 商品ID
         */
        @ApiField("product_id")
        private String productId;

        /**
         *基本属性
         */
        @ApiField("product_base")
        private MerchantProductInfoModel productInfo;

        /**
         * sku信息列表(可为多个)，每个sku信息串即为一个确定的商品，比如白色的37码的鞋子
         */
        @ApiField("sku_list")
        private List<MerchantProductInfoModel.SkuDetail> skuDetailList;

        /**
         * 商品其他属性
         */
        @ApiField("attrext")
        private MerchantProductInfoModel.AttrExtInfo attrExtInfo;

        /**
         * 商品状态
         */
        @ApiField("status")
        private MerchantProductStatusEnum productStatus;

        public ProductInfoData() {
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public MerchantProductInfoModel getProductInfo() {
            return productInfo;
        }

        public void setProductInfo(MerchantProductInfoModel productInfo) {
            this.productInfo = productInfo;
        }

        public List<MerchantProductInfoModel.SkuDetail> getSkuDetailList() {
            return skuDetailList;
        }

        public void setSkuDetailList(List<MerchantProductInfoModel.SkuDetail> skuDetailList) {
            this.skuDetailList = skuDetailList;
        }

        public MerchantProductInfoModel.AttrExtInfo getAttrExtInfo() {
            return attrExtInfo;
        }

        public void setAttrExtInfo(MerchantProductInfoModel.AttrExtInfo attrExtInfo) {
            this.attrExtInfo = attrExtInfo;
        }

        public MerchantProductStatusEnum getProductStatus() {
            return productStatus;
        }

        public void setProductStatus(MerchantProductStatusEnum productStatus) {
            this.productStatus = productStatus;
        }
    }
}
