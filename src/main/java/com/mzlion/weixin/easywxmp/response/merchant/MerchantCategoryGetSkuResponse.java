/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.merchant;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取指定分类的所有SKU
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantCategoryGetSkuResponse extends WxResponse {

    /**
     * SKU属性表
     */
    @ApiField("sku_table")
    private List<SkuTable> skuTableList;

    public MerchantCategoryGetSkuResponse() {
    }

    public List<SkuTable> getSkuTableList() {
        return skuTableList;
    }

    public void setSkuTableList(List<SkuTable> skuTableList) {
        this.skuTableList = skuTableList;
    }

    /**
     * SKU
     */
    public static class Sku implements Serializable {

        /**
         * sku id
         */
        private String id;

        /**
         * sku名称
         */
        private String name;

        public Sku() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * SKU属性表
     */
    public static class SkuTable extends Sku {

        /**
         * SKU属性列表
         */
        @ApiField("value_list")
        private List<Sku> valueList;


        public SkuTable() {
        }

        public List<Sku> getValueList() {
            return valueList;
        }

        public void setValueList(List<Sku> valueList) {
            this.valueList = valueList;
        }
    }
}
