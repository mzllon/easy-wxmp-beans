/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 卡券开放类目查询接口
 *
 * @author mzlion on 2016/12/22.
 */
public class CardGetApplyProtocolResponse extends WxResponse {

    private static final long serialVersionUID = -4816599663987894345L;

    /**
     * 卡券开放类目列表
     */
    @ApiField("category")
    private List<PrimaryCardApplyCategory> primaryCardApplyCategoryList;

    public CardGetApplyProtocolResponse() {
    }

    public List<PrimaryCardApplyCategory> getPrimaryCardApplyCategoryList() {
        return primaryCardApplyCategoryList;
    }

    public void setPrimaryCardApplyCategoryList(List<PrimaryCardApplyCategory> primaryCardApplyCategoryList) {
        this.primaryCardApplyCategoryList = primaryCardApplyCategoryList;
    }

    /**
     * 一级卡券开放类目
     *
     * @author mzlion  on 2016/12/22.
     */
    public static class PrimaryCardApplyCategory implements Serializable {

        private static final long serialVersionUID = -4182730358596149556L;

        /**
         * 一级目录id
         */
        @ApiField("primary_category_id")
        private Integer primaryCategoryId;

        /**
         * 一级类目名称
         */
        @ApiField("category_name")
        private String categoryName;

        /**
         * 二级类目列表
         */
        @ApiField("secondary_category")
        private List<SecondaryCardApplyCategory> secondaryCategoryList;

        public Integer getPrimaryCategoryId() {
            return primaryCategoryId;
        }

        public void setPrimaryCategoryId(Integer primaryCategoryId) {
            this.primaryCategoryId = primaryCategoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public List<SecondaryCardApplyCategory> getSecondaryCategoryList() {
            return secondaryCategoryList;
        }

        public void setSecondaryCategoryList(List<SecondaryCardApplyCategory> secondaryCategoryList) {
            this.secondaryCategoryList = secondaryCategoryList;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("PrimaryCardApplyCategory{");
            sb.append("primaryCategoryId=").append(primaryCategoryId);
            sb.append(", categoryName='").append(categoryName).append('\'');
            sb.append(", secondaryCategoryList=").append(secondaryCategoryList);
            sb.append('}');
            return sb.toString();
        }
    }

    /**
     * 二级卡券开放类目
     *
     * @author mzlion on 2016/12/22.
     */
    public static class SecondaryCardApplyCategory implements Serializable {

        private static final long serialVersionUID = -7889484623583114240L;

        /**
         * 二级目录id
         */
        @ApiField("secondary_category_id")
        private Integer secondaryCategoryId;

        /**
         * 二级类目名称
         */
        @ApiField("category_name")
        private String categoryName;

        @ApiField("can_choose_prepaid_card")
        private int canChoosePrepaidCard;
        @ApiField("can_choose_payment_card")
        private int canChoosePaymentCard;

        public Integer getSecondaryCategoryId() {
            return secondaryCategoryId;
        }

        public void setSecondaryCategoryId(Integer secondaryCategoryId) {
            this.secondaryCategoryId = secondaryCategoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public int getCanChoosePrepaidCard() {
            return canChoosePrepaidCard;
        }

        public void setCanChoosePrepaidCard(int canChoosePrepaidCard) {
            this.canChoosePrepaidCard = canChoosePrepaidCard;
        }

        public int getCanChoosePaymentCard() {
            return canChoosePaymentCard;
        }

        public void setCanChoosePaymentCard(int canChoosePaymentCard) {
            this.canChoosePaymentCard = canChoosePaymentCard;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("SecondaryCardApplyCategory{");
            sb.append("secondaryCategoryId=").append(secondaryCategoryId);
            sb.append(", categoryName='").append(categoryName).append('\'');
            sb.append(", canChoosePrepaidCard=").append(canChoosePrepaidCard);
            sb.append(", canChoosePaymentCard=").append(canChoosePaymentCard);
            sb.append('}');
            return sb.toString();
        }
    }
}
