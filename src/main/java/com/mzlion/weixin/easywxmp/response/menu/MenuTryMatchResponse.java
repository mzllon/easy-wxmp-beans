/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.menu;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.MenuButtonModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 测试个性化菜单匹配结果响应参数
 *
 * @author mzlion on 2017/1/5.
 */
public class MenuTryMatchResponse extends WxResponse {

    private static final long serialVersionUID = -2342600270257838726L;

    /**
     * 菜单配置
     */
    @ApiField(value = "button")
    private List<MenuButtonModel> menuButtonList;

    public List<MenuButtonModel> getMenuButtonList() {
        return menuButtonList;
    }

    public void setMenuButtonList(List<MenuButtonModel> menuButtonList) {
        this.menuButtonList = menuButtonList;
    }
}
