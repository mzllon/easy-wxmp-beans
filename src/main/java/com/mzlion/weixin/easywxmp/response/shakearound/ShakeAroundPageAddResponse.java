/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 新增摇一摇出来的页面信息
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundPageAddResponse extends WxResponse {

    private static final long serialVersionUID = 2121203356187228460L;

    /**
     * 页面信息
     */
    @ApiField("data")
    private PageDataWrapper pageData;

    public ShakeAroundPageAddResponse() {
    }

    public PageDataWrapper getPageData() {
        return pageData;
    }

    public void setPageData(PageDataWrapper pageData) {
        this.pageData = pageData;
    }

    /**
     * 页面信息
     */
    public static class PageDataWrapper implements Serializable {

        private static final long serialVersionUID = 792038629643383326L;
        /**
         * 新增页面的页面id
         */
        @ApiField("page_id")
        private String pageId;

        public PageDataWrapper() {
        }

        public String getPageId() {
            return pageId;
        }

        public void setPageId(String pageId) {
            this.pageId = pageId;
        }
    }
}
