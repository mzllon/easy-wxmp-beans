/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.datacube;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取用户增减数据
 *
 * @author mzlion on 2017/4/17.
 */
public class DataCubeGetUserCumulateResponse extends WxResponse {

    /**
     * 统计列表
     */
    @ApiField("list")
    private List<UserSummaryWrapper> userSummaryList;

    public List<UserSummaryWrapper> getUserSummaryList() {
        return userSummaryList;
    }

    public void setUserSummaryList(List<UserSummaryWrapper> userSummaryList) {
        this.userSummaryList = userSummaryList;
    }

    /**
     * 累计用户数据
     */
    public static class UserSummaryWrapper implements Serializable {

        /**
         * 数据的日期
         */
        @ApiField("ref_date")
        private String refDate;

        /**
         * 总用户量
         */
        @ApiField("cumulate_user")
        private int cumulateUser;

        public String getRefDate() {
            return refDate;
        }

        public void setRefDate(String refDate) {
            this.refDate = refDate;
        }

        public int getCumulateUser() {
            return cumulateUser;
        }

        public void setCumulateUser(int cumulateUser) {
            this.cumulateUser = cumulateUser;
        }
    }
}
