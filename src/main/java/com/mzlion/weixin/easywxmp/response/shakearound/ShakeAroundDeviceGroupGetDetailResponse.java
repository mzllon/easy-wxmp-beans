/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceGroupDataModel;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 查询分组详情
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceGroupGetDetailResponse extends WxResponse {

    private static final long serialVersionUID = 5624035042638597337L;

    /**
     * 分组明细
     */
    @ApiField("data")
    private ShakeAroundDeviceGroupGetDetailData groupGetDetailData;

    public ShakeAroundDeviceGroupGetDetailResponse() {
    }

    public ShakeAroundDeviceGroupGetDetailData getGroupGetDetailData() {
        return groupGetDetailData;
    }

    public void setGroupGetDetailData(ShakeAroundDeviceGroupGetDetailData groupGetDetailData) {
        this.groupGetDetailData = groupGetDetailData;
    }

    public static class ShakeAroundDeviceGroupGetDetailData extends ShakeAroundDeviceGroupDataModel {

        private static final long serialVersionUID = 6420296481259712539L;

        /**
         * 此账号下现有的总分组数
         */
        @ApiField("total_count")
        private int totalCount;

        /**
         * 申请结果
         */
        @ApiField("groups")
        private List<ShakeAroundDeviceIdentifierModel> deviceList;

        public ShakeAroundDeviceGroupGetDetailData() {
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<ShakeAroundDeviceIdentifierModel> getDeviceList() {
            return deviceList;
        }

        public void setDeviceList(List<ShakeAroundDeviceIdentifierModel> deviceList) {
            this.deviceList = deviceList;
        }
    }
}
