/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.poi;


import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ExtendWxPoiModel;
import com.mzlion.weixin.easywxmp.model.WxPoiWrapperModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询门店信息响应参数
 * <p>
 * 创建门店接口是为商户提供创建自己门店数据的接口，门店数据字段越完整，商户页面展示越丰富，越能够吸引更多用户，并提高曝光度。
 * 创建门店接口调用成功后会返回errcode 0、errmsg ok，会实时返回唯一的poiid。
 * </p>
 *
 * @author mzlion on 2017/4/24.
 */
public class PoiGetPoiResponse extends WxResponse {

    /**
     * 门店信息
     */
    @ApiField("business")
    private WxPoiWrapperModel<ExtendWxPoiModel> wxPoiWrapper;

    public PoiGetPoiResponse() {
    }

    public WxPoiWrapperModel<ExtendWxPoiModel> getWxPoiWrapper() {
        return wxPoiWrapper;
    }

    public void setWxPoiWrapper(WxPoiWrapperModel<ExtendWxPoiModel> wxPoiWrapper) {
        this.wxPoiWrapper = wxPoiWrapper;
    }
}
