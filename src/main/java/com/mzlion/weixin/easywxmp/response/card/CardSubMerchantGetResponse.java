/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.card.CardSubMerchantModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 更新子商户接口响应对象
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantGetResponse extends WxResponse {

    @ApiField("info")
    private CardSubMerchantModel cardSubMerchant;

    public CardSubMerchantGetResponse() {
    }

    public CardSubMerchantModel getCardSubMerchant() {
        return cardSubMerchant;
    }

    public void setCardSubMerchant(CardSubMerchantModel cardSubMerchant) {
        this.cardSubMerchant = cardSubMerchant;
    }

}
