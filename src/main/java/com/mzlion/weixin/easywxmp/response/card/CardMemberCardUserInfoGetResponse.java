/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.CommonFieldIdEnum;
import com.mzlion.weixin.easywxmp.constants.UserCardStatusEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 拉取会员信息（积分查询）接口
 *
 * @author mzlion on 2016/12/25.
 */
public class CardMemberCardUserInfoGetResponse extends WxResponse {

    /**
     * 用户在本公众号内唯一识别码
     */
    @ApiField("openid")
    private String openId;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 会员卡编号
     */
    @ApiField("membership_number")
    private String membershipNumber;

    /**
     * 积分信息
     */
    private Integer bonus;

    /**
     * 余额信息
     */
    private String balance;

    /**
     * 用户性别
     */
    private String sex;

    /**
     * 会员信息
     */
    @ApiField("user_info")
    private UserInfo userInfo;

    /**
     * 当前用户的会员卡状态
     *
     * @see UserCardStatusEnum
     */
    @ApiField("user_card_status")
    private UserCardStatusEnum userCardStatus;

    /**
     * 该卡是否已经被激活，true表示已经被激活，false表示未被激活
     */
    @ApiField("has_active")
    private Boolean hasActive;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMembershipNumber() {
        return membershipNumber;
    }

    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = membershipNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserCardStatusEnum getUserCardStatus() {
        return userCardStatus;
    }

    public void setUserCardStatus(UserCardStatusEnum userCardStatus) {
        this.userCardStatus = userCardStatus;
    }

    public Boolean isHasActive() {
        return hasActive;
    }

    public void setHasActive(Boolean hasActive) {
        this.hasActive = hasActive;
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }

    /**
     * 会员信息
     *
     * @author mzlion on 2016/12/25.
     */
    public static class UserInfo implements Serializable {

        private static final long serialVersionUID = 4605613734009735631L;

        /**
         * 开发者设置的会员卡会员信息类目，如等级。
         */
        @ApiField("common_field_list")
        private List<CommonField> commonFieldList;

        /**
         * 开发者设置的会员卡会员信息类目，如等级。
         */
        @ApiField("custom_field_list")
        private List<CustomField> customFieldList;

        public List<CommonField> getCommonFieldList() {
            return commonFieldList;
        }

        public void setCommonFieldList(List<CommonField> commonFieldList) {
            this.commonFieldList = commonFieldList;
        }

        public List<CustomField> getCustomFieldList() {
            return customFieldList;
        }

        public void setCustomFieldList(List<CustomField> customFieldList) {
            this.customFieldList = customFieldList;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }


    /**
     * 通用类目信息
     *
     * @author mzlion on 2016/12/25.
     */
    public static class CommonField implements Serializable {

        private static final long serialVersionUID = 3605260868065858879L;

        /**
         * 会员信息类目名称
         */
        @ApiField("name")
        private CommonFieldIdEnum commonFieldId;

        /**
         * 会员卡信息类目值，比如等级值等
         */
        private String value;

        public CommonFieldIdEnum getCommonFieldId() {
            return commonFieldId;
        }

        public void setCommonFieldId(CommonFieldIdEnum commonFieldId) {
            this.commonFieldId = commonFieldId;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }

    /**
     * 自定义类目信息
     */
    public static class CustomField implements Serializable {

        private static final long serialVersionUID = -8051828649411209514L;

        /**
         * 会员信息类目名称
         */
        private String name;

        /**
         * 会员卡信息类目值，比如等级值等
         */
        private String value;

        /**
         * 填写项目为多选时的返回
         */
        @ApiField("value_list")
        private List<String> valueList;

        public CustomField() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public List<String> getValueList() {
            return valueList;
        }

        public void setValueList(List<String> valueList) {
            this.valueList = valueList;
        }

        @Override
        public String toString() {
            return ReflectionUtils.toString(this);
        }
    }

}
