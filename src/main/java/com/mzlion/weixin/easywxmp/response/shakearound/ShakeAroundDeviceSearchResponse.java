/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ShakeAroundDeviceIdentifierModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 查询设备列表
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundDeviceSearchResponse extends WxResponse {

    private static final long serialVersionUID = 1205826426444379213L;

    /**
     * 查询设备的信息
     */
    @ApiField("data")
    private ShakeAroundDeviceIdentifierDataWrapper shakeAroundDeviceIdentifierData;

    public ShakeAroundDeviceSearchResponse() {
    }

    public ShakeAroundDeviceIdentifierDataWrapper getShakeAroundDeviceIdentifierData() {
        return shakeAroundDeviceIdentifierData;
    }

    public void setShakeAroundDeviceIdentifierData(ShakeAroundDeviceIdentifierDataWrapper shakeAroundDeviceIdentifierData) {
        this.shakeAroundDeviceIdentifierData = shakeAroundDeviceIdentifierData;
    }

    /**
     * 查询设备的信息
     */
    public static class ShakeAroundDeviceIdentifierDataWrapper implements Serializable {

        private static final long serialVersionUID = -460819914678647670L;

        /**
         * 指定的设备信息列表
         */
        @ApiField("devices")
        private List<ShakeAroundDeviceIdentifierModel> shakeAroundDeviceIdentifierList;

        /**
         * 商户名下的设备总量
         */
        @ApiField("total_count")
        private int totalCount;

        public ShakeAroundDeviceIdentifierDataWrapper() {
        }

        public List<ShakeAroundDeviceIdentifierModel> getShakeAroundDeviceIdentifierList() {
            return shakeAroundDeviceIdentifierList;
        }

        public void setShakeAroundDeviceIdentifierList(List<ShakeAroundDeviceIdentifierModel> shakeAroundDeviceIdentifierList) {
            this.shakeAroundDeviceIdentifierList = shakeAroundDeviceIdentifierList;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }
    }
}
