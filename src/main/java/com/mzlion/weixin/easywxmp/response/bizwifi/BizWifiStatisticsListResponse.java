/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * Wi-Fi数据统计
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiStatisticsListResponse extends WxResponse {

    private static final long serialVersionUID = 8927436721012487813L;

    /**
     * Wi-Fi数据统计
     */
    @ApiField("data")
    private List<ShopStatistics> shopStatisticsList;

    public BizWifiStatisticsListResponse() {
    }

    public List<ShopStatistics> getShopStatisticsList() {
        return shopStatisticsList;
    }

    public void setShopStatisticsList(List<ShopStatistics> shopStatisticsList) {
        this.shopStatisticsList = shopStatisticsList;
    }

    /**
     * Wi-Fi数据统计
     */
    public static class ShopStatistics implements Serializable {

        private static final long serialVersionUID = 5666560082106708450L;

        /**
         * 门店ID，-1为总统计
         */
        @ApiField("shop_id")
        private String shopId;

        /**
         * 统计时间，单位为毫秒
         */
        @ApiField("statis_time")
        private int statisTime;

        /**
         * Wi-Fi连接总人数
         */
        @ApiField("total_user")
        private String totalUser;

        /**
         * 商家主页访问人数
         */
        @ApiField("homepage_uv")
        private String homepage_uv;

        /**
         * 新增公众号关注人数
         */
        @ApiField("new_fans")
        private String newFans;

        /**
         * 累计公众号关注人数
         */
        @ApiField("total_fans")
        private String totalFans;

        /**
         * 微信方式连Wi-Fi人数
         */
        @ApiField("wxconnect_user")
        private String wxConnectUser;

        /**
         * 连网后消息发送人数
         */
        @ApiField("connect_msg_user")
        private String connectMsgUser;

        public ShopStatistics() {
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public int getStatisTime() {
            return statisTime;
        }

        public void setStatisTime(int statisTime) {
            this.statisTime = statisTime;
        }

        public String getTotalUser() {
            return totalUser;
        }

        public void setTotalUser(String totalUser) {
            this.totalUser = totalUser;
        }

        public String getHomepage_uv() {
            return homepage_uv;
        }

        public void setHomepage_uv(String homepage_uv) {
            this.homepage_uv = homepage_uv;
        }

        public String getNewFans() {
            return newFans;
        }

        public void setNewFans(String newFans) {
            this.newFans = newFans;
        }

        public String getTotalFans() {
            return totalFans;
        }

        public void setTotalFans(String totalFans) {
            this.totalFans = totalFans;
        }

        public String getWxConnectUser() {
            return wxConnectUser;
        }

        public void setWxConnectUser(String wxConnectUser) {
            this.wxConnectUser = wxConnectUser;
        }

        public String getConnectMsgUser() {
            return connectMsgUser;
        }

        public void setConnectMsgUser(String connectMsgUser) {
            this.connectMsgUser = connectMsgUser;
        }
    }
}
