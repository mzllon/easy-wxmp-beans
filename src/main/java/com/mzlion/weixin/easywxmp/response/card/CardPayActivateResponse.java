/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;

/**
 * 开通券点账户接口
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayActivateResponse extends WxResponse {

    private static final long serialVersionUID = 2126132157595470396L;

    /**
     * 奖励券点数量，以元为单位，微信卡券对每一个新开通券点账户的商户奖励200个券点
     */
    private int reward;

    public CardPayActivateResponse() {
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }
}
