/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.message;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.KFAccountModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 获取所有客服账号
 *
 * @author mzlion on 2017/4/16.
 */
public class CustomServiceKFAccountGetKfListResponse extends WxResponse {

    /**
     * 客服列表
     */
    @ApiField("kf_list")
    private List<KFAccountModel> kfAccountModelList;

    public List<KFAccountModel> getKfAccountModelList() {
        return kfAccountModelList;
    }

    public void setKfAccountModelList(List<KFAccountModel> kfAccountModelList) {
        this.kfAccountModelList = kfAccountModelList;
    }
}
