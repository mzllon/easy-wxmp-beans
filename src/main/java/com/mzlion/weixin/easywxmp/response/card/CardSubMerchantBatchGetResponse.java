/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.card.CardSubMerchantModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 子商户批量查询接口响应结果对象
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantBatchGetResponse extends WxResponse {

    private static final long serialVersionUID = -3641077870741742602L;

    /**
     * 商户列表
     */
    @ApiField("info_list")
    private List<CardSubMerchantModel> merchantList;

    /**
     * 拉渠道列表中最后一个子商户的id
     */
    @ApiField("next_begin_id")
    private String nextBeginId;

    public CardSubMerchantBatchGetResponse() {
    }

    public List<CardSubMerchantModel> getMerchantList() {
        return merchantList;
    }

    public void setMerchantList(List<CardSubMerchantModel> merchantList) {
        this.merchantList = merchantList;
    }

    public String getNextBeginId() {
        return nextBeginId;
    }

    public void setNextBeginId(String nextBeginId) {
        this.nextBeginId = nextBeginId;
    }
}
