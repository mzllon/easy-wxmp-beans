package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 查询门店卡券投放信息
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiCouponPutGetResponse extends WxResponse {

    private static final long serialVersionUID = -7098719806599946795L;

    /**
     * 卡券投放信息
     */
    @ApiField("data")
    private CouponPutData couponPutData;

    public BizWifiCouponPutGetResponse() {
    }

    public CouponPutData getCouponPutData() {
        return couponPutData;
    }

    public void setCouponPutData(CouponPutData couponPutData) {
        this.couponPutData = couponPutData;
    }

    /**
     * 卡券投放信息
     */
    public static class CouponPutData implements Serializable {

        private static final long serialVersionUID = 8026452576789220092L;

        /**
         * 门店ID
         */
        @ApiField("shop_id")
        private String shopId;

        /**
         * 卡券ID
         */
        @ApiField("card_id")
        private String cardId;

        /**
         * 卡券投放状态（0表示生效中，1表示未生效，2表示已过期）
         */
        @ApiField("card_status")
        private String cardStatus;

        /**
         * 卡券描述
         */
        @ApiField("card_describe")
        private String cardDescribe;

        /**
         * 卡券投放开始时间（单位是秒）
         */
        @ApiField("start_time")
        private Long startTime;

        /**
         * 卡券投放结束时间（单位是秒）
         */
        @ApiField("end_time")
        private Long endTime;

        public CouponPutData() {
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getCardStatus() {
            return cardStatus;
        }

        public void setCardStatus(String cardStatus) {
            this.cardStatus = cardStatus;
        }

        public String getCardDescribe() {
            return cardDescribe;
        }

        public void setCardDescribe(String cardDescribe) {
            this.cardDescribe = cardDescribe;
        }

        public Long getStartTime() {
            return startTime;
        }

        public void setStartTime(Long startTime) {
            this.startTime = startTime;
        }

        public Long getEndTime() {
            return endTime;
        }

        public void setEndTime(Long endTime) {
            this.endTime = endTime;
        }
    }
}
