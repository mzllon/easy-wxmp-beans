/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.merchant;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取所有分组
 *
 * @author mzlion on 2017/4/25.
 */
public class MerchantGroupGetAllResponse extends WxResponse {

    /**
     * 分组ID
     */
    @ApiField("groups_detail")
    private List<GroupDetail> groupDetailList;

    public MerchantGroupGetAllResponse() {
    }

    public List<GroupDetail> getGroupDetailList() {
        return groupDetailList;
    }

    public void setGroupDetailList(List<GroupDetail> groupDetailList) {
        this.groupDetailList = groupDetailList;
    }

    public static class GroupDetail implements Serializable {

        /**
         * 分组ID
         */
        @ApiField("group_id")
        private String groupId;

        /**
         * 分组名称
         */
        @ApiField("group_name")
        private String groupName;

        public GroupDetail() {
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }
    }
}
