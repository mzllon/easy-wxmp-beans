/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 获取物料二维码
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiQRCodeGetResponse extends WxResponse {

    private static final long serialVersionUID = 5848835747925423793L;
    /**
     * 物料二维码
     */
    @ApiField("data")
    private QrCodeData qrCodeData;

    public BizWifiQRCodeGetResponse() {
    }

    public QrCodeData getQrCodeData() {
        return qrCodeData;
    }

    public void setQrCodeData(QrCodeData qrCodeData) {
        this.qrCodeData = qrCodeData;
    }

    /**
     * 物料二维码
     */
    public static class QrCodeData implements Serializable {

        private static final long serialVersionUID = -2103404586670072410L;

        /**
         * 二维码图片url
         */
        @ApiField("qrcode_url")
        private String qrCodeUrl;

        public QrCodeData() {
        }

        public String getQrCodeUrl() {
            return qrCodeUrl;
        }

        public void setQrCodeUrl(String qrCodeUrl) {
            this.qrCodeUrl = qrCodeUrl;
        }
    }
}
