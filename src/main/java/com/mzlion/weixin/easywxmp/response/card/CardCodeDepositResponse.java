/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 导入code接口
 *
 * @author mzlion on 2017/4/17.
 */
public class CardCodeDepositResponse extends WxResponse {

    /**
     * 成功个数
     */
    @ApiField("succ_code")
    private int succCode;

    /**
     * 重复导入的code会自动被过滤
     */
    @ApiField("duplicate_code")
    private String duplicateCode;

    /**
     * 失败个数
     */
    @ApiField("fail_code")
    private int failCode;

    public int getSuccCode() {
        return succCode;
    }

    public void setSuccCode(int succCode) {
        this.succCode = succCode;
    }

    public String getDuplicateCode() {
        return duplicateCode;
    }

    public void setDuplicateCode(String duplicateCode) {
        this.duplicateCode = duplicateCode;
    }

    public int getFailCode() {
        return failCode;
    }

    public void setFailCode(int failCode) {
        this.failCode = failCode;
    }
}
