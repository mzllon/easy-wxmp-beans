/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 对优惠券批价
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayGetOrderListResponse extends WxResponse {

    private static final long serialVersionUID = 2126132157595470396L;

    /**
     * 符合条件的订单总数量
     */
    @ApiField("total_num")
    private int totalNum;

    /**
     * 订单信息
     */
    @ApiField("order_list")
    private List<OrderInfo> orderInfoList;

    public CardPayGetOrderListResponse() {
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public List<OrderInfo> getOrderInfoList() {
        return orderInfoList;
    }

    public void setOrderInfoList(List<OrderInfo> orderInfoList) {
        this.orderInfoList = orderInfoList;
    }

    public static class OrderInfo implements Serializable {

        private static final long serialVersionUID = -1750999875585079713L;

        /**
         * 订单号
         */
        @ApiField("order_id")
        private String orderId;

        /**
         * 订单状态
         * ORDER_STATUS_WAITING 等待支付
         * ORDER_STATUS_SUCC 支付成功
         * ORDER_STATUS_FINANCE_SUCC 加代币成功
         * ORDER_STATUS_QUANTITY_SUCC 加库存成功
         * ORDER_STATUS_HAS_REFUND 已退币
         * ORDER_STATUS_REFUND_WAITING 等待退币确认
         * ORDER_STATUS_ROLLBACK 已回退,系统失败
         * ORDER_STATUS_HAS_RECEIPT 已开发票
         */
        private String status;

        /**
         * 订单创建时间
         */
        @ApiField("create_time")
        private Long createTime;

        /**
         * 支付完成时间
         */
        @ApiField("pay_finish_time")
        private Long payFinishTime;

        /**
         * 支付描述，一般为微信支付充值
         */
        private String desc;

        /**
         * 本次充值的付费券点数量，以元为单位
         */
        @ApiField("free_coin_count")
        private double freeCoinCount;

        /**
         * 二维码的数据流，开发者可以使用写入一个文件的方法显示该二维码
         */
        @ApiField("pay_coin_count")
        private double payCoinCount;

        /**
         * 回退的免费券点
         */
        @ApiField("refund_free_coin_count")
        private double refundFreeCoinCount;

        /**
         * 回退的付费券点
         */
        @ApiField("refund_pay_coin_count")
        private double refundPayCoinCount;

        /**
         * 支付人的openid
         */
        @ApiField("openId")
        private String openid;

        /**
         * 订单类型
         * ORDER_TYPE_WXPAY 为充值
         */
        @ApiField("order_tpye")
        private String order_tpye;

        public OrderInfo() {
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        public Long getPayFinishTime() {
            return payFinishTime;
        }

        public void setPayFinishTime(Long payFinishTime) {
            this.payFinishTime = payFinishTime;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public double getFreeCoinCount() {
            return freeCoinCount;
        }

        public void setFreeCoinCount(double freeCoinCount) {
            this.freeCoinCount = freeCoinCount;
        }

        public double getPayCoinCount() {
            return payCoinCount;
        }

        public void setPayCoinCount(double payCoinCount) {
            this.payCoinCount = payCoinCount;
        }

        public double getRefundFreeCoinCount() {
            return refundFreeCoinCount;
        }

        public void setRefundFreeCoinCount(double refundFreeCoinCount) {
            this.refundFreeCoinCount = refundFreeCoinCount;
        }

        public double getRefundPayCoinCount() {
            return refundPayCoinCount;
        }

        public void setRefundPayCoinCount(double refundPayCoinCount) {
            this.refundPayCoinCount = refundPayCoinCount;
        }

        public String getOpenid() {
            return openid;
        }

        public void setOpenid(String openid) {
            this.openid = openid;
        }

        public String getOrder_tpye() {
            return order_tpye;
        }

        public void setOrder_tpye(String order_tpye) {
            this.order_tpye = order_tpye;
        }
    }
}
