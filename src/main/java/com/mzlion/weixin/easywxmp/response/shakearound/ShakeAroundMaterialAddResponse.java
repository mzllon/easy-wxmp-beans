/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * ​上传图片素材
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundMaterialAddResponse extends WxResponse {

    private static final long serialVersionUID = 2909407961908462199L;

    /**
     * 图片素材
     */
    @ApiField("data")
    private MaterialData materialData;

    /**
     * 图片素材
     */
    public static class MaterialData implements Serializable {

        private static final long serialVersionUID = -2341704105520122184L;

        /**
         * 图片url地址，若type=icon，可用在“新增页面”和“编辑页面”的“icon_url”字段；
         * 若type= license，可用在“申请入驻”的“qualification_cert_urls”字段
         */
        @ApiField("pic_url")
        private String picUrl;

        public MaterialData() {
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }
    }
}
