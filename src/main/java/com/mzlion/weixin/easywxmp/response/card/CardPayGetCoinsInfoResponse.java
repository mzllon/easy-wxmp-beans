/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 查询券点余额接口
 *
 * @author mzlion on 2017/4/20.
 */
public class CardPayGetCoinsInfoResponse extends WxResponse {

    private static final long serialVersionUID = 2126132157595470396L;

    /**
     * 全部券点数目
     */
    @ApiField("total_coin")
    private double totalCoin;

    /**
     * 免费券点数目
     */
    @ApiField("free_coin")
    private double freeCoin;

    /**
     * 付费券点额度
     */
    @ApiField("pay_coin")
    private double payCoin;

    public CardPayGetCoinsInfoResponse() {
    }

    public double getTotalCoin() {
        return totalCoin;
    }

    public void setTotalCoin(double totalCoin) {
        this.totalCoin = totalCoin;
    }

    public double getFreeCoin() {
        return freeCoin;
    }

    public void setFreeCoin(double freeCoin) {
        this.freeCoin = freeCoin;
    }

    public double getPayCoin() {
        return payCoin;
    }

    public void setPayCoin(double payCoin) {
        this.payCoin = payCoin;
    }
}
