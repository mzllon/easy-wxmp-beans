/**
 * 素材管理(封装了接口响应对象)
 *
 * @author mzlion on 2017/4/20.
 */
package com.mzlion.weixin.easywxmp.response.media;