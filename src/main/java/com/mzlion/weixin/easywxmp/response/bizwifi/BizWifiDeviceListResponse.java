/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 查询设备
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiDeviceListResponse extends WxResponse {

    private static final long serialVersionUID = 1240903257678397233L;

    /**
     * 设备
     */
    @ApiField("data")
    private DeviceDataWrapper deviceData;

    public BizWifiDeviceListResponse() {
    }

    public DeviceDataWrapper getDeviceData() {
        return deviceData;
    }

    public void setDeviceData(DeviceDataWrapper deviceData) {
        this.deviceData = deviceData;
    }

    /**
     * 设备
     */
    public static class DeviceDataWrapper implements Serializable {

        private static final long serialVersionUID = 6294761967370397919L;

        /**
         * 总数
         */
        @ApiField("totalcount")
        private int totalCount;

        /**
         * 分页下标，默认从1开始
         */
        @ApiField("pageindex")
        private int pageIndex = 1;

        /**
         * 分页页数
         */
        @ApiField("pagecount")
        private int pageCount;

        /**
         * 设备列表
         */
        @ApiField("records")
        private List<Device> deviceList;

    }

    /**
     * 设备信息
     */
    public static class Device implements Serializable {

        private static final long serialVersionUID = 5952079847473935346L;

        /**
         * 门店ID
         */
        @ApiField("shop_id")
        private String shopId;

        /**
         * 连网设备ssid
         */
        private String ssid;

        /**
         * 无线MAC地址
         */
        private String bssid;

        /**
         * 门店内设备的设备类型，
         * 0-未添加设备，
         * 4-密码型设备，
         * 31-portal型设备
         */
        @ApiField("protocol_type")
        private String protocolType;

        public Device() {
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getBssid() {
            return bssid;
        }

        public void setBssid(String bssid) {
            this.bssid = bssid;
        }

        public String getProtocolType() {
            return protocolType;
        }

        public void setProtocolType(String protocolType) {
            this.protocolType = protocolType;
        }
    }
}
