/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.card.CardSubMerchantModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 创建子商户接口响应参数
 *
 * @author mzlion on 2016/12/22.
 */
public class CardSubMerchantSubmitResponse extends WxResponse {

    private static final long serialVersionUID = -851085437368121811L;

    @ApiField("info")
    private CardSubMerchantModel cardSubMerchant;

    public CardSubMerchantSubmitResponse() {
    }

    public CardSubMerchantModel getCardSubMerchant() {
        return cardSubMerchant;
    }

    public void setCardSubMerchant(CardSubMerchantModel cardSubMerchant) {
        this.cardSubMerchant = cardSubMerchant;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CardSubMerchantSubmitResponse{");
        sb.append("cardSubMerchant=").append(cardSubMerchant);
        sb.append('}');
        return sb.toString();
    }
}
