/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.menu;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.MenuButtonModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取自定义菜单配置接口请求参数
 *
 * @author mzlion on 2017-1-5
 */
public class MenuGetCurrentSelfMenuInfoResponse extends WxResponse {

    private static final long serialVersionUID = 5402506659802973888L;

    /**
     * 菜单是否开启，0代表未开启，1代表开启
     */
    @ApiField("is_menu_open")
    private Integer isMenuOpen;

    /**
     * 菜单信息
     */
    @ApiField("selfmenu_info")
    private SelfMenuInfoWrapper selfMenuInfo;

    public Integer getIsMenuOpen() {
        return isMenuOpen;
    }

    public void setIsMenuOpen(Integer isMenuOpen) {
        this.isMenuOpen = isMenuOpen;
    }

    public SelfMenuInfoWrapper getSelfMenuInfo() {
        return selfMenuInfo;
    }

    public void setSelfMenuInfo(SelfMenuInfoWrapper selfMenuInfo) {
        this.selfMenuInfo = selfMenuInfo;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GetCurrentSelfMenuInfoResponse{");
        sb.append("isMenuOpen=").append(isMenuOpen);
        sb.append(", selfMenuInfo=").append(selfMenuInfo);
        sb.append('}');
        return sb.toString();
    }

    /**
     * 菜单包装类
     *
     * @author mzlion
     */
    public static class SelfMenuInfoWrapper implements Serializable {

        @ApiField("button")
        private List<PriButtonWrapper> buttonList;

        public List<PriButtonWrapper> getButtonList() {
            return buttonList;
        }

        public void setButtonList(List<PriButtonWrapper> buttonList) {
            this.buttonList = buttonList;
        }
    }

    /**
     * 一级菜单包装类
     *
     * @author mzlion on 2017/1/4.
     */
    public static class PriButtonWrapper {

        /**
         * 菜单名称
         */
        private String name;

        /**
         * 二级菜单数组，个数应为1~5个
         */
        @ApiField("sub_button")
        private SubButtonWrapper subButton;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public SubButtonWrapper getSubButton() {
            return subButton;
        }

        public void setSubButton(SubButtonWrapper subButton) {
            this.subButton = subButton;
        }
    }

    /**
     * 二级菜单包装类
     *
     * @author mzlion on 2017/1/4.
     */
    public static class SubButtonWrapper implements Serializable {

        /**
         * 二级菜单列表
         */
        @ApiField("list")
        private final List<ButtonWrapper> subButtonList;

        public SubButtonWrapper(List<ButtonWrapper> subButtonList) {
            this.subButtonList = subButtonList;
        }

        public List<ButtonWrapper> getSubButtonList() {
            return subButtonList;
        }

    }

    /**
     * 扩展菜单信息
     */
    public static class ButtonWrapper extends MenuButtonModel {

        /**
         * 图文消息的信息
         */
        @ApiField("news_info")
        private final NewsInfoWrapper newsInfo;


        public ButtonWrapper(NewsInfoWrapper newsInfo) {
            this.newsInfo = newsInfo;
        }

        public NewsInfoWrapper getNewsInfo() {
            return newsInfo;
        }
    }

    /**
     * 图文消息的信息包装类
     *
     * @author mzlion
     */
    public static class NewsInfoWrapper implements Serializable {

        /**
         * 图文消息的信息
         */
        @ApiField("list")
        private final List<NewsWrapper> newsList;

        public NewsInfoWrapper(List<NewsWrapper> newsList) {
            this.newsList = newsList;
        }

        public List<NewsWrapper> getNewsList() {
            return newsList;
        }

    }


    /**
     * 图文消息的信息
     *
     * @author mzlion
     */
    public static class NewsWrapper implements Serializable {

        /**
         * 图文消息的标题
         */
        private String title;

        /**
         * 摘要
         */
        private String digest;

        /**
         * 作者
         */
        private String author;

        /**
         * 是否显示封面，0为不显示，1为显示
         */
        @ApiField("show_cover")
        private String showCover;

        /**
         * 正文的URL
         */
        @ApiField("content_url")
        private String contentUrl;

        /**
         * 原文的URL，若置空则无查看原文入口
         */
        @ApiField("source_url")
        private String sourceUrl;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDigest() {
            return digest;
        }

        public void setDigest(String digest) {
            this.digest = digest;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getShowCover() {
            return showCover;
        }

        public void setShowCover(String showCover) {
            this.showCover = showCover;
        }

        public String getContentUrl() {
            return contentUrl;
        }

        public void setContentUrl(String contentUrl) {
            this.contentUrl = contentUrl;
        }

        public String getSourceUrl() {
            return sourceUrl;
        }

        public void setSourceUrl(String sourceUrl) {
            this.sourceUrl = sourceUrl;
        }
    }

}
