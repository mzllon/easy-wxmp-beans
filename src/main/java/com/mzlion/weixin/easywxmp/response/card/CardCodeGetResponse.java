/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.constants.UserCardStatusEnum;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 *
 * Created by mzlion on 2017/4/17.
 */
public class CardCodeGetResponse extends WxResponse {

    /**
     * 卡券信息
     */
    private SimpleCard card;

    /**
     * 用户openid
     */
    @ApiField("openid")
    private String openId;

    /**
     * 是否可以核销，true为可以核销，false为不可核销
     */
    @ApiField("can_consume")
    private Boolean canConsume;

    /**
     * 当前code对应卡券的状态
     * NORMAL          正常
     * CONSUMED     已核销
     * EXPIRE              已过期
     * GIFTING            转赠中
     * GIFT_TIMEOUT  转赠超时
     * DELETE              已删除
     * UNAVAILABLE   已失效
     * code未被添加或被转赠领取的情况则统一报错：invalid serial code
     */
    @ApiField("user_card_status")
    private UserCardStatusEnum userCardStatus;

    public SimpleCard getCard() {
        return card;
    }

    public void setCard(SimpleCard card) {
        this.card = card;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Boolean getCanConsume() {
        return canConsume;
    }

    public void setCanConsume(Boolean canConsume) {
        this.canConsume = canConsume;
    }

    public UserCardStatusEnum getUserCardStatus() {
        return userCardStatus;
    }

    public void setUserCardStatus(UserCardStatusEnum userCardStatus) {
        this.userCardStatus = userCardStatus;
    }

    /**
     * 卡券简易信息
     */
    public static class SimpleCard implements Serializable {

        /**
         * 卡券ID
         */
        @ApiField("card_id")
        private String cardId;

        /**
         * 起始使用时间
         */
        @ApiField("begin_time")
        private Long beginTime;

        /**
         * 结束时间
         */
        @ApiField("end_time")
        private Long endTime;

        public SimpleCard() {
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public Long getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(Long beginTime) {
            this.beginTime = beginTime;
        }

        public Long getEndTime() {
            return endTime;
        }

        public void setEndTime(Long endTime) {
            this.endTime = endTime;
        }
    }
}
