/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.core.reflect.ReflectionUtils;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 增加支付即会员规则接口响应参数
 *
 * @author mzlion on 2017/1/6.
 */
public class CardPayGiftCardAddResponse extends WxResponse {

    /**
     * 本次设置的规则id，供后续修改删除使用
     */
    @ApiField("rule_id")
    private String ruleId;

    /**
     * 设置成功的mchid列表
     */
    @ApiField("succ_mchid_list")
    private List<String> successMchIdList;

    /**
     * 设置失败的mchid列表
     */
    @ApiField("fail_mchid_list")
    private List<FailMerchant> failMchIdList;

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public List<String> getSuccessMchIdList() {
        return successMchIdList;
    }

    public void setSuccessMchIdList(List<String> successMchIdList) {
        this.successMchIdList = successMchIdList;
    }

    public List<FailMerchant> getFailMchIdList() {
        return failMchIdList;
    }

    public void setFailMchIdList(List<FailMerchant> failMchIdList) {
        this.failMchIdList = failMchIdList;
    }

    /**
     * 设置支付即会员规则失败信息
     *
     * @author mzlion
     */
    public static class FailMerchant implements Serializable {

        /**
         * 支付的商户号
         */
        @ApiField("mchid")
        private String mchId;

        /**
         * 该mchid当前被占用的appid，商户须使用该appid解除绑定后重新设置
         */
        @ApiField("occupy_appid")
        private String occupyAppId;

        /**
         * 该mchid当前被占用的rule_id，商户须使用修改或删除该rule_id重新设置
         */
        @ApiField("occupy_rule_id")
        private String occupyRuleId;

        @ApiField("errcode")
        private String errorCode;

        @ApiField("errmsg")
        private String errorMsg;

        public String getMchId() {
            return mchId;
        }

        public void setMchId(String mchId) {
            this.mchId = mchId;
        }

        public String getOccupyAppId() {
            return occupyAppId;
        }

        public void setOccupyAppId(String occupyAppId) {
            this.occupyAppId = occupyAppId;
        }

        public String getOccupyRuleId() {
            return occupyRuleId;
        }

        public void setOccupyRuleId(String occupyRuleId) {
            this.occupyRuleId = occupyRuleId;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }
    }

    @Override
    public String toString() {
        return ReflectionUtils.toString(this);
    }
}
