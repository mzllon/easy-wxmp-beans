/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 根据activate_ticket获取用户信息
 * <p>支持开发者根据activate_ticket获取到用户填写的信息。</p>
 *
 * @author kudo on 2017/6/8.
 */
public class CardMemberCardActivateTempInfoGetResponse extends WxResponse {

    @ApiField("info")
    private TempInfo tempInfo;

    public CardMemberCardActivateTempInfoGetResponse() {
    }

    public TempInfo getTempInfo() {
        return tempInfo;
    }

    public void setTempInfo(TempInfo tempInfo) {
        this.tempInfo = tempInfo;
    }

    public static class TempInfo implements Serializable {

        /**
         * 开发者设置的会员卡会员信息类目，如等级。
         */
        @ApiField("common_field_list")
        private List<CardMemberCardUserInfoGetResponse.CommonField> commonFieldList;

        /**
         * 开发者设置的会员卡会员信息类目，如等级。
         */
        @ApiField("custom_field_list")
        private List<CardMemberCardUserInfoGetResponse.CustomField> customFieldList;

        public TempInfo() {
        }

        public List<CardMemberCardUserInfoGetResponse.CommonField> getCommonFieldList() {
            return commonFieldList;
        }

        public void setCommonFieldList(List<CardMemberCardUserInfoGetResponse.CommonField> commonFieldList) {
            this.commonFieldList = commonFieldList;
        }

        public List<CardMemberCardUserInfoGetResponse.CustomField> getCustomFieldList() {
            return customFieldList;
        }

        public void setCustomFieldList(List<CardMemberCardUserInfoGetResponse.CustomField> customFieldList) {
            this.customFieldList = customFieldList;
        }
    }
}
