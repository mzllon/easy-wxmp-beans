/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.scan;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 获取商品二维码
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductGetQrCodeResponse extends WxResponse {

    private static final long serialVersionUID = -6840648703687996862L;

    /**
     * 商品二维码的图片链接，可直接下载到本地
     */
    @ApiField("pic_url")
    String picUrl;

    /**
     * 商品二维码的内容，以http://p.url.cn/为前缀，加上pid和extinfo三部分组成
     */
    @ApiField("qrcode_url")
    String qrCodeUrl;

    public ScanProductGetQrCodeResponse() {
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }
}
