/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.scan;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 批量查询商品信息
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductGetListResponse extends WxResponse {

    private static final long serialVersionUID = 5270219034751561374L;

    /**
     * 命中筛选条件的商品总数
     */
    private int total;

    /**
     * 商品信息列表
     */
    @ApiField("key_list")
    private List<BrandKey> brandKeyList;

    public ScanProductGetListResponse() {
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<BrandKey> getBrandKeyList() {
        return brandKeyList;
    }

    public void setBrandKeyList(List<BrandKey> brandKeyList) {
        this.brandKeyList = brandKeyList;
    }

    /**
     * 商品关键信息
     */
    public static class BrandKey implements Serializable {

        private static final long serialVersionUID = -6396994159166299437L;

        /**
         * 商品编码标准，支持ean13、ean8和qrcode标准
         */
        @ApiField("keystandard")
        String keyStandard;

        /**
         * 商品编码内容。直接填写商品条码，
         * 标准是ean13，则直接填写商品条码，如“6901939621608”。
         * 标准是qrcode，二维码的内容可由商户自定义，建议使用商品条码，≤20个字符，由大小字母、数字、下划线和连字符组成。
         * 注意：编码标准是ean13时，编码内容必须在商户的号段之下，否则会报错
         */
        @ApiField("keystr")
        String keyStr;

        /**
         * 商品类目ID
         */
        @ApiField("category_id")
        private String categoryId;

        /**
         * 商品类目名称
         */
        @ApiField("category_name")
        private String categoryName;

        /**
         * 商品信息的最后更新时间（整型）
         */
        @ApiField("update_time")
        private long update_time;

        /**
         * 商品主页的状态，
         * on为发布状态，
         * off为未发布状态，
         * check为审核中状态，
         * reject为审核未通过状态。
         */
        private String status;

        public BrandKey() {
        }

        public String getKeyStandard() {
            return keyStandard;
        }

        public void setKeyStandard(String keyStandard) {
            this.keyStandard = keyStandard;
        }

        public String getKeyStr() {
            return keyStr;
        }

        public void setKeyStr(String keyStr) {
            this.keyStr = keyStr;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public long getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(long update_time) {
            this.update_time = update_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
