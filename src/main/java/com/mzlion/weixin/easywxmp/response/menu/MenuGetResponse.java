/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.menu;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.MenuButtonModel;
import com.mzlion.weixin.easywxmp.model.MenuMatchRuleModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义菜单查询接口响应参数
 * <p>
 * 使用接口创建自定义菜单后，开发者还可使用接口查询自定义菜单的结构。
 * 另外请注意，在设置了个性化菜单后，使用本自定义菜单查询接口可以获取默认菜单和全部个性化菜单信息。
 * </p>
 *
 * @author mzlion on 2017/1/4.
 */
public class MenuGetResponse extends WxResponse {

    private static final long serialVersionUID = -261563091382960933L;

    /**
     * 系统默认菜单包装类
     */
    @ApiField("menu")
    private DefaultMenuWrapper defaultMenuWrapper;

    /**
     * 个性菜单包装类
     */
    @ApiField("conditionalmenu")
    private List<ConditionalMenuWrapper> conditionalMenuWrapperList;

    public DefaultMenuWrapper getDefaultMenuWrapper() {
        return defaultMenuWrapper;
    }

    public void setDefaultMenuWrapper(DefaultMenuWrapper defaultMenuWrapper) {
        this.defaultMenuWrapper = defaultMenuWrapper;
    }

    public List<ConditionalMenuWrapper> getConditionalMenuWrapperList() {
        return conditionalMenuWrapperList;
    }

    public void setConditionalMenuWrapperList(List<ConditionalMenuWrapper> conditionalMenuWrapperList) {
        this.conditionalMenuWrapperList = conditionalMenuWrapperList;
    }

    /**
     * 系统默认菜单包装类
     *
     * @author mzlion
     */
    public static class DefaultMenuWrapper implements Serializable {

        /**
         * 默认菜单列表
         */
        @ApiField("button")
        private List<MenuButtonModel> menuButtonList;

        /**
         * 默认菜单编号
         */
        @ApiField("menuid")
        private String menuId;

        public List<MenuButtonModel> getMenuButtonList() {
            return menuButtonList;
        }

        public void setMenuButtonList(List<MenuButtonModel> menuButtonList) {
            this.menuButtonList = menuButtonList;
        }

        public String getMenuId() {
            return menuId;
        }

        public void setMenuId(String menuId) {
            this.menuId = menuId;
        }
    }

    /**
     * 个性菜单菜单包装类
     *
     * @author mzlion
     */
    public static class ConditionalMenuWrapper extends DefaultMenuWrapper {

        /**
         * 菜单匹配规则
         */
        private MenuMatchRuleModel menuMatchRule;

        public MenuMatchRuleModel getMenuMatchRule() {
            return menuMatchRule;
        }

        public void setMenuMatchRule(MenuMatchRuleModel menuMatchRule) {
            this.menuMatchRule = menuMatchRule;
        }
    }

}


