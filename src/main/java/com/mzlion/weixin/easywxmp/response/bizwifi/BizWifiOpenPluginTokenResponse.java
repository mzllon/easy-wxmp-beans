/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.bizwifi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;

/**
 * 第三方平台获取开插件wifi_token
 *
 * @author mzlion on 2017/4/18.
 */
public class BizWifiOpenPluginTokenResponse extends WxResponse {

    private static final long serialVersionUID = 570919024006056172L;

    /**
     * 插件wifi_token
     */
    @ApiField("data")
    private BizWifiOpenPluginToken bizWifiOpenPluginToken;

    public BizWifiOpenPluginTokenResponse() {
    }

    public BizWifiOpenPluginToken getBizWifiOpenPluginToken() {
        return bizWifiOpenPluginToken;
    }

    public void setBizWifiOpenPluginToken(BizWifiOpenPluginToken bizWifiOpenPluginToken) {
        this.bizWifiOpenPluginToken = bizWifiOpenPluginToken;
    }

    /**
     * 插件wifi_token
     */
    public static class BizWifiOpenPluginToken implements Serializable {

        private static final long serialVersionUID = 6311607384615156013L;

        /**
         * 该公众号是否已开通微信连Wi-Fi插件，true-已开通，false-未开通
         */
        @ApiField("is_open")
        private boolean isOpen;

        /**
         * 开通插件的凭证，当is_open为false时才返回值
         */
        @ApiField("wifi_token")
        private String wifiToken;

        public BizWifiOpenPluginToken() {
        }

        public boolean isOpen() {
            return isOpen;
        }

        public void setIsOpen(boolean isOpen) {
            this.isOpen = isOpen;
        }

        public String getWifiToken() {
            return wifiToken;
        }

        public void setWifiToken(String wifiToken) {
            this.wifiToken = wifiToken;
        }
    }
}
