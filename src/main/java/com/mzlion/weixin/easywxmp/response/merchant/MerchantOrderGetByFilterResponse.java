/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.merchant;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 根据订单状态/创建时间获取订单详情
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantOrderGetByFilterResponse extends WxResponse {

    /**
     * 商品ID
     */
    @ApiField("order_list")
    private List<OrderData> orderDataList;

    public MerchantOrderGetByFilterResponse() {
    }

    public List<OrderData> getOrderDataList() {
        return orderDataList;
    }

    public void setOrderDataList(List<OrderData> orderDataList) {
        this.orderDataList = orderDataList;
    }

    public static class OrderData implements Serializable {

        /**
         * 订单ID
         */
        @ApiField("order_id")
        private String orderId;

        /**
         * 订单状态
         */
        @ApiField("order_status")
        private Integer orderStatus;

        /**
         * 买家微信OPENID
         */
        @ApiField("buyer_openid")
        private String buyerOpenid;

        /**
         * 买家微信昵称
         */
        @ApiField("buyer_nick")
        private String buyerNick;

        /**
         * 收货人姓名
         */
        @ApiField("receiver_name")
        private String receiverName;

        /**
         * 收货地址省份
         */
        @ApiField("receiver_province")
        private String receiverProvince;

        /**
         * 收货地址城市
         */
        @ApiField("receiver_city")
        private String receiverCity;

        /**
         * 收货地址区/县
         */
        @ApiField("receiver_zone")
        private String receiverZone;

        /**
         * 收货详细地址
         */
        @ApiField("receiver_address")
        private String receiverAddress;

        /**
         * 收货人移动电话
         */
        @ApiField("receiver_mobile")
        private String receiverMobile;

        /**
         * 收货人固定电话
         */
        @ApiField("receiver_phone")
        private String receiverPhone;

        /**
         * 商品ID
         */
        @ApiField("product_id")
        private String productId;

        /**
         * 商品名称
         */
        @ApiField("product_name")
        private String productName;

        /**
         * 商品价格(单位 : 分)
         */
        @ApiField("product_price")
        private int productPrice;

        /**
         * 商品SKU
         */
        @ApiField("product_sku")
        private String productSku;

        /**
         * 商品个数
         */
        @ApiField("product_count")
        private int productCount;

        /**
         * 商品图片
         */
        @ApiField("product_img")
        private String productImg;

        /**
         * 运单ID
         */
        @ApiField("delivery_id")
        private String deliveryId;

        /**
         * 物流公司编码
         */
        @ApiField("delivery_company")
        private String deliveryCompany;

        /**
         * 交易ID
         */
        @ApiField("trans_id")
        private String transId;


        /**
         * 订单总价格(单位:分)
         */
        @ApiField("order_total_price")
        private long orderTotalPrice;

        /**
         * 订单创建时间
         */
        @ApiField("order_create_time")
        private long orderCreateTime;

        /**
         * 订单运费价格(单位:分)
         */
        @ApiField("order_express_price")
        private long orderExpressPrice;

        public OrderData() {
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getBuyerOpenid() {
            return buyerOpenid;
        }

        public void setBuyerOpenid(String buyerOpenid) {
            this.buyerOpenid = buyerOpenid;
        }

        public String getBuyerNick() {
            return buyerNick;
        }

        public void setBuyerNick(String buyerNick) {
            this.buyerNick = buyerNick;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getReceiverProvince() {
            return receiverProvince;
        }

        public void setReceiverProvince(String receiverProvince) {
            this.receiverProvince = receiverProvince;
        }

        public String getReceiverCity() {
            return receiverCity;
        }

        public void setReceiverCity(String receiverCity) {
            this.receiverCity = receiverCity;
        }

        public String getReceiverZone() {
            return receiverZone;
        }

        public void setReceiverZone(String receiverZone) {
            this.receiverZone = receiverZone;
        }

        public String getReceiverAddress() {
            return receiverAddress;
        }

        public void setReceiverAddress(String receiverAddress) {
            this.receiverAddress = receiverAddress;
        }

        public String getReceiverMobile() {
            return receiverMobile;
        }

        public void setReceiverMobile(String receiverMobile) {
            this.receiverMobile = receiverMobile;
        }

        public String getReceiverPhone() {
            return receiverPhone;
        }

        public void setReceiverPhone(String receiverPhone) {
            this.receiverPhone = receiverPhone;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public int getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(int productPrice) {
            this.productPrice = productPrice;
        }

        public String getProductSku() {
            return productSku;
        }

        public void setProductSku(String productSku) {
            this.productSku = productSku;
        }

        public int getProductCount() {
            return productCount;
        }

        public void setProductCount(int productCount) {
            this.productCount = productCount;
        }

        public String getProductImg() {
            return productImg;
        }

        public void setProductImg(String productImg) {
            this.productImg = productImg;
        }

        public String getDeliveryId() {
            return deliveryId;
        }

        public void setDeliveryId(String deliveryId) {
            this.deliveryId = deliveryId;
        }

        public String getDeliveryCompany() {
            return deliveryCompany;
        }

        public void setDeliveryCompany(String deliveryCompany) {
            this.deliveryCompany = deliveryCompany;
        }

        public String getTransId() {
            return transId;
        }

        public void setTransId(String transId) {
            this.transId = transId;
        }

        public long getOrderTotalPrice() {
            return orderTotalPrice;
        }

        public void setOrderTotalPrice(long orderTotalPrice) {
            this.orderTotalPrice = orderTotalPrice;
        }

        public long getOrderCreateTime() {
            return orderCreateTime;
        }

        public void setOrderCreateTime(long orderCreateTime) {
            this.orderCreateTime = orderCreateTime;
        }

        public long getOrderExpressPrice() {
            return orderExpressPrice;
        }

        public void setOrderExpressPrice(long orderExpressPrice) {
            this.orderExpressPrice = orderExpressPrice;
        }
    }
}
