/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.shakearound;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 批量查询设备统计数据接口
 *
 * @author mzlion on 2017/4/19.
 */
public class ShakeAroundStatisticsDeviceListResponse extends WxResponse {

    private static final long serialVersionUID = 4545011220228642021L;

    /**
     * 统计查询结果
     */
    @ApiField("data")
    private StatisticsDeviceListDataWrapper statisticsDeviceListData;

    public ShakeAroundStatisticsDeviceListResponse() {
    }

    public StatisticsDeviceListDataWrapper getStatisticsDeviceListData() {
        return statisticsDeviceListData;
    }

    public void setStatisticsDeviceListData(StatisticsDeviceListDataWrapper statisticsDeviceListData) {
        this.statisticsDeviceListData = statisticsDeviceListData;
    }

    /**
     * 统计查询结果
     */
    public static class StatisticsDeviceListDataWrapper implements Serializable {

        private static final long serialVersionUID = 1948154292265847877L;

        /**
         * 统计数据
         */
        @ApiField("devices")
        private List<StatisticsData> statisticsDataList;

        /**
         * 所查询的日期时间戳
         */
        private Long date;

        /**
         * 设备总数
         */
        @ApiField("total_count")
        private int totalCount;

        /**
         * 指定查询的结果页序号；返回结果按摇周边人数降序排序，每50条记录为一页
         */
        @ApiField("page_index")
        private Integer pageIndex;

        public StatisticsDeviceListDataWrapper() {
        }

        public List<StatisticsData> getStatisticsDataList() {
            return statisticsDataList;
        }

        public void setStatisticsDataList(List<StatisticsData> statisticsDataList) {
            this.statisticsDataList = statisticsDataList;
        }

        public Long getDate() {
            return date;
        }

        public void setDate(Long date) {
            this.date = date;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public Integer getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(Integer pageIndex) {
            this.pageIndex = pageIndex;
        }
    }

    public static class StatisticsData implements Serializable {

        private static final long serialVersionUID = 9113246167668031020L;

        /**
         * 设备编号
         */
        @ApiField("device_id")
        private String deviceId;

        /**
         * uuid
         */
        private String uuid;

        /**
         * major
         */
        private String major;

        /**
         * minor
         */
        private String minor;

        /**
         * 点击摇周边消息的次数
         */
        @ApiField("click_pv")
        private int clickPv;

        /**
         * 点击摇周边消息的人数
         */
        @ApiField("click_uv")
        private int clickUv;

        /**
         * 摇周边的次数
         */
        @ApiField("shake_pv")
        private int shakePv;

        /**
         * 摇周边的人数
         */
        @ApiField("shake_uv")
        private int shakeUv;

        public StatisticsData() {
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getMajor() {
            return major;
        }

        public void setMajor(String major) {
            this.major = major;
        }

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }

        public int getClickPv() {
            return clickPv;
        }

        public void setClickPv(int clickPv) {
            this.clickPv = clickPv;
        }

        public int getClickUv() {
            return clickUv;
        }

        public void setClickUv(int clickUv) {
            this.clickUv = clickUv;
        }

        public int getShakePv() {
            return shakePv;
        }

        public void setShakePv(int shakePv) {
            this.shakePv = shakePv;
        }

        public int getShakeUv() {
            return shakeUv;
        }

        public void setShakeUv(int shakeUv) {
            this.shakeUv = shakeUv;
        }
    }
}
