/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.poi;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.model.ExtendWxPoiModel;
import com.mzlion.weixin.easywxmp.model.WxPoiWrapperModel;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.util.List;

/**
 * 查询门店列表响应参数
 *
 * @author mzlion on 2017/1/5.
 */
public class PoiGetPoiListResponse extends WxResponse {

    /**
     * 门店列表
     */
    @ApiField("business_list")
    private List<WxPoiWrapperModel<ExtendWxPoiModel>> wxPoiWrapperList;

    /**
     * 门店总数量
     */
    @ApiField(value = "total_count")
    private int totalCount;

    public PoiGetPoiListResponse() {
    }

    public List<WxPoiWrapperModel<ExtendWxPoiModel>> getWxPoiWrapperList() {
        return wxPoiWrapperList;
    }

    public void setWxPoiWrapperList(List<WxPoiWrapperModel<ExtendWxPoiModel>> wxPoiWrapperList) {
        this.wxPoiWrapperList = wxPoiWrapperList;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
