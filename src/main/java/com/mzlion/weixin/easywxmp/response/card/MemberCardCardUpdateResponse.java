/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.card;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 更改会员卡信息接口
 *
 * @author mzlion on 2017/4/17.
 */
public class MemberCardCardUpdateResponse extends WxResponse {

    /**
     * 此次更新是否需要提审，true为需要，false为不需要
     */
    @ApiField("send_check")
    private Boolean sendCheck;

    public MemberCardCardUpdateResponse() {
    }

    public Boolean getSendCheck() {
        return sendCheck;
    }

    public void setSendCheck(Boolean sendCheck) {
        this.sendCheck = sendCheck;
    }
}
