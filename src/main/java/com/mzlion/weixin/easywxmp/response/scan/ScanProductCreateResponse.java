/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.scan;

import com.mzlion.weixin.easywxmp.WxResponse;

/**
 * 创建商品
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanProductCreateResponse extends WxResponse {

    private static final long serialVersionUID = -3389779720903236924L;

    /**
     * 转译后的商品id，将直接编入“获取商品二维码接口”返回的二维码内容
     */
    String pid;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
