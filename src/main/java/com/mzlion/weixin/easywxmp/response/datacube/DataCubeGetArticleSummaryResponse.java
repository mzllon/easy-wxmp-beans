/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.datacube;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取图文群发每日数据
 *
 * @author mzlion on 2017/4/17.
 */
public class DataCubeGetArticleSummaryResponse extends WxResponse {

    /**
     * 统计列表
     */
    @ApiField("list")
    private List<ArticleSummaryWrapper> userSummaryList;

    public List<ArticleSummaryWrapper> getUserSummaryList() {
        return userSummaryList;
    }

    public void setUserSummaryList(List<ArticleSummaryWrapper> userSummaryList) {
        this.userSummaryList = userSummaryList;
    }

    /**
     * 累计用户数据
     */
    public static class ArticleSummaryWrapper implements Serializable {

        /**
         * 数据的日期，需在begin_date和end_date之间
         */
        @ApiField("ref_date")
        private String refDate;

        /**
         * 请注意：这里的msgid实际上是由msgid（图文消息id，这也就是群发接口调用后返回的msg_data_id）和index（消息次序索引）组成，
         * 例如12003_3， 其中12003是msgid，即一次群发的消息的id； 3为index，假设该次群发的图文消息共5个文章（因为可能为多图文），3表示5个中的第3个
         */
        @ApiField("msgid")
        private String msgId;

        /**
         * 图文消息的标题
         */
        private String title;

        /**
         * 图文页（点击群发图文卡片进入的页面）的阅读人数
         */
        @ApiField("int_page_read_user")
        private int intPageReadUser;

        /**
         * 图文页的阅读次数
         */
        @ApiField("int_page_read_count")
        private int intPageReadCount;

        /**
         * 原文页（点击图文页“阅读原文”进入的页面）的阅读人数，无原文页时此处数据为0
         */
        @ApiField("ori_page_read_user")
        private int oriPageReadUser;

        /**
         * 原文页的阅读次数
         */
        @ApiField("ori_page_read_count")
        private int oriPageReadCount;

        /**
         * 分享的人数
         */
        @ApiField("share_user")
        private int shareUser;

        /**
         * 分享的次数
         */
        @ApiField("share_count")
        private int shareCount;

        /**
         * 收藏的人数
         */
        @ApiField("add_to_fav_user")
        private int addToFavUser;

        /**
         * 收藏的次数
         */
        @ApiField("add_to_fav_count")
        private int addToFavCount;

        public String getRefDate() {
            return refDate;
        }

        public void setRefDate(String refDate) {
            this.refDate = refDate;
        }

        public String getMsgId() {
            return msgId;
        }

        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getIntPageReadUser() {
            return intPageReadUser;
        }

        public void setIntPageReadUser(int intPageReadUser) {
            this.intPageReadUser = intPageReadUser;
        }

        public int getIntPageReadCount() {
            return intPageReadCount;
        }

        public void setIntPageReadCount(int intPageReadCount) {
            this.intPageReadCount = intPageReadCount;
        }

        public int getOriPageReadUser() {
            return oriPageReadUser;
        }

        public void setOriPageReadUser(int oriPageReadUser) {
            this.oriPageReadUser = oriPageReadUser;
        }

        public int getOriPageReadCount() {
            return oriPageReadCount;
        }

        public void setOriPageReadCount(int oriPageReadCount) {
            this.oriPageReadCount = oriPageReadCount;
        }

        public int getShareUser() {
            return shareUser;
        }

        public void setShareUser(int shareUser) {
            this.shareUser = shareUser;
        }

        public int getShareCount() {
            return shareCount;
        }

        public void setShareCount(int shareCount) {
            this.shareCount = shareCount;
        }

        public int getAddToFavUser() {
            return addToFavUser;
        }

        public void setAddToFavUser(int addToFavUser) {
            this.addToFavUser = addToFavUser;
        }

        public int getAddToFavCount() {
            return addToFavCount;
        }

        public void setAddToFavCount(int addToFavCount) {
            this.addToFavCount = addToFavCount;
        }
    }
}
