/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.merchant;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取指定分类的所有属性
 *
 * @author mzlion on 2017/4/24.
 */
public class MerchantCategoryGetPropertyResponse extends WxResponse {

    /**
     * SKU属性表
     */
    @ApiField("properties")
    private List<PropertyTable> propertyTableList;

    public MerchantCategoryGetPropertyResponse() {
    }

    public List<PropertyTable> getPropertyTableList() {
        return propertyTableList;
    }

    public void setPropertyTableList(List<PropertyTable> propertyTableList) {
        this.propertyTableList = propertyTableList;
    }

    /**
     * SKU属性
     */
    public static class Property implements Serializable {

        /**
         * 属性值id
         */
        private String id;

        /**
         * 属性值id
         */
        private String name;

        public Property() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * SKU属性表
     */
    public static class PropertyTable extends Property {

        /**
         * SKU属性列表
         */
        @ApiField("property_value")
        private List<Property> valueList;


        public PropertyTable() {
        }

        public List<Property> getValueList() {
            return valueList;
        }

        public void setValueList(List<Property> valueList) {
            this.valueList = valueList;
        }
    }
}
