/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.datacube;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 获取用户增减数据
 *
 * @author mzlion on 2017/4/17.
 */
public class DataCubeGetUserSummaryResponse extends WxResponse {

    /**
     * 统计列表
     */
    @ApiField("list")
    private List<UserSummaryWrapper> userSummaryList;

    public List<UserSummaryWrapper> getUserSummaryList() {
        return userSummaryList;
    }

    public void setUserSummaryList(List<UserSummaryWrapper> userSummaryList) {
        this.userSummaryList = userSummaryList;
    }

    /**
     * 按日的用户汇总量
     */
    public static class UserSummaryWrapper implements Serializable {

        /**
         * 数据的日期
         */
        @ApiField("ref_date")
        private String refDate;

        /**
         * 用户的渠道，数值代表的含义如下：
         * 0代表其他合计
         * 1代表公众号搜索
         * 17代表名片分享
         * 30代表扫描二维码
         * 43代表图文页右上角菜单
         * 51代表支付后关注（在支付完成页）
         * 57代表图文页内公众号名称
         * 75代表公众号文章广告
         * 78代表朋友圈广告
         */
        @ApiField("user_source")
        private int userSource;

        /**
         * 新增的用户数量
         */
        @ApiField("new_user")
        private int newUser;

        /**
         * 取消关注的用户数量，new_user减去cancel_user即为净增用户数量
         */
        @ApiField("cancel_user")
        private int cancelUser;

        public String getRefDate() {
            return refDate;
        }

        public void setRefDate(String refDate) {
            this.refDate = refDate;
        }

        public int getUserSource() {
            return userSource;
        }

        public void setUserSource(int userSource) {
            this.userSource = userSource;
        }

        public int getNewUser() {
            return newUser;
        }

        public void setNewUser(int newUser) {
            this.newUser = newUser;
        }

        public int getCancelUser() {
            return cancelUser;
        }

        public void setCancelUser(int cancelUser) {
            this.cancelUser = cancelUser;
        }

    }
}
