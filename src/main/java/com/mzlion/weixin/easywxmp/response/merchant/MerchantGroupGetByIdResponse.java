/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.response.merchant;

import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

import java.io.Serializable;
import java.util.List;

/**
 * 根据分组ID获取分组信息
 *
 * @author mzlion on 2017/4/25.
 */
public class MerchantGroupGetByIdResponse extends WxResponse {

    /**
     * 分组详细信息
     */
    @ApiField("group_detail")
    private GroupDetail groupDetail;

    public MerchantGroupGetByIdResponse() {
    }

    public GroupDetail getGroupDetail() {
        return groupDetail;
    }

    public void setGroupDetail(GroupDetail groupDetail) {
        this.groupDetail = groupDetail;
    }

    public static class GroupDetail implements Serializable {

        /**
         * 分组ID
         */
        @ApiField("group_id")
        private String groupId;

        /**
         * 分组名称
         */
        @ApiField("group_name")
        private String groupName;

        /**
         * 商品ID集合
         */
        @ApiField("product_list")
        private List<String> productIdList;

        public GroupDetail() {
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public List<String> getProductIdList() {
            return productIdList;
        }

        public void setProductIdList(List<String> productIdList) {
            this.productIdList = productIdList;
        }
    }
}
