/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages;

/**
 * 回复消息的基础类
 *
 * @author mzlion on 2017/4/16.
 */
public abstract class OutMsg extends AbsWxMsg implements OutMsgXmlBuilder {

    /**
     * 返回具体当前消息体的存放的XML
     *
     * @return XML消息片段
     */
    public abstract String getCurrentMsgSnippetXml();


    /**
     * 将消息实体转为XML字符串
     *
     * @return XML字符串
     */
    @Override
    public String build() {
        final StringBuilder builder = new StringBuilder(200);
        builder.append("<xml>\n");

        builder.append("<ToUserName>");
        builder.append("<![CDATA[").append(super.toUserName).append("]]>");
        builder.append("</ToUserName>\n");

        builder.append("<FromUserName>");
        builder.append("<![CDATA[").append(super.fromUserName).append("]]>");
        builder.append("</FromUserName>\n");

        builder.append("<CreateTime>");
        builder.append(super.createTime);
        builder.append("</CreateTime>\n");

        builder.append("<MsgType>");
        builder.append("<![CDATA[").append(this.msgType().getMsgType()).append("]]>");
        builder.append("</MsgType>\n");

        builder.append(getCurrentMsgSnippetXml()).append("\n");

        builder.append("</xml>");
        return builder.toString();
    }

}
