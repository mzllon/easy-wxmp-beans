/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;

import java.io.Serializable;

/**
 * 定义了微信消息基础接口
 *
 * @author mzlion on 2017/04/14.
 */
public interface WxMsg extends Serializable {

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    MsgTypeEnum msgType();

}
