/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.out;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.OutMsg;
import com.mzlion.weixin.easywxmp.model.ArticleItemModel;

import java.util.List;

/**
 * 回复图文消息
 *
 * @author mzlion on 2017/4/16.
 */
public class OutNewsMsg extends OutMsg {

    /**
     * 图文消息个数，限制为8条以内
     */
    private int articleCount;

    /**
     * 多条图文消息信息，默认第一个item为大图,注意，如果图文数超过8，则将会无响应
     */
    private List<ArticleItemModel> articleItemList;

    public OutNewsMsg() {
    }

    public int getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(int articleCount) {
        this.articleCount = articleCount;
    }

    public List<ArticleItemModel> getArticleItemList() {
        return articleItemList;
    }

    public void setArticleItemList(List<ArticleItemModel> articleItemList) {
        this.articleItemList = articleItemList;
    }

    /**
     * 返回具体当前消息体的存放的XML
     *
     * @return XML消息片段
     */
    @Override
    public String getCurrentMsgSnippetXml() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("<ArticleCount>").append(this.articleCount).append("</ArticleCount>\n");
        builder.append("<Articles>");
        for (ArticleItemModel articleItem : articleItemList) {
            builder.append("<item>\n");
            builder.append("<Title><![CDATA[").append(articleItem.getTitle()).append("]]></Title>\n");
            builder.append("<Description><![CDATA[").append(articleItem.getDescription()).append("]]></Description>\n");
            builder.append("<PicUrl><![CDATA[").append(articleItem.getPicUrl()).append("]]></PicUrl>\n");
            builder.append("<Url><![CDATA[").append(articleItem.getUrl()).append("]]></Url>\n");
            builder.append("/item>\n");
        }
        builder.append("</Articles>");
        return builder.toString();
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.NEWS;
    }

}
