/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages;

import java.io.Serializable;

/**
 * 回复消息中的图片包装类
 *
 * @author mzlion on 2017/4/16.
 */
public class OutMediaImage implements Serializable {

    /**
     * 通过素材管理中的接口上传多媒体文件，得到的id。
     */
    private String mediaId;

    public OutMediaImage(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }
}
