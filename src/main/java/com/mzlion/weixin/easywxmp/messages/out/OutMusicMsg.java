/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.out;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.OutMsg;

import java.io.Serializable;

/**
 * 回复音乐消息
 *
 * @author mzlion on 2017/4/16.
 */
public class OutMusicMsg extends OutMsg {

    /**
     * 音乐
     */
    private Music music;

    public OutMusicMsg() {
    }

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }

    /**
     * 返回具体当前消息体的存放的XML
     *
     * @return XML消息片段
     */
    @Override
    public String getCurrentMsgSnippetXml() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("<Music>\n");
        builder.append("<Title><![CDATA[").append(this.music.getTitle()).append("]]></Title>\n");
        builder.append("<Description><![CDATA[").append(this.music.getDescription()).append("]]></Description>\n");
        builder.append("<MusicUrl><![CDATA[").append(this.music.getMusicURL()).append("]]></MusicUrl>\n");
        builder.append("<HQMusicUrl><![CDATA[").append(this.music.getHqMusicUrl()).append("]]></HQMusicUrl>\n");
        builder.append("<ThumbMediaId><![CDATA[").append(this.music.getThumbMediaId()).append("]]></ThumbMediaId>\n");
        builder.append("</Music>\n");
        return builder.toString();
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.MUSIC;
    }

    /**
     * 音乐包装类
     *
     * @author mzlion
     */
    public static class Music implements Serializable {

        /**
         * 音乐标题
         */
        private String title;

        /**
         * 音乐描述
         */
        private String description;

        /**
         * 音乐链接
         */
        private String musicURL;

        /**
         * 高质量音乐链接，WIFI环境优先使用该链接播放音乐
         */
        private String hqMusicUrl;

        /**
         * 缩略图的媒体id，通过素材管理中的接口上传多媒体文件，得到的id
         */
        private String thumbMediaId;

        public Music() {
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMusicURL() {
            return musicURL;
        }

        public void setMusicURL(String musicURL) {
            this.musicURL = musicURL;
        }

        public String getHqMusicUrl() {
            return hqMusicUrl;
        }

        public void setHqMusicUrl(String hqMusicUrl) {
            this.hqMusicUrl = hqMusicUrl;
        }

        public String getThumbMediaId() {
            return thumbMediaId;
        }

        public void setThumbMediaId(String thumbMediaId) {
            this.thumbMediaId = thumbMediaId;
        }
    }
}
