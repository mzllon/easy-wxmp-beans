package com.mzlion.weixin.easywxmp.messages;

/**
 * Java实体和微信XML消息转换接口
 *
 * @author mzlion on 2017/4/16.
 */
public interface OutMsgXmlBuilder {

    /**
     * 将消息实体转为XML字符串。如果返回{@code null}则表示不回复消息
     *
     * @return XML字符串
     */
    String build();
}
