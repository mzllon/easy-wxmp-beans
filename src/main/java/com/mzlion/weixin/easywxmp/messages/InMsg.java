/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages;

/**
 * 接收消息的基础类
 * <p>
 * <b>请注意：</b>
 * <ol>
 * <li>关于重试的消息排重，推荐使用msgid排重。</li>
 * <li>微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次。假如服务器无法保证在五秒内处理并回复，
 * 可以直接回复空串，微信服务器不会对此作任何处理，并且不会发起重试。详情请见“发送消息-被动回复消息”。</li>
 * <li>如果开发者需要对用户消息在5秒内立即做出回应，即使用“发送消息-被动回复消息”接口向用户被动回复消息时，可以在
 * 公众平台官网的开发者中心处设置消息加密。开启加密后，用户发来的消息和开发者回复的消息都会被加密（但开发者通过客服
 * 接口等API调用形式向用户发送消息，则不受影响）。关于消息加解密的详细说明，请见“发送消息-被动回复消息加解密说明”。</li>
 * </ol>
 *
 * @author mzlion  on 2016/12/26.
 */
public abstract class InMsg extends AbsWxMsg {

    private static final long serialVersionUID = 2782383555034602015L;

    /**
     * 消息ID，64位整型
     */
    protected Long msgId;

    /**
     * 保留默认构造函数
     */
    public InMsg() {
    }

    /**
     * 便捷的构造函数
     *
     * @param fromUserName 开发者微信号
     * @param toUserName   发送方的openId
     * @param createTime   消息创建时间
     * @param msgId        消息ID
     */
    public InMsg(String fromUserName, String toUserName, Long createTime, Long msgId) {
        this.toUserName = toUserName;
        this.fromUserName = fromUserName;
        this.createTime = createTime;
        this.msgId = msgId;
    }

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

}
