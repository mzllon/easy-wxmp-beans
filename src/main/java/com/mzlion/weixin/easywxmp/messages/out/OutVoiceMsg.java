/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.out;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.OutMediaImage;
import com.mzlion.weixin.easywxmp.messages.OutMsg;

/**
 * 回复语音消息
 *
 * @author mzlion on 2016/12/26.
 */
public class OutVoiceMsg extends OutMsg {

    /**
     * 语音
     */
    private OutMediaImage voice;

    /**
     * 保留默认构造函数
     */
    public OutVoiceMsg() {
    }

    public OutMediaImage getVoice() {
        return voice;
    }

    public void setVoice(OutMediaImage voice) {
        this.voice = voice;
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.VOICE;
    }

    /**
     * 返回具体当前消息体的存放的XML
     *
     * @return XML消息片段
     */
    @Override
    public String getCurrentMsgSnippetXml() {
        return String.format("<Voice>\n<MediaId><![CDATA[%s]]></MediaId>\n</Voice>", this.voice.getMediaId());
    }


}
