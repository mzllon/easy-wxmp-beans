/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.out;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.OutMsg;

/**
 * 商品扫码结果消息
 *
 * @author mzlion on 2017/4/18.
 */
public class OutScanProductMsg extends OutMsg {

    private static final long serialVersionUID = -5792333320243248431L;

    /**
     * 商品编码标准
     */
    private String keyStandard;

    /**
     * 商品编码内容
     */
    private String keyStr;

    /**
     * 调用“获取商品二维码接口”时传入的extinfo，为标识参数
     */
    private String extInfo;

    /**
     * 防伪信息的内容
     */
    private String antiFake;

    /**
     * 商品防伪查询的结果，real表示码为真，fake表示码为假，not_active表示该防伪码未激活。
     */
    private String codeResult;

    public OutScanProductMsg() {
    }


    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public String getAntiFake() {
        return antiFake;
    }

    public void setAntiFake(String antiFake) {
        this.antiFake = antiFake;
    }

    public String getCodeResult() {
        return codeResult;
    }

    public void setCodeResult(String codeResult) {
        this.codeResult = codeResult;
    }

    /**
     * 返回具体当前消息体的存放的XML
     *
     * @return XML消息片段
     */
    @Override
    public String getCurrentMsgSnippetXml() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("<ScanProduct>\n");
        builder.append("<KeyStandard><![CDATA[").append(this.keyStandard).append("]]></KeyStandard>\n");
        builder.append("<KeyStr><![CDATA[").append(this.keyStr).append("]]></KeyStr>\n");
        builder.append("<ExtInfo><![CDATA[").append(this.extInfo).append("]]></ExtInfo>\n");
        builder.append("<AntiFake>\n");
        builder.append("<CodeResult><![CDATA[").append(this.codeResult).append("]]></CodeResult>\n");
        builder.append("</AntiFake>\n");
        builder.append("</ScanProduct>");
        return builder.toString();
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.SCAN_PRODUCT;
    }

}
