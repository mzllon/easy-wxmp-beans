/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.in;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.InMsg;

/**
 * 文本消息
 *
 * @author mzlion on 2016/12/26.
 */
public class InTextMsg extends InMsg {

    /**
     * 保留默认构造函数
     */
    public InTextMsg() {
    }

    /**
     * 文本消息内容
     */
    private String content;


    public InTextMsg(String fromUserName, String toUserName, Long createTime, Long msgId) {
        super(fromUserName, toUserName, createTime, msgId);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.TEXT;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InTextMsg{");
        sb.append("content='").append(content).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
