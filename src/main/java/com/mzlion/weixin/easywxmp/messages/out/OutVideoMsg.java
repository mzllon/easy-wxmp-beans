/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.out;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.OutMediaImage;
import com.mzlion.weixin.easywxmp.messages.OutMsg;

/**
 * 回复视频消息
 *
 * @author mzlion on 2017/4/16.
 */
public class OutVideoMsg extends OutMsg {

    private Video video;

    public OutVideoMsg() {
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    /**
     * 返回具体当前消息体的存放的XML
     *
     * @return XML消息片段
     */
    @Override
    public String getCurrentMsgSnippetXml() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("<Video>\n");
        builder.append("<MediaId><![CDATA[").append(this.video.getMediaId()).append("]]></MediaId>\n");
        builder.append("<Title><![CDATA[").append(this.video.getTitle()).append("]]></Title>\n");
        builder.append("<Description><![CDATA[").append(this.video.getDescription()).append("]]></Description>\n");
        builder.append("</Video>");
        return builder.toString();
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.VIDEO;
    }

    /**
     * 视频包装类
     */
    public static class Video extends OutMediaImage {

        /**
         * 视频消息的标题
         */
        private String title;

        /**
         * 视频消息的描述
         */
        private String description;

        public Video(String mediaId) {
            super(mediaId);
        }

        public Video(String mediaId, String title, String description) {
            super(mediaId);
            this.title = title;
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
