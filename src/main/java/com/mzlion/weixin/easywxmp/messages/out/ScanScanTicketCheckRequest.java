package com.mzlion.weixin.easywxmp.messages.out;

import com.mzlion.weixin.easywxmp.WxRequest;
import com.mzlion.weixin.easywxmp.WxResponse;
import com.mzlion.weixin.easywxmp.support.mapping.ApiField;

/**
 * 清除扫码记录
 * <p>
 * 当商品主页设置了“原生防伪组件”时（详情请查看【创建商品】），用户每一次扫码查看主页，均会被计数，进而展示在“防伪查询详情”中。
 * 如果商户希望某个码的扫码记录“归零”，可调用该接口，清除该码的被扫码记录。
 * </p>
 *
 * @author mzlion on 2017/4/18.
 */
public class ScanScanTicketCheckRequest extends WxRequest<WxResponse> {

    private static final long serialVersionUID = -8663289188362470010L;

    /**
     * 商品编码标准，支持ean13、ean8和qrcode标准
     */
    @ApiField("keystandard")
    String keyStandard;

    /**
     * 商品编码内容。直接填写商品条码，
     * 标准是ean13，则直接填写商品条码，如“6901939621608”。
     * 标准是qrcode，二维码的内容可由商户自定义，建议使用商品条码，≤20个字符，由大小字母、数字、下划线和连字符组成。
     * 注意：编码标准是ean13时，编码内容必须在商户的号段之下，否则会报错
     */
    @ApiField("keystr")
    String keyStr;

    /**
     * 调用“获取商品二维码接口”时传入的extinfo，为标识参数
     */
    @ApiField("extinfo")
    String extInfo;

    public ScanScanTicketCheckRequest() {
    }

    public String getKeyStandard() {
        return keyStandard;
    }

    public void setKeyStandard(String keyStandard) {
        this.keyStandard = keyStandard;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    /**
     * 获取请求地址
     *
     * @return 返回请求地址
     */
    @Override
    public String serviceUrl() {
        return "/scan/scanticket/check";
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxResponse> responseClass() {
        return WxResponse.class;
    }
}
