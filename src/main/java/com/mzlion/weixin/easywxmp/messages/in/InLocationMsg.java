/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.in;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.InMsg;

/**
 * 地理位置消息
 *
 * @author mzlion on 2017/1/5.
 */
public class InLocationMsg extends InMsg {

    /**
     * X坐标信息
     */
    private Double locationX;

    /**
     * Y坐标信息
     */
    private Double locationY;

    /**
     * 精度，可理解为精度或者比例尺、越精细的话 scale越高
     */
    private Double scale;

    /**
     * 地理位置的字符串信息
     */
    private String label;

    /**
     * 保留默认构造函数
     */
    public InLocationMsg() {
    }

    public InLocationMsg(String fromUserName, String toUserName, Long createTime, Long msgId) {
        super(fromUserName, toUserName, createTime, msgId);
    }

    public Double getLocationX() {
        return locationX;
    }

    public void setLocationX(Double locationX) {
        this.locationX = locationX;
    }

    public Double getLocationY() {
        return locationY;
    }

    public void setLocationY(Double locationY) {
        this.locationY = locationY;
    }

    public Double getScale() {
        return scale;
    }

    public void setScale(Double scale) {
        this.scale = scale;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.LOCATION;
    }

}
