/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages.in;

import com.mzlion.weixin.easywxmp.constants.MsgTypeEnum;
import com.mzlion.weixin.easywxmp.messages.AbsInMediaMsg;

/**
 * 图片消息
 *
 * @author mzlion on 2016/12/26.
 */
public class InImageMsg extends AbsInMediaMsg {

    /**
     * 图片链接（由系统生成）
     */
    private String picUrl;

    /**
     * 保留默认构造函数
     */
    public InImageMsg() {
    }

    public InImageMsg(String fromUserName, String toUserName, Long createTime, Long msgId) {
        super(fromUserName, toUserName, createTime, msgId);
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }


    /**
     * 消息类型
     *
     * @return {@link MsgTypeEnum}
     */
    @Override
    public MsgTypeEnum msgType() {
        return MsgTypeEnum.IMAGE;
    }
}
