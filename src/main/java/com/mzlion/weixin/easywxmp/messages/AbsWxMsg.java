/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp.messages;

/**
 * 基础消息公共类
 *
 * @author mzlion on 2017/4/16.
 */
public abstract class AbsWxMsg implements WxMsg {

    private static final long serialVersionUID = 2097323297945392608L;

    /**
     * 当消息为接收消息类型时，表示公众号
     * 当消息为发送消息类型时，表示用户openId
     */
    protected String toUserName;

    /**
     * 当消息为接收消息类型时，表示用户openId
     * 当消息为发送消息类型时，表示公众号
     */
    protected String fromUserName;

    /**
     * 消息创建时间 （整型）
     */
    protected Long createTime;

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

}
