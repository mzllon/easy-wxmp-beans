/*
 * Copyright (C) 2016-2017 mzlion(mzllon@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mzlion.weixin.easywxmp;

import java.io.File;
import java.io.InputStream;

/**
 * 素材上传基础请求对象
 *
 * @author mzlion on 2016/12/22.
 */
public abstract class UploadWxRequest<Resp extends WxResponse> extends WxRequest<Resp> {

    protected String contentType;        //素材类型
    protected File uploadFile;            //素材文件
    protected InputStream uploadStream;   //素材内容
    protected long contentLength;         //素材大小
    protected String filename;            //素材名称


    public String getContentType() {
        return contentType;
    }

    public File getUploadFile() {
        return uploadFile;
    }

    public InputStream getUploadStream() {
        return uploadStream;
    }

    /**
     * 素材大小
     *
     * @return 素材大小
     */
    public long length() {
        return this.uploadFile != null ? this.uploadFile.length() : this.contentLength;
    }

    /**
     * 素材名称
     *
     * @return 素材名称
     */
    public String filename() {
        return this.uploadFile != null ? this.uploadFile.getName() : this.filename;
    }

    /**
     * POST素材是的参数名
     *
     * @return POST素材是的参数名
     */
    public String formName() {
        return "media";
    }

}
